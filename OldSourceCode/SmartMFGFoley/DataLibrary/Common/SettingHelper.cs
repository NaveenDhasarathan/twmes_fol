﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace DataLibrary.Common
{
    public class SettingHelper
    {
        private static SettingHelper _appSettings;

        public string appSettingValue { get; set; }

        public static string AppSetting(string section, string Key)
        {
            _appSettings = GetCurrentSettings(section, Key);
            return _appSettings.appSettingValue;
        }

        public SettingHelper(IConfiguration config, string Key)
        {
            this.appSettingValue = config.GetValue<string>(Key);
        }


        public static SettingHelper GetCurrentSettings(string section, string Key)
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json"), optional: false, reloadOnChange: true)
                            .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.Development.json"), optional: false, reloadOnChange: true)
                            .AddEnvironmentVariables();

            IConfigurationRoot configuration = builder.Build();

            var settings = new SettingHelper(configuration.GetSection(section), Key);

            return settings;
        }
    }
}
