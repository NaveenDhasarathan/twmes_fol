﻿namespace DataLibrary.Common
{
    public static class AppSettings
    {
        public static string GetLabelIdSeed()
        {
            return SettingHelper.AppSetting("Labels", "Seed");
        }
    }
}
