﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Sql
{
    public static class PurchaseOrderLabelsSql
    {
        public static string GetLabelsByRawMtlsId = @"
                select * from dbo.PURCHASE_ORDER_LABELS where Ib_Raw_Mtls_Id=@Ib_Raw_Mtls_Id and Bag_No=0";

        public static string GetLabelsByRawMtlsIdAll = @"
                select * from dbo.PURCHASE_ORDER_LABELS where Ib_Raw_Mtls_Id=@Ib_Raw_Mtls_Id --and Bag_No=0";


        public static string GetNewLabelId = @"select max(convert(bigint,label_Id))+1 from [dbo].[PURCHASE_ORDER_LABELS] --Where label_Id Not like '68%'";
        public static string GetNewLabelIdSTO = @"select  max(convert(bigint,label_Id))+1 from [dbo].[RECEIVED_RAW_MATERIALS] Where label_Id like '68%'";
        public static string GetNewLabelIdSEED = @"select replace('0' + cast(next value for strg_unit_inbound_raw_seq as char),' ','')";

        public static string GetNewPackagingMaterialId = @"select max(convert(bigint,label_Id))+1 from [dbo].[PURCHASE_ORDER_LABELS] Where label_Id like '999%'";

        public static string AddLabel = @"
            INSERT INTO dbo.PURCHASE_ORDER_LABELS
                        (IB_Raw_Mtls_Id,
                        Label_Id,
                        Product_Received_Date,                     
                        Amt_Received,
                        UOM,
                        Lot_Nbr,
                        Tot_Bag_No,
                        Vendor_Batch,
                        CountryOrigin,
                        Bag_No,
                        Last_Modified_Time,
                        Last_Modified_By,
                        Batch_Id, Bill_Of_Lading,
                        Note,
                        Packaging_Material
                        )
                    VALUES
                        (@IB_Raw_Mtls_Id,
                        @Label_Id,
                        @Product_Received_Date,                     
                        @Amt_Received,
                        @UOM,
                        @Lot_Nbr,
                        @Tot_Bag_No,
                        @Vendor_Batch,
                        @CountryOrigin,
                        @Bag_No,
                        @Last_Modified_Time,
                        @Last_Modified_By,
                        @Batch_Id,
                        @Bill_Of_Lading,
                        @Note,
                        @Packaging_Material
                        )";

        public static string AddLabelForPackagingMat = @"
            INSERT INTO dbo.PURCHASE_ORDER_LABELS
                        (IB_Raw_Mtls_Id,
                        Label_Id,
                        Product_Received_Date,                     
                        Amt_Received,
                        UOM,
                        Lot_Nbr,
                        Tot_Bag_No,
                        Vendor_Batch,
                        CountryOrigin,
                        Bag_No,
                        Last_Modified_Time,
                        Last_Modified_By,
                        Batch_Id, Bill_Of_Lading,
                        Note,
                        Packaging_Material
                        )
                    VALUES
                        (@IB_Raw_Mtls_Id,
                        NEXT VALUE FOR STRG_UNIT_IM_SEQ,
                        @Product_Received_Date,                     
                        @Amt_Received,
                        @UOM,
                        @Lot_Nbr,
                        @Tot_Bag_No,
                        @Vendor_Batch,
                        @CountryOrigin,
                        @Bag_No,
                        @Last_Modified_Time,
                        @Last_Modified_By,
                        @Batch_Id,
                        @Bill_Of_Lading,
                        @Note,
                        @Packaging_Material
                        )";

        public static string GetLabel = @"
                select * from dbo.PURCHASE_ORDER_LABELS where Pol_Seq=@Pol_Seq";

        public static string GetLabelSTO = @"
                select * from dbo.RECEIVED_RAW_MATERIALS where Pol_Seq=@Pol_Seq";

        public static string GetLabelsFromDeliveryAndReceiveRawMtlsSingle = @"
                select 
	                Ib_Raw_Mtls_Id,POL_SEQ,LABEL_ID,Product_Received_Date,Label_Print_Date,Amt_Received,
	                Lot_Nbr,Label_Scan_Date,Tot_Bag_No,Vendor_Batch,CountryOrigin,UOM,batch_id
                from dbo.RECEIVED_RAW_MATERIALS 
                WHERE LABEL_ID = @Pol_Seq
                UNION ALL
                SELECT 
	                i.Ib_Raw_Mtls_Id,s.STO_DD_SEQ POL_SEQ,s.STORAGE_UNIT_NBR LABEL_ID,s.DELIVERY_DATE Product_Received_Date,s.DELIVERY_DATE Label_Print_Date,s.AMT_RECEIVED Amt_Received,
	                s.BILL_OF_LADING Lot_Nbr,s.SCAN_DATE Label_Scan_Date,1 Tot_Bag_No,s.BATCH_NBR Vendor_Batch,s.BILL_OF_LADING CountryOrigin,s.UOM,batch_id
                FROM STO_DELIVERY_DETAILS s
				inner join INBOUND_RAW_MATERIALS i on i.delivery_no=s.delivery_nbr
                WHERE STORAGE_UNIT_NBR = @Pol_Seq";

        public static string GetLabelsFromDeliveryAndReceiveRawMtls = @"
                select 
	                Ib_Raw_Mtls_Id,POL_SEQ,LABEL_ID,Product_Received_Date,Label_Print_Date,Amt_Received,
	                Lot_Nbr,Label_Scan_Date,Tot_Bag_No,Vendor_Batch,CountryOrigin
                from dbo.RECEIVED_RAW_MATERIALS 
                WHERE LABEL_ID IN @Pol_Seq
                UNION ALL
                SELECT 
	                Ib_Raw_Mtls_Id,STO_DD_SEQ POL_SEQ,STORAGE_UNIT_NBR LABEL_ID,DELIVERY_DATE Product_Received_Date,DELIVERY_DATE Label_Print_Date,AMT_RECEIVED Amt_Received,
	                BILL_OF_LADING Lot_Nbr,SCAN_DATE Label_Scan_Date,1 Tot_Bag_No,BATCH_NBR Vendor_Batch,BILL_OF_LADING CountryOrigin
                FROM STO_DELIVERY_DETAILS
                WHERE STORAGE_UNIT_NBR IN @Pol_Seq";

        public static string GetLabelForSTO = @"
                select * from dbo.RECEIVED_RAW_MATERIALS where Pol_Seq in @Pol_Seq";

        public static string GetAllStoLabelDetails = @"
                select 
	                Ib_Raw_Mtls_Id,POL_SEQ,LABEL_ID,Product_Received_Date,Label_Print_Date,Amt_Received,
	                Lot_Nbr,Label_Scan_Date,Tot_Bag_No,Vendor_Batch,CountryOrigin
                from dbo.RECEIVED_RAW_MATERIALS 
                WHERE LABEL_ID IN @Pol_Seq
                UNION ALL
                SELECT 
	                i.Ib_Raw_Mtls_Id,s.STO_DD_SEQ POL_SEQ,s.STORAGE_UNIT_NBR LABEL_ID,s.DELIVERY_DATE Product_Received_Date,s.DELIVERY_DATE Label_Print_Date,s.AMT_RECEIVED Amt_Received,
	                s.BILL_OF_LADING Lot_Nbr,s.SCAN_DATE Label_Scan_Date,1 Tot_Bag_No,s.BATCH_NBR Vendor_Batch,s.BILL_OF_LADING CountryOrigin
                FROM STO_DELIVERY_DETAILS s
				inner join INBOUND_RAW_MATERIALS i on i.delivery_no=s.delivery_nbr
                WHERE STORAGE_UNIT_NBR IN @Pol_Seq";

        public static string GetLabelByLabelId_MtlId = @"
                select * from dbo.PURCHASE_ORDER_LABELS where label_Id=@label_Id and Ib_Raw_Mtls_Id=@Ib_Raw_Mtls_Id";

        public static string GetLabelByLabelId = @"
                select * from dbo.RECEIVED_RAW_MATERIALS where label_Id = @label_Id";

        public static string GetLabelByLabelIdPO = @"
                select distinct * from dbo.PURCHASE_ORDER_LABELS where label_Id in @label_Id";

        public static string UpdateLabel = @"
            UPDATE dbo.PURCHASE_ORDER_LABELS
                SET Lot_Nbr = @Lot_Nbr,                    
                    Tot_Bag_No = @Tot_Bag_No,
                    Amt_Received = @Amt_Received,
                    CountryOrigin = @CountryOrigin,
                    Last_Modified_By = @Last_Modified_By,
                    Last_Modified_Time = getdate()
                WHERE Label_Id=@Label_Id";

        public static string UpdateBagLabels = @"
            UPDATE dbo.PURCHASE_ORDER_LABELS
                SET Lot_Nbr = @Lot_Nbr,                    
                    Tot_Bag_No = @Tot_Bag_No,
                    Amt_Received = @Amt_Received,
                    CountryOrigin = @CountryOrigin,
                    Last_Modified_By = @Last_Modified_By,
                    Last_Modified_Time = getdate()
                WHERE Label_Id=@Label_Id and Bag_No!=0";

        public static string UpdatePalletLabel = @"
            UPDATE dbo.PURCHASE_ORDER_LABELS
                SET Lot_Nbr = @Lot_Nbr,                    
                    Tot_Bag_No = @Tot_Bag_No,
                    Amt_Received=@Amt_Received,
                    CountryOrigin = @CountryOrigin,
                    Last_Modified_By = @Last_Modified_By,
                    Last_Modified_Time = getdate()
                WHERE Pol_Seq=@Pol_Seq";


        public static string UpdateLabelScanDate = @"
            UPDATE dbo.PURCHASE_ORDER_LABELS
                SET Label_Scan_Date = @Label_Scan_Date                                   
                WHERE label_Id = @Label_Id";

        public static string UpdateLabelScanDateForPO = @"
            UPDATE dbo.PURCHASE_ORDER_LABELS SET Label_Scan_Date = @Label_Scan_Date WHERE label_Id in @Label_Id";

        public static string UpdateLabelAfterScan = @"
            UPDATE dbo.PURCHASE_ORDER_LABELS
                SET Label_Scan_Date = @Label_Scan_Date, Batch_Sent_Date =@Batch_Sent_Date                                   
                WHERE label_Id=@Label_Id";


        public static string GetPrinterSettings = @"
            Select ID, Process, PrinterName, LabelName 
            from dbo.Printer_Settings WHERE Process=@Process
        ";

        public static string DeleteLabel = @"
            DELETE FROM dbo.PURCHASE_ORDER_LABELS
                WHERE Label_Id = @Label_Id";

        public static string DeleteLabelForSTO = @"
            DELETE FROM dbo.RECEIVED_RAW_MATERIALS
                WHERE Label_Id = @Label_Id";

        public static string DeleteBagLabel = @"
            DELETE FROM dbo.PURCHASE_ORDER_LABELS
                WHERE Label_Id = @Label_Id and Bag_No!=0";

        public static string GetLabelInfo = @"select p.POL_SEQ, p.LABEL_ID, p.batch_id,p.uom,
            p.AMT_RECEIVED, p.PRODUCT_RECEIVED_DATE, 
            p.bag_no, p.countryOrigin, p.Vendor_Batch, 
            i.DOCUMENT_NBR, s.SAP_MATL_NBR, s.SAP_MATL_DESCR from PURCHASE_ORDER_LABELS p inner join INBOUND_RAW_MATERIALS i on p.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID 
            inner join SAP_MATL_MASTER s on s.SAP_MATL_NBR =i.MATL_NBR where p.LABEL_ID in @labelIds";

        public static string GetLabelInfoForSTO = @"select p.POL_SEQ, p.LABEL_ID, 
            p.AMT_RECEIVED, p.PRODUCT_RECEIVED_DATE, 
            p.bag_no, p.countryOrigin, p.Vendor_Batch, 
            i.DOCUMENT_NBR, s.SAP_MATL_NBR, s.SAP_MATL_DESCR from RECEIVED_RAW_MATERIALS p inner join INBOUND_RAW_MATERIALS i on p.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID 
            inner join SAP_MATL_MASTER s on s.SAP_MATL_NBR =i.MATL_NBR where p.LABEL_ID in @labelIds";


        public static string GetSingleLabelInfo = @"select p.POL_SEQ, p.LABEL_ID, 
            p.AMT_RECEIVED, p.PRODUCT_RECEIVED_DATE, 
            p.bag_no, p.countryOrigin, p.Vendor_Batch, 
            i.DOCUMENT_NBR, s.SAP_MATL_NBR, s.SAP_MATL_DESCR from PURCHASE_ORDER_LABELS p inner join INBOUND_RAW_MATERIALS i on p.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID 
            inner join SAP_MATL_MASTER s on s.SAP_MATL_NBR =i.MATL_NBR where p.LABEL_ID =@labelId and p.bag_no=@bagNo";

        public static string GetLabelInfoBySeqId = @"select p.POL_SEQ, p.LABEL_ID, 
            p.AMT_RECEIVED, p.PRODUCT_RECEIVED_DATE, 
            p.bag_no, p.countryOrigin, p.Vendor_Batch, 
            i.DOCUMENT_NBR, i.MATL_NBR, s.SAP_MATL_DESCR from PURCHASE_ORDER_LABELS p inner join INBOUND_RAW_MATERIALS i on p.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID 
            inner join SAP_MATL_MASTER s on s.SAP_MATL_NBR =i.MATL_NBR where p.POL_SEQ in @Ids";

        public static string GetLabelMaterialInfoBySeqId = @"select p.LABEL_ID, p.Batch_ID,           
            i.DOCUMENT_NBR, i.MATL_NBR, s.SAP_MATL_DESCR from PURCHASE_ORDER_LABELS p inner join INBOUND_RAW_MATERIALS i on p.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID 
            inner join SAP_MATL_MASTER s on s.SAP_MATL_NBR =i.MATL_NBR where p.POL_SEQ =@Pol_Seq";

        public static string GetBatchId = @"Declare @batchId varchar(10);
            EXEC PROC_CreateNextBatch @PO_NBR=@PONBR,@MATL_NBR=@MATNBR,@BATCH_TYPE='R',@USER=@User, @Batch_Id =@batchId output
            select @batchId as Batch_Id;";

        //public static string GetCurrentBatchId = @"select * from RECEIVED_RAW_MATERIALS where day(product_received_date)=@CurrentDate and month(product_received_date)=@CurrentMonth and year(product_received_date)=@CurrentYear  and Ib_Raw_Mtls_Id=@Ib_Raw_Mtls_Id --and vendor_batch=@VendorBatch";
        public static string GetCurrentBatchIdSTODelivery = @"select s.* from STO_DELIVERY_DETAILS s
				inner join INBOUND_RAW_MATERIALS i on i.delivery_no=s.delivery_nbr
                where i.Ib_Raw_Mtls_Id=@Ib_Raw_Mtls_Id and batch_nbr=@VendorBatch";

        //public static string GetCurrentBatchId = @"select BATCH_ID from RECEIVED_RAW_MATERIALS where day(product_received_date)=@CurrentDate and month(product_received_date)=@CurrentMonth and year(product_received_date)=@CurrentYear  and Ib_Raw_Mtls_Id=@Ib_Raw_Mtls_Id and vendor_batch=@VendorBatch UNION ALL select BATCH_ID from PURCHASE_ORDER_LABELS where day(product_received_date)=@CurrentDate and month(product_received_date)=@CurrentMonth and year(product_received_date)=@CurrentYear  and Ib_Raw_Mtls_Id=@Ib_Raw_Mtls_Id and vendor_batch=@VendorBatch";
        public static string GetCurrentBatchId = @"select BATCH_ID 
                from RECEIVED_RAW_MATERIALS p
                inner join inbound_raw_materials i on p.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID 
                where i.matl_nbr=@Ib_Raw_Mtls_Id and vendor_batch=@VendorBatch 
                and i.supplier_nbr=@POSupplierNo
                UNION ALL 
                select BATCH_ID 
                from PURCHASE_ORDER_LABELS  p
                inner join inbound_raw_materials i on p.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID 
                where  i.matl_nbr=@Ib_Raw_Mtls_Id and vendor_batch=@VendorBatch
                and i.supplier_nbr=@POSupplierNo";

        public static string UpdateLabelPrintDateOld = @"
            //UPDATE dbo.PURCHASE_ORDER_LABELS
            //    SET Label_Print_Date = @Label_Print_Date                                   
            //    WHERE Pol_Seq in @Pol_Seq;
            
            UPDATE received_raw_materials       
                SET Label_Print_Date = @Label_Print_Date                                   
                WHERE Pol_Seq in @Pol_Seq;
            ";

        public static string UpdateLabelPrintDate = @"
            UPDATE received_raw_materials       
                SET Label_Print_Date = @Label_Print_Date                                   
                WHERE label_id in @Pol_Seq;
                
            UPDATE STO_DELIVERY_DETAILS       
                SET Label_Print_Date = @Label_Print_Date                                   
                WHERE 
                STORAGE_UNIT_NBR!='INV MGT' and STORAGE_UNIT_NBR!='NEEDLABEL'
                AND STORAGE_UNIT_NBR IN @Pol_Seq;
            
            UPDATE dbo.PURCHASE_ORDER_LABELS
                SET Label_Print_Date = @Label_Print_Date                                   
                WHERE label_id in @Pol_Seq;

            ";

        public static string UpdateLabelPrintDateByPOS = @"
            UPDATE received_raw_materials       
                SET Label_Print_Date = @Label_Print_Date                                   
                WHERE pol_seq in @Pol_Seq;
                
            UPDATE STO_DELIVERY_DETAILS       
                SET Label_Print_Date = @Label_Print_Date                                   
                WHERE 
                STORAGE_UNIT_NBR!='INV MGT' and STORAGE_UNIT_NBR!='NEEDLABEL'
                AND sto_dd_seq IN @Pol_Seq
            ";

        public static string UpdateLabelPrintDatePO = @"
            UPDATE PURCHASE_ORDER_LABELS       
                SET Label_Print_Date = @Label_Print_Date                                   
                WHERE Pol_Seq in @Pol_Seq
            ";

        public static string UpdateLabelPrintDate_Both = @"
            UPDATE received_raw_materials       
                SET Label_Print_Date = @Label_Print_Date                                   
                WHERE Pol_Seq in @Pol_Seq;

            UPDATE STO_DELIVERY_DETAILS       
                SET Label_Print_Date = @Label_Print_Date                                   
                WHERE STORAGE_UNIT_NBR IN @Pol_Seq
            ";

        public static string UpdateBatchSentDate = @"
            UPDATE dbo.PURCHASE_ORDER_LABELS
                SET Batch_Sent_Time = @Batch_Sent_Time                                   
                WHERE Batch_Id = @Batch_Id";

        public static string UpdateSentSAPDate = @"
            UPDATE dbo.PURCHASE_ORDER_LABELS
                SET Sent_Sap_Date = @Sent_Sap_Date, Sent_Sap_By=@Sent_Sap_By                                   
                WHERE IB_RAW_MTLS_ID = @Ib_Raw_Mtls_Id and Sent_Sap_Date is null";
    }


}
