﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Sql
{
    public static class ProductionScheduleSql
    {
        public static string LoadProductionSchedule = @"SELECT * FROM PO_SCHEDULE_VW ORDER BY START_DATE DESC;";
        public static string LoadResources = @"SELECT DISTINCT RESOURCE FROM PO_SCHEDULE_VW ORDER BY RESOURCE;";
        public static string LoadMaterials = @"SELECT DISTINCT MATERIAL_NUMBER, MATERIAL_DESCRIPTION, RESOURCE FROM PO_SCHEDULE_VW ORDER BY RESOURCE;";
        public static string LoadMaterialList = @"SELECT RESOURCE, MATERIAL_DESCRIPTION FROM PO_SCHEDULE_VW GROUP BY RESOURCE, MATERIAL_DESCRIPTION ORDER BY RESOURCE;";
        public static string LoadPSModel = @"SELECT * FROM PO_SCHEDULE_VW WHERE PROCESS_ORDER_NUMBER = @po;";
    }
}
