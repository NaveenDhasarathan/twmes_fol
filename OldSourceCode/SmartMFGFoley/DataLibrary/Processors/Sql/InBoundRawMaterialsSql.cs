﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Sql
{
    public static class InBoundRawMaterialsSql
    {


        public static string GetRawMaterialByDocType = @"
               SELECT i.IB_RAW_MTLS_ID,
        DOCUMENT_NBR, 
        (CASE WHEN DOCUMENT_TYPE='p' then 'PO' else 'STO' END) as DOCUMENT_TYPE,
        ITEM_NBR,
        SUPPLIER_NAME, 
        DOCUMENT_DATE,
        SUPPLIER_NBR, 
        QUANTITY,
        MATL_NBR,
        REQUESTED_DELIVERY_DATE,
        LTRIM(RTRIM(i.UOM)) Uom,
        LTRIM(RTRIM(s.UOM)) ITMEUom,
        s.SAP_MATL_TYPE,
        t.MATL_TYPE_DESCR,
		t.ADDITIONAL_INFO,
        SAP_MATL_DESCR,
		(CASE WHEN s.SAP_MATL_TYPE='VERP' THEN AMT_RECEIVED2 ELSE AMT_RECEIVED1 END) AMT_RECEIVED,
        i.DELIVERY_NO,
		(SELECT ISNULL(COUNT(*),0) FROM PURCHASE_ORDER_LABELS PP WHERE PP.SENT_SAP_DATE IS NOT NULL AND i.IB_RAW_MTLS_ID = PP.IB_RAW_MTLS_ID) SENT_TO_SAP
		FROM [dbo].[INBOUND_RAW_MATERIALS] i 
		--LEFT JOIN (SELECT sum(AMT_RECEIVED) as AMT_RECEIVED,IB_RAW_MTLS_ID from PURCHASE_ORDER_LABELS WHERE BAG_NO=0 and LABEL_SCAN_DATE is not null  GROUP BY IB_RAW_MTLS_ID ) as p on i.IB_RAW_MTLS_ID = p.IB_RAW_MTLS_ID
		LEFT JOIN (SELECT sum(case when s.UOM = 'LB' then isnull(AMT_RECEIVED * 0.4536,0) else AMT_RECEIVED  end) as AMT_RECEIVED1,IB_RAW_MTLS_ID from PURCHASE_ORDER_LABELS s WHERE BAG_NO=0 and LABEL_SCAN_DATE is not null GROUP BY IB_RAW_MTLS_ID ) as p on i.IB_RAW_MTLS_ID = p.IB_RAW_MTLS_ID
		LEFT JOIN (SELECT sum(case when s.UOM = 'LB' then isnull(AMT_RECEIVED * 0.4536,0) else AMT_RECEIVED  end) as AMT_RECEIVED2,IB_RAW_MTLS_ID from PURCHASE_ORDER_LABELS s WHERE BAG_NO=0 GROUP BY IB_RAW_MTLS_ID ) as p2 on i.IB_RAW_MTLS_ID = p2.IB_RAW_MTLS_ID
        INNER JOIN [dbo].SAP_MATL_MASTER s 
        ON i.MATL_NBR = s.SAP_MATL_NBR 
        LEFT JOIN [dbo].MATL_TYPE_DESCR t on s.SAP_MATL_TYPE=t.MATL_TYPE
        WHERE i.DOCUMENT_TYPE =@DocumentType
        ORDER BY DOCUMENT_NBR";

        public static string GetAllSTO = @"
        SELECT i.IB_RAW_MTLS_ID,
        DOCUMENT_NBR, 
        (CASE WHEN DOCUMENT_TYPE='p' then 'PO' else 'STO' END) as DOCUMENT_TYPE,
        ITEM_NBR,
        SUPPLIER_NAME, 
        DOCUMENT_DATE,
        SUPPLIER_NBR, 
        QUANTITY,
        MATL_NBR,
        REQUESTED_DELIVERY_DATE,
        LTRIM(RTRIM(s.UOM)) Uom,
        LTRIM(RTRIM(s.UOM)) ITMEUom,
        s.SAP_MATL_TYPE,
        t.MATL_TYPE_DESCR,
		t.ADDITIONAL_INFO,
        SAP_MATL_DESCR,
		--(CASE WHEN s.SAP_MATL_TYPE='VERP' THEN AMT_RECEIVED2 ELSE AMT_RECEIVED1 END) AMT_RECEIVED,
		ISNULL((select sum(isnull(r.AMT_RECEIVED,0.0)) from [dbo].PURCHASE_ORDER_LABELS r inner join INBOUND_RAW_MATERIALS j on j.IB_RAW_MTLS_ID=r.IB_RAW_MTLS_ID where r.BAG_NO = 0 and j.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID),0.0) AMT_RECEIVED,
        ISNULL((select sum(isnull(AMT_RECEIVED,0.0)) from [dbo].PURCHASE_ORDER_LABELS r inner join INBOUND_RAW_MATERIALS j on j.IB_RAW_MTLS_ID=r.IB_RAW_MTLS_ID where  r.BAG_NO = 0 and j.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID and r.Label_Scan_Date is not null),0.0) Quantity_Received,
        i.DELIVERY_NO,
		(SELECT ISNULL(COUNT(*),0) FROM PURCHASE_ORDER_LABELS PP WHERE PP.SENT_SAP_DATE IS NOT NULL AND i.IB_RAW_MTLS_ID = PP.IB_RAW_MTLS_ID) SENT_TO_SAP,
	 CASE when i.document_type = 'S' THEN isnull((SELECT top 1 WM_IM 
                    FROM [dbo].[STORAGE_LOCATION_XREF] xr WHERE xr.PLANT_NO=i.supplier_nbr and xr.STORAGE_LOCATION=(select top 1 sdd.source_sloc from STO_DELIVERY_DETAILS sdd where
                    sdd.delivery_nbr = i.delivery_no)),'IM')
                    else 'IM' end  as FRM
                     ,(CASE WHEN i.storage_location ='BALE' THEN 'WM' ELSE 'IM' end) Too 
		FROM [dbo].[INBOUND_RAW_MATERIALS] i 
		--LEFT JOIN (SELECT sum(AMT_RECEIVED)                                                                   as AMT_RECEIVED,IB_RAW_MTLS_ID from PURCHASE_ORDER_LABELS WHERE BAG_NO=0 and LABEL_SCAN_DATE is not null  GROUP BY IB_RAW_MTLS_ID ) as p on i.IB_RAW_MTLS_ID = p.IB_RAW_MTLS_ID
        LEFT JOIN (SELECT sum(case when s.UOM = 'LB' then isnull(AMT_RECEIVED * 0.4536,0) else AMT_RECEIVED  end) as AMT_RECEIVED1,IB_RAW_MTLS_ID from PURCHASE_ORDER_LABELS s WHERE BAG_NO=0 and LABEL_SCAN_DATE is not null GROUP BY IB_RAW_MTLS_ID ) as p on i.IB_RAW_MTLS_ID = p.IB_RAW_MTLS_ID
		LEFT JOIN (SELECT sum(case when s.UOM = 'LB' then isnull(AMT_RECEIVED * 0.4536,0) else AMT_RECEIVED  end) as AMT_RECEIVED2,IB_RAW_MTLS_ID from PURCHASE_ORDER_LABELS s WHERE BAG_NO=0 GROUP BY IB_RAW_MTLS_ID ) as p2 on i.IB_RAW_MTLS_ID = p2.IB_RAW_MTLS_ID
        INNER JOIN [dbo].SAP_MATL_MASTER s 
        ON i.MATL_NBR = s.SAP_MATL_NBR 
        LEFT JOIN [dbo].MATL_TYPE_DESCR t on s.SAP_MATL_TYPE=t.MATL_TYPE
        WHERE i.DOCUMENT_TYPE in ('p','s')
        ORDER BY DOCUMENT_NBR";



        public static string GetInternalSTO = @"
        SELECT i.IB_RAW_MTLS_ID,
        DOCUMENT_NBR, 
        (CASE WHEN DOCUMENT_TYPE='p' then 'PO' else 'STO' END) as DOCUMENT_TYPE,
        i.ITEM_NBR,
        SUPPLIER_NAME, 
        DOCUMENT_DATE,
        SUPPLIER_NBR, 
        QUANTITY,
        i.MATL_NBR,
        REQUESTED_DELIVERY_DATE,
        LTRIM(RTRIM(i.UOM)) UOM,
        s.SAP_MATL_TYPE,
        t.MATL_TYPE_DESCR,
		t.ADDITIONAL_INFO,
        SAP_MATL_DESCR,
		--(case when s.SAP_MATL_TYPE='VERP' then ISNULL((select sum(isnull(amt_received,0.0)) from sto_delivery_details sdd where sdd.DELIVERY_NBR=i.DELIVERY_NO and sdd.scan_date is not null),0.0) else 0.0 end)+ 
		--ISNULL((select sum(isnull(amt_received,0.0)) from sto_delivery_details sdd where sdd.DELIVERY_NBR=i.DELIVERY_NO and (sdd.scan_date is not null or (s.SAP_MATL_TYPE='VERP' and sdd.DATE_RECEIVED is not null) ) and (s.SAP_MATL_TYPE='VERP' or (sdd.Storage_Unit_Nbr!='NEEDLABEL' and sdd.Storage_Unit_Nbr!='INV MGT'))),0.0) + 
		--ISNULL((select sum(isnull(AMT_RECEIVED,0.0)) from RECEIVED_RAW_MATERIALS samt where samt.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID AND DELIVERY_SCAN_DATE IS NOT NULL AND LABEL_SCAN_DATE IS NOT NULL),0.0) AMT_RECEIVED, 
        --ISNULL((select sum(isnull(AMT_RECEIVED,0.0)) from [STO_DELIVERY_DETAILS] samt where samt.DELIVERY_NBR=i.DELIVERY_NO AND (samt.ITEM_NBR=i.ITEM_NBR  or STO_DOC_ITEM_NBR=i.ITEM_NBR)),0.0) AMT_RECEIVED,        
        ISNULL((select sum(isnull(AMT_RECEIVED,0.0)) from [dbo].[RECEIVED_RAW_MATERIALS] r inner join INBOUND_RAW_MATERIALS j on j.IB_RAW_MTLS_ID=r.IB_RAW_MTLS_ID where (r.Delivery_Scan_Date is not null and  r.Label_Scan_Date is not null) and j.DELIVERY_NO=i.DELIVERY_NO AND (j.ITEM_NBR=i.ITEM_NBR)),0.0) AMT_RECEIVED,
        ISNULL((select sum(isnull(AMT_RECEIVED,0.0)) from [dbo].[RECEIVED_RAW_MATERIALS] r inner join INBOUND_RAW_MATERIALS j on j.IB_RAW_MTLS_ID=r.IB_RAW_MTLS_ID where  j.DELIVERY_NO=i.DELIVERY_NO AND (j.ITEM_NBR=i.ITEM_NBR)),0.0) Quantity_Received,
        i.DELIVERY_NO,
		(select top 1 LABEL_ID from RECEIVED_RAW_MATERIALS samt where samt.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID) storage_unit_nbr,
        (select ISNULL(COUNT(STO_DD_SEQ),0) from STO_DELIVERY_DETAILS samt where samt.DELIVERY_NBR=i.DELIVERY_NO and Date_Sent_SAP IS NOT NULL) + 
		(select ISNULL(COUNT(POL_SEQ),0) from RECEIVED_RAW_MATERIALS samt where samt.IB_RAW_MTLS_ID=i.IB_RAW_MTLS_ID and SENT_SAP_DATE IS NOT NULL) SENT_TO_SAP
		,isnull(i.po_closed,'N') po_closed,
 CASE when i.document_type = 'S' THEN (SELECT top 1 WM_IM 
                    FROM [dbo].[STORAGE_LOCATION_XREF] xr WHERE xr.PLANT_NO=i.supplier_nbr and xr.STORAGE_LOCATION=(select top 1 sdd.source_sloc from STO_DELIVERY_DETAILS sdd where
                    sdd.delivery_nbr = i.delivery_no))
                    else'IM'end  as FRM
                     ,(CASE WHEN i.storage_location ='BALE' THEN 'WM' ELSE 'IM' end) Too 
		FROM [dbo].[INBOUND_RAW_MATERIALS] i 
        INNER JOIN [dbo].SAP_MATL_MASTER s 
        ON i.MATL_NBR = s.SAP_MATL_NBR 
        LEFT JOIN [dbo].MATL_TYPE_DESCR t on s.SAP_MATL_TYPE=t.MATL_TYPE
        WHERE i.DOCUMENT_TYPE in ('s') and i.Delivery_NO !=''
		ORDER BY DELIVERY_NO, ITEM_NBR";

        public static string GetIRMInfoForSAP = @"  SELECT i.IB_RAW_MTLS_ID,
        DOCUMENT_NBR, 
        (CASE WHEN DOCUMENT_TYPE='p' then 'PO' else 'STO' END) as DOCUMENT_TYPE,
        ITEM_NBR,
        SUPPLIER_NAME, 
        DOCUMENT_DATE,
        SUPPLIER_NBR, 
        QUANTITY,
        MATL_NBR,
        REQUESTED_DELIVERY_DATE,
        LTRIM(RTRIM(i.UOM)) UOM,
        s.SAP_MATL_TYPE,
        t.MATL_TYPE_DESCR,
		t.ADDITIONAL_INFO,
        SAP_MATL_DESCR,
		AMT_RECEIVED,
        i.Delivery_No,
        i.STORAGE_LOCATION,
		xd.LGFSB
		FROM [dbo].[INBOUND_RAW_MATERIALS] i 
		LEFT JOIN (SELECT sum(AMT_RECEIVED) as AMT_RECEIVED,IB_RAW_MTLS_ID from PURCHASE_ORDER_LABELS Where bag_no<>0 group by IB_RAW_MTLS_ID ) as p on i.IB_RAW_MTLS_ID = p.IB_RAW_MTLS_ID
        INNER JOIN [dbo].SAP_MATL_MASTER s 
        ON i.MATL_NBR = s.SAP_MATL_NBR 
        LEFT JOIN [dbo].MATL_TYPE_DESCR t on s.SAP_MATL_TYPE=t.MATL_TYPE
		LEFT JOIN [dbo].[XMATL_DATA] xd on CONVERT(int, xd.MATNR)=CONVERT(int, i.MATL_NBR)
        WHERE i.IB_RAW_MTLS_ID=@IB_RAW_MTLS_ID";
        public static string GetIRMInfoForSAPPOMaterials = @"  SELECT i.IB_RAW_MTLS_ID,
        DOCUMENT_NBR, 
        (CASE WHEN DOCUMENT_TYPE='p' then 'PO' else 'STO' END) as DOCUMENT_TYPE,
        ITEM_NBR,
        SUPPLIER_NAME, 
        DOCUMENT_DATE,
        SUPPLIER_NBR, 
        QUANTITY,
        MATL_NBR,
        REQUESTED_DELIVERY_DATE,
        LTRIM(RTRIM(i.UOM)) UOM,
        s.SAP_MATL_TYPE,
        t.MATL_TYPE_DESCR,
		t.ADDITIONAL_INFO,
        SAP_MATL_DESCR,
		AMT_RECEIVED,
        i.Delivery_No,
        i.STORAGE_LOCATION,
		xd.LGFSB
		FROM [dbo].[INBOUND_RAW_MATERIALS] i 
		LEFT JOIN (SELECT sum(AMT_RECEIVED) as AMT_RECEIVED,IB_RAW_MTLS_ID from PURCHASE_ORDER_LABELS Where bag_no<>0 group by IB_RAW_MTLS_ID ) as p on i.IB_RAW_MTLS_ID = p.IB_RAW_MTLS_ID
        INNER JOIN [dbo].SAP_MATL_MASTER s 
        ON i.MATL_NBR = s.SAP_MATL_NBR 
        LEFT JOIN [dbo].MATL_TYPE_DESCR t on s.SAP_MATL_TYPE=t.MATL_TYPE
		LEFT JOIN [dbo].[XMATL_DATA] xd on CONVERT(int, xd.MATNR)=CONVERT(int, i.MATL_NBR)
        WHERE i.Delivery_No=@Delivery_No";

        public static string GetExternalPOByNBR = @"
                select IB_RAW_MTLS_ID,
                DOCUMENT_NBR,
                ITEM_NBR,
                DOCUMENT_TYPE,
                SUPPLIER_NBR,
                SUPPLIER_NAME,
                DOCUMENT_DATE,
                MATL_NBR,
                STORAGE_LOCATION,
                QUANTITY,
                LTRIM(RTRIM(UOM)) UOM,
                LOT_NBR,
                Delivery_No,
                QUALITY from INBOUND_RAW_MATERIALS where DOCUMENT_NBR=@Document_NBR";

        public static string GetRawMaterialByRawMatlId = @"
                    select IB_RAW_MTLS_ID,
                    DOCUMENT_NBR,
                    ITEM_NBR,
                    DOCUMENT_TYPE,
                    SUPPLIER_NBR,
                    SUPPLIER_NAME,
                    DOCUMENT_DATE,
                    MATL_NBR,
                    STORAGE_LOCATION,
                    QUANTITY,
                    LTRIM(RTRIM(i.UOM)) UOM,
                    LOT_NBR,
                    Delivery_No,
                    QUALITY,
                    s.SAP_MATL_DESCR, i.STORAGE_LOCATION, s.BATCH_MANAGED, s.SAP_MATL_TYPE, i.STO_Batch, s.BATCH_MANAGED,
                    CASE when i.document_type = 'S' THEN (SELECT top 1 WM_IM 
                    FROM [dbo].[STORAGE_LOCATION_XREF] xr WHERE xr.PLANT_NO=i.supplier_nbr and xr.STORAGE_LOCATION=(select top 1 sdd.source_sloc from STO_DELIVERY_DETAILS sdd where
                    sdd.delivery_nbr = i.delivery_no))
                    else'IM'end  as FRM
                     ,(CASE WHEN i.storage_location ='BALE' THEN 'WM' ELSE 'IM' end) Too 
                    from INBOUND_RAW_MATERIALS i join [dbo].SAP_MATL_MASTER s on i.MATL_NBR = s.SAP_MATL_NBR where IB_RAW_MTLS_ID=@IB_RAW_MTLS_ID";


        //    public static string GetRawMaterialByRawMatlId = @"
        //            select IB_RAW_MTLS_ID,
        //            DOCUMENT_NBR,
        //            ITEM_NBR,
        //            DOCUMENT_TYPE,
        //            SUPPLIER_NBR,
        //            SUPPLIER_NAME,
        //            DOCUMENT_DATE,
        //            MATL_NBR,
        //            STORAGE_LOCATION,
        //            QUANTITY,
        //            LTRIM(RTRIM(i.UOM)) UOM,
        //            LOT_NBR,
        //            Delivery_No,
        //            QUALITY,
        //s.SAP_MATL_DESCR, i.STORAGE_LOCATION, s.BATCH_MANAGED, s.SAP_MATL_TYPE, i.STO_Batch, s.BATCH_MANAGED 
        //            ,	(SELECT top 1 WM_IM 
        //            	FROM [dbo].[STORAGE_LOCATION_XREF] xr WHERE xr.STORAGE_LOCATION=i.STORAGE_LOCATION) FRM
        //            ,(CASE WHEN i.storage_location ='BALE' THEN 'WM' ELSE 'IM' end) Too 
        //            from INBOUND_RAW_MATERIALS i join [dbo].SAP_MATL_MASTER s on i.MATL_NBR = s.SAP_MATL_NBR where IB_RAW_MTLS_ID=@IB_RAW_MTLS_ID";

        public static string GetRawMaterialByRawMatlNo = @"
                select IB_RAW_MTLS_ID,
                DOCUMENT_NBR,
                ITEM_NBR,
                DOCUMENT_TYPE,
                SUPPLIER_NBR,
                SUPPLIER_NAME,
                DOCUMENT_DATE,
                MATL_NBR,
                STORAGE_LOCATION,
                QUANTITY,
                LTRIM(RTRIM(i.UOM)) UOM,
                LOT_NBR,
                Delivery_No,
                QUALITY,
				s.SAP_MATL_DESCR, i.STORAGE_LOCATION, s.BATCH_MANAGED, s.SAP_MATL_TYPE, i.STO_Batch, s.BATCH_MANAGED 
                from INBOUND_RAW_MATERIALS i join [dbo].SAP_MATL_MASTER s on i.MATL_NBR = s.SAP_MATL_NBR where i.MATL_NBR=@IB_RAW_MTLS_ID";

        public static string GetRawMaterialByDeliveryNo = @"
                select IB_RAW_MTLS_ID,
                DOCUMENT_NBR,
                ITEM_NBR,
                DOCUMENT_TYPE,
                SUPPLIER_NBR,
                SUPPLIER_NAME,
                DOCUMENT_DATE,
                MATL_NBR,
                STORAGE_LOCATION,
                QUANTITY,
                LTRIM(RTRIM(i.UOM)) UOM,
                LOT_NBR,
                Delivery_No,
                QUALITY,
				s.SAP_MATL_DESCR, i.STORAGE_LOCATION, s.BATCH_MANAGED, s.SAP_MATL_TYPE 
                from INBOUND_RAW_MATERIALS i join [dbo].SAP_MATL_MASTER s on i.MATL_NBR = s.SAP_MATL_NBR where Delivery_No=@Delivery_No";

        public static string GetRawMaterialByDeliveryNoNew = @"
                select IB_RAW_MTLS_ID,
                DOCUMENT_NBR,
                ITEM_NBR,
                DOCUMENT_TYPE,
                SUPPLIER_NBR,
                SUPPLIER_NAME,
                DOCUMENT_DATE,
                MATL_NBR,
                STORAGE_LOCATION,
                QUANTITY,
                LTRIM(RTRIM(i.UOM)) UOM,
                LOT_NBR,
                Delivery_No,
                QUALITY,
				s.SAP_MATL_DESCR, i.STORAGE_LOCATION, s.BATCH_MANAGED, s.SAP_MATL_TYPE from INBOUND_RAW_MATERIALS i join [dbo].SAP_MATL_MASTER s on i.MATL_NBR = s.SAP_MATL_NBR where Delivery_No=@Delivery_No";

    }
}
