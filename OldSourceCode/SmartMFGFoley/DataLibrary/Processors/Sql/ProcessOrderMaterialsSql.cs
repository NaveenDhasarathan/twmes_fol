﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Sql
{
    public static class ProcessOrderMaterialsSql
    {
		public static string LoadBOM = @"SELECT POM.PO_NBR,
											 POM.SAP_MATL_NBR,
											 SMM.SAP_DESCR_SIMPLE MATERIAL_DESCRIPTION,
											 POM.QUANTITY_REQUIRED,
											 POM.QUANTITY_COMPLETED,
											 LTRIM(RTRIM(POM.UOM)) UOM,
											 POM.MATL_TYPE
												FROM PROCESS_ORDER_MATERIALS POM
													JOIN SAP_MATL_MASTER SMM ON POM.SAP_MATL_NBR = SMM.SAP_MATL_NBR
													WHERE POM.PO_NBR = @PO
													AND POM.MATL_TYPE IN ('C','F') AND POM.UOM IN ('LB', 'KG');";		
	

		public static string LoadBOMScanned = @"SELECT  POM.PO_NBR,
		                                        POM.SAP_MATL_NBR,
		                                        SMM.SAP_DESCR_SIMPLE MATERIAL_DESCRIPTION,
		                                        POM.QUANTITY_REQUIRED,
		                                        POM.QUANTITY_COMPLETED,
												LTRIM(RTRIM(POM.UOM)) UOM,
		                                        POM.MATL_TYPE,		                            
		                                        0 AS SCANNED_AMT,
		                                        0 AS FEEDER_NO
		                                        FROM PROCESS_ORDER_MATERIALS POM		
		                                        JOIN SAP_MATL_MASTER SMM ON POM.SAP_MATL_NBR = SMM.SAP_MATL_NBR		
		                                        WHERE POM.PO_NBR = @PO
		                                        AND POM.MATL_TYPE IN ('C','F') AND POM.UOM IN ('LB', 'KG')                 
																	
                                        UNION							
									
                                        SELECT  RMS.PO_NBR,
		                                        RMS.SAP_MATL_NBR,
		                                        SMM.SAP_DESCR_SIMPLE MATERIAL_DESCRIPTION,
		                                        0 AS QUANTITY_REQUIRED,
		                                        0 AS QUANTITY_COMPLETED,
		                                        '' AS UOM,
		                                        '' AS MATL_TYPE,		                            
		                                        SUM(CASE WHEN RMS.ENTERED_AMT IS NULL THEN RMS.SCANNED_AMT ELSE RMS.ENTERED_AMT END) AS [SCANNED_AMT],
		                                        RMS.FEEDER_NO
		                                        FROM RAW_MATERIAL_STAGING RMS		
		                                        JOIN SAP_MATL_MASTER SMM ON RMS.SAP_MATL_NBR = SMM.SAP_MATL_NBR
		                                        WHERE RMS.PO_NBR = @PO
		                                        GROUP BY RMS.PO_NBR,
				                                        RMS.SAP_MATL_NBR,
				                                        RMS.FEEDER_NO,
				                                        SMM.SAP_DESCR_SIMPLE;";
	

		public static string LoadALTBOMScanned=@"SELECT  ALT.PO_NBR,
												ALT.SUBST_MATL_NBR SAP_MATL_NBR,
												SMM.SAP_DESCR_SIMPLE MATERIAL_DESCRIPTION,
												ALT.SUBST_QTY QUANTITY_REQUIRED,
												0 AS QUANTITY_COMPLETED,
												'' AS UOM,
												'' AS MATL_TYPE,	                            
												0 AS SCANNED_AMT,
												0 AS FEEDER_NO
												FROM PO_ALTERNATE_BOM ALT		
												JOIN SAP_MATL_MASTER SMM ON ALT.SUBST_MATL_NBR = SMM.SAP_MATL_NBR		
												WHERE ALT.PO_NBR = @PO	
										UNION	
										SELECT  RMS.PO_NBR,
												RMS.SAP_MATL_NBR,
												SMM.SAP_DESCR_SIMPLE MATERIAL_DESCRIPTION,
												0 AS QUANTITY_REQUIRED,
												0 AS QUANTITY_COMPLETED,
												'' AS UOM,
												'' AS MATL_TYPE,		                            
												SUM(CASE WHEN RMS.ENTERED_AMT IS NULL THEN RMS.SCANNED_AMT ELSE RMS.ENTERED_AMT END) AS [SCANNED_AMT],
												RMS.FEEDER_NO
												FROM RAW_MATERIAL_STAGING RMS		
												JOIN SAP_MATL_MASTER SMM ON RMS.SAP_MATL_NBR = SMM.SAP_MATL_NBR
												WHERE RMS.PO_NBR = @PO
												GROUP BY RMS.PO_NBR,
														RMS.SAP_MATL_NBR,
														RMS.FEEDER_NO,
														SMM.SAP_DESCR_SIMPLE;";		

		public static string LoadEquipmentSetup =@"SELECT RSD.PO_NBR,
											 RSD.SETTING_NAME,
											 RSD.SETTING1_VALUE EXPECTED_VALUE,
											 IP21.KEPWARE_TAG
										FROM PO_RUNSHEET_LABELS PRL 
										JOIN PO_RUNSHEET_SETTING_DETAILS RSD ON PRL.RUNSHEET_COLUMN_LABEL = RSD.SETTING_NAME AND RSD.PO_NBR = @PO
										JOIN IP21_TAGS IP21 ON PRL.CORRESPONDING_TAG_NAME = IP21.IP21_TAG AND IP21.MES_USAGE = 'PROCESS_DATA' AND IP21.RESOURCE_ID = @res
										WHERE PRL.RUNSHEET_GROUP IN ('MISC_SETTINGS', 'ZONE_TEMPS', 'MISC_SETTINGS2')
										ORDER BY PRL.RUNSHEET_DISPLAY_ORDER;";		
		
		public static string LoadEquipmentActual = @"SELECT TOP 1 * FROM SPC_DATA_STORE WHERE PO_NBR = @PO AND KEPWARE_TAG_REFERENCE = @tag ORDER BY DATE_TIME_RECORDED DESC;";
	

		public static string LoadDetails= @"SELECT * FROM PO_SCHEDULE_VW WHERE PROCESS_ORDER_NUMBER = @PO ORDER BY PROCESS_ORDER_NUMBER;";

		public static string GetAltMaterials = @"SELECT P.*, S.SAP_DESCR_SIMPLE FROM PO_ALTERNATE_BOM P INNER JOIN SAP_MATL_MASTER S ON P.SUBST_MATL_NBR = S.SAP_MATL_NBR WHERE PO_NBR=@PO_NBR";
		
		public static string GetAltMaterialsForUpdate = @"SELECT P.* FROM PO_ALTERNATE_BOM P WHERE Po_Alt_Bom_Seq=@Po_Alt_Bom_Seq";
		
		public static string AddAltMaterials = @"
            INSERT INTO dbo.PO_ALTERNATE_BOM
                        (PO_NBR,
                        SUBSTITUTION_TYPE,
                        ORG_MATL_NBR,
                        ORG_QTY,
                        SUBST_MATL_NBR,
                        SUBST_QTY                       
                        )
                    VALUES
                        (@PO_NBR,
                        @SUBSTITUTION_TYPE,
                        @ORG_MATL_NBR,
                        @ORG_QTY,
                        @SUBST_MATL_NBR,
                        @SUBST_QTY
                      )
            SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]";

		public static string UpdateAltMaterials = @"Update dbo.PO_ALTERNATE_BOM Set SUBST_QTY=@SUBST_QTY Where Po_Alt_Bom_Seq=@Po_Alt_Bom_Seq";
		public static string DeleteAltMaterials = @"Delete dbo.PO_ALTERNATE_BOM Where Po_Alt_Bom_Seq=@Po_Alt_Bom_Seq";

		public static string AddRawMaterialsToStaging = @"INSERT INTO [dbo].[RAW_MATERIAL_STAGING](PO_NBR,SAP_MATL_NBR,FEEDER_NO,SCANNED_AMT,SCANNED_UOM,SCANNED_DATE_TIME,SCANNED_BY,ENTERED_AMT,LABEL_ID,CONSUMED_AMT
		) VALUES(@PO_NBR,
		@SAP_MATL_NBR,
		@FEEDER_NO,
		@SCANNED_AMT,
		@SCANNED_UOM,
		@SCANNED_DATE_TIME,
		@SCANNED_BY,
		@ENTERED_AMT,
		@LABEL_ID,@CONSUMED_AMT);
		SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]";

		//public static string GetInventoryByMatNbr = @"Select * from dbo.Sap_Storage_Bin_Inventory Where Matl_nbr=@Matl_Nbr ";
		public static string GetInventoryByMatNbr = @"Select Label_Id+'-'+convert(varchar,bag_no) as Label_Id, Amt_Received, 'ABC1234' as Location, FORMAT(Product_Received_Date,'MM/dd/yyyy') as Product_Received_Date from [dbo].[PURCHASE_ORDER_LABELS] P INNER JOIN [dbo].[INBOUND_RAW_MATERIALS] I ON P.IB_RAW_MTLS_ID = I.IB_RAW_MTLS_ID WHERE I.MATL_NBR=@Matl_Nbr and P.Bag_No!=0 order by Product_Received_Date asc";

	}
}
