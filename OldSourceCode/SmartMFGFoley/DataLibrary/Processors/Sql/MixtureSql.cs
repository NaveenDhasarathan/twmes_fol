﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Sql
{
    public static class MixtureSql
    {
        public static string InsertMixture = @"INSERT INTO [dbo].[MIXTURE](
	[PO_NBR],
	[MATL_NBR],
	[LOT_NBR],
	[WEIGHT],	
	[DRUM_NBR],
	[CREATE_DATE],
	[CREATED_BY],
	[INSTRUCTIONS],	
	[SCAN_DATE],
	[SCAN_BY],
	[DRUM_WGT],
	[WEIGHT_DATE],
	[WEIGHTED_BY],
	[USED_DATE],
	[USED_BY]) VALUES (
		@Po_Nbr,
		@Matl_Nbr,
		@Lot_Nbr,
		@Weight,	
	    @Drum_Nbr,
		@Create_Date,
		@Created_By,
		@Instructions,
		@Scan_Date,
		@Scan_By,
		@Drum_Wgt,
		@Weight_Date,
		@Weighted_By,
		@Used_Date,
		@Used_By)";
    }
}
