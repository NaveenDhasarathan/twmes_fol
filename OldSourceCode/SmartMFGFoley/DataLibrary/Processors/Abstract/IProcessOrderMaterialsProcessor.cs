﻿using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Abstract
{
    public interface IProcessOrderMaterialsProcessor
    {
		public List<BOMModel> LoadBOM(long? PO);

		public List<BOMModel> LoadBOMScanned();

		public List<BOMModel> LoadALTBOMScanned();

		public List<BOMModel> LoadEquipmentSetup();

		public List<BOMModel> LoadEquipmentActual();

		public List<ProductionScheduleModel> LoadDetails();

		public List<POAltMaterialDescModel> LoadAltMaterial(long PO);

		public int AddAlternateMaterials(POAlternateBOM altBOM);

		public POAlternateBOM GetAltMaterialForUpdate(long Po_Alt_Bom_Seq);

		public List<SapStorageBinInventory> GetInventoryByMatNbr(string matNbr);

		public int AddRawMaterialsToStaging(RawMaterialStagingModel rmModel);
		public int UpdateAltMaterials(long Po_Alt_Bom_Seq, decimal SubQty);
		public int DeleteAltMaterials(long Po_Alt_Bom_Seq);
	}
}
