﻿using DataLibrary.Models;
using System.Collections.Generic;

namespace DataLibrary.Processors.Abstract
{
    public interface IInboundRawMaterialsProcessor
    {
        public List<POMaterialModel> GetRawMaterialByDocType(string docType);
        List<POMaterialModel> GetAllSTO();
        List<POMaterialModel> GetInternalSTO();
        List<InboundRawMaterialsModel> GetExternalPOByNBR(long docNbr);
        RawMaterialDetailModel GetRawMaterialByRawMatlNo(long rawMatlId);
        RawMaterialDetailModel GetRawMaterialByRawMatlId(long rawMatlId);
        List<RawMaterialDetailModel> GetRawMaterialByDeliveryNo(string DeliveryNo);      
        POMaterialModel GetIRMInfoForSAP(long rawMatlId);
        POMaterialModel GetIRMInfoForSAPPOMaterials(long DeliveryNo);
        public List<STODeliveryDetailsModel> GetSTOByMtrl_Nbr(string Mtrl_Nbr);

    }
}
