﻿using DataLibrary.DataAccess.Abstract;
using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using System.Collections.Generic;
using System.Linq;


namespace DataLibrary.Processors
{
    public class RunSheetProcessor : IRunSheet
    {
        private readonly IReadWriteDataAccess _smartMfgDb;

        public RunSheetProcessor(IReadWriteDataAccess smartMfgDb)
        {
            _smartMfgDb = smartMfgDb;
        }
        public RunSheetModel GetOldRunSheet(int Material_Num, string Ext_Nbr)
        {
            return _smartMfgDb.LoadData<RunSheetModel>(Sql.RunSheetSql.GetOldRunSheet, new { @Material_Num = Material_Num, @Ext_Nbr = Ext_Nbr }).FirstOrDefault();
        }

        public List<BOMModel> GetBOMScanned(long PO)
        {
            return _smartMfgDb.LoadData<BOMModel>(Sql.RunSheetSql.GetBOMScanned, new { @PO = PO });
        }

        public IEnumerable<BOMModel> GetBOMPOScanned(int Type, long PO, bool altCheck)
        {
          
            List<BOMModel> data;

            if (altCheck == true)
            {
                data = GetAltBomScanned(PO);
            }
            else
            {
                data = GetBOMScanned(PO);
            }

            List<BOMModel> bom = new List<BOMModel>();
            if (Type == 2)
            {
                bom.Add(new BOMModel
                {
                    MATERIAL_DESCRIPTION = "Select..."
                });
                bom.Add(new BOMModel
                {
                    MATERIAL_DESCRIPTION = "Blend",
                    SAP_MATL_NBR = "1"
                });
                //bom.Add(new BOMViewModel
                //{
                //    MATERIAL_DESCRIPTION = "Mix"
                //});
                bom.Add(new BOMModel
                {
                    MATERIAL_DESCRIPTION = "Tumble",
                    SAP_MATL_NBR = "2"
                });
            }

            foreach (var row in data)
            {
                bom.Add(new BOMModel
                {
                    PO_NBR = row.PO_NBR,
                    SAP_MATL_NBR = row.SAP_MATL_NBR,
                    QUANTITY_REQUIRED = row.QUANTITY_REQUIRED,
                    QUANTITY_COMPLETED = row.QUANTITY_COMPLETED,
                    MATERIAL_DESCRIPTION = row.MATERIAL_DESCRIPTION,
                    SCANNED_AMT = row.SCANNED_AMT,
                    FEEDER_NO = row.FEEDER_NO,
                    UOM = row.UOM,
                    MATL_TYPE = row.MATL_TYPE
                });
            }

            return bom;
        }

        public List<BOMModel> GetAltBomScanned(long PO)
        {
            return _smartMfgDb.LoadData<BOMModel>(Sql.RunSheetSql.GetAltBomScanned, new { @PO = PO });
        }

        public int AddMixture(Mixture mix)
        {
            return _smartMfgDb.SaveData<Mixture>(Sql.MixtureSql.InsertMixture, new Mixture { Po_Nbr=mix.Po_Nbr,
            Matl_Nbr=mix.Matl_Nbr,
            Lot_Nbr=mix.Lot_Nbr,
            Weight=mix.Weight,
            Drum_Nbr=mix.Drum_Nbr,
            Create_Date=mix.Create_Date,
            Created_By = mix.Created_By,
            Instructions = mix.Instructions,
            Scan_Date=mix.Scan_Date,
            Scan_By = mix.Scan_By,
            Drum_Wgt = mix.Drum_Wgt,
            Weight_Date=mix.Weight_Date,
            Weighted_By =mix.Weighted_By,
            Used_Date = mix.Used_Date,
            Used_By = mix.Used_By});
        }
    }
}
