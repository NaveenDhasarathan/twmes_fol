﻿using System;

namespace DataLibrary.Models
{
    public class LabelInfoModel
    {
        public long Pol_Seq { get; set; }
        public long Ib_Raw_Mtls_Id { get; set; }        
        public string Label_Id { get; set; }     
        public DateTime Product_Received_Date { get; set; } 
        public decimal Amt_Received { get; set; }   

        public string Lot_Nbr { get; set; }
        public int Tot_Bag_No { get; set; }            
        public string CountryOrigin { get; set; }
        public string Vendor_Batch { get; set; }
        public int Bag_No { get; set; }
        public string Sap_Matl_Descr { get; set; }
        public string Sap_Matl_Nbr { get; set; }
        public long Document_Nbr { get; set; }
        public string batch_id { get; set; }
        public string UOM { get; set; }

    }
}
