﻿using System;
using System.ComponentModel;

namespace DataLibrary.Models
{
    public class RunSheetModel
    {
        public long Id { get; set; }
        public long Po_Nbr { get; set; }
        public string Batch_Nbr { get; set; }
        public string Issued_By { get; set; }
        public DateTime Issued_Date { get; set; }
        public string Verified_By { get; set; }
        public DateTime Create_Date { get; set; }
        public string Created_By { get; set; }
        public DateTime Update_Date { get; set; }
        public string Updated_By { get; set; }
        public bool Production_Ready { get; set; }
        public string Ext_Nbr { get; set; }
        public int Material_Num { get; set; }
        public long Master_Rate { get; set; }
        public long Rework_Rate { get; set; }

        [DisplayName("Feeder #1")]
        public string Feeder1_Material { get; set; }
        public decimal Feeder1_Percetage { get; set; }

        [DisplayName("Feeder #2")]
        public string Feeder2_Material { get; set; }
        public decimal Feeder2_Percetage { get; set; }

        [DisplayName("Feeder #3")]
        public string Feeder3_Material { get; set; }
        public decimal Feeder3_Percetage { get; set; }

        [DisplayName("Feeder #4")]
        public string Feeder4_Material { get; set; }
        public decimal Feeder4_Percetage { get; set; }

        [DisplayName("Feeder #5")]
        public string Feeder5_Material { get; set; }
        public decimal Feeder5_Percetage { get; set; }

        [DisplayName("Feeder #6")]
        public string Feeder6_Material { get; set; }
        public decimal Feeder6_Percetage { get; set; }

        [DisplayName("Feeder #7")]
        public string Feeder7_Material { get; set; }
        public decimal Feeder7_Percetage { get; set; }

        [DisplayName("Extruder Motor")]
        public int Extruder_Motor_Setpoint { get; set; }

        [DisplayName("Extruder Limit")]
        public int Extruder_Motor_Limit { get; set; }
        public string Extruder_Motor_Unit { get; set; }

        [DisplayName("Extruder Vaccuum")]
        public int Extruder_Vacuum_Setpoint { get; set; }
        public int Extruder_Vacuum_Limit { get; set; }
        public string Extruder_Vacuum_Unit { get; set; }
        public int Side_Stuffer_Barrel5_Setpoint { get; set; }
        public int Side_Stuffer_Barrel5_Limit { get; set; }
        public string Side_Stuffer_Barrel5_Unit { get; set; }
        [DisplayName("Side Stuffer Barrel 4")]
        public int Side_Stuffer_Barrel4_Setpoint { get; set; }
        public int Side_Stuffer_Barrel4_Limit { get; set; }
        public int Side_Stuffer_Barrel4_Unit { get; set; }
        public int Side_Stuffer_Barrel7_Setpoint { get; set; }
        public int Side_Stuffer_Barrel7_Limit { get; set; }
        public string Side_Stuffer_Barrel7_Unit { get; set; }

        [DisplayName("Side Stuffer Barrel 6")]
        public int Side_Stuffer_Barrel6_Setpoint { get; set; }
        public int Side_Stuffer_Barrel6_Limit { get; set; }
        public string Side_Stuffer_Barrel6_Vacuum_Unit { get; set; }
        public string Item_Comments { get; set; }

        [DisplayName("Barrel 1")]
        public int Zone1_Setpoint { get; set; }
        public int Zone1_Limit { get; set; }
        public string Zone1_Unit { get; set; }

        [DisplayName("Barrel 2")]
        public int Zone2_Setpoint { get; set; }
        public int Zone2_Limit { get; set; }
        public string Zone2_Unit { get; set; }

        [DisplayName("Barrel 3")]
        public int Zone3_Setpoint { get; set; }
        public int Zone3_Limit { get; set; }
        public string Zone3_Unit { get; set; }

        [DisplayName("Barrel 4")]
        public int Zone4_Setpoint { get; set; }
        public int Zone4_Limit { get; set; }
        public string Zone4_Unit { get; set; }

        [DisplayName("Barrel 5")]
        public int Zone5_Setpoint { get; set; }
        public int Zone5_Limit { get; set; }
        public string Zone5_Unit { get; set; }

        [DisplayName("Barrel 6")]
        public int Zone6_Setpoint { get; set; }
        public int Zone6_Limit { get; set; }
        public string Zone6_Unit { get; set; }

        [DisplayName("Barrel 7")]
        public int Zone7_Setpoint { get; set; }
        public int Zone7_Limit { get; set; }
        public string Zone7_Unit { get; set; }

        [DisplayName("Barrel 8")]
        public int Zone8_Setpoint { get; set; }
        public int Zone8_Limit { get; set; }
        public string Zone8_Unit { get; set; }

        [DisplayName("Barrel 9")]
        public int Zone9_Setpoint { get; set; }
        public int Zone9_Limit { get; set; }
        public string Zone9_Unit { get; set; }
        public int Zone10_Setpoint { get; set; }
        public int Zone10_Limit { get; set; }
        public string Zone10_Unit { get; set; }
        public int Zone11_Setpoint { get; set; }
        public int Zone11_Limit { get; set; }
        public string Zone11_Unit { get; set; }
        public int Die_Tempreture_Setpoint { get; set; }
        public int Die_Tempreture_Limit { get; set; }
        public string Die_Tempreture_Unit { get; set; }
        public string Breaker_Plate { get; set; }
        public string Screen_Pack { get; set; }
        public string Screw_Design { get; set; }
        public string Die_Hole_Diameter { get; set; }
        public string No_Die_Holes { get; set; }
        public string Vacuum_Insert_Type { get; set; }
        public string Strand_Vacuum { get; set; }
        public string Air_Knife { get; set; }
        public string Die_Air_Knife { get; set; }
        public string Barrel5_Vp { get; set; }
        public string Barrel4_Vp { get; set; }
        public string Barrel7_Vp { get; set; }
        public string Barrel6_Vp { get; set; }
        public string Safety_Notes { get; set; }
        public string Special_Rework_Notes { get; set; }
        public string Notes { get; set; }
        public string Additional_Notes { get; set; }
        public string Packing_Type { get; set; }
        public decimal Pack_Qty { get; set; }
        public string Pack_Default_Wgt { get; set; }
        public string Logo_Nologo { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Coversheet { get; set; }
        public string Coversheetfilename { get; set; }
        public string Coversheetext { get; set; }
    }
}
