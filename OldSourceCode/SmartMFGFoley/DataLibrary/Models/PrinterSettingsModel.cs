﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class PrinterSettingsModel
    {
        public long ID { get; set; }

        public string Process { get; set; }   
        public string PrinterName { get; set; }

        [StringLength(50)]
        public string LabelName { get; set; }
      
        public enum Processes
        {
            DrumTumble = 1,
            RawMatRec = 2,
            UnConsume = 3,
            ProdEX1LG = 4,
            ProdEX1SM = 5,
            ProdEX2LG = 6,
            ProdEX2SM = 7,
            ProdEX3LG = 8,
            ProdEX3SM = 9,
            ProdEX4LG = 10,
            ProdEX4SM = 11,
            RepackLG = 12,
            RepackSM = 13,
            PalletRawMatRec = 14,
            ItemRawMatRec = 15
        }

        public enum Printers //Once netwrk installs all and you have them registered with Martin's API add them here. 
        {
            FOLMES1 = 1,
            FOLLOGISTICS = 2
        }

        public enum Labels //Get with Martin and add all labels here. 
        {
            Drumble = 1,
            MES_RAW_MAT = 2
        }
    }
}
