﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class POMaterialModel
    {
        public long Ib_Raw_Mtls_Id { get; set; }
        public long Document_Nbr { get; set; }
        public int Item_Nbr { get; set; }
        public string Document_Type { get; set; }
        public string Supplier_Nbr { get; set; }
        public string Supplier_Name { get; set; }
        public DateTime Document_Date { get; set; }
        public string Matl_Nbr { get; set; }
        public DateTime? Requested_Delivery_Date { get; set; }
        public decimal Quantity { get; set; }
        public string Sap_Matl_Descr { get; set; }
        public string Sap_Matl_Type { get; set; }
        public string Matl_type_descr { get; set; }
        public string Additional_info { get; set; }
        public string Uom { get; set; }
        public string Style { get; set; }
        public decimal Amt_Received { get; set; }
        public string Storage_Location { get; set; }
        public string Delivery_No { get; set; }
        public string storage_unit_nbr { get; set; }
        public int SENT_TO_SAP { get; set; }
        public string LGFSB { get; set; }
        public string ITMEUom { get; set; }
        public string po_closed { get; set; }
        public decimal Quantity_Received { get; set; }
        public string Frm { get; set; }
        public string Too { get; set; }

    }
}
