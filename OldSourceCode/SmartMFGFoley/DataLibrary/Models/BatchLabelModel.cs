﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class BatchLabelModel
    {
        public string Label_Id { get; set; }
        public string Batch_Id { get; set; }       
        public string Sap_Matl_Descr { get; set; }
        public string Matl_Nbr { get; set; }
        public long Document_Nbr { get; set; }
        public DateTime? Batch_Sent_Time { get; set; }
    }
}
