﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class RawMaterialSAPModel
    {
        public string Storage_Unit_Nbr { get; set; }
        public string Batch_Nbr { get; set; }
        public string Vendor_Batch { get; set; }
        public decimal Amt_Received { get; set; }        
        public string Uom { get; set; }       
        public string Bill_Of_Lading { get; set; }
        public int Item_Nbr { get; set; }
        public string Matl_Nbr { get; set; }
        public int ItemNo { get; set; }
        public string MatlItemNbr { get; set; }
        
    }
}
