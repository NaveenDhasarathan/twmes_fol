﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLibrary.Models
{
    public class PurchaseOrderLabelsModel
    {
        [BindNever]
        public long Pol_Seq { get; set; }

        public long Ib_Raw_Mtls_Id { get; set; }

        [StringLength(10)]
        [DisplayName("Label ID")]
        public string Label_Id { get; set; }

        [DisplayName("Product Received Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Product_Received_Date { get; set; }

        [DisplayName("Label Print Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? Label_Print_Date { get; set; }

        [Required(ErrorMessage ="Amount received is required.")]
        [Column(TypeName = "numeric")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n0}")]
        [DisplayName("Amount Received")]
        public decimal Amt_Received { get; set; }


        [StringLength(3)]
        [DisplayName("UOM")]
        public string Uom { get; set; }

        [DisplayName("Label Scan Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? Label_Scan_Date { get; set; }

        [DisplayName("TPMS Trans Master Key")]
        public long? Tpms_Trans_Master_Key { get; set; }

        [DisplayName("LOT Number")]
        public string Lot_Nbr { get; set; }

        [DisplayName("Quality Check")]
        public bool Quality { get; set; }

        //[Required(ErrorMessage ="Total Bag Number is required.")]
        [DisplayName("Total Bag #")]
        public int Tot_Bag_No { get; set; }

        [Required(ErrorMessage ="Country of Origin is required.")]
        [DisplayName("Country of Origin")]
        public string CountryOrigin { get; set; }

        [Required(ErrorMessage ="Batch # is required.")]
        [DisplayName("Vendor Batch")]
        public string Vendor_Batch { get; set; }

        [DisplayName("Bag #")]
        public int Bag_No { get; set; }
        public DateTime Last_Modified_Time { get; set; }
        public string Last_Modified_By { get; set; }
        public string Batch_Id { get; set; }
        public DateTime? Batch_Sent_Time { get; set; }
        public string Bill_Of_Lading { get; set; }
        public string Note { get; set; }     
        public DateTime? Sent_Sap_Date { get; set; }
        public string Sent_Sap_By { get; set; }
        public bool Packaging_Material { get; set; }
        [DisplayName("Delivery Scan Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? Delivery_Scan_Date { get; set; }
        public string ITEM_NBR { get; set; }
        public string MATL_NBR { get; set; }
        public string ItemNo { get; set; }
        public string LineItems { get; set; }
        public string MatlItemNbr { get; set; }
        
    }
}
