﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class RawMaterialStagingModel
    {
         public long Rms_Id { get; set; }

        public long? Po_Nbr { get; set; }

        [StringLength(18)]
        public string Sap_Matl_Nbr { get; set; }

        public int? Feeder_No { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Scanned_Amt { get; set; }

        [StringLength(3)]
        public string Scanned_Uom { get; set; }

        public DateTime Scanned_Date_Time { get; set; }

        [StringLength(15)]
        public string Scanned_By { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Entered_Amt { get; set; }

        public string? Label_Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Consumed_Amt { get; set; }
    }
}
