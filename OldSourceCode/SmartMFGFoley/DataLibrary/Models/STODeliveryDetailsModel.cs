﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class STODeliveryDetailsModel
    {
        public long Sto_DD_Seq { get; set; }
        public long Delivery_Nbr { get; set; }
        public string? Storage_Unit_Nbr { get; set; }
        public string Source_Plant { get; set; }
        public string Batch_Nbr { get; set; }
        public string Matl_Nbr { get; set; }
        public DateTime? Delivery_Date { get; set; }
        public DateTime? Date_Added {get;set;}
        public DateTime? Date_Updated { get; set; }
        public DateTime? Date_Received { get; set; }
        public DateTime? Date_Sent_Sap { get; set; }
        public decimal Amt_Received { get; set; }
        public string Received_By { get; set; }
        public string Uom { get; set; }
        public string Scanned_In { get; set; }
        public string Bill_Of_Lading { get; set; }
        public string Note { get; set; }
        public string Sap_Sent_By { get; set; }
        public string Item_Nbr { get; set; }
        public string Batch_Id { get; set; }
        public DateTime? Scan_Date { get; set; }
        public string IB_Raw_Mtls_Id { get; set; }
        public string SAP_MATL_DESCR { get; set; }
        public string STO_DOC_NBR { get; set; }
        public string STO_DOC_ITEM_NBR { get; set; }
        public string ItemNo { get; set; }
        public DateTime? Label_Print_Date { get; set; }
        public bool ValidBatchID { get; set; }

    }
    public class STODeliveryDetailsUpdateModel
    {
        public string Storage_Unit_Nbr { get; set; }
        public string Batch_Nbr { get; set; }
        public string Matl_Nbr { get; set; }
        public decimal Amt_Received { get; set; }
    }
}
