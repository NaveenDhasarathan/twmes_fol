﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.DataAccess.Abstract
{
    public interface IWriteDataAccess: System.IDisposable
    {
        int SaveData<T>(string sql, T data, bool returnId = false);
        int SaveDataInTransaction<T>(string sql, T data, bool returnId = false);
    }
}
