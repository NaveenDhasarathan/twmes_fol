﻿
namespace DataLibrary.DataAccess.Abstract
{
    public interface IReadWriteDataAccess: IReadDataAccess, IWriteDataAccess
    {
    }
}