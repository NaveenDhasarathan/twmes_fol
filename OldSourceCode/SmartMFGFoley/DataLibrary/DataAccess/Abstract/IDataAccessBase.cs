﻿using System.Data;

namespace DataLibrary.DataAccess.Abstract
{
    public interface IDataAccessBase
    {
        IDbConnection CreateConnection();
        
        IDbConnection CreateConnection(string section);
    }
}
