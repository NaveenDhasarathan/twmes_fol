﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.DataAccess.Abstract
{
    public interface IReadDataAccess: System.IDisposable
    {
        List<T> LoadData<T>(string sql);
        List<T> LoadData<T>(string sql, object data);
    }
}
