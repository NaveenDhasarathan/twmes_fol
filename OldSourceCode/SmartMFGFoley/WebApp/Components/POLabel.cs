﻿using DataLibrary.DataAccess.Abstract;
using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Components
{
    public class POLabel: ViewComponent
    {
        private readonly IRawMaterialProcessor _labelProcessor;
        private readonly IReadWriteDataAccess _readWriteAccess;
        private readonly IInboundRawMaterialsProcessor _rawMaterialProcessor;
        private readonly ISapMatlMasterProcessor _sapMaterialProcessor;
        public POLabel(IRawMaterialProcessor labelProcessor,
            IReadWriteDataAccess readWriteAccess,
            IInboundRawMaterialsProcessor rawMaterialProcessor,
            ISapMatlMasterProcessor sapMaterialProcessor)
        {
            _labelProcessor = labelProcessor;
            _readWriteAccess = readWriteAccess;
            _rawMaterialProcessor = rawMaterialProcessor;
            _sapMaterialProcessor = sapMaterialProcessor;
        }

        public IViewComponentResult Invoke(long id)
        {
            return View(new PurchaseOrderLabelsModel { Ib_Raw_Mtls_Id=id });
        }

    }
}
