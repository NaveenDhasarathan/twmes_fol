﻿using DataLibrary.Models;
using System.Collections.Generic;
using System.ComponentModel;


namespace WebApp.ViewModels
{
     
    public class RawMaterialChildViewModel
    {       
        public POMaterialModel POMaterial { get; set; }
        public string LineItems { get; set; }      
        public string Style { get; set; } 
    }


    public class RawMaterialsGroupedViewModel
        {
           // public IReadOnlyList<RawMaterialViewModel> POs { get; set; }
        public List<RawMaterialChildViewModel> PurchaseOrderList { get; set; }

        public decimal TotalQTY { get; set; }

        }
    }

