﻿$(function () {
    $(document).ready(function () {
        $('#Amt_Received').on("blur", function () {
            $("#Amt_Received").val(parseFloat($("#Amt_Received").val()).toFixed(3));
        });
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear() + "-" + (month) + "-" + (day);
        $('#Product_Received_Date').val(today);

        var table = $('#stoListing').dataTable({
            dom: 'Bfrtip',
            searchHighlight: true,
            buttons: [
                {
                    extend: 'excel',
                    title: $('#DocNum').val() + '_' + $('#DeliveryNo').val() + '_' + $('#StoItemNbr').val(),
                    exportOptions: {
                        columns: 'th:not(.noprint),tr:not(.noprint)',
                        headers: 'th:not(.noprint),tr:not(.noprint)'
                    }
                    , header: false
                }
                //'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "lengthMenu": [[50, 100, -1], [50, 100, "All"]],
            //"searching": true,
            "columnDefs": [{
                "orderable": false,
                "className": 'select-checkbox',
                "targets": 0

            }],
            "select": {
                style: 'os',
                selector: 'td:first-child'
            },

            "paging": true,
            "ordering": true,
            "aaSorting": [1, 'desc'],
            "info": true,
            "dom": '<"top">t<"bottom"lp><"clear">',
            "initComplete": function () {
                this.api().columns('.searchable').every(function () {
                    var column = this;
                    var select = $('<select class="input-group input-group-sm"><option value="">Show All</option></select>')
                        //.appendTo($(column.footer()).empty())
                        .appendTo($(column.header()))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        if (d.trim() != "")
                            if (!InArray(d))
                                select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            }
        });

        $('#ExportExcelSTO').click(() => {
            //tableSTO.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
            $('#stoListing').DataTable().buttons(0, 0).trigger()
            //tableSTO.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
        });
        table.DataTable().on('select', function (e, dt, type, indexes) {

            if (type === 'row') {
                var rows = table.DataTable().rows(indexes).nodes().to$();
                var sel = false;
                $.each(rows, function () {
                    if ($(this).hasClass('Ignoreme')) {
                        table.DataTable().row($(this)).deselect();
                        sel = true;
                    }
                })
                if (sel)
                    alert("The label need to be scanned before sending information to SAP.");
            }
        });

        //$('input[type=radio][name=hasLabel]').change(function () {
        //    var haslbl = $('input[name="hasLabel"]:checked').val();
        //    if (haslbl == '') {
        //        $('#STOWithLabel').show();
        //        $('#STO-save-btn').show();

        //        $('.NoLabel').hide();
        //        alert(haslbl);
        //    }
        //    if (haslbl != '') {
        //        if (haslbl == 'NoLabel') {
        //            $('#STOWithLabel').show();
        //            $('#STO-save-btn').show();
        //            $('.NoLabel').hide();
        //        }
        //        else if (haslbl == 'NoLabel1') {
        //            $('#STOWithLabel').hide();
        //            $('#STO-save-btn').hide();
        //            $('.NoLabel').show();
        //        }
        //        //else
        //        //location.href = '/RawMaterial/PurchaseOrderLabel/'+this.value;
        //    }

        //});

        //Reprint button on the top
        $('#selectReprint').on('click', function (e) {
            var printType = [];
            $.each($("input[name='type']:checked"), function () {
                printType.push($(this).val());
            });

            var table = $('#stoListing').dataTable();
            var tblData = table.DataTable().rows('.selected').data();
            var arrLabel = [];
            $.each(tblData, function (i, val) {
                $.each(val, function (j, value) {
                    if (j == 3) {
                        arrLabel.push(value);
                        //console.log(value);
                    }
                });
            });
            if (arrLabel.length < 1) {
                $("#sMsg").html("Please select labels to print.");
                $('#alertMsg').show();
                setTimeout(function () {
                    $('#alertMsg').hide();
                }, 2000);
            }
            else {
                $("#modalTitle1").html("Sending reprint request...");
                $("#labelModal").modal('show');
                $('#cancelModal').hide();
                $.ajax({
                    type: "POST",
                    url: "/RawMaterial/ConfirmReprint",
                    traditional: true,
                    data: {
                        pol_seq: arrLabel
                    },
                    //contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    error: function () {
                        showErrorMessage("Error occurred, label printing request was not sent successfully.");
                    },
                    success: function (data) {

                        if (data.success) {
                            var msg = "Label printing request was sent.";

                            if (!data.updatePrintDate)
                                msg += " But, the last print date was not updated successfully.";
                            showSuccessMessage(msg);
                            setTimeout(function () {
                                $("#labelModal").modal('hide');
                                location.reload();
                            }, 3000);
                        }
                        else {
                            showErrorMessage("Error occurred, label printing request was not sent successfully.");
                            $('#cancelModal').show();
                        }
                    }
                });
            }
        });
    });
    var HeaderArray = ["PO Number", "Delivery Number", "Request Delivery Date", "Document Type", "Material Type", "SAP Sent", "Supplier Name", "Supplier Number",
        "Item Number", "Material Number", "Storage Unit Number", "Batch Number", "Batch ID", "Quantity Received", "Date Received", "Label Scan Date", "Delivery Scan Date", "Date Sent to SAP", "Print Date"
        , "Order No.", "Delivery No. :", "Item No", "Label ID", "Container Weight", "Material No.", "Total Bags", "Vendor Batch", "Ascend Batch", "Material Description", "Received Date", "Quantity", "Scan Date", "Storage Location"];
    function InArray(needle) {
        //return (HeaderArray.indexOf(needle) > -1);
        var result = false;
        result = HeaderArray.includes(needle)
        if (!result) {
            for (var i = 0; i <= HeaderArray.length; i++) {
                result = needle.includes(HeaderArray[i]);
                if (result)
                    return result;
            }
        }
        return result;
    }

    //Send to SAP button on the top
    //$('#selectSap').on('click', function (e) {

    //    var table = $('#stoListing').dataTable();
    //    var tblData = table.DataTable().rows('.selected').data();
    //    var arrLabel = [];
    //    $.each(tblData, function (i, val) {
    //        let lb = "";
    //        let dt = "";
    //        $.each(val, function (j, value) {                
    //            if (j == 1)
    //                lb = value;              
    //            if (j == 8)
    //                dt = value;                
    //        });
    //        if (dt == "" || dt == null)
    //            arrLabel.push(lb);
    //    });
    //    if (arrLabel.length < 1 ) {
    //        $("#sMsg").html("Please select records to send to SAP.");
    //        $("#sMsg").addClass("text-danger");
    //        $('#alertMsg').show();
    //        setTimeout(function () {
    //            $('#alertMsg').hide();
    //        }, 2000);
    //    }
    //    else {
    //        $("#modalTitle").html("Sending information to SAP...");
    //        $("#stoModal").modal('show');
    //        $('#cancelModal').hide();
    //        $.ajax({
    //            type: "POST",
    //            url: "/RawMaterial/STOToSap",
    //            traditional: true,
    //            data: {
    //                arrLabel: arrLabel                   
    //            },
    //            //contentType: 'application/json; charset=utf-8',
    //            dataType: "json",
    //            error: function () {
    //                showErrorMessage("Error occurred, the request was not sent successfully.");
    //            },
    //            success: function (data) {
    //                if (data.success) {
    //                    var msg = "Sent to SAP successfully.";                      
    //                    showSuccessMessage(msg);
    //                    setTimeout(function () {
    //                        location.reload();
    //                        $("#stoModal").modal('hide');
    //                    }, 3000);
    //                }
    //                else {
    //                    showErrorMessage("Error occurred, the request was not sent successfully.");
    //                    $('#cancelModal').show();
    //                }
    //            }
    //        });
    //    }
    //});
    $("#PMReceived").change(function () {

        if ($('input[type=checkbox]').prop('checked')) {
            $("#stoModal").modal('show');
            $("#modalTitle1").html("Updating Date Received..");
            $.ajax({
                type: "POST",
                url: "/RawMaterial/UpdateSTOPackaging",
                traditional: true,
                data: {
                    deliveryNo: $(this).val()

                },
                dataType: "json",
                error: function () {
                    showErrorMessage("Error occurred, no label is updated.");

                },
                success: function (data) {
                    if (data.success) {
                        showSuccessMessage("Receiving information was sent successfully!");
                        setTimeout(function () {
                            $("#stoModal").modal('hide');
                            location.reload();
                        }, 3000);
                    }
                    else
                        showErrorMessage("Error occurred, no receiving information was sent. Error details: " + data.errMsg);
                }
            });
        } else {
            alert("Check box is Unchecked");
        }
    });

    $('#STOReportToSAP').click(function () {

        $('#success-alert3').hide();
        $('#error-alert3').hide();
        $("#stoModal3").modal('show');
        //$("#modalTitle1").html("Sending Raw Material Receiving to SAP...");
        var deliveryNbr = $("#DeliveryNo").val();
        var matl = $("#DocNum").val();
        var packaging = $("#PackagingMaterial").val();
        var frm_value = $("#frm_value").val();
        var too_value = $("#too_value").val();

        $("#labelModalSpinner3").show();


        $.ajax({
            type: "POST",
            url: "/RawMaterial/STOToSap",
            traditional: true,
            data: {
                deliveryNbr,
                matl,
                packaging,
                stoItemNbr: $("#StoItemNbr").val(),
                frm_value,
                too_value
            },
            dataType: "json",
            error: function () {
                showErrorMessage3("Error occurred, no label is updated.");
            },
            success: function (data) {
                if (data.success) {
                    $("#labelModalSpinner3").hide();
                    showSuccessMessage3("Receiving information was sent successfully!");
                    setTimeout(function () {
                        $("#stoModal").modal('hide');
                        location.reload();
                    }, 3000);
                }
                else
                    showErrorMessage3("Error occurred, no receiving information was sent. Error details: " + data.errMsg);
                $("#labelModalSpinner3").hide();

            }
        });

    }
    );



    function SetBatchNo() {
        var mtldetail = $("#Item_Nbr option:selected").text().split("/");
        if (mtldetail[4] != "") {
            $('#Batch_No').val(mtldetail[4]);
            $('#Batch_No').attr('readonly', true);
            $('#Batch_No').addClass('input-disabled');
        }
        else {
            $('#Batch_No').val(mtldetail[4]);
            $('#Batch_No').attr('readonly', false);
            $('#Batch_No').removeClass('input-disabled');
        }
        CheckBatchIDOUTSIDE();
    }
    function CheckBatchIDOUTSIDE() {
        var Batch_No = $('#Batch_No').val();
        if (Batch_No != null && Batch_No != '') {
            $.ajax({
                type: "GET",
                url: "/RawMaterial/SearchForBatchID",
                data: { BatchID: Batch_No },
                //contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: function () {
                    showErrorMessage("Error occurred, no label is deleted.");
                },
                success: function (data) {
                    if (data == true) {
                        console.log("Found in nppqs");
                        $('#STOWithLabel').hide();
                        $('#STO-save-btn').hide();
                        $('.NoLabel').hide();
                        $('#POLabel-footer').hide();

                    }
                    else {
                        console.log("Not Found in nppqs");
                        $('#STOWithLabel').show();
                        $('#STO-save-btn').show();
                        $('.NoLabel').show();
                        $('#POLabel-footer').hide();
                    }
                    //if (data== 1) {
                    //    showSuccessMessage("Success! Label has been deleted.");
                    //    setTimeout(function () {
                    //        $("#labelModal").modal('hide');
                    //        location.reload();
                    //    }, 2000);
                    //}
                    //else
                    //    showErrorMessage("Error occurred, no label is deleted.");
                }
            });
        }
        else {
            $('#STOWithLabel').show();
            $('#STO-save-btn').show();
            $('.NoLabel').show();
            console.log("hi1");
            //$('#POLabel-footer').hide();
        }
    }
    //$('#Item_Nbr').change(function () {
    //    SetBatchNo();
    //});
    //SetBatchNo();



    $("#NewSTOForm").submit(function (e) {
        var valid = true;

        //if ($("#Amt_Received").val() == 0) {
        //    alert("Please Enter Quantity Received");
        //    valid = false;
        //}
        //if ($("#Container_No").val() == 0) {
        //    alert("Please Enter Container No");
        //    valid = false;
        //}
        e.preventDefault();
        var arr = [];
        var RowsToCreate = $("#RowsToCreate").val();
        var iNumber = "";
        var iBatch = "";
        var iContainer = "";
        var iAmount = "";
        var iUnit = "";
        //var iCO = "";
        //var iBOL = "";
        var itmdetil_15 = "";
        var itmdetil_16 = "";

        for (var ii = 1; ii <= RowsToCreate; ii++) {
            iNumber += $("#Item_Nbr_" + ii).val() + ",";
            iBatch += $("#Batch_No_" + ii).val() + ",";
            iContainer += $("#Container_No_" + ii).val() + ",";
            iAmount += $("#Amt_Received_" + ii).val() + ",";
            iUnit += $("#UnitPick_" + ii).val() + ",";
            //iBOL += $("#Bill_Of_Lading_" + ii).val() + ",";
            //iCO += $("#CountryOrigin_" + ii + " option:selected").text() + ",";
            itmdetil_15 += $("#Item_NbrVLS_" + ii).val().split("/")[3] + ",";
            itmdetil_16 += $("#Item_NbrVLS_" + ii).val().split("/")[0] + ",";
        }
        arr.push(iNumber);//0
        arr.push($("#Product_Received_Date").val());//1
        arr.push(iAmount);//2
        arr.push(iContainer);  //3
        //arr.push($("#Tot_Bag_No").val());//4         
        arr.push(iBatch);//4
        arr.push($("#CountryOrigin option:selected").text());//5
        //arr.push($("#palletNo").val());//7
        arr.push($("#PONum").val());//6       
        arr.push($("#Matl_Nbr").val());//7
        arr.push($("#Bill_Of_Lading").val());//8
        arr.push($("#Note").val());//9
        arr.push($("#PackagingMaterial").val());//10
        //arr.push($("#StOuom").val());//11
        arr.push(iUnit);//11
        arr.push($("#StoItemNbr").val());//12
        arr.push($("#Source_Plant").val());//13
        arr.push($("#serialNo").val());//14
        arr.push(itmdetil_15);//15
        arr.push(itmdetil_16);//16
        arr.push(RowsToCreate);//17
        arr.push($("#POSupplierNo").val());//18

        if (valid) {

            $("#modalTitle").html("Create and save Labels");
            $("#stoModal").modal('show');
            $("#labelModalSpinner2").show();
            $('#cancelModal').hide();
            $(".modal-backdrop").hide();
            $.ajax({
                type: "POST",
                url: "/RawMaterial/SaveSTO",
                traditional: true,
                data: {
                    arr: arr
                },
                //contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: function () {
                    $("#labelModalSpinner").hide();
                    showErrorMessage("Error! Failed to create label(s).");
                },
                success: function (data) {
                    $("#labelModalSpinner").hide();

                    var msg = "";
                    if (data.newId > 0) {
                        msg = "Success! The record was saved. ";
                        showSuccessMessage(msg);
                        setTimeout(function () { $("#stoModal").modal('hide'); location.reload(); }, 3000);
                    }
                    else {
                        msg = "Error! Failed to save record. ";
                        showErrorMessage(msg);
                        $('#cancelModal').show();
                    }

                }
            });

        }
    });


    //Delete
    $('.btnDelete').click(function () {
        $('#updateLabelHtml').hide();
        $('#success-alert').hide();
        $("#btnReprintConfirm").hide();
        $("#btnUpdateConfirm").hide();
        $("#modalTitle1").html("Are you sure to delete all the labels?");
        $("#labelModal").modal('show');
        $('#labelModalSpinner').show();
        $(".modal-backdrop").hide();

        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: "/RawMaterial/GetLabelForDel_STO",
            data: { pol_seq: id },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                alert("Error occurred, no label info found.");
                $("#labelModal").modal('hide');
                $('#labelModalSpinner').hide();
            },
            success: function (data) {
                $('#labelModalSpinner').hide();
                var str = "";
                /* $.each(data, function (ind, value) {*/
                $.each(data, function (key, val) {
                    if (val == null)
                        val = "";
                    if (key == 'pol_seq')
                        $('#seq').val(val);
                    if (key == 'label_Id')
                        str += "<tr><td class='font-weight-bold'>Label Id:</td><td>" + val + "<input type='hidden' name='labelId' id='labelId' value='" + val + "'></td></tr>";
                    if (key == 'p')
                        str += "<tr><td class='font-weight-bold'>Product Received Date:</td><td>" + val + "</td></tr>";
                    if (key == 'v')
                        str += "<tr><td class='font-weight-bold'>Label Print Date:</td><td>" + val + "</td></tr>";
                    if (key == 'amt_Received')
                        str += "<tr><td class='font-weight-bold'>Amount Received:</td><td>" + val + " KG </td></tr>";
                    if (key == 'batch_No')
                        str += "<tr><td class='font-weight-bold'>Vendor Batch #:</td><td>" + val + "</td></tr>";

                });
                $("#delRec1").html("<table class='table'>" + str + "</table>");
                $("#delConfirm").show();
            }
        });
    });

    $('#delConfirm').click(function () {
        var id = $('#labelId').val();
        $.ajax({
            type: "POST",
            url: "/RawMaterial/ConfirmLabelDelForSTODetail",
            data: { labelId: id },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                showErrorMessage("Error occurred, no label is deleted.");
            },
            success: function (data) {
                if (data.affected > 0) {
                    showSuccessMessage("Success! Label has been deleted.");
                    setTimeout(function () {
                        $("#labelModal").modal('hide');
                        location.reload();
                    }, 2000);
                }
                else
                    showErrorMessage("Error occurred, no label is deleted.");
            }
        });
    });

    //Update
    $('.btnUpdate').click(function () {
        $('#success-alert').hide();
        $('#error-alert').hide();
        $("#delRec1").html("");
        $("#delRec1").hide();
        $("#btnReprintConfirm").hide();
        $("#delConfirm").hide();
        $("#modalTitle1").html("Edit Labels");
        $("#labelModal").modal('show');
        $('#labelModalSpinner').show();
        $(".modal-backdrop").hide();

        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: "/RawMaterial/GetLabelForDel_STO",
            data: { pol_seq: id },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                $("#labelModal").modal('hide');
                alert("Error occurred, no label info found.");
                $('#labelModalSpinner').hide();
            },
            success: function (data) {
                $('#labelModalSpinner').hide();
                var str = "";
                /* $.each(data, function (ind, value) {*/
                //alert(data.pol_seq);
                $.each(data, function (key, val) {
                    if (val == null)
                        val = "";
                    if (key == 'pol_seq')
                        $('#labelRecId').val(val);
                    if (key == 'label_Id')
                        $('#labelIdUpd').val(val);

                    if (key == 'p')
                        $('#labelReceiveDate').val(val);

                    //if (key == 'v')
                    //    str += "<tr><td class='font-weight-bold'>Label Print Date:</td><td><input type='date' id='pDate' value='" + val +"' required></td></tr>";
                    if (key == 'amt_Received')
                        $("#amtUpd").val(val);

                    if (key == 'lot_Nbr')
                        $('#lotNoUpd').val(val);

                    //if (key == 'tot_Bag_No') {
                    //    $('#bagNoUpd').val(val);
                    //    $('#labelTotalBags').val(val);
                    //}

                    if (key == 'vendor_Batch') {
                        $('#batchNoUpd').val(val);
                        $('#batchNoUpd').attr("readonly", false);
                        $('#labelVendorBatch').val(val);
                    }

                    if (key == 'countryOrigin') {
                        $("#originUpd option").filter(function () {
                            //may want to use $.trim in here
                            return $.trim($(this).text()) == $.trim(val);
                        }).prop('selected', true);
                        $('#originUpd').attr("readonly", false);

                    }
                });

                $('#updateLabelHtml').show();
                $("#btnUpdateConfirm").show();
            }
        });
    });

    //Print
    $('.btnReprint').click(function () {
        $('#success-alert').hide();
        $("#delRec1").html("");
        $('#updateLabelHtml').hide();
        $("#delConfirm").hide();
        $("#btnUpdateConfirm").hide();
        $('#labelModalSpinner').show();
        //$("#modalTitle").html("Reprint Labels");
        $("#modalTitle1").html("Reprint Labels");
        $("#labelModal").modal('show');
        $(".modal-backdrop").hide();

        var id = $(this).data('id');
        var matId = $("#DocNum").val();
        $.ajax({
            type: "POST",
            url: "/RawMaterial/GetLabelForDel_Label",
            data: { pol_seq: id },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                alert("Error occurred, no label info found.");
                $("#labelModal").modal('hide');
                $('#labelModalSpinner').hide();
            },
            success: function (data) {
                $('#labelModalSpinner').hide();
                var str = "";
                var totBag = 0;
                $.each(data, function (key, val) {
                    if (val == null)
                        val = "";
                    if (key == 'pol_seq') {
                        $('#seq').val(val);
                        $('#labelRecId').val(val);
                    }
                    if (key == 'label_Id')
                        $('#labelIdUpd').val(val);

                    if (key == 'p')
                        $('#labelReceiveDate').val(val);

                    //if (key == 'v')
                    //    str += "<tr><td class='font-weight-bold'>Label Print Date:</td><td><input type='date' id='pDate' value='" + val +"' required></td></tr>";
                    if (key == 'amt_Received') {
                        $("#amtUpd").val(val);
                    }

                    if (key == 'lot_Nbr')
                        $('#lotNoUpd').val(val);

                    //if (key == 'tot_Bag_No') {
                    //    $('#bagNoUpd').val(val);
                    //    $('#labelTotalBags').val(val);
                    //}

                    if (key == 'vendor_Batch') {
                        $('#batchNoUpd').val(val);
                        $('#batchNoUpd').attr("readonly", true);
                        $('#labelVendorBatch').val(val);
                    }

                    if (key == 'countryOrigin') {
                        $("#originUpd option").filter(function () {
                            //may want to use $.trim in here
                            return $.trim($(this).text()) == $.trim(val);
                        }).prop('selected', true);
                        $('#originUpd').attr("readonly", true);
                    }
                    if (key == 'tot_Bag_No') {
                        totBag = val;
                    }
                });
                //if (totBag > 0) {
                //    var drp = "";
                //    for (var i = 1; i < totBag + 1; i++) {
                //        drp += "<option value='" + i + "'>" + i + "</option>";
                //    }
                //    str += "<tr><td colspan='2' class='font-weight-bold'>Please select bag # to print</td></tr><tr><td class='font-weight-bold'>From <select id='from' class='form-control wid-100'>" + drp + "</select> </td><td class='font-weight-bold'>To <select id='to' class='form-control wid-100'>" + drp + "</select></td></tr>";
                //}

                //$("#delRec1").html("<input type='hidden' id='matId' value='" + matId + "'><table class='table-sm'>" + str + "</table>");
                $("#btnReprintConfirm").show();
                $('#updateLabelHtml').show();
                //$("#btnUpdateConfirm").show();
            }
        });
    });

    //Reprint with begin no and end no for single bag only
    $('#btnReprintConfirm').click(function () {

        var id = $('#seq').val();
        //var fm = $('#from').val();
        //var to = $('#to').val();
        var matId = $("#matId").val();
        var amtUpd = $("#amtUpd").val();
        $.ajax({
            type: "POST",
            url: "/RawMaterial/ConfirmReprintSTO_WithAmount",
            data: { pol_seq: id, matId: matId, amtUpd: amtUpd },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                showErrorMessage("Error occurred, label printing request was not sent successfully.");
            },
            success: function (data) {

                if (data.success) {
                    var msg = "Label printing request was sent.";

                    if (!data.updatePrintDate)
                        msg += " But, the last print date was not updated successfully.";
                    showSuccessMessage(msg);
                    setTimeout(function () {
                        location.reload();
                    }, 3000);
                    //$("#labelModal").modal('hide');
                }
                else {
                    showErrorMessage("Error occurred, label printing request was not sent successfully.");
                }
            }
        });
    });

})