﻿using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    public class ProcessOrderController : Controller
    {
        private readonly IProcessOrder _prodSchedule;
        private readonly IProcessOrderMaterialsProcessor _poMaterialProcessor;
        private readonly ISapMatlMasterProcessor _sapMaterialProcessor;
        private readonly IRawMaterialProcessor _labelProcessor;

        public ProcessOrderController(IProcessOrder prodSchedule, 
            IProcessOrderMaterialsProcessor bomProcessor, 
            ISapMatlMasterProcessor sapMaterialProcessor, 
            IRawMaterialProcessor labelProcessor)
        {
            _prodSchedule = prodSchedule;
            _poMaterialProcessor = bomProcessor;
            _sapMaterialProcessor = sapMaterialProcessor;
            _labelProcessor = labelProcessor;
        }
        public IActionResult Index()
        {
            ViewBag.Message = "Production Schedule";

            dynamic psModel = new ExpandoObject();
            psModel.Resources = _prodSchedule.LoadResources();
            psModel.Materials = _prodSchedule.LoadMaterialList();
            psModel.Schedule = _prodSchedule.LoadProductionSchedule();

            return View(psModel);
        }

        public IActionResult MaterialList(long po)
        {
            List<BOMModel> bomModel= new List<BOMModel>();
            bomModel = _poMaterialProcessor.LoadBOM(po);
            List<BOMViewModel> bomVm = new List<BOMViewModel>();
            decimal total = bomModel.Sum(b => b.QUANTITY_REQUIRED); ;
            foreach(BOMModel bom in bomModel)
            {
                string cls = "";
                if (bom.MATL_TYPE.ToUpper() == "C")
                    cls = "Success";
                if (bom.MATL_TYPE.ToUpper() == "F")
                    cls = "WhiteBG";

                bomVm.Add(new BOMViewModel
                {
                    Po_Nbr = bom.PO_NBR,
                    Sap_Matl_Nbr = bom.SAP_MATL_NBR,
                    Material_Description = bom.MATERIAL_DESCRIPTION,
                    Quantity_Required = bom.QUANTITY_REQUIRED,
                    RowStyle = cls,
                    Percent = Math.Round((bom.QUANTITY_REQUIRED / total) * 100, 2)
                });
                total += bom.QUANTITY_REQUIRED;
            }

            dynamic podModel = new ExpandoObject();           
            podModel.BOM = new BOMListViewModel { BOMList=bomVm, Total=total };
            podModel.Details = _prodSchedule.LoadPSModel(po);
            podModel.AltBOM = _poMaterialProcessor.LoadAltMaterial(po);
            return View("ProcessOrderMaterialDetails",podModel);
        }

        public JsonResult GetInventory(string matNbr)
        {
            matNbr = "10277464";
            var matList = _poMaterialProcessor.GetInventoryByMatNbr(matNbr);
            if (matList != null)
            {
                return Json(new { mat=matList.ToArray() });
            }
            return null;
        }
        public IActionResult Runsheet(long po)
        {
            //po = 101302498;
            po = 101278778;
            //ProductionScheduleModel pModel = new ProductionScheduleModel();
            //pModel = _prodSchedule.LoadPSModel(po);
            //List<POAltMaterialDescModel> altModel = new List<POAltMaterialDescModel>();
            //altModel = _poMaterialProcessor.LoadAltMaterial(101308246);
            //BOMViewModel bm = new BOMViewModel();
            //dynamic podModel = new ExpandoObject();
            //podModel.BOM = _poMaterialProcessor.LoadBOM(101302498);
            //podModel.Details = pModel;
            //podModel.AltBOM = altModel;
            return View("Runsheet");
        }

        public JsonResult GetAltMaterialForUpdate(string pol_seq)
        {
            POAlternateBOM altMat = _poMaterialProcessor.GetAltMaterialForUpdate(long.Parse(pol_seq));
            if (altMat != null)
            {
                return Json(new {altMat});
            }
            return null;
        }
        public JsonResult AltBOMSave(string matNum, string PONum, string matAmt)
        {
           
            if (matNum != "" && PONum != "")
            {
                try
                {
                    int rtn = _poMaterialProcessor.AddAlternateMaterials(new POAlternateBOM
                    {
                        Po_Nbr = Convert.ToInt32(PONum),
                        Substitution_Type = "M",
                        Subst_Matl_Nbr = matNum,
                        Subst_Qty = Convert.ToDecimal(matAmt),
                        Org_Matl_Nbr = "Manual"

                    });

                    return Json(new { success = true });
                }
                catch
                {
                    return Json(new { success = false });
                }
            }

            return null;
        }

        public JsonResult SaveAltBomUpdate(string pol_seq, string subQty)
        {
            int rtn;
            try
            {
               rtn = _poMaterialProcessor.UpdateAltMaterials(long.Parse(pol_seq), decimal.Parse(subQty));
            }
            catch
            {              
                rtn = -1;
            }
            return Json(new {rtn});
        }

        
        public JsonResult ConfirmAltDel(string pol_seq)
        {
            int rtn;
            try
            {
                rtn = _poMaterialProcessor.DeleteAltMaterials(long.Parse(pol_seq));
            }
            catch{               
                rtn = -1;
            }
            return Json(new { rtn });
        }

        public JsonResult GetSAPList(string matNum)
        {
            var sapList = _sapMaterialProcessor.GetSapMaterialByMATLDesc(matNum+"%").ToList();           

            return Json(new { sapList });
        }

        public JsonResult ScannedInRawMaterial(string scan, string qtyScanned, string poPicked)
        {
            string _labelId = "";
            int _bagNo=1;
            if (scan != "")
            {
                var arr = scan.Split("-"); //bag label:688000001-1
                _labelId = arr[0].Trim();
                _bagNo = Int32.Parse(arr[1]);
            }
            try
            {
                //Check smartMFG
                LabelInfoModel rmModel = _labelProcessor.GetSingleLabelInfo(_labelId, _bagNo);
                if (rmModel != null)
                {
                    decimal qty = (!string.IsNullOrEmpty(qtyScanned)) ? decimal.Parse(qtyScanned) : 0;
                    int rtn = _poMaterialProcessor.AddRawMaterialsToStaging(new RawMaterialStagingModel
                    {
                        Po_Nbr = Int64.Parse(poPicked),
                        Sap_Matl_Nbr = rmModel.Sap_Matl_Nbr,
                        //Feeder_No = rmModel.Feeder_No,
                        Scanned_Amt = rmModel.Amt_Received,
                        Scanned_Uom = "KG",
                        Scanned_Date_Time = DateTime.Now,
                        Scanned_By = User.Identity.Name.Replace("APM\\", "").ToLower(),
                        Label_Id = rmModel.Label_Id + "-" + rmModel.Bag_No.ToString()

                    });
                    return Json(new { rtn, label_Id = scan, totScanned = qty + rmModel.Amt_Received });
                }
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
            }

            return null;
            //Check NPPQS
            //RAW_MATERIAL_STAGING rms = new RAW_MATERIAL_STAGING();
            //SmartMFGDataLibrary.DataAccess.ODACDataAccess.GetItemInfo(scan, rms);
            //if (rms != null)
            //{
            //    SAP_MATL_MASTER SAPInfo = new SAP_MATL_MASTER();
            //    dynamic labelInfo = new ExpandoObject();
            //    if (rms.SAP_MATL_NBR != null)
            //    {
            //        SAPInfo = db.SAP_MATL_MASTER.FirstOrDefault(s => s.SAP_MATL_NBR == rms.SAP_MATL_NBR);
            //        if (SAPInfo != null)
            //        {
            //            labelInfo.ScannedMaterial = SAPInfo.SAP_MATL_NBR + " - " + SAPInfo.SAP_MATL_DESCR.ToString();
            //            labelInfo.SAPMat = SAPInfo.SAP_MATL_DESCR;
            //        }
            //        else
            //        {
            //            labelInfo.SAPMat = string.Empty;
            //            labelInfo.ScannedMaterial = string.Empty;
            //        }

            //    }
            //    else
            //    {
            //        labelInfo.SAPMat = string.Empty;
            //        labelInfo.ScannedMaterial = string.Empty;
            //    }

            //    labelInfo.MatNum = rms.SAP_MATL_NBR.ToString();
            //    labelInfo.AmtRec = rms.SCANNED_AMT.ToString();


            //    string JSONPOL = JsonConvert.SerializeObject(scanSearch, Formatting.None,
            //            new JsonSerializerSettings()
            //            {
            //                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            //            });


            //    string JSONDyn = JsonConvert.SerializeObject(labelInfo);


            //    return Json(JSONDyn, JsonRequestBehavior.AllowGet);
            //}

            return null;
        }

        //public JsonResult ScanSaved(string scan, long PONum, string feeder, string manAmt)
        //{
        //    //Check smartMFG
        //    LabelInfoModel rmModel = _labelProcessor.GetSingleLabelInfo(scan);

        //    if (rmModel != null)
        //    {
                


        //        return Json(new { success = true });
        //    }

        //    ////Check NPPQS - Don't be confused, I know I am calling a new object of what we are saving. Least confusing way of querying NPPQS without having to create NPPQS model. 
        //    //RAW_MATERIAL_STAGING rms = new RAW_MATERIAL_STAGING();
        //    //SmartMFGDataLibrary.DataAccess.ODACDataAccess.GetItemInfo(scan, rms);
        //    //if (rms != null)
        //    //{
        //    //    RAW_MATERIAL_STAGING rawMatStage = new RAW_MATERIAL_STAGING();

        //    //    if (feeder != string.Empty && feeder != null)
        //    //    {
        //    //        rawMatStage.FEEDER_NO = Convert.ToInt32(feeder);
        //    //    }

        //    //    if (manAmt != string.Empty && manAmt != null)
        //    //    {
        //    //        rawMatStage.ENTERED_AMT = Convert.ToInt32(manAmt);
        //    //    }
        //    //    rawMatStage.PO_NBR = PONum;
        //    //    rawMatStage.SAP_MATL_NBR = rms.SAP_MATL_NBR;
        //    //    rawMatStage.SCANNED_AMT = rms.SCANNED_AMT;
        //    //    rawMatStage.SCANNED_BY = User.Identity.GetUserName();//Trim @AscendMaterials.com
        //    //    rawMatStage.SCANNED_DATE_TIME = DateTime.Now;
        //    //    rawMatStage.SCANNED_UOM = "LB"; //I am only asking for LB from NPPQS
        //    //    rawMatStage.LABEL_ID = Convert.ToInt32(scan);

        //    //    db.RAW_MATERIAL_STAGING.Add(rawMatStage);
        //    //    db.SaveChanges();

        //    //    return Json(new { success = true });
        //    //}
        //    return null;

        //}    
      
    }
}
