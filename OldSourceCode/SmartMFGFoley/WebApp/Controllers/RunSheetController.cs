﻿using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace WebApp.Controllers
{
    public class RunSheetController : Controller
    {
        private readonly IProcessOrder _prodSchedule;
        private readonly IRunSheet _runSheet;

        public RunSheetController(IProcessOrder prodSchedule, IRunSheet runSheet)
        {
            _prodSchedule = prodSchedule;
            _runSheet = runSheet;
        }
        public ActionResult Create(long PO)
        {
            //Security            
            //var user = User.Identity.Name.Replace("APM\\", "").ToLower();
            //UserProcessor userProcessor = new UserProcessor();
            //UserRoles userRole = UserProcessor.UserRoles.RunSheet;
            //bool canEdit = userProcessor.UserHasAccess(user, userRole, UserProcessor.UserAccessLevels.CRUD); //Check if run sheet can be edited. 
            //ViewBag.CanEdit = canEdit;

            //var packTypes = new List<SelectListItem>();
            //packTypes.Add(new SelectListItem() { Text = "Silo", Value = "Silo" });
            //packTypes.Add(new SelectListItem() { Text = "Box", Value = "Box" });
            //packTypes.Add(new SelectListItem() { Text = "Pallet", Value = "Pallet" });
            //ViewBag.PackTypes = packTypes;

            //var wgtDefaults = new List<SelectListItem>();
            //wgtDefaults.Add(new SelectListItem() { Text = "1500", Value = "1500" });
            //wgtDefaults.Add(new SelectListItem() { Text = "1600", Value = "1600" });
            //wgtDefaults.Add(new SelectListItem() { Text = "1725", Value = "1725" });
            //wgtDefaults.Add(new SelectListItem() { Text = "2204", Value = "2204" });
            //ViewBag.WgtDefaults = wgtDefaults;

            //var logo = new List<SelectListItem>();
            //logo.Add(new SelectListItem() { Text = "Logo", Value = "Logo" });
            //logo.Add(new SelectListItem() { Text = "No Logo", Value = "No Logo" });
            //ViewBag.Logo = logo;

            //PO_ALTERNATE_BOM alt = db.PO_ALTERNATE_BOM.FirstOrDefault(a => a.PO_NBR == PO);
            bool altCheck = false;
            //if (alt != null)
            //{
            //    altCheck = true;
            //}

            ViewBag.AltBom = _runSheet.GetBOMPOScanned(2, PO, altCheck);

            //ViewBag.SolutionPrep = db.SOLUTIONPREPs.Where(d => d.PO_NBR == PO);

            //ViewBag.Drums = db.DRUMBLE.Where(d => d.PO_NBR == PO);

            //ViewBag.PSM = ProductionScheduleModel.GetPSModel(PO);

            //ViewBag.Blends = db.BLENDs.Where(b => b.PO_NBR == PO);

            //Check for previous runsheet, only one to activate the button.
            ProductionScheduleModel info = _prodSchedule.LoadPSModel(PO);
            ViewBag.PSM = info;
            RunSheetModel runSheetPrev = _runSheet.GetOldRunSheet(info.Material_Number, info.Resource);
            ViewBag.Previous = false;
            if (runSheetPrev != null)
            {
                ViewBag.Previous = true;
            }

            RunSheetModel runCreate = new RunSheetModel();
            runCreate.Feeder1_Percetage = 0;
            runCreate.Feeder2_Percetage = 0;
            runCreate.Feeder3_Percetage = 0;
            runCreate.Feeder4_Percetage = 0;
            runCreate.Feeder5_Percetage = 0;
            runCreate.Feeder6_Percetage = 0;
            runCreate.Feeder7_Percetage = 0;
            runCreate.Master_Rate = 0;

            runCreate.Ext_Nbr = info.Resource;
            runCreate.Material_Num = info.Material_Number;
            runCreate.Po_Nbr = info.Process_Order_Number;

            return View("CreateRunSheet", runCreate);
        }

        public JsonResult SaveMixture(string[] arrLot, string[] arrWgt)
        {
            return null;
        }
    }
}
