﻿namespace WebApp.Common
{
    public static class AppSettings
    {       
        public static string GetPrintingAPIkey()
        {           
            return SettingHelper.AppSetting("APISettings","PrintingApiKey");
        }

        public static string GetSAPApiUrl()
        {           
            return SettingHelper.AppSetting("APISettings","SAPApiUrl");
        }

        public static string GetLimsUserName()
        {
            return SettingHelper.AppSetting("Lims", "User");
        }

        public static string GetLimsPw()
        {
            return SettingHelper.AppSetting("Lims", "Password");
        }

        public static string GetLimsEndpointAddresse()
        {
            return SettingHelper.AppSetting("Lims", "EndpointAddress");
        }

        public static string GetLabelIdSeed()
        {
            return SettingHelper.AppSetting("Labels", "Seed");
        }

        public static string GetPackagingMaterialIdSeed()
        {
            return SettingHelper.AppSetting("PackagingMaterialId", "Seed");
        }
        public static string GetPackagingMaterialValue()
        {
            return SettingHelper.AppSetting("PackagingMaterial", "Value");
        }
        public static string GetWM_VALUE()
        {
            return SettingHelper.AppSetting("APISettings", "WM_VALUE");
        }
        public static string GetWM_VALUE_BL()
        {
            return SettingHelper.AppSetting("APISettings", "WM_VALUE_BL");
        }
        

        public static string GetWM_VALUE_FG()
        {
            return SettingHelper.AppSetting("APISettings", "WM_VALUE_FG");
        }

        public static string GetWM_STG_TYPE()
        {
            return SettingHelper.AppSetting("APISettings", "WM_STG_TYPE");
        }
        
        public static string IM_PO_STG_TYPE()
        {
            return SettingHelper.AppSetting("APISettings", "IM_PO_MOVE_TYPE");
        }
        public static string GetDEST_WM_STG_TYPE()
        {
            return SettingHelper.AppSetting("APISettings", "DEST_WM_STG_TYPE");
        }
        public static string GetWM_STG_SECTION()
        {
            return SettingHelper.AppSetting("APISettings", "WM_STG_SECTION");
        }
        public static string GetWM_STG_UNIT_TYPE()
        {
            return SettingHelper.AppSetting("APISettings", "WM_STG_UNIT_TYPE");
        }
        public static string GetIM_STG_TYPE()
        {
            return SettingHelper.AppSetting("APISettings", "IM_STG_TYPE");
        }
        public static string GetWM_STG_BIN()
        {
            return SettingHelper.AppSetting("APISettings", "WM_STG_BIN");
        }
        public static string GetPlant()
        {
            return SettingHelper.AppSetting("APISettings", "PLANT");
        }
        public static string GetMOVE_TYPE()
        {
            return SettingHelper.AppSetting("APISettings", "MOVE_TYPE");
        }

        public static string GetSTOUrl()
        {
            return SettingHelper.AppSetting("APISettings", "STOUrl");
        }
        public static string GetSTO_MOVE_TYPE()
        {
            return SettingHelper.AppSetting("APISettings", "STO_MOVE_TYPE");
        }

        public static string GetWM_STG_TYPE_FG()
        {
            return SettingHelper.AppSetting("APISettings", "WM_STG_TYPE_FG");
        }

    }
}
