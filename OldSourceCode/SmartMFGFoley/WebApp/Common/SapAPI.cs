﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WebApp.Common
{
    public static class SapAPI
    {
        public static async Task<string> SendRawMaterialsToSAP(JObject input, bool STO)
        {
            string resultContent = string.Empty;
            try
            {
                string apiUrl = STO ? AppSettings.GetSTOUrl() : AppSettings.GetSAPApiUrl();
                string partial = STO ? "Report/RAW/INT" : "Report/SAP";
                using (var client = new HttpClient())
                {
                    var httpContent = new StringContent(input.ToString());
                    httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    client.BaseAddress = new Uri(apiUrl);
                    var response = await client.PostAsync(partial, httpContent).ConfigureAwait(false);
                    resultContent = await response.Content.ReadAsStringAsync();
                    return resultContent;
                }
            }
            catch (Exception ex)
            {
                throw (ex);                
            }
        }
    }
}
