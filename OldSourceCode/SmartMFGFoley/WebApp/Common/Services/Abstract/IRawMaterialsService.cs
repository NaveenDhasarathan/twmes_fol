﻿using WebApp.Models.Services;
using WebApp.Models.Warehouse;

namespace WebApp.Common.Services.Abstract
{
    public interface IRawMaterialsService
    {
        StorageUnitSearchResultModel SearchForStorageUnit(StorageUnitSearchRequestModel request);
        bool SearchForBatchID(string BatchID);

        StorageUnitModel SearchSmartMfgForStorageUnit(string storageUnitNumber);

        StorageUnitModel SearchExternalForStorageUnit(string storageUnitNumber);
        bool SearchExternalForBatchID(string BatchID);

        StorageUnitModel SearchNppqsForStorageUnit(string storageUnitNumber);
        StorageUnitModel SearchNppqsForBatchID(string BatchID);

        StorageUnitModel SearchShopFloorForStorageUnit(string storageUnitNumber);
    }
}
