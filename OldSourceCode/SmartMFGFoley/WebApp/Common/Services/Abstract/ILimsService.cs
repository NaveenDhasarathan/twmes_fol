﻿using DataLibrary.Models;

namespace WebApp.Common.Services.Abstract
{
    public interface ILimsService
    {
        bool CreateSUZRawMaterialSample(BatchLabelModel batchModel);
    }
}
