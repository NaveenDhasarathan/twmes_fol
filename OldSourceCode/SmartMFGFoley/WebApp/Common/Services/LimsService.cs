﻿using DataLibrary.Models;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Xml;
using System.Xml.Linq;
//using Thermo.SampleManager.WCFService;
using Thermo.SM.LIMSML.Helper.High;
using Thermo.SM.LIMSML.Helper.Low;
using WebApp.Common.Services.Abstract;
using Action = Thermo.SM.LIMSML.Helper.Low.Action;

namespace WebApp.Common.Services
{
    public class LimsService : ILimsService
    {
        public bool CreateSUZRawMaterialSample(BatchLabelModel batchModel)
        {
            // User userProfile = _authService.GetUserProfileAsync().Result;

            //Setup call to SampleManager web service
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(AppSettings.GetLimsEndpointAddresse());
            ILIMSWebService client = new ChannelFactory<ILIMSWebService>(basicHttpBinding, endpointAddress).CreateChannel();
            RichDocument limsml = new RichDocument();

            //SampleManager login
            limsml.AddUserLogin(AppSettings.GetLimsUserName(), AppSettings.GetLimsPw());

            //Set the response type
            limsml.SetResponseType(ResponseType.Data);

            //Create a new transaction 
            Transaction transaction = limsml.AddTransaction();
            Entity entity = transaction.AddEntity("GENERIC");

            //Name of the Sample Manager Web Srvice Method
            Action action = entity.AddAction("CREATE_BATCH");

            //Web Srvice Method properties         
            action.AddParameter("BATCH_ID", batchModel.Batch_Id);
            action.AddParameter("MATERIAL_ID", batchModel.Matl_Nbr);
            action.AddParameter("PLANT_ID", AppSettings.GetPlant());
            action.AddParameter("PO", batchModel.Document_Nbr.ToString());
            action.AddParameter("ASSET", "NA");

            //Process and recieve response
            string res = client.Process(limsml.GetXml());
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(res);
            var nodes = xmlDoc.GetElementsByTagName("errors");
            bool blnError = false;
            for (int i = 0; i < nodes.Count; i++)
            {
                if (!string.IsNullOrEmpty(nodes[i].InnerXml))
                {
                    blnError = true;
                    break;
                }
            }
            
            if (!blnError) {     
            //Update        
            ILIMSWebService clientUpdate = new ChannelFactory<ILIMSWebService>(basicHttpBinding, endpointAddress).CreateChannel();
            RichDocument limsmlUpdate = new RichDocument();

            //SampleManager login
            limsmlUpdate.AddUserLogin(AppSettings.GetLimsUserName(), AppSettings.GetLimsPw());

            //Set the response type
            limsmlUpdate.SetResponseType(ResponseType.Data);
            Transaction transUpdate = limsmlUpdate.AddTransaction();
            Entity entityUpdate = transUpdate.AddEntity("GENERIC");

            //Name of the Sample Manager Web Srvice Method
            Action actionUpdate = entityUpdate.AddAction("UPDATE_BATCH_STATUS");

            //Web Srvice Method properties         
            actionUpdate.AddParameter("BATCH_ID", batchModel.Batch_Id);
            actionUpdate.AddParameter("STATUS", "Complete");           

            //Process and recieve response
            string resUpdate = clientUpdate.Process(limsmlUpdate.GetXml());

            XmlDocument xmlDocUpdate = new XmlDocument();
            xmlDocUpdate.LoadXml(resUpdate);
            var nodesUpdate = xmlDocUpdate.GetElementsByTagName("errors");
            
            for (int i = 0; i < nodesUpdate.Count; i++)
                {
                    if (!string.IsNullOrEmpty(nodes[i].InnerXml))
                    {
                        blnError = true;
                        break;
                    }
                }
            }
            return blnError;
        }

       
    }
}
