﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WebApp.Common
{
   
    public static class ZebraAPI
    {       
        public static async Task<HttpStatusCode> SendLabelPrintingRequest(JObject input)
        {
            using (var client = new HttpClient())
            {
                var httpContent = new StringContent(input.ToString());
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                client.BaseAddress = new Uri("http://zplprinting/");
                client.DefaultRequestHeaders.Add("ApiKey", AppSettings.GetPrintingAPIkey());
                ////Can get a response of zpl and use viewer to see the label
                ////var response = await client.PostAsync("api/Label/Generate/zpl", httpContent).ConfigureAwait(false);
                //Comment out to avoid sending post to the printer
                var response = await client.PostAsync("api/Label/Print", httpContent).ConfigureAwait(false);
                var resultContent = await response.Content.ReadAsStringAsync();
                return response.StatusCode;

            }
        }
    }
}
