#pragma checksum "D:\Workspace\TWMES\twmes_fol\OldSourceCode\SmartMFGFoley\WebApp\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "53e7f86db598d075323b175b740ec1e799aae45b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Workspace\TWMES\twmes_fol\OldSourceCode\SmartMFGFoley\WebApp\Views\_ViewImports.cshtml"
using SmartMFG_SUZ;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Workspace\TWMES\twmes_fol\OldSourceCode\SmartMFGFoley\WebApp\Views\_ViewImports.cshtml"
using SmartMFG_SUZ.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"53e7f86db598d075323b175b740ec1e799aae45b", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ce074e23a9dcdc29823732dc0a0fc1631753b954", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "D:\Workspace\TWMES\twmes_fol\OldSourceCode\SmartMFGFoley\WebApp\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Where would you like to go?";

#line default
#line hidden
#nullable disable
            WriteLiteral("<br />\r\n<br />\r\n<div class=\"apm-landing\">\r\n    <div class=\"apm-card-blue\"");
            BeginWriteAttribute("onclick", " onclick=\"", 136, "\"", 212, 4);
            WriteAttributeValue("", 146, "location.href=\'", 146, 15, true);
#nullable restore
#line 7 "D:\Workspace\TWMES\twmes_fol\OldSourceCode\SmartMFGFoley\WebApp\Views\Home\Index.cshtml"
WriteAttributeValue("", 161, Url.Action("Index", "ProcessOrder"), 161, 36, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 197, "\';return", 197, 8, true);
            WriteAttributeValue(" ", 205, "false;", 206, 7, true);
            EndWriteAttribute();
            WriteLiteral(@">
        <h6><strong>Production</strong></h6>
        <p>View production information including production rates and amounts, Process Order Production and Raw Material Consumption tables, and byproduct data or transition a process order. </p>
        <br />
        <button class=""btn btn-block""");
            BeginWriteAttribute("onclick", " onclick=\"", 512, "\"", 588, 4);
            WriteAttributeValue("", 522, "location.href=\'", 522, 15, true);
#nullable restore
#line 11 "D:\Workspace\TWMES\twmes_fol\OldSourceCode\SmartMFGFoley\WebApp\Views\Home\Index.cshtml"
WriteAttributeValue("", 537, Url.Action("Index", "ProcessOrder"), 537, 36, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 573, "\';return", 573, 8, true);
            WriteAttributeValue(" ", 581, "false;", 582, 7, true);
            EndWriteAttribute();
            WriteLiteral(@">Continue</button><br />Under construction
    </div>
    <div class=""apm-card-orange"">
        <h6>Raw Materials</h6>
        <p style=""margin-bottom: 0rem;"">View incoming Raw Material Receiving information including PO, shipment, and delivery numbers, expected delivery dates, supplier details, and number of units and quantity to be received at Ascend Performance Materials.</p>
       
        <br />
        <button class=""btn btn-block""");
            BeginWriteAttribute("onclick", " onclick=\"", 1038, "\"", 1119, 4);
            WriteAttributeValue("", 1048, "location.href=\'", 1048, 15, true);
#nullable restore
#line 18 "D:\Workspace\TWMES\twmes_fol\OldSourceCode\SmartMFGFoley\WebApp\Views\Home\Index.cshtml"
WriteAttributeValue("", 1063, Url.Action("STO", "InboundRawMaterials"), 1063, 41, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1104, "\';return", 1104, 8, true);
            WriteAttributeValue(" ", 1112, "false;", 1113, 7, true);
            EndWriteAttribute();
            WriteLiteral(">Go to STO!</button>\r\n        <button class=\"btn btn-block\"");
            BeginWriteAttribute("onclick", " onclick=\"", 1179, "\"", 1248, 3);
            WriteAttributeValue("", 1189, "location.href=\'", 1189, 15, true);
#nullable restore
#line 19 "D:\Workspace\TWMES\twmes_fol\OldSourceCode\SmartMFGFoley\WebApp\Views\Home\Index.cshtml"
WriteAttributeValue("", 1204, Url.Action("Index", "InboundRawMaterials"), 1204, 43, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1247, "\'", 1247, 1, true);
            EndWriteAttribute();
            WriteLiteral(">Go to Purchase Order!</button>\r\n    </div>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
