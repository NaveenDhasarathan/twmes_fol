﻿namespace WebApp.Models.Services
{
    public class StorageUnitSearchResultModel
    {
        public Warehouse.StorageUnitModel StorageUnit { get; set; }

        public bool InSmartMfg { get; set; }

        public bool Success { get; set; }

        public string ErrorMessage { get; set; }
    }
}
