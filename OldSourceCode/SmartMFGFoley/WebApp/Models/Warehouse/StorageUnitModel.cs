﻿namespace WebApp.Models.Warehouse
{
    public class StorageUnitModel
    {
        public string UnitNumber { get; set; }

        public string MaterialId { get; set; }

        public string BatchId { get; set; }

        public decimal NetQuantity { get; set; }

        public string UnitOfMeasure { get; set; }
    }
}
