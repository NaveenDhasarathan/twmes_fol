import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './modules/core/pages/page-not-found/page-not-found.component';
import { CustomPreloadingStrategyService } from './modules/core/services/preload.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import(`./modules/core/core.module`).then(m => m.CoreModule),
    data: {
      preload: true
    }
  },
  {
    path: 'admin',
    loadChildren: () => import(`./modules/admin/admin.module`).then(m => m.AdminModule)
  },
  {
    path: 'packout',
    loadChildren: () => import(`./modules/packout/packout.module`).then(m => m.PackoutModule)
  },
  {
    path: 'scanner',
    loadChildren: () => import(`./modules/scanner/scanner.module`).then(m => m.ScannerModule)
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

const isIframe = window !== window.parent && !window.opener;

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: false,
    // Don't perform initial navigation in iframes
    initialNavigation: !isIframe ? 'enabled' : 'disabled',
    preloadingStrategy: CustomPreloadingStrategyService
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
