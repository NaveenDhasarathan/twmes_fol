export const environment = {
  production: true,
  "resources": {
    "SmartMFG_Packout":{
      "resourceUri": "."
    }
  },
  scanning:{
    "scannerPrefix": "",
    "serialIdPreFix": "4S",
    "containerIdPrefix": "9B"
  },
  labels:{
    "shuttleLabel": "Shuttle"
  },
  typeAheadResultLength: 10,
  "plant": 1720
};
