// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  "resources": {
    "SmartMFG_Packout":{
      "resourceUri": "."
    }
  },
  scanning:{
    "scannerPrefix": "",
    "serialIdPreFix": "4S",
    "containerIdPrefix": "9B"
  },
  labels:{
    "shuttleLabel": "Shuttle"
  },
  typeAheadResultLength: 10,
  "plant": 1720
};
