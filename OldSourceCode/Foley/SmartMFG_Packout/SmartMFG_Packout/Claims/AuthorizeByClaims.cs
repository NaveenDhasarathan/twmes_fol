﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout
{

    public class AuthorizeByClaim : AuthorizeAttribute, IAuthorizationFilter
    {
        private string[] Claims { get; }

        public AuthorizeByClaim(params string[] claims)
        {
            Claims = claims;
        }
                
        public void OnAuthorization(AuthorizationFilterContext context)
        {

            foreach (string claim in Claims) {

                var foundClaim = context.HttpContext.User.Claims.First(c => c.Type == claim);

                if (foundClaim == null || foundClaim.Value == "False")
                {
                    context.Result = new UnauthorizedResult();
                }
            }

        }

    }
}
