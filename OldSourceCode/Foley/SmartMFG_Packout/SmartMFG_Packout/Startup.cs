using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using SmartMFG_Packout.Models.Auth;
using SmartMFG_Packout.Repositories;
using SmartMFG_Packout.Repositories.Abstract;
using SmartMFG_Packout.Services;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Text;

namespace SmartMFG_Packout
{
    public class Startup
    {
        private SymmetricSecurityKey _signingKey;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            string secret = Configuration.GetValue<string>("JwtIssuerOptions:ApiTokenSecret");
            _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secret)) ;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IConfiguration>(Configuration);

            //JWT Setup
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = Configuration.GetValue<string>("JwtIssuerOptions:Issuer"),
                ValidAudience = Configuration.GetValue<string>("JwtIssuerOptions:Audience"),
                IssuerSigningKey = _signingKey,
                ClockSkew = TimeSpan.Zero
            };

            // Adding JWT Authentication  
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            // Adding Jwt Bearer  
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = tokenValidationParameters;
            });

            //Services
            services.AddSingleton<IJwtService, JwtService>();
            services.AddSingleton<IAuthService, AuthService>();
            services.AddTransient<IProcessOrderService, ProcessOrderService>();
            services.AddTransient<IBatchMasterService, BatchMasterService>();
            services.AddTransient<IBatchLabelService, BatchLabelService>();
            services.AddTransient<IPackoutService, PackoutService>();
            services.AddTransient<IPrintService, PrintService>();
            services.AddTransient<ILIMSService, LIMSService>();
            services.AddTransient<IScannerService, ScannerService>();
            services.AddTransient<IAdminPrinterService, AdminPrinterService>();
            services.AddTransient<IAdminUserService, AdminUserService>();
            services.AddTransient<IAdminMaterialService, AdminMaterialService>();

            //Repositories
            services.AddTransient<IAuthRepository, AuthRepository>();
            services.AddTransient<IProcessOrderRepository, ProcessOrderRepository>();
            services.AddTransient<IBatchMasterRepository, BatchMasterRepository>();
            services.AddTransient<IBatchLabelRepository, BatchLabelRepository>();
            services.AddTransient<IPackoutRepository, PackoutRepository>();
            services.AddTransient<IScannerRepository, ScannerRepository>();
            services.AddTransient<IPackoutRepository, PackoutRepository>();
            services.AddTransient<IAdminPrinterRepository, AdminPrinterRepository>();
            services.AddTransient<IAdminPrinterRepository, AdminPrinterRepository>();
            services.AddTransient<IAdminUserRepository, AdminUserRepository>();
            services.AddTransient<IAdminMaterialRepository, AdminMaterialRepository>();

            //Controllers
            services.AddControllers().AddNewtonsoftJson();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(swagger =>
            {
                //This is to generate the Default UI of Swagger Documentation    
                swagger.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Pensacola SmartMFG API",
                    Description = "Pensacola SmartMFG Swagger"
                });
                // To Enable authorization using Swagger (JWT)    
                swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Enter 'Bearer' [space] and then your valid token in the text input below.\r\n\r\nExample: \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\"",
                });
                swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            new string[] {}

                    }
                });
            });

            // In production, the Angular files will be served from this director
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseSpaStaticFiles(new StaticFileOptions
                {
                    OnPrepareResponse = ctx =>
                    {
                        ctx.Context.Response.Headers.Add("Cache-Control", "max-age=3000, must-revalidate");
                    }
                });

                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Load the index.html page when the url is invoked without the ending '/'
            // Avoids problems with Angular when loading http://domain/AngularWebApp instead of http://domain/AngularWebApp/
            app.UseRewriter(new RewriteOptions().Add(rewriteContext =>
            {
                var request = rewriteContext.HttpContext.Request;
                if (request.Path == PathString.Empty)
                {
                    rewriteContext.HttpContext.Response.Redirect(request.PathBase + '/', true);
                    rewriteContext.Result = RuleResult.EndResponse;
                }
            }));

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pensacola SmartMFG API v1");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
