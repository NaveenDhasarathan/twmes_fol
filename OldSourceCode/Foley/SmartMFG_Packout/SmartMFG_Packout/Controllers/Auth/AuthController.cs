﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SmartMFG_Packout.Models.Auth;
using SmartMFG_Packout.Services;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _UserService;
        private readonly IJwtService _JwtService;

        public AuthController(IAuthService UserService, IJwtService jwtService)
        {
            _UserService = UserService;
            _JwtService = jwtService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Post([FromBody] Credential credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid client request");
            }

            credentials.UserName = _UserService.NormalizeUser(credentials.UserName);

            var identity = await _UserService.GetClaimsIdentity(credentials.UserName, credentials.Password);
            if (identity == null)
            {
                return BadRequest(ModelState.TryAddModelError("login_failure", "Invalid username or password."));
            }

            var jwt = await _JwtService.GenerateJwt(identity, credentials.UserName);
            
            
            return Ok(jwt);
        }

        [HttpPost]
        [Route("refresh")]
        public async Task<IActionResult> Refresh([FromBody] RefreshTokenDto tokens)
        {
            if (tokens == null)
            {
                return BadRequest("Invalid client request");
            }

            var jwt = await _JwtService.GenerateRefreshedJwt(tokens);

            return Ok(jwt);
        }

        [HttpPost]
        [Route("revoke")]
        public async Task<IActionResult> Revoke()
        {
           await _UserService.RevokeRefreshToken();
           return NoContent();
        }

        [AuthorizeByClaim("IsAuthorized")]
        [Route("profile")]
        [HttpGet]
        public async Task<IActionResult> GetUserProfileAsync()
        {
            var result = await _UserService.GetUserProfileAsync();
            return Ok(result);
        }

    }
}
