﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Scanner;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Controllers
{
    [AuthorizeByClaim("IsAuthorized")]
    [Route("api/[controller]")]
    [ApiController]
    public class ScannerController : ControllerBase
    {
        private readonly IScannerService _ScannerService;
        public ScannerController(IScannerService ScannerService)
        {
            _ScannerService = ScannerService;
        }

        //SHUTTLE
        [Route("shuttle/active")]
        [HttpGet]
        public async Task<IActionResult> GetActiveShuttlesAsync([FromQuery] string? location = null, [FromQuery] string? area = null)
        {
            var result = await _ScannerService.GetActiveShuttlesAsync(location, area);
            return Ok(result);
        }

        [Route("shuttle/active")]
        [HttpPost]
        public async Task<IActionResult> CreateActiveShuttleAsync([FromBody] ShuttleCreateDto shuttleDto)
        {
            try
            {
                var result = await _ScannerService.CreateActiveShuttleAsync(shuttleDto);
                return Ok(result);
            }
            catch(Exception e) { 
                return StatusCode(500, new { message = e.Message });
            }
            
        }


        //SHUTTLE CONTENTS
        [Route("shuttle/content")]
        [HttpGet]
        public async Task<IActionResult> GetShuttleContentAsync([FromQuery] int shuttleId )
        {
            var result = await _ScannerService.GetShuttleContentAsync(shuttleId);
            return Ok(result);
        }

        [Route("shuttle/content")]
        [HttpPost]
        public async Task<IActionResult> AddContentToShuttle([FromQuery] int shuttleId, [FromQuery] string plant, [FromQuery]  string serialId, [FromQuery]string loader, [FromQuery] string user, [FromQuery] string blockedReasonText)
        {
            var result = await _ScannerService.AddContentToShuttle(shuttleId, plant, serialId, loader, user, blockedReasonText);
            return Ok(result);
        }

        [Route("shuttle/content")]
        [HttpDelete]
        public async Task<IActionResult> RemoveContentFromShuttle([FromQuery] int shuttleId, [FromQuery] string serialId, [FromQuery] string remover)
        {
            var result = await _ScannerService.RemoveContentFromShuttle(shuttleId, serialId, remover);
            return Ok(result);
        }

        //SHUTTLE FINALIZE
        [Route("shuttle/finalize")]
        [HttpGet]
        public async Task<IActionResult> FinalizeShuttleAsync([FromQuery] int shuttleId)
        {
            var result = await _ScannerService.FinalizeShuttleAsync(shuttleId);
            return Ok(result);
        }

        //SHUTTLE WAREHOUSES
        [Route("shuttle/warehouses")]
        [HttpGet]
        public async Task<IActionResult> GetExternalWhsAsync([FromQuery] string localPlant)
        {
            var result = await _ScannerService.GetExternalWhsAsync(localPlant);
            return Ok(result);
        }

        //SHUTTLE SEA CONTAINERS
        [Route("shuttle/seacontainers")]
        [HttpGet]
        public async Task<IActionResult> GetSeaContainerTypesAsync()
        {
            var result = await _ScannerService.GetSeaContainerTypesAsync();
            return Ok(result);
        }

        //PRINT SHUTTLE LABEL
        [Route("shuttle/label")]
        [HttpPost]
        public async Task<IActionResult> PrintShuttleLabelAsync([FromBody] PrintDTO printDto)
        {
            var result = await _ScannerService.PrintShuttleLabelAsync(printDto);
            return Ok(result);
        }

        //PRINT PICK LIST
        [Route("shuttle/report")]
        [HttpGet]
        public async Task<IActionResult> PrintShuttleReportAsync([FromQuery] string PrinterIp, [FromQuery] string PrinterName, [FromQuery] int ShuttleID)
        {
            var result = await _ScannerService.PrintShuttleReportAsync(PrinterIp, PrinterName, ShuttleID);
            return Ok(result);
        }

        //SHUTTLE AREAS
        [Route("shuttle/areas")]
        [HttpGet]
        public async Task<IActionResult> GetShuttleAreasAsync([FromQuery] string plant)
        {
            var result = await _ScannerService.GetShuttleAreasAsync(plant);
            return Ok(result);
        }

        //SHUTTLE AREAS
        [Route("shuttle/containers")]
        [HttpGet]
        public async Task<IActionResult> GetShuttleContainersAsync()
        {
            var result = await _ScannerService.GetShuttleContainersAsync();
            return Ok(result);
        }

        //RTP ISSUE
        [Route("rtp/issue")]
        [HttpGet]
        public async Task<IActionResult> ProcessRtpIssueAsync([FromQuery] int serialId, [FromQuery] string username)
        {
            var result = await _ScannerService.ProcessRtpIssueAsync(serialId, username);
            return Ok(result);
        }

        //RTP CANCEL
        [Route("rtp/cancel")]
        [HttpGet]
        public async Task<IActionResult> ProcessRtpCancelAsync([FromQuery] int serialId, [FromQuery] string username)
        {
            var result = await _ScannerService.ProcessRtpCancelAsync(serialId, username);
            return Ok(result);
        }

        //RTP HISTORY
        [Route("rtp/history")]
        [HttpGet]
        public async Task<IActionResult> GetRtpHistoryAsync([FromQuery] string scannerId, [FromQuery] string rtpAction)
        {
            var result = await _ScannerService.GetRtpHistoryAsync(scannerId, rtpAction);
            return Ok(result);
        }

        //Re-Package
        [Route("repackage")]
        [HttpPost]
        public async Task<IActionResult> ProcessRepackageAsync([FromQuery] int serialId, [FromQuery] int plant,  [FromQuery] int po, [FromQuery] string batch, [FromQuery] string username)
        {
            var result = await _ScannerService.ProcessRepackageAsync(serialId, plant, po, batch, username);
            return Ok(result);
        }

        //Re-Package HISTORY
        [Route("repackage/history")]
        [HttpGet]
        public async Task<IActionResult> GetRepackageHistoryAsync([FromQuery] int po,  [FromQuery] string batch)
        {
            var result = await _ScannerService.GetRepackageHistoryAsync(po, batch);
            return Ok(result);
        }

        //Re-Package serial id is valid
        [Route("repackage/serialId")]
        [HttpGet]
        public async Task<IActionResult> CheckSerialIdIsValidRepackageAsync([FromQuery] int serialId)
        {
            var result = await _ScannerService.CheckSerialIdIsValidRepackageAsync(serialId);
            return Ok(result);
        }
    }
}
