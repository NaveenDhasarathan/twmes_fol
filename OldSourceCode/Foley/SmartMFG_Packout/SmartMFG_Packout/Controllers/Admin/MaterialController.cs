﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Admin;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartMFG_Packout.Controllers
{
    [AuthorizeByClaim("IsAuthorized")]
    [Route("api/Admin/[controller]")]
    [ApiController]
    public class MaterialController : ControllerBase
    {
        private readonly IAdminMaterialService _MaterialService;

        public MaterialController(IAdminMaterialService MaterialService)
        {
            _MaterialService = MaterialService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetMaterialsAsync()
        {
            var result = await _MaterialService.GetMaterialsAsync();
            return Ok(result);
        }

        [HttpGet("{materialId}")]
        public async Task<IActionResult> GetMaterialAsync([FromRoute] int materialId)
        {
            var result = await _MaterialService.GetMaterialAsync(materialId);
            return Ok(result);
        }

        [HttpPost("")]
        public async Task<IActionResult> InsertMaterialAsync([FromBody] MaterialDto materialDto)
        {
            var result = await _MaterialService.InsertMaterialAsync(materialDto);
            return Ok(result);
        }

        [HttpPut("")]
        public async Task<IActionResult> UpdateMaterialAsync([FromBody] Material material)
        {
            var result = await _MaterialService.UpdateMaterialAsync(material);
            return Ok(result);
        }

        [HttpGet("{materialId}/location")]
        public async Task<IActionResult> GetMaterialLocationsAsync([FromRoute] int materialId)
        {
            var result = await _MaterialService.GetMaterialLocationsAsync(materialId);
            return Ok(result);
        }

        [HttpPost("location")]
        public async Task<IActionResult> InsertMaterialLocationsAsync([FromBody] List<MaterialLocation> materialLocations)
        {
            var result = await _MaterialService.InsertMaterialLocationsAsync(materialLocations);
            return Ok(result);
        }

        [HttpDelete("location")]
        public async Task<IActionResult> DeleteMaterialLocationsAsync([FromBody] List<MaterialLocation> materialLocations)
        {
            var result = await _MaterialService.DeleteMaterialLocationsAsync(materialLocations);
            return Ok(result);
        }

        [HttpGet("label/template")]
        public async Task<IActionResult> GetMaterialLabelTemplatesAsync()
        {
            var result = await _MaterialService.GetMaterialLabelTemplatesAsync();
            return Ok(result);
        }

        [HttpGet("production/unit")]
        public async Task<IActionResult> GetMaterialProductionUnitAsync()
        {
            var result = await _MaterialService.GetMaterialProductionUnitsAsync();
            return Ok(result);
        }

        [HttpGet("container/type")]
        public async Task<IActionResult> GetContainerTypesAsync()
        {
            var result = await _MaterialService.GetContainerTypesAsync();
            return Ok(result);
        }

    }
}
