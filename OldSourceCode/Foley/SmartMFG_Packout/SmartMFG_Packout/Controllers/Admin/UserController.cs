﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartMFG_Packout.Controllers
{
    [AuthorizeByClaim("IsAuthorized")]
    [Route("api/Admin/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IAdminUserService _UserService;

        public UserController(IAdminUserService UserService)
        {
            _UserService = UserService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetUsersAsync()
        {
            var result = await _UserService.GetUsersAsync();
            return Ok(result);
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserAsync([FromRoute] string userId)
        {
            var result = await _UserService.GetUserAsync(userId);
            return Ok(result);
        }

        [HttpPut("")]
        public async Task<IActionResult> UpdateUserAsync([FromBody] UserProfile user)
        {
            var result = await _UserService.UpdateUserAsync(user);
            return Ok(result);
        }

        [HttpPost("")]
        public async Task<IActionResult> InsertUsersAsync([FromBody] UserProfile user)
        {
            try
            {
                var result = await _UserService.InsertUsersAsync(user);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpDelete("")]
        public async Task<IActionResult> DeleteUsersAsync([FromQuery] string username)
        {
            var result = await _UserService.DeleteUsersAsync(username);
            return Ok(result);
        }

        [HttpGet("claims")]
        public async Task<IActionResult> GetUserClaimsAsync([FromQuery] string plantId)
        {
            var result = await _UserService.GetClaimsAsync(plantId);
            return Ok(result);
        }

        [HttpGet("roles")]
        public async Task<IActionResult> GetUserRolesAsync([FromQuery] string plantId)
        {
            var result = await _UserService.GetUserRolesAsync(plantId);
            return Ok(result);
        }

        [HttpGet("role/claims")]
        public async Task<IActionResult> GetUserRoleClaimsAsync()
        {
            var result = await _UserService.GetUserRoleClaimsAsync();
            return Ok(result);
        }
    }
}
