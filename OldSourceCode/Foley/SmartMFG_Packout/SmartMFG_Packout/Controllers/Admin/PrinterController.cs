﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartMFG_Packout.Controllers
{
    [AuthorizeByClaim("IsAuthorized")]
    [Route("api/Admin/[controller]")]
    [ApiController]
    public class PrinterController : ControllerBase
    {
        private readonly IAdminPrinterService _PrinterService;

        public PrinterController(IAdminPrinterService PrinterService)
        {
            _PrinterService = PrinterService;
        }

        [HttpGet("Printer")]
        public async Task<IActionResult> GetPrintersAsync([FromQuery] int ?areaSeq = null)
        {
            var result = await _PrinterService.GetPrintersAsync(areaSeq);
            return Ok(result);
        }

        [HttpPut("Printer")]
        public async Task<IActionResult> UpdatePrintersAsync([FromBody] Printer Info)
        {
            var result = await _PrinterService.UpdatePrintersAsync(Info);
            return Ok(result);
        }

        [HttpPost("Printer")]
        public async Task<IActionResult> InsertPrintersAsync([FromBody] Printer Info)
        {
            var result = await _PrinterService.InsertPrintersAsync(Info);
            return Ok(result);
        }

        [HttpDelete("Printer")]
        public async Task<IActionResult> DeletePrintersAsync([FromQuery] string api_Printer_Name)
        {
            var result = await _PrinterService.DeletePrintersAsync(api_Printer_Name);
            return Ok(result);
        }
    }
}
