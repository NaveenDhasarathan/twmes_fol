﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Controllers
{
    [AuthorizeByClaim("IsAuthorized")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessOrderController : ControllerBase
    {
        private readonly IProcessOrderService _ProcessOrderService;
        public ProcessOrderController(IProcessOrderService ProcessOrderService)
        {
            _ProcessOrderService = ProcessOrderService;
        }

        [Route("list")]
        [HttpPost]
        public async Task<IActionResult> GetProcessOrderListAsync([FromBody] ProcessorOrderFilter filter)
        {
            var result = await _ProcessOrderService.GetProcessOrderListAsync(filter);
            return Ok(result);
        }

        [Route("{po}")]
        [HttpGet]
        public async Task<IActionResult> GetProcessOrderAsync([FromRoute] int po)
        {
            var result = await _ProcessOrderService.GetProcessOrderAsync(po);
            return Ok(result);
        }

        [HttpPost("status")]
        public async Task<IActionResult> UpdateProcessOrderStatusAsync([FromQuery] int processOrderId, [FromQuery] string assetName, [FromQuery] string username, [FromQuery] int newStatus)
        {
            var result = await _ProcessOrderService.UpdateProcessOrderStatusAsync(processOrderId, assetName, username, newStatus);
            return Ok(result);
        }

    }
}
