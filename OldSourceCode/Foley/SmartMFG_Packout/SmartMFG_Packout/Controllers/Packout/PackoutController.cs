﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Controllers
{
    [AuthorizeByClaim("IsAuthorized")]
    [Route("api/[controller]")]
    [ApiController]
    public class PackoutController : ControllerBase
    {
        private readonly IPackoutService _PackoutService;

        public PackoutController(IPackoutService UserService)
        {
            _PackoutService = UserService;
        }
        
        //BATCH MODES
        [Route("batch/modes")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutBatchModesAsync()
        {
            var result = await _PackoutService.GetPackoutBatchModesAsync();
            return Ok(result);
        }

        //AREAS
        [Route("areas")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutAreasAsync([FromQuery] int plant)
        {
            var result = await _PackoutService.GetPackoutAreasAsync(plant);
            return Ok(result);
        }

        //STATIONS
        [Route("stations")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutStationsAsync([FromQuery] int areaSeq)
        {
            var result = await _PackoutService.GetPackoutStationsAsync(areaSeq);
            return Ok(result);
        }

        //SOURCES
        [Route("sources")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutSourcesAsync([FromQuery] int areaSeq, [FromQuery] int stationSeq)
        {
            var result = await _PackoutService.GetPackoutSourcesAsync(areaSeq, stationSeq);
            return Ok(result);
        }

        //LOCATIONS
        [Route("locations")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutLocationsAsync()
        {
            var result = await _PackoutService.GetLocationsAsync();
            return Ok(result);
        }

        //PWASTE AREAS
        [Route("pwaste/areas")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutWasteAreasAsync([FromQuery] int plant)
        {
            var result = await _PackoutService.GetPackoutWasteAreasAsync(plant);
            return Ok(result);
        }

        //PWASTE SOURCES
        [Route("pwaste/sources")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutWasteSourcesAsync([FromQuery] int areaSeq)
        {
            var result = await _PackoutService.GetPackoutWasteSourcesAsync(areaSeq);
            return Ok(result);
        }

        //PWASTE CASUE CODES
        [Route("pwaste/causecodes")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutWasteCauseCodesAsync([FromQuery] int plant)
        {
            var result = await _PackoutService.GetPackoutWasteCauseCodesAsync(plant);
            return Ok(result);
        }

        //PWASTE TYPES
        [Route("pwaste/types")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutWasteTypesAsync([FromQuery] int areaSeq)
        {
            var result = await _PackoutService.GetPackoutWasteTypesAsync(areaSeq);
            return Ok(result);
        }

        //PWASTE Materials
        [Route("pwaste/materials")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutWasteMaterialsAsync()
        {
            var result = await _PackoutService.GetPackoutWasteMaterialsAsync();
            return Ok(result);
        }

        //F_GRADE CAUSE CODES
        [Route("fgrade/causecodes")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutFgradeCauseCodesAsync()
        {
            var result = await _PackoutService.GetPackoutFgradeCauseCodesAsync();
            return Ok(result);
        }

        //LOCATION Materials
        [Route("location/{locationCode}/materials")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutLocationMaterialsAsync([FromRoute] string locationCode)
        {
            var result = await _PackoutService.GetPackoutLocationMaterialsAsync(locationCode);
            return Ok(result);
        }

        //PRINTERS
        [Route("printers")]
        [HttpGet]
        public async Task<IActionResult> GetPackoutPrintersAsync([FromQuery] int? areaSeq = null, [FromQuery] int? stationSeq = null, [FromQuery] string? shuttleArea = null, [FromQuery] char? printerType = null)
        {
            var result = await _PackoutService.GetPackoutPrintersAsync(areaSeq, stationSeq, shuttleArea, printerType);
            return Ok(result);
        }

        //Atline Lines
        [Route("lines")]
        [HttpGet]
        public async Task<IActionResult> GetNppPackagingLinesAsync()
        {
            var result = await _PackoutService.GetNppPackagingLinesAsync();
            return Ok(result);
        }

    }
}
