﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Controllers
{
    [AuthorizeByClaim("IsAuthorized")]
    [Route("api/[controller]")]
    [ApiController]
    public class BatchLabelController : ControllerBase
    {
        private readonly IBatchLabelService _BatchLabelService;
        private readonly IPrintService _PrintService;

        public BatchLabelController(IBatchLabelService BatchLabelService, IPrintService PrintService)
        {
            _BatchLabelService = BatchLabelService;
            _PrintService = PrintService;
        }

        [HttpGet]
        public async Task<IActionResult> GetBatchLabelAsync([FromQuery] string serialId)
        {
            var result = await _BatchLabelService.GetBatchLabelAsync(serialId);
            return Ok(result);
        }

        [HttpGet("batch")]
        public async Task<IActionResult> GetBatchLabelsAsync([FromQuery] string batchId)
        {
            var result = await _BatchLabelService.GetBatchLabelsAsync(batchId);
            return Ok(result);
        }

        [AuthorizeByClaim("CanAddBatchLabels")]
        [HttpPost]
        public async Task<IActionResult> CreateBatchLabelAsync([FromQuery]string batchId, [FromQuery] int labelCount, [FromQuery] string user)
        {
            var result = await _BatchLabelService.CreateBatchLabelAsync(batchId, labelCount, user);
            return Ok(result);
        }

        [AuthorizeByClaim("CanAddBatchLabels")]
        [HttpPut("add")]
        public async Task<IActionResult> AddBatchLabelAsync([FromQuery] string batchId, [FromQuery] int batchModeId, [FromQuery] int labelCount, [FromQuery] string user)
        {
            var result = await _BatchLabelService.AddBatchLabelAsync(batchId, batchModeId, labelCount, user);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateBatchLabelAsync([FromBody] BatchLabel batchLabel)
        {
            var result = await _BatchLabelService.UpdateBatchLabelAsync(batchLabel);
            return Ok(result);
        }

        [AuthorizeByClaim("CanPrintBatchLabels", "CanReprintBatchLabels")]
        [HttpPost("print")]
        public async Task<IActionResult> PrintBatchLabelsAsync([FromBody] PrintDTO printDto,[FromQuery] string userId)
        {
            var result = await _BatchLabelService.PrintBatchLabelsAsync(printDto, userId);
            return Ok(result);
        }

        [HttpPost("zpl")]
        public async Task<IActionResult> GetLabelZplAsync([FromBody] PrintDTO printDDto)
        {
            var result = await _PrintService.GetLabelZplAsync(printDDto);
            return Ok(result);
        }

    }
}
