﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Controllers
{
    [AuthorizeByClaim("IsAuthorized")]
    [Route("api/[controller]")]
    [ApiController]
    public class BatchMasterController : ControllerBase
    {
        private readonly IBatchMasterService _BatchMasterService;

        public BatchMasterController(IBatchMasterService BatchMasterService)
        {
            _BatchMasterService = BatchMasterService;
        }

        [Route("list")]
        [HttpPost]
        public async Task<IActionResult> GetBatchListAsync([FromBody] BatchMasterFilter filter)
        {
            var result = await _BatchMasterService.GetBatchMasterListAsync(filter);
            return Ok(result);
        }

        [Route("key/{batchId}")]
        [HttpGet]
        public async Task<IActionResult> GetBatchMasterAsync([FromRoute] int batchId)
        {
            var result = await _BatchMasterService.GetBatchMasterAsync(batchId);
            return Ok(result);
        }

        [Route("id/{batchId}")]
        [HttpGet]
        public async Task<IActionResult> GetBatchMasterAsync([FromRoute] string batchId)
        {
            var result = await _BatchMasterService.GetBatchMasterAsync(batchId);
            return Ok(result);
        }

        [AuthorizeByClaim("CanCreateBatch")]
        [HttpPost]
        public async Task<IActionResult> CreateBatchMasterAsync([FromBody] BatchMasterCreateDto BatchMasterDto)
        {
            var result = await _BatchMasterService.CreateBatchMasterAsync(BatchMasterDto);
            return Ok(result);
        }

        [HttpPost("toller")]
        public async Task<IActionResult> CreateTollerBatchMasterAsync([FromQuery] int materialId, [FromQuery] string username, [FromQuery] string locationCode, [FromQuery] string businessCode)
        {
            var result = await _BatchMasterService.CreateTollerBatchMasterAsync(materialId,username,locationCode,businessCode);
            return Ok(result);
        }

        [HttpPost("existing")]
        public async Task<IActionResult> CreateExistingBatchMasterAsync([FromQuery] int materialId, [FromQuery] string batchId, [FromQuery] string username, [FromQuery] string locationCode)
        {
            var result = await _BatchMasterService.CreateExistingBatchMasterAsync(materialId, batchId, username, locationCode);
            return Ok(result);
        }

        [AuthorizeByClaim("CanCreateBatch")]
        [HttpPost("pwaste")]
        public async Task<IActionResult> CreatePwasteBatchMasterAsync([FromBody] BatchMasterPwasteCreateDto BatchMasterDto)
        {
            var result = await _BatchMasterService.CreatePwasteBatchMasterAsync(BatchMasterDto);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateBatchMasterAsync([FromBody] BatchMaster batchMaster)
        {
            var result = await _BatchMasterService.UpdateBatchMasterAsync(batchMaster);
            return Ok(result);
        }


        [Route("pallet/count")]
        [HttpGet]
        public async Task<IActionResult> GetDefaultBatchMasterPalletCountAsync([FromQuery] int areaSeq, [FromQuery] string cpLine, [FromQuery] int materialId)
        {
            var result = await _BatchMasterService.GetDefaultBatchMasterPalletCountAsync(areaSeq, cpLine, materialId);
            return Ok(result);
        }

        [AuthorizeByClaim("CanSuspectUnsuspectBatch")]
        [HttpPost("suspect")]
        public async Task<IActionResult> SuspectBatchMasterAsync([FromQuery] int batchMasterKey, [FromQuery] string batchId, [FromQuery] string batchComment, [FromQuery] string user, [FromQuery] bool sendToLims)
        {
            var result = await _BatchMasterService.SuspectBatchMasterAsync(batchMasterKey, batchId, batchComment, user, sendToLims);
            return Ok(result);
        }

        [AuthorizeByClaim("CanSuspectUnsuspectBatch")]
        [HttpPost("unsuspect")]
        public async Task<IActionResult> UnSuspectBatchMasterAsync([FromQuery] int batchMasterKey, [FromQuery] string batchId, [FromQuery] string batchComment, [FromQuery] string user)
        {
            var result = await _BatchMasterService.UnSuspectBatchMasterAsync(batchMasterKey, batchId, batchComment, user);
            return Ok(result);
        }

        [AuthorizeByClaim("CanChangeBatchMode")]
        [HttpPost("assign")]
        public async Task<IActionResult> AssignBatchToAtlineAsync([FromQuery] int processOrderId, [FromQuery] int batchMasterKey, [FromQuery] string station, [FromQuery] string user)
        {
            var result = await _BatchMasterService.AssignBatchToAtlineAsync(processOrderId, batchMasterKey, station, user);
            return Ok(result);
        }

        [AuthorizeByClaim("CanChangeBatchMode")]
        [HttpPost("mode/atline")]
        public async Task<IActionResult> ChangeBatchModeAtLine([FromQuery] int processOrderId, [FromQuery] int batchMasterKey, [FromQuery] string station, [FromQuery] bool assign, [FromQuery] string user)
        {
            var result = await _BatchMasterService.ChangeBatchModeAtLine(processOrderId, batchMasterKey, station, assign, user);
            return Ok(result);
        }

        [AuthorizeByClaim("CanChangeBatchMode")]
        [HttpPost("mode/preprint")]
        public async Task<IActionResult> ChangeBatchModePrePrint( [FromQuery] int batchMasterKey, [FromQuery] string station, [FromQuery] string user)
        {
            var result = await _BatchMasterService.ChangeBatchModePrePrint(batchMasterKey, station, user);
            return Ok(result);
        }

        [AuthorizeByClaim("CanCompleteBatch")]
        [HttpPost("close")]
        public async Task<IActionResult> CloseBatchAsync([FromQuery] int batchMasterKey, [FromQuery] string user)
        {
            var result = await _BatchMasterService.CloseBatchAsync(batchMasterKey, user);
            return Ok(result);
        }

        [HttpGet("comment")]
        public async Task<IActionResult> GetBatchCommentsAsync([FromQuery] int batchMasterKey)
        {
            var result = await _BatchMasterService.GetBatchCommentsAsync(batchMasterKey);
            return Ok(result);
        }

        [AuthorizeByClaim("CanCreateBatchComment")]
        [HttpPost("comment")]
        public async Task<IActionResult> CreateBatchCommentAsync([FromBody] BatchCommentDto batchCommentDto)
        {
            var result = await _BatchMasterService.CreateBatchCommentAsync(batchCommentDto);
            return Ok(result);
        }

        [HttpGet("comment/codes")]
        public async Task<IActionResult> GetBatchCommentCodesAsync()
        {
            var result = await _BatchMasterService.GetBatchCommentCodesAsync();
            return Ok(result);
        }


    }
}
