﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SmartMFG_Packout.Models.Auth;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class JwtService :IJwtService
    {
        private readonly IAuthService _userService;
        private readonly IConfiguration _configuration;

        public JwtService(IAuthService userService, IConfiguration configuration)
        {
            _configuration = configuration;
            _userService = userService;
        }

        public async Task<dynamic> GenerateJwt(ClaimsIdentity identity, string userName)
        {
            var token = new
            {
                id = identity.Claims.Single(c => c.Type == "id").Value,
                auth_token = await GenerateAccessToken(userName, identity),
                refresh_token =  GenerateRefreshToken(),
                expires_in = (int)TimeSpan.FromMinutes(_configuration.GetValue<int>("JwtIssuerOptions:ValidFor")).TotalSeconds,
                expires_inactivity = (int)TimeSpan.FromMinutes(_configuration.GetValue<int>("JwtIssuerOptions:Inactivity")).TotalSeconds
            };

            //save refresh token info to db
            try
            {
                await _userService.UpdateRefreshToken(userName, new RefreshToken { REFRESH_TOKEN = token.refresh_token, REFRESH_TOKEN_EXPIRY = DateTime.UtcNow.Add(TimeSpan.FromMinutes(_configuration.GetValue<int>("JwtIssuerOptions:ValidFor")))});
            }
            catch {
                throw new Exception("Unable to save Refresh token.");
            }

            return token;
        }

        public async Task<dynamic> GenerateRefreshedJwt(RefreshTokenDto tokens)
        {
            string accessToken = tokens.ACCESS_TOKEN;
            string refreshToken = tokens.REFRESH_TOKEN;

            ClaimsPrincipal principal = GetPrincipalFromExpiredToken(accessToken);

            string username = principal.Identity.Name; //this is mapped to the Name claim by default

            //check that refresh token is valid
            if (await IsRefeshTokenValid(username, refreshToken))
            {
                return await CreateNewJwtToken(username, tokens);
            }
            else {
                throw new Exception("Invalid client request");
            }

        }

        private async Task<dynamic> CreateNewJwtToken(string username, RefreshTokenDto tokens) {
            var identity = _userService.GenerateClaimsIdentity(username, username);

            var token = new
            {
                id = identity.Claims.Single(c => c.Type == "id").Value,
                auth_token = await GenerateAccessToken(username, identity),
                refresh_token = GenerateRefreshToken(),
                expires_in = (int)TimeSpan.FromMinutes(_configuration.GetValue<int>("JwtIssuerOptions:ValidFor")).TotalSeconds,
                expires_inactivity = (int)TimeSpan.FromMinutes(_configuration.GetValue<int>("JwtIssuerOptions:Inactivity")).TotalSeconds
            };

            //save refresh token info to db
            try
            {
                await _userService.UpdateRefreshToken(username, new RefreshToken { REFRESH_TOKEN = token.refresh_token, REFRESH_TOKEN_EXPIRY = DateTime.UtcNow.Add(TimeSpan.FromMinutes(_configuration.GetValue<int>("JwtIssuerOptions:RefreshValidFor"))) });
            }
            catch
            {
                throw new Exception("Unable to save Refresh token.");
            }

            return token;
        }

        private async Task<bool> IsRefeshTokenValid(string username, string token) {
            bool isValid = true;
            RefreshToken savedToken = await _userService.GetRefreshToken(username);
            if (token == null || savedToken.REFRESH_TOKEN != token || savedToken.REFRESH_TOKEN_EXPIRY <= DateTime.Now)
            {
                isValid = false; 
            }

            return isValid;
        }


        public string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {

            string secret = _configuration.GetValue<string>("JwtIssuerOptions:ApiTokenSecret");

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = _configuration.GetValue<string>("JwtIssuerOptions:Issuer"),

                ValidateAudience = true,
                ValidAudience = _configuration.GetValue<string>("JwtIssuerOptions:Audience"),

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secret)),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            
            
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);

            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");
            return principal;
        }

        private  async Task<string> GenerateAccessToken(string userName, ClaimsIdentity identity)
        {
            var claims = new[]
            {
                 new Claim(JwtRegisteredClaimNames.Sub, userName),
                 new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                 new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(DateTime.UtcNow).ToString(), ClaimValueTypes.Integer64),
            };

            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration.GetValue<string>("JwtIssuerOptions:ApiTokenSecret")));

            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: _configuration.GetValue<string>("JwtIssuerOptions:Issuer"),
                audience: _configuration.GetValue<string>("JwtIssuerOptions:Audience") ,
                claims: identity.Claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(_configuration.GetValue<int>("JwtIssuerOptions:ValidFor"))),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)  
                );

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        private static long ToUnixEpochDate(DateTime date)
          => (long)Math.Round((date.ToUniversalTime() -
                               new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                              .TotalSeconds);

    }
}
