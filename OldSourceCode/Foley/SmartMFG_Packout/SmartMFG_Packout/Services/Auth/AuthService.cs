﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Auth;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class AuthService : IAuthService
    {
        private readonly IAuthRepository _userRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;


        public AuthService(IHttpContextAccessor httpContextAccessor, IAuthRepository userRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _userRepository = userRepository;

        }


        public async Task<int> UpdateRefreshToken(string userId, RefreshToken refreshToken)
        {
            var result = await _userRepository.UpdateRefreshToken(userId, refreshToken);
            return result;
        }

        public async Task<RefreshToken> GetRefreshToken(string userId)
        {
            var result = await _userRepository.GetRefreshToken(userId);
            return result;
        }

        public async Task<IEnumerable<UserClaim>> GetUserClaimsAsync(string user)
        {

            IEnumerable<UserClaim> claims;

            try
            {
                claims = await _userRepository.GetUserClaimsAsync(user);
            }
            catch (Exception e)
            {
                throw new Exception("Unable to get user claims.");
            }

            return claims;
        }

        public async Task<UserProfile> GetUserProfileAsync()
        {

            var user = GetUserId();
            return await GetUserProfileAsync(user);
        }

        public async Task<UserProfile> GetUserProfileAsync(string user)
        {

            UserProfile profile;

            try
            {
                profile = await _userRepository.GetUserProfileAsync(user);
            }
            catch (Exception e)
            {
                throw new Exception("Unable to get user profile.");
            }

            return profile;
        }

        public string NormalizeUser(string user)
        {
            return user.ToUpper().Replace("APM" + "\\", "").Replace("@ASCENDMATERIALS.COM", "");
        }

        public async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the user to verifty
            UserProfile userToVerify = await _userRepository.GetUserProfileAsync(userName);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            // check the credentials
            if (CheckPasswordAsync(userToVerify.USER_ID, password) && userToVerify.ACTIVE)
            {
                return await Task.FromResult(GenerateClaimsIdentity(userName, userToVerify.USER_ID));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }


        public ClaimsIdentity GenerateClaimsIdentity(string userName, string id)
        {
            //get claims
            IEnumerable<UserClaim> claims = GetUserClaimsAsync(userName).Result;

            //create claims identiy and set id
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new GenericIdentity(userName, "Token"), new[] {
                new System.Security.Claims.Claim("id", id)
            });

            //get user profile and add authorization cliam if profile exists
            UserProfile profile = GetUserProfileAsync(userName).Result;

            bool Authorized = profile != null && profile.ACTIVE ? true : false;

            if (Authorized)
            {
                claimsIdentity.AddClaim(new System.Security.Claims.Claim("IsAuthorized", "True"));
            }

            //add each claim from profile to identity
            foreach (UserClaim c in claims) {
                claimsIdentity.AddClaim(new System.Security.Claims.Claim(c.CLAIM_TYPE, c.CLAIM_VALUE));
            }

            return claimsIdentity;
        }

        public async Task<int> RevokeRefreshToken()
        {
            string user = GetUserId();
            RefreshToken refreshToken = new RefreshToken() { REFRESH_TOKEN = null, REFRESH_TOKEN_EXPIRY = null };
            var result = await _userRepository.UpdateRefreshToken(user, refreshToken);

            return result;
        }

        public string GetUserId()
        {
            return _httpContextAccessor.HttpContext.User.Identity.Name;
        }

        private bool CheckPasswordAsync(string username, string password) {
            bool isValid = false;

            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "apm.ascend.com"))
            {
                // validate the credentials
                isValid = pc.ValidateCredentials(username, password);
            }

            return isValid;
        }

    }
}
