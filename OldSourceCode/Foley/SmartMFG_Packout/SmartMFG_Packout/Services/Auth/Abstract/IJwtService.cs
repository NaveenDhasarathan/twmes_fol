﻿using Newtonsoft.Json;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IJwtService
    {
        Task<dynamic> GenerateJwt(ClaimsIdentity identity, string userName);
        Task<dynamic> GenerateRefreshedJwt(RefreshTokenDto tokens);
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
    }
}
