﻿using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IAuthService
    {
        Task<int> UpdateRefreshToken(string userId, RefreshToken refreshToken);
        Task<RefreshToken> GetRefreshToken(string userId);
        Task<UserProfile> GetUserProfileAsync();
        Task<UserProfile> GetUserProfileAsync(string user);
        Task<IEnumerable<UserClaim>> GetUserClaimsAsync(string user);
        string NormalizeUser(string user);
        Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password);
        Task<int> RevokeRefreshToken();
        ClaimsIdentity GenerateClaimsIdentity(string userName, string id);
    }
}
