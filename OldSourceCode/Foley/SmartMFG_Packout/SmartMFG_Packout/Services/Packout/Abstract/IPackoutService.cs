﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IPackoutService
    {
        //BATCH MODES
        Task<IEnumerable<BatchMode>> GetPackoutBatchModesAsync();

        //AREAS
        Task<IEnumerable<PackoutArea>> GetPackoutAreasAsync(int plant);

        //STATIONS
        Task<IEnumerable<PackoutStation>> GetPackoutStationsAsync(int areaSeq);

        //SOURCES
        Task<IEnumerable<PackoutSource>> GetPackoutSourcesAsync(int areaSeq, int stationSeq);

        //LOCATIONS
        Task<IEnumerable<Location>> GetLocationsAsync();

        //PWASTE AREAS
        Task<IEnumerable<PwasteArea>> GetPackoutWasteAreasAsync(int plant);

        //PWASTE SOURCES
        Task<IEnumerable<PwasteSource>> GetPackoutWasteSourcesAsync(int areaSeq);

        //PWASTE CAUSE CODES
        Task<IEnumerable<PwasteCauseCode>> GetPackoutWasteCauseCodesAsync(int plant);

        //PWASTE TYPE
        Task<IEnumerable<PwasteType>> GetPackoutWasteTypesAsync(int areaSeq);

        //PWASTE Materials
        Task<IEnumerable<Material>> GetPackoutWasteMaterialsAsync();

        //F_GRADE CAUSE CODES
        Task<IEnumerable<FgradeCauseCode>> GetPackoutFgradeCauseCodesAsync();

        //LOCATION MATERIALS
        Task<IEnumerable<Material>> GetPackoutLocationMaterialsAsync(string locationCode);

        //PRNITERS
        Task<IEnumerable<Printer>> GetPackoutPrintersAsync(int ?areaSeq, int ?stationSeq, string? shuttleArea, char? printerType);

        //ATLINE LINES
        Task<IEnumerable<PackagingLine>> GetNppPackagingLinesAsync();
    }
}
