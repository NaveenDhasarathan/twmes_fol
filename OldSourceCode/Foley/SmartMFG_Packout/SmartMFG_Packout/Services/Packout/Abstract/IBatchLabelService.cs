﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IBatchLabelService
    {
        Task<BatchLabel> GetBatchLabelAsync(string serialId);
        Task<IEnumerable<BatchLabel>> GetBatchLabelsAsync(string batchId);
        Task<IEnumerable<BatchLabel>> CreateBatchLabelAsync(string batchId, int labelCount, string user);
        Task<IEnumerable<BatchLabel>> AddBatchLabelAsync(string batchId, int batchModeId, int labelCount, string user);
        Task<int> UpdateBatchLabelAsync(BatchLabel batchLabel);
        Task<string> PrintBatchLabelsAsync(PrintDTO printDto, string user);
    }
}
