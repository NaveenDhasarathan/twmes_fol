﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IProcessOrderService
    {
        Task<IEnumerable<ProcessOrder>> GetProcessOrderListAsync(ProcessorOrderFilter filter);
        Task<ProcessOrder> GetProcessOrderAsync(int po);
        Task<int> UpdateProcessOrderStatusAsync(int processOrderId, string assetName, string username, int newStatus);
    }
}
