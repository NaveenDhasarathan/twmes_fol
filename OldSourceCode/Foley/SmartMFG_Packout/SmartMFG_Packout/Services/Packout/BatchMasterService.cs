﻿using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class BatchMasterService : IBatchMasterService
    {
        private readonly IBatchMasterRepository _BatchMasterRepository;
        private readonly ILIMSService _LIMSService;

        public BatchMasterService(IBatchMasterRepository BatchMasterRepository, ILIMSService LIMSService)
        {
            _BatchMasterRepository = BatchMasterRepository;
            _LIMSService = LIMSService;
        }

        public async Task<IEnumerable<BatchMaster>> GetBatchMasterListAsync(BatchMasterFilter filter)
        {
            var result = await _BatchMasterRepository.GetBatchMasterListAsync(filter);
            return result;
        }

        public async Task<BatchMaster> GetBatchMasterAsync(int batchId)
        {
            var result = await _BatchMasterRepository.GetBatchMasterAsync(batchId);
            return result;

        }

        public async Task<BatchMaster> GetBatchMasterAsync(string batchId)
        {
            var result = await _BatchMasterRepository.GetBatchMasterAsync(batchId);
            return result;

        }

        public async Task<int> UpdateBatchMasterAsync(BatchMaster batchMaster)
        {
            var result = await _BatchMasterRepository.UpdateBatchMasterAsync(batchMaster);

            return result;

        }

        public async Task<int> GetDefaultBatchMasterPalletCountAsync(int areaSeq, string cpLine, int materialId)
        {
            var result = await _BatchMasterRepository.GetDefaultBatchMasterPalletCountAsync(areaSeq, cpLine, materialId);
            return result;

        }
        
        public async Task<BatchMaster> CreateBatchMasterAsync(BatchMasterCreateDto BatchMasterDto)
        {
            BatchMaster batch = await _BatchMasterRepository.CreateBatchMasterAsync(BatchMasterDto);

            try
            {
                string batchId = batch.BATCH_ID.ToString();
                string materialId = batch.MATERIAL_ID.ToString();
                string plantId = batch.LOCATION_CODE.ToString();
                string areaSeq = batch.AREA_SEQ.ToString();
                string po = batch.PROCESS_ORDER_ID.ToString();
                string asset = batch.MFG_SOURCE.ToString();
                string suspect = BatchMasterDto.SUSPECT ? "T" : "F"; 

                //make call to samplemanager
                await _LIMSService.CreateBatchAsync(batchId, materialId, plantId, areaSeq, po, asset, suspect);
            }
            catch(Exception e) {
                throw new Exception("Unable to send new batch to LIMS: " + e.Message);
            }
                
            return batch;

        }

        public async Task<BatchMaster> CreateTollerBatchMasterAsync(int materialId, string username, string locationCode, string businessCode)
        {
            BatchMaster batch = await _BatchMasterRepository.CreateTollerBatchMasterAsync(materialId, username, locationCode, businessCode);

            try
            {
                string batchId = batch.BATCH_ID.ToString();
                string bmaterialId = batch.MATERIAL_ID.ToString();
                string plantId = batch.LOCATION_CODE.ToString();
                string po = batch.PROCESS_ORDER_ID.ToString();
                string asset = batch.MFG_SOURCE.ToString();
                string area = batch.AREA_SEQ.ToString();

                //make call to samplemanager
                await _LIMSService.CreateBatchAsync(batchId, bmaterialId, plantId, area, po, asset, "F");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to send new batch to LIMS: " + e.Message);
            }

            return batch;
        }

        public async Task<BatchMaster> CreateExistingBatchMasterAsync(int materialId, string batchId, string username, string locationCode)
        {
            BatchMaster batch = await _BatchMasterRepository.CreateExistingBatchMasterAsync(materialId, batchId, username, locationCode);

            try
            {
                string b_batchId = batch.BATCH_ID.ToString();
                string b_materialId = batch.MATERIAL_ID.ToString();
                string b_plantId = batch.LOCATION_CODE.ToString();
                string b_po = batch.PROCESS_ORDER_ID == null ? "" : batch.PROCESS_ORDER_ID.ToString();
                string b_asset = batch.MFG_SOURCE == null ? "" : batch.MFG_SOURCE.ToString();
                string b_area = batch.AREA_SEQ == null ? "" : batch.AREA_SEQ.ToString();

                //make call to samplemanager
                await _LIMSService.CreateBatchAsync(b_batchId, b_materialId, b_plantId, b_area, b_po, b_asset, "F");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to send new batch to LIMS: " + e.Message);
            }

            return batch;
        }

        public async Task<BatchMaster> CreatePwasteBatchMasterAsync(BatchMasterPwasteCreateDto BatchMasterDto)
        {
            var result = await _BatchMasterRepository.CreatePwasteBatchMasterAsync(BatchMasterDto);
            return result;

        }

        public async Task<BatchMaster> SuspectBatchMasterAsync(int batchMasterKey, string batchId, string batchComment, string user, bool sendToLims)
        {
            var result = await _BatchMasterRepository.SuspectBatchMasterAsync(batchMasterKey, batchComment, user);

            if (sendToLims) {
                try
                {
                    string status = "SUSPECT";

                    //make call to samplemanager
                    await _LIMSService.UpdateBatchStatusAsync(batchId, status);
                }
                catch (Exception e)
                {
                    throw new Exception("Unable to send updated batch status to LIMS: " + e.Message);
                }
            }
            
            return result;

        }

        public async Task<BatchMaster> UnSuspectBatchMasterAsync(int batchMasterKey, string batchId, string batchComment, string user)
        {
            var result = await _BatchMasterRepository.UnSuspectBatchMasterAsync(batchMasterKey, batchComment, user);

            try
            {
                string status = "UNSUSPECT";

                //make call to samplemanager
                await _LIMSService.UpdateBatchStatusAsync(batchId, status);
            }
            catch (Exception e)
            {
                throw new Exception("Unable to send updated batch status to LIMS: " + e.Message);
            }

            return result;

        }

        public async Task<int> AssignBatchToAtlineAsync(int processOrderId, int batchMasterKey, string station, string user)
        {
            var result = await _BatchMasterRepository.AssignBatchToAtlineAsync(processOrderId, batchMasterKey, station, user);
            return result;

        }

        public async Task<int> ChangeBatchModeAtLine(int processOrderId, int batchMasterKey, string station, bool assign, string user)
        {
            var result = await _BatchMasterRepository.ChangeBatchModeAtLine(processOrderId, batchMasterKey, station, assign, user);
            return result;

        }

        public async Task<int> ChangeBatchModePrePrint(int batchMasterKey, string station, string user)
        {
            var result = await _BatchMasterRepository.ChangeBatchModePrePrint(batchMasterKey, station, user);
            return result;

        }

        public async Task<BatchMaster> CloseBatchAsync(int batchMasterKey, string user)
        {
            BatchMaster batch = await _BatchMasterRepository.CloseBatchAsync(batchMasterKey, user);

            //make call to samplemanager

            try
            {
                string batchId = batch.BATCH_ID.ToString();
                string status = "COMPLETE";

                //make call to samplemanager
                await _LIMSService.UpdateBatchStatusAsync(batchId, status);
            }
            catch (Exception e)
            {
                throw new Exception("Unable to send updated batch status to LIMS: " + e.Message);
            }

            return batch;

        }

        public async Task<IEnumerable<BatchComment>> GetBatchCommentsAsync(int batchMasterKey)
        {
            var result = await _BatchMasterRepository.GetBatchCommentsAsync(batchMasterKey);
            return result;

        }

        public async Task<BatchComment> CreateBatchCommentAsync(BatchCommentDto batchCommentDto)
        {
            var result = await _BatchMasterRepository.CreateBatchCommentAsync(batchCommentDto);
            return result;

        }

        public async Task<IEnumerable<CommentCode>> GetBatchCommentCodesAsync()
        {
            var result = await _BatchMasterRepository.GetBatchCommentCodesAsync();
            return result;

        }
    }
}
