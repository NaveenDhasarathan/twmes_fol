﻿using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class BatchLabelService : IBatchLabelService
    {
        private readonly IBatchLabelRepository _BatchLabelRepository;
        private readonly IPrintService _PrintService;

        public BatchLabelService(IBatchLabelRepository BatchLabelRepository, IPrintService PrintService)
        {
            _BatchLabelRepository = BatchLabelRepository;
            _PrintService = PrintService;
        }

        public async Task<BatchLabel> GetBatchLabelAsync(string serialId)
        {
            var result = await _BatchLabelRepository.GetBatchLabelAsync(serialId);
            return result;
        }

        public async Task<IEnumerable<BatchLabel>> GetBatchLabelsAsync(string batchId)
        {
            var result = await _BatchLabelRepository.GetBatchLabelsAsync(batchId);
            return result;
        }

        public async Task<IEnumerable<BatchLabel>> CreateBatchLabelAsync(string batchId, int labelCount, string user)
        {
            List<BatchLabel> batchLabels = new List<BatchLabel>();
            
            
            for (int i = 0; i < labelCount; i++)
            {
                BatchLabel newLabel = await _BatchLabelRepository.CreateBatchLabelAsync(batchId, user);

                batchLabels.Add(newLabel);
            }

            
            return batchLabels;
        }

        public async Task<IEnumerable<BatchLabel>> AddBatchLabelAsync(string batchId, int batchModeId, int labelCount, string user)
        {
            List<BatchLabel> batchLabels = new List<BatchLabel>();

            if (batchModeId == 1) {
                for (int i = 0; i < labelCount; i++)
                {
                    BatchLabel newLabel = await _BatchLabelRepository.CreateBatchLabelAsync(batchId, user);

                    batchLabels.Add(newLabel);
                }
            }

            var result = await _BatchLabelRepository.UpdateBatchLabelCountAsync(batchId, labelCount, user);

            return batchLabels;
        }

        public async Task<int> UpdateBatchLabelAsync(BatchLabel batchLabel)
        {
            var result = await _BatchLabelRepository.UpdateBatchLabelAsync(batchLabel);
            return result;
        }

        public async Task<string> PrintBatchLabelsAsync(PrintDTO printDto, string user)
        {
            string printResult = "";
            
            try
            {
                printResult = await _PrintService.PrintLabelsAsync(printDto);
            }
            catch (Exception e)
            {
                throw new Exception("Error calling label printer: " + e.Message);
            }

            try
            {
                foreach (ProductionLabel label in printDto.labels)
                {
                    await _BatchLabelRepository.PrintBatchLabelAudit(label.serialId, user);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Unable to add print to audit: " + e.Message);
            }

            return printResult;
        }

    }
}
