﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class ProcessOrderService : IProcessOrderService
    {
        private readonly IConfiguration _Configuration;
        private readonly IProcessOrderRepository _ProcessOrderRepository;
        public ProcessOrderService(IConfiguration Configuration, IProcessOrderRepository ProcessOrderRepository)
        {
            _Configuration = Configuration;
            _ProcessOrderRepository = ProcessOrderRepository;
        }

        public async Task<IEnumerable<ProcessOrder>> GetProcessOrderListAsync(ProcessorOrderFilter filter)
        {
            var result = await _ProcessOrderRepository.GetProcessOrderListAsync(filter);
            return result;
            
        }

        public async Task<ProcessOrder> GetProcessOrderAsync(int po)
        {
            var result = await _ProcessOrderRepository.GetProcessOrderAsync(po);
            return result;

        }

        public async Task<int> UpdateProcessOrderStatusAsync(int processOrderId, string assetName, string username, int newStatus)
        {
            string baseUrl = _Configuration.GetSection("SapApiBaseUrl").Value;
            var poStatusChangeDto = new JObject(
                                    new JProperty("PROCESS_ORDER", processOrderId.ToString()),
                                    new JProperty("ASSET", assetName),
                                    new JProperty("USER", username)
                                );

            var httpContent = new StringContent(JsonConvert.SerializeObject(poStatusChangeDto), Encoding.UTF8, "application/json");

            try {
                //if 2 release po
                if (newStatus == 2)
                {
                    HttpClient client = new HttpClient();
                    HttpResponseMessage response = await client.PostAsync(
                        baseUrl + "values/Write/RELPO", httpContent);

                    response.EnsureSuccessStatusCode();
                }//if 4 close po
                else if (newStatus == 4)
                {
                    HttpClient client = new HttpClient();
                    HttpResponseMessage response = await client.PostAsync(
                        baseUrl + "values/Write/ClosePO", httpContent);

                    response.EnsureSuccessStatusCode();
                }
                else
                {
                    throw new Exception("Status can only be changed to released(2) or closed(4)");
                }
            } catch(Exception e) {
                throw new Exception("Unable change SAP process order status: " + e.Message);
            }
            
            
            var result = await _ProcessOrderRepository.UpdateProcessOrderStatusAsync(processOrderId, username, newStatus);
            return result;

        }

    }
}
