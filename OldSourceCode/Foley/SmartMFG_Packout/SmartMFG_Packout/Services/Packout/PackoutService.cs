﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class PackoutService : IPackoutService
    {
        private readonly IPackoutRepository _PackoutRepository;
  
        public PackoutService(IPackoutRepository PackoutRepository)
        {
            _PackoutRepository = PackoutRepository;
        }

        //BATCH MODES
        public async Task<IEnumerable<BatchMode>> GetPackoutBatchModesAsync()
        {
            var result = await _PackoutRepository.GetPackoutBatchModesAsync();
            return result;
        }

        //AREAS
        public async Task<IEnumerable<PackoutArea>> GetPackoutAreasAsync(int plant)
        {
            var result = await _PackoutRepository.GetPackoutAreasAsync(plant);
            return result;
        }

        //STATIONS
        public async Task<IEnumerable<PackoutStation>> GetPackoutStationsAsync(int areaSeq)
        {
            var result = await _PackoutRepository.GetPackoutStationsAsync(areaSeq);
            return result;
        }

        //SOURCES
        public async Task<IEnumerable<PackoutSource>> GetPackoutSourcesAsync(int areaSeq, int stationSeq)
        {
            var result = await _PackoutRepository.GetPackoutSourcesAsync(areaSeq, stationSeq);
            return result;
        }

        //LOCATIONS
        public async Task<IEnumerable<Location>> GetLocationsAsync()
        {
            var result = await _PackoutRepository.GetLocationsAsync();
            return result;
        }

        //PWASTE AREAS
        public async Task<IEnumerable<PwasteArea>> GetPackoutWasteAreasAsync(int plant)
        {
            var result = await _PackoutRepository.GetPackoutWasteAreasAsync(plant);
            return result;
        }

        //PWASTE SOURCES
        public async Task<IEnumerable<PwasteSource>> GetPackoutWasteSourcesAsync(int areaSeq)
        {
            var result = await _PackoutRepository.GetPackoutWasteSourcesAsync(areaSeq);
            return result;
        }

        //PWASTE CAUSE CODES
        public async Task<IEnumerable<PwasteCauseCode>> GetPackoutWasteCauseCodesAsync(int plant)
        {
            var result = await _PackoutRepository.GetPackoutWasteCauseCodesAsync(plant);
            return result;
        }

        //PWASTE TYPE
        public async Task<IEnumerable<PwasteType>> GetPackoutWasteTypesAsync(int areaSeq)
        {
            var result = await _PackoutRepository.GetPackoutWasteTypesAsync(areaSeq);
            return result;
        }

        //PWASTE Materials
        public async Task<IEnumerable<Material>> GetPackoutWasteMaterialsAsync()
        {
            var result = await _PackoutRepository.GetPackoutWasteMaterialsAsync();
            return result;
        }

        //F_GRADE CAUSE CODES
        public async Task<IEnumerable<FgradeCauseCode>> GetPackoutFgradeCauseCodesAsync() {
            var result = await _PackoutRepository.GetPackoutFgradeCauseCodesAsync();
            return result;
        }

        //LOCATION MATERIALS
        public async Task<IEnumerable<Material>> GetPackoutLocationMaterialsAsync(string locationCode)
        {
            var result = await _PackoutRepository.GetPackoutLocationMaterialsAsync(locationCode);
            return result;
        }

        //PRINTERS
        public async Task<IEnumerable<Printer>> GetPackoutPrintersAsync(int ?areaSeq, int ?stationSeq, string? shuttleArea, char? printerType)
        {
            var result = await _PackoutRepository.GetPackoutPrintersAsync(areaSeq, stationSeq, shuttleArea, printerType);
            return result;
        }

        //ATLINE LINES
        public async Task<IEnumerable<PackagingLine>> GetNppPackagingLinesAsync()
        {
            var result = await _PackoutRepository.GetNppPackagingLinesAsync();
            return result;
        }
    }
}
