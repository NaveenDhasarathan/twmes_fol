﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IPrintService
    {
        Task<string> PrintLabelsAsync(PrintDTO printDto);
        Task<string> PrintShuttlePickListAsync(IPEndPoint printerIpEndPoint, string text);
        Task<string> GetLabelZplAsync(PrintDTO printDto);
    }
}
