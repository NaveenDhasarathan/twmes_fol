﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Services.Abstract;
using SOI.Printers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class PrintService : IPrintService
    {
        private readonly IConfiguration _Configuration;
        public PrintService(IConfiguration configuration)
        {
            _Configuration = configuration;
        }

        public async Task<string> PrintLabelsAsync(PrintDTO printDto)
        {
            var key = _Configuration.GetSection("PrintingApi").GetSection("ApiKey").Value;
            var endpoint = _Configuration.GetSection("PrintingApi").GetSection("Url").Value;

            try
            {
                using (var client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(printDto);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                    client.BaseAddress = new Uri(endpoint);
                    client.DefaultRequestHeaders.Add("ApiKey", key);

                    var response = await client.PostAsync("api/Label/Print", httpContent);
                    string resultContent = await response.Content.ReadAsStringAsync();

                    return resultContent;
                }
            }
            catch (Exception e) {
                throw new Exception("Failed call print api: " + e.Message.ToString());
            }
        }

        public Task<string> PrintShuttlePickListAsync(IPEndPoint printerIpEndPoint, string text)
        {
            bool Success = false;
            NetPrinter.PrintJobEventArgs e = new NetPrinter.PrintJobEventArgs(printerIpEndPoint.Address.ToString(), printerIpEndPoint.Port, text, Success);
            NetPrinter.OnPrintJobStarted(e);

            Socket socket;
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (Exception ex)
            {
                NetPrinter.OnPrintJobFinished(e);
                throw new Exception("Unable to instantiate a socket", ex);
            }
            try
            {
                socket.Connect((EndPoint)printerIpEndPoint);
            }
            catch (Exception ex)
            {
                NetPrinter.OnPrintJobFinished(e);
                throw new Exception("Unable to connect to printer, " + printerIpEndPoint.Address.ToString(), ex);
            }
            byte[] bytes;
            try
            {
                bytes = Encoding.ASCII.GetBytes(string.Format(text));
            }
            catch (Exception ex)
            {
                NetPrinter.OnPrintJobFinished(e);
                throw new Exception("Unable Format data, " + text, ex);
            }
            try
            {
                if (socket.Send(bytes, bytes.Length, SocketFlags.None) != bytes.Length)
                {
                    throw new Exception("Data was lost during transmission");
                }
            }
            catch (Exception ex)
            {
                NetPrinter.OnPrintJobFinished(e);
                throw new Exception("Unable to send data, " + text, ex);
            }
            try
            {
                socket.Close();
            }
            catch (Exception ex)
            {
                NetPrinter.OnPrintJobFinished(e);
                throw new Exception("Error while closing Socket handle, " + socket.Handle.ToString(), ex);
            }
            e.Success = true;
            NetPrinter.OnPrintJobFinished(e);
            return Task.FromResult(text);
        }

        public async Task<string> GetLabelZplAsync(PrintDTO printDto)
        {
            var key = _Configuration.GetSection("PrintingApi").GetSection("ApiKey").Value;
            var endpoint = _Configuration.GetSection("PrintingApi").GetSection("Url").Value;

            try { 
                using (var client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(printDto);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                    client.BaseAddress = new Uri(endpoint);
                    client.DefaultRequestHeaders.Add("ApiKey", key);

                    var response = await client.PostAsync("api/Label/Generate/Zpl", httpContent);
                    string resultContent = await response.Content.ReadAsStringAsync();

                    return  Regex.Replace(resultContent, @"\t|\n|\r", "");
                }
            }
            catch (Exception e)
            {
                throw new Exception("Failed call print api: " + e.Message.ToString());
            }
        }

    }
}
