﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceModel;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface ILIMSService
    {
        Task<string> CreateBatchAsync(string batchId, string materialId, string plantId, string areaSeq, string po, string asset, string suspect);
        Task<string> UpdateBatchStatusAsync(string batchId, string status);
        Task<string> GetMaterialStockStatus(string materialId);
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace = "http://www.thermo.com/informatics/xmlns/limswebservice", ConfigurationName = "ILIMSWebService")]
    public interface ILIMSWebService
    {

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/Process", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/ProcessResponse")]
        string Process(string request);

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/Process", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/ProcessResponse")]
        Task<string> ProcessAsync(string request);

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/CheckAuth", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/CheckAuthResponse")]
        string CheckAuth();

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/CheckAuth", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/CheckAuthResponse")]
        Task<string> CheckAuthAsync();

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/GetAllLogFiles", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/GetAllLogFilesResponse")]
        object GetAllLogFiles();

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/GetAllLogFiles", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/GetAllLogFilesResponse")]
        Task<object> GetAllLogFilesAsync();

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/EraseAllLogFiles", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/EraseAllLogFilesResponse")]
        string EraseAllLogFiles();

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/EraseAllLogFiles", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/EraseAllLogFilesResponse")]
        Task<string> EraseAllLogFilesAsync();

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/GetSingleLogFile", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/GetSingleLogFileResponse")]
        object GetSingleLogFile(string transaction, string direction);

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/GetSingleLogFile", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/GetSingleLogFileResponse")]
        Task<object> GetSingleLogFileAsync(string transaction, string direction);

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/LogSOAPMessages", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/LogSOAPMessagesResponse")]
        bool LogSOAPMessages(bool logging);

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/LogSOAPMessages", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/LogSOAPMessagesResponse")]
        Task<bool> LogSOAPMessagesAsync(bool logging);

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/ViewLogStatus", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/ViewLogStatusResponse")]
        bool ViewLogStatus();

        [OperationContractAttribute(Action = "http://www.thermo.com/informatics/xmlns/limswebservice/ViewLogStatus", ReplyAction = "http://www.thermo.com/informatics/xmlns/limswebservice/ILIMSWebService/ViewLogStatusResponse")]
        Task<bool> ViewLogStatusAsync();
    }
}
