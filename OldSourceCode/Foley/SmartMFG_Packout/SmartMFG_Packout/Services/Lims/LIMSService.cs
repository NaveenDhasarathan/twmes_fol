﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceModel;
using SmartMFG_Packout.Services.Abstract;
using Microsoft.Extensions.Configuration;
using Thermo.SM.LIMSML.Helper.High;
using Thermo.SM.LIMSML.Helper.Low;
using Action = Thermo.SM.LIMSML.Helper.Low.Action;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Xml;

namespace SmartMFG_Packout.Services
{

    public class LIMSService : ILIMSService
    {
        private readonly IConfiguration _Configuration;

        public LIMSService(IConfiguration Configuration)
        {
            _Configuration = Configuration;
        }

        public async Task<string> CreateBatchAsync(string batchId, string materialId, string plantId, string areaSeq, string po, string asset, string suspect)
        {
            var locationSetting = _Configuration.GetSection("SampleManagerApi");
            if (!locationSetting.Exists())
            {
                throw new Exception("Could not find SampleManager info");
            }
            string endpoint = locationSetting.GetSection("Endpoint").Value;
            string username = locationSetting.GetSection("Username").Value;
            string password = locationSetting.GetSection("Password").Value;


            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(endpoint);
            ILIMSWebService client = new ChannelFactory<ILIMSWebService>(basicHttpBinding, endpointAddress).CreateChannel();
            RichDocument limsml = new RichDocument();

            limsml.AddUserLogin(username, password);
            limsml.SetResponseType(ResponseType.Data);

            Transaction transaction = limsml.AddTransaction();
            Entity entity = transaction.AddEntity("ACCESS_LOG");

            Action action = entity.AddAction("CREATE_BATCH");

            action.AddParameter("BATCH_ID", batchId);
            action.AddParameter("MATERIAL_ID", materialId);
            action.AddParameter("PLANT_ID", plantId);
            action.AddParameter("PO", po);
            action.AddParameter("ASSET", asset);
            action.AddParameter("SUSPECT", suspect);
            action.AddParameter("AREASEQ", areaSeq);

            string res = await client.ProcessAsync(limsml.GetXml());

            return res;
        }

        public async Task<string> UpdateBatchStatusAsync(string batchId, string status)
        {
            var locationSetting = _Configuration.GetSection("SampleManagerApi");
            if (!locationSetting.Exists())
            {
                throw new Exception("Could not find SampleManager info");
            }
            string endpoint = locationSetting.GetSection("Endpoint").Value;
            string username = locationSetting.GetSection("Username").Value;
            string password = locationSetting.GetSection("Password").Value;


            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(endpoint);
            ILIMSWebService client = new ChannelFactory<ILIMSWebService>(basicHttpBinding, endpointAddress).CreateChannel();
            RichDocument limsml = new RichDocument();

            limsml.AddUserLogin(username, password);
            limsml.SetResponseType(ResponseType.Data);

            Transaction transaction = limsml.AddTransaction();
            Entity entity = transaction.AddEntity("ACCESS_LOG");

            Action action = entity.AddAction("UPDATE_BATCH_STATUS");

            action.AddParameter("BATCH_ID", batchId);
            action.AddParameter("STATUS", status);

            string res = await client.ProcessAsync(limsml.GetXml());

            return res;
        }


        public async Task<string> GetMaterialStockStatus(string materialId)
        {
            var locationSetting = _Configuration.GetSection("SampleManagerApi");
            if (!locationSetting.Exists())
            {
                throw new Exception("Could not find SampleManager info");
            }
            string endpoint = locationSetting.GetSection("Endpoint").Value;
            string username = locationSetting.GetSection("Username").Value;
            string password = locationSetting.GetSection("Password").Value;


            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(endpoint);
            ILIMSWebService client = new ChannelFactory<ILIMSWebService>(basicHttpBinding, endpointAddress).CreateChannel();
            RichDocument limsml = new RichDocument();

            limsml.AddUserLogin(username, password);
            limsml.SetResponseType(ResponseType.System);

            Transaction transaction = limsml.AddTransaction();
            Entity entity = transaction.AddEntity("GENERIC");

            Action action = entity.AddAction("GET_STOCKS_STATUS");

            action.AddParameter("MATERIAL_ID", materialId);

            string res = await client.ProcessAsync(limsml.GetXml());
            XmlDocument doc = new XmlDocument();
            
            doc.LoadXml(res);
            
            var returnValue = doc.SelectSingleNode("//*[@id='RETURN']").InnerText;

            return returnValue;

        }

        public class LIMSWebServiceClient : ClientBase<ILIMSWebService>, ILIMSWebService
        {

            public LIMSWebServiceClient()
            {
            }

            public LIMSWebServiceClient(string endpointConfigurationName) :
                    base(endpointConfigurationName)
            {
            }

            public LIMSWebServiceClient(string endpointConfigurationName, string remoteAddress) :
                    base(endpointConfigurationName, remoteAddress)
            {
            }

            public LIMSWebServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
                    base(endpointConfigurationName, remoteAddress)
            {
            }

            public LIMSWebServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
                    base(binding, remoteAddress)
            {
            }

            public string Process(string request)
            {
                return base.Channel.Process(request);
            }

            public Task<string> ProcessAsync(string request)
            {
                return base.Channel.ProcessAsync(request);
            }

            public string CheckAuth()
            {
                return base.Channel.CheckAuth();
            }

            public Task<string> CheckAuthAsync()
            {
                return base.Channel.CheckAuthAsync();
            }

            public object GetAllLogFiles()
            {
                return base.Channel.GetAllLogFiles();
            }

            public Task<object> GetAllLogFilesAsync()
            {
                return base.Channel.GetAllLogFilesAsync();
            }

            public string EraseAllLogFiles()
            {
                return base.Channel.EraseAllLogFiles();
            }

            public Task<string> EraseAllLogFilesAsync()
            {
                return base.Channel.EraseAllLogFilesAsync();
            }

            public object GetSingleLogFile(string transaction, string direction)
            {
                return base.Channel.GetSingleLogFile(transaction, direction);
            }

            public Task<object> GetSingleLogFileAsync(string transaction, string direction)
            {
                return base.Channel.GetSingleLogFileAsync(transaction, direction);
            }

            public bool LogSOAPMessages(bool logging)
            {
                return base.Channel.LogSOAPMessages(logging);
            }

            public Task<bool> LogSOAPMessagesAsync(bool logging)
            {
                return base.Channel.LogSOAPMessagesAsync(logging);
            }

            public bool ViewLogStatus()
            {
                return base.Channel.ViewLogStatus();
            }

            public Task<bool> ViewLogStatusAsync()
            {
                return base.Channel.ViewLogStatusAsync();
            }
        }
    }
}
