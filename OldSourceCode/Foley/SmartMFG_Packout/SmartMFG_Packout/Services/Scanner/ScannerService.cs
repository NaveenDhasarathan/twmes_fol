﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Scanner;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class ScannerService : IScannerService
    {
        private readonly IConfiguration _configuration;
        private readonly IScannerRepository _scannerRepository;
        private readonly ILIMSService _limsService;
        private readonly IBatchLabelService _batchLabelService;
        private readonly IPrintService _printService;
        public ScannerService(IConfiguration configuration, IScannerRepository scannerRepository, ILIMSService limsService, IBatchLabelService batchLabelService, IPrintService printService)
        {
            _configuration = configuration;
            _batchLabelService = batchLabelService;
            _limsService = limsService;
            _scannerRepository = scannerRepository;
            _printService = printService;
        }

        public async Task<IEnumerable<Shuttle>> GetActiveShuttlesAsync(string? location, string? area)
        {
            var result = await _scannerRepository.GetActiveShuttlesAsync(location, area);
            return result;
        }

        public async Task<Shuttle> CreateActiveShuttleAsync(ShuttleCreateDto shuttleDto)
        {
            try {

                var result = await _scannerRepository.CreateActiveShuttleAsync(shuttleDto);
                return result;
            }
            catch {
                throw;
            }
        }

        public async Task<IEnumerable<ShuttleContent>> GetShuttleContentAsync(int shuttleId)
        {
            var result = await _scannerRepository.GetShuttleContentAsync(shuttleId);
            return result;
        }

        public async Task<ShuttleContent> AddContentToShuttle(int shuttleId, string plant, string serialId, string loader, string user, string blockedReasonText) 
        {
            var batchLabel = await _batchLabelService.GetBatchLabelAsync(serialId);

            if (batchLabel == null) {
                throw new Exception("Serial Id: " + serialId + "could not be found.");
            }

            //var status = await _limsService.GetMaterialStockStatus(batchLabel.MATERIAL_ID.ToString());

            //status = status == "Y" ? "X" : "";

            var result = await ReportingProductionAsync(serialId, shuttleId.ToString(), plant, loader, user);

            return result;
        }

        public async Task<int> RemoveContentFromShuttle(int shuttleId, string serialId, string remover)
        {
            var result = await _scannerRepository.RemoveContentFromShuttle(shuttleId, serialId, remover);
            return result;
        }

        public async Task<Shuttle> FinalizeShuttleAsync(int shuttleId) {
            var result = await _scannerRepository.FinalizeShuttleAsync(shuttleId);
            return result;
        }

        public async Task<IEnumerable<ExternalWhs>> GetExternalWhsAsync(string localPlant) {
            var result = await _scannerRepository.GetExternalWhsAsync(localPlant);
            return result;
        }

        public async Task<IEnumerable<SeaContainerType>> GetSeaContainerTypesAsync() {
            var result = await _scannerRepository.GetSeaContainerTypesAsync();
            return result;
        }

        public async Task<string> PrintShuttleLabelAsync(PrintDTO printDto)
        {
            string printResult = "";

            try
            {
                printResult = await _printService.PrintLabelsAsync(printDto);
            }
            catch (Exception e)
            {
                throw new Exception("Error calling label printer: " + e.Message);
            }

            return printResult;
        }

        public async Task<string> PrintShuttleReportAsync(string PrinterIp, string PrinterName, int ShuttleID)
        {
            IEnumerable<ShuttleReportContents> shuttleReportData = await _scannerRepository.GetShuttleReportData(ShuttleID);
            ShuttleReportLocation shuttleReportLocationData = await _scannerRepository.GetShuttleReportLocationData(ShuttleID);
            ShuttleReportSummary shuttleReportSummary = ShuttleSummary(shuttleReportData);

            DateTime dt = DateTime.Now;
            int PageCharWidth = 80;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string status = "Initialiized";
           
            if (shuttleReportLocationData == null)
            {
                throw new Exception("Unable to find Location Data for Shuttle ID: " + ShuttleID);
            }
            else
            {
                if (shuttleReportLocationData.LOCATION != shuttleReportLocationData.DESTINATION)
                {
                    sb.Append("Ship From:".PadRight(40));
                    if (shuttleReportLocationData.DESTINATION == "1234" || shuttleReportLocationData.DESTINATION == "4321")
                    {
                        sb.AppendLine("Forwarding Agent:");
                        sb.Append(String.Format("{0}", shuttleReportLocationData.FROM_NAME).PadRight(40));
                        sb.AppendLine(String.Format("{0}", "C H POWELL & COMPANY"));
                        sb.AppendLine(String.Format("{0}", "BIWI TECHNOLOGY PART"));
                        sb.Append(String.Format("{0}", shuttleReportLocationData.FROM_STREET).PadRight(40));
                        sb.AppendLine(String.Format("{0}", "605 GLOBAL WAY SUITE 109"));
                        sb.Append(String.Format("{0}", shuttleReportLocationData.FROM_LOCATION).PadRight(40));
                        sb.AppendLine(String.Format("{0}", "LINTHICUM, MD 21090 USA"));
                        sb.Append("\r\n");
                    }
                    else
                    {
                        sb.AppendLine("Ship To:");
                        sb.Append(String.Format("{0}", shuttleReportLocationData.FROM_NAME).PadRight(40));
                        sb.AppendLine(String.Format("{0}", shuttleReportLocationData.TO_NAME.Trim()));
                        sb.Append(String.Format("{0}", shuttleReportLocationData.FROM_STREET).PadRight(40));
                        sb.AppendLine(String.Format("{0}", shuttleReportLocationData.TO_STREET.Trim()));
                        sb.Append(String.Format("{0}", shuttleReportLocationData.FROM_LOCATION).PadRight(40));
                        sb.AppendLine(String.Format("{0}", shuttleReportLocationData.TO_LOCATION.Trim()));
                        sb.Append("\r\n");
                        sb.Append(String.Format("Freight code: {0}", _configuration.GetSection("Freight_Code").Value.PadRight(40)));
                    }
                    sb.Append(String.Format("Shuttle ID: {0}\r\n", ShuttleID.ToString()));
                    sb.Append("\r\n");
                    sb.Append("Attention freight carrier, the above freight code and shuttle ID \r\nmust appear on freight invoice to receive payment.\r\n");
                    sb.Append("\r\n");
                }

                sb.Append(CenteredText(String.Format("NPPQS Packout List for Shuttle ID: {0}, ContainerID: {1}", ShuttleID.ToString(), shuttleReportLocationData.SAP_CONTAINER), PageCharWidth) + "\r\n");
                sb.Append(String.Empty.PadRight(PageCharWidth, '-'));
                sb.Append("\r\n");

                // net_lbs, gross_lbs, load_time
                sb.Append("  ");
                sb.Append(CenteredText("Material", 10));
                sb.Append(CenteredText("Serial ID", 12));
                sb.Append(CenteredText("Batch ID", 11));
                sb.Append(CenteredText("Pkg", 5));
                sb.Append(CenteredText(" Net Lbs", 13));
                sb.Append(CenteredText("Load Time", 11));
                sb.Append(CenteredText("Container", 16));
                sb.Append("\r\n");

                sb.Append("   ");
                sb.Append(String.Empty.PadRight(74, '-') + "\r\n");

                foreach (ShuttleReportContents dr in shuttleReportData)
                {
                    if (dr.STATUS != status)
                    {
                        status = dr.STATUS;
                        if (status == "CL")
                        {
                            sb.Append(" First Time Scans:\r\n");
                        }
                        else if (status == "PL")
                        {
                            sb.Append(" Previous Scans:\r\n");
                        }
                        else if (status == "RM")
                        {
                            sb.Append(" Scans removed from this Shuttle run:\r\n");
                        }
                        else
                        {
                            sb.Append(" Unknown:\r\n");
                        }
                    }

                    sb.Append("  ");
                    sb.Append(CenteredText(dr.MATERIAL_ID.ToString(), 10));
                    sb.Append(CenteredText(dr.SERIAL_ID.ToString(), 12));
                    sb.Append(CenteredText(dr.BATCH_ID, 11));
                    sb.Append(CenteredText(dr.BATCH_CONTAINER_NUM.ToString().PadLeft(3), 5));
                    try
                    {
                        sb.Append(CenteredText(((double)(dr.NET_LBS)).ToString("F3"), 13));
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                    }
                    sb.Append(CenteredText(((DateTime)(dr.CURRENT_LOAD_TIME)).ToString("MM/dd HH:mm"), 11));
                    sb.Append(CenteredText(dr.EXC_SAP_CONTAINER.ToString(), 16) + "\r\n");
                }

                sb.Append("   ");
                sb.Append(String.Empty.PadRight(74, '-') + "\r\n");

                sb.Append(String.Format("   Number of Packages: {0}\r\n", shuttleReportSummary.packageCount.ToString()));
                sb.Append(String.Format("     Net Pounds Total: {0}\r\n", shuttleReportSummary.netLbs.ToString("F3")));
                sb.Append(String.Format("   Gross Pounds Total: {0}\r\n", shuttleReportSummary.grossLbs.ToString("F3")));
                sb.Append(String.Empty.PadRight(PageCharWidth, '-') + "\r\n\r\n");
                sb.Append("Checked By:____________________________ Received By:____________________________".PadLeft(PageCharWidth, ' ') + "\r\n");

                if (shuttleReportLocationData.LOCATION != shuttleReportLocationData.DESTINATION)
                {
                    sb.Append("\r\nTrailer ID:______________________________ Seal ID:______________________________".PadLeft(PageCharWidth, ' ') + "\r\n\r\n");
                    sb.Append("Driver Signature:______________________________ Date: __________________________".PadLeft(PageCharWidth, ' ') + "\r\n");
                }
                sb.Append(CenteredText("Printed: " + DateTime.Now.ToString("MM/dd/yyyy HH:mm"), PageCharWidth));

            }

            IPAddress ipaddress = IPAddress.Parse(PrinterIp);
            IPEndPoint ipEndpoint = new IPEndPoint(ipaddress, 9100);

             //SOI.Printers.NetPrinter.Print(PrinterIp, 9100, sb.ToString());
            string result = await _printService.PrintShuttlePickListAsync(ipEndpoint,sb.ToString());
            await PrintContainerPackList(ShuttleID, PrinterName);

            return result;

        }

        public async Task PrintContainerPackList(int ShuttleID, string SapPrinterName)
        {
            IEnumerable<ShuttleSapPrintInfo> shuttleSapPrintInfoList= await _scannerRepository.GetShuttleReportSapData(ShuttleID);

            string printDublicateWhsFlag = _configuration.GetSection("DUPLICATE_PRINT_TO_WHS").Value;
            string whsShippingSapPrinter = _configuration.GetSection("WhsShippingSapPrinter").Value;


            foreach (ShuttleSapPrintInfo dr in shuttleSapPrintInfoList)
            {
                ERP.PrintContainerPackList(dr.warehouse, dr.storage_type, dr.sap_container, SapPrinterName, dr.start_date, dr.end_date);

                if (printDublicateWhsFlag.ToUpper() == "TRUE")
                {
                    ERP.PrintContainerPackList(dr.warehouse, dr.storage_type, dr.sap_container, whsShippingSapPrinter, dr.start_date, dr.end_date);
                }
            }
        }

        public async Task<IEnumerable<ShuttleArea>> GetShuttleAreasAsync(string? plant) {
            var result = await _scannerRepository.GetShuttleAreasAsync(plant);
            return result;
        }

        public async Task<IEnumerable<ShuttleContainer>> GetShuttleContainersAsync()
        {
            var result = await _scannerRepository.GetShuttleContainersAsync();
            return result;
        }

        public async Task<ShuttleContent> ReportingProductionAsync(string serialId, string shuttleId, string plant, string loader, string username)
        {
            string baseUrl = _configuration.GetSection("SapApiBaseUrl").Value;

            var rtpIssueDto = new JObject(
                                    new JProperty("SERIALID", serialId.ToString()),
                                    new JProperty("SHUTTLEID", shuttleId),
                                    new JProperty("USER", username),
                                    new JProperty("LOADER", username),
                                    new JProperty("PLANT", plant)
                                );

            ShuttleContent result;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var httpContent = new StringContent(JsonConvert.SerializeObject(rtpIssueDto), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync(baseUrl + "values/POST/PRODUCTION", httpContent);

                    response.EnsureSuccessStatusCode();

                    using (HttpContent content = response.Content)
                    {
                        Task<string> res = content.ReadAsStringAsync();
                        result = new ShuttleContent(JObject.Parse(res.Result)); 
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Unable to report production to TPMS for serial id: " + e.Message);
            }

            return result;

        }

        public async Task<bool> ProcessRtpIssueAsync(int serialId, string username)
        {
            string baseUrl = _configuration.GetSection("SapApiBaseUrl").Value;

            var rtpIssueDto = new JObject(
                                    new JProperty("SERIALID", serialId.ToString()),
                                    new JProperty("SCANNERID", username),
                                    new JProperty("RTPACTION", "ISSUE")
                                );

            var httpContent = new StringContent(JsonConvert.SerializeObject(rtpIssueDto), Encoding.UTF8, "application/json");
            bool result = false;
            try
            {

                    HttpClient client = new HttpClient();
                    HttpResponseMessage response = await client.PostAsync(
                        baseUrl + "values/RTP/ISSUE", httpContent);

                response.EnsureSuccessStatusCode();

                result = true;

            }
            catch (Exception e)
            {
                throw new Exception("Unable change RTP ISSUE serial id: " + e.Message);
            }

            return await Task.FromResult(result);

        }

        public async Task<bool> ProcessRtpCancelAsync(int serialId, string username)
        {
            string baseUrl = _configuration.GetSection("SapApiBaseUrl").Value;

            var rtpCancelDto = new JObject(
                                    new JProperty("SERIALID", serialId.ToString()),
                                    new JProperty("SCANNERID", username),
                                    new JProperty("RTPACTION", "CANCEL")
                                );

            var httpContent = new StringContent(JsonConvert.SerializeObject(rtpCancelDto), Encoding.UTF8, "application/json");
            bool result = false;
            try
            {

                using (HttpClient client = new HttpClient()) 
                { 
                
                    HttpResponseMessage response = await client.PostAsync(
                        baseUrl + "values/RTP/CANCEL", httpContent);

                    response.EnsureSuccessStatusCode();

                    result = true;
                }

            }
            catch (Exception e)
            {
                throw new Exception("Unable change RTP ISSUE serial id: " + e.Message);
            }

            return await Task.FromResult(result);
        }

        public async Task<IEnumerable<RtpHistory>> GetRtpHistoryAsync(string scannerId, string rtpAction)
        {
            string baseUrl = _configuration.GetSection("SapApiBaseUrl").Value;

            var rtpHistoryDto = new JObject(
                                    new JProperty("SCANNERID", scannerId.ToString()),
                                    new JProperty("RTPACTION", rtpAction.ToUpper())
                                );

            IEnumerable<RtpHistory> result;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var httpContent = new StringContent(JsonConvert.SerializeObject(rtpHistoryDto), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync(baseUrl + "values/RTP/HISTORY", httpContent);

                    response.EnsureSuccessStatusCode();

                    using (HttpContent content = response.Content)
                    {
                        Task<string> res = content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<IEnumerable<RtpHistory>>(res.Result);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Unable get RTP HISTORY for serial id: " + e.Message);
            }

            return result;
        }

        public async Task<bool> ProcessRepackageAsync(int serialId, int plant, int po, string batch, string username)
        {
            string baseUrl = _configuration.GetSection("SapApiBaseUrl").Value;

            var repackageDto = new JObject(
                                    new JProperty("SERIALID", serialId.ToString()),
                                    new JProperty("PROCESS_ORDER", po.ToString()),
                                    new JProperty("BATCH", batch),
                                    new JProperty("SCANNERID", username),
                                    new JProperty("PLANT", plant.ToString())
                                );

            var httpContent = new StringContent(JsonConvert.SerializeObject(repackageDto), Encoding.UTF8, "application/json");

            bool result = false;

            try
            {

                using (HttpClient client = new HttpClient())
                {

                    HttpResponseMessage response = await client.PostAsync(baseUrl + "values/REPACK", httpContent);

                    response.EnsureSuccessStatusCode();

                    result = true;
                }

            }
            catch (Exception e)
            {
                throw new Exception("Unable to Re-Package serial id: " + e.Message);
            }

            return await Task.FromResult(result);
        }

        public async Task<IEnumerable<RepackageHistory>> GetRepackageHistoryAsync(int po, string batch)
        {
            string baseUrl = _configuration.GetSection("SapApiBaseUrl").Value;

            var repackageHistoryDto = new JObject(
                                    new JProperty("PROCESS_ORDER", po.ToString()),
                                    new JProperty("BATCH", batch)
                                );

            var httpContent = new StringContent(JsonConvert.SerializeObject(repackageHistoryDto), Encoding.UTF8, "application/json");

            IEnumerable<RepackageHistory> result;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    
                    HttpResponseMessage response = await client.PostAsync(baseUrl + "values/GETINFOPROCESSBATCH", httpContent);

                    response.EnsureSuccessStatusCode();

                    using (HttpContent content = response.Content)
                    {
                        Task<string> res = content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<IEnumerable<RepackageHistory>>(res.Result);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Unable to get REPACKAGE HISTORY: " + e.Message);
            }

            return result;
        }

        public async Task<bool> CheckSerialIdIsValidRepackageAsync(int serialId)
        {
            string baseUrl = _configuration.GetSection("SapApiBaseUrl").Value;

            var repackageHistoryDto = new JObject(
                                    new JProperty("SERIALID", serialId.ToString())
                                );

            var httpContent = new StringContent(JsonConvert.SerializeObject(repackageHistoryDto), Encoding.UTF8, "application/json");

            BatchLabel result;

            try
            {
                using (HttpClient client = new HttpClient())
                {

                    HttpResponseMessage response = await client.PostAsync(baseUrl + "values/GETINFOBYSERIALID", httpContent);

                    response.EnsureSuccessStatusCode();

                    using (HttpContent content = response.Content)
                    {
                        Task<string> res = content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<BatchLabel>(res.Result);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Unable to validate serial id: " + e.Message);
            }

            return result == null ? true: false;
        }

        private string CenteredText(string text, int length)
        {
            text = text == null ? "" : text;

            string result = text;

            if (text.Length <= length)
            {
                result = text.PadRight((length - text.Length) / 2 + text.Length).PadLeft(length, ' ');
            }

            return result;
        }

        private ShuttleReportSummary ShuttleSummary(IEnumerable<ShuttleReportContents> shuttleReportData)
        {

            ShuttleReportSummary reportSummary = new ShuttleReportSummary();

            if (shuttleReportData.Count() > 0)
            {
                reportSummary.sapContainer = shuttleReportData.First().SAP_CONTAINER;
            }

            foreach (ShuttleReportContents dr in shuttleReportData)
            {
                if ((dr.STATUS== "CL") || (dr.STATUS == "PL"))
                {
                    reportSummary.netLbs += dr.NET_LBS;
                    reportSummary.grossLbs += dr.GROSS_LBS;
                    reportSummary.packageCount++;
                }
            }

            return reportSummary;
        }
    }
}
