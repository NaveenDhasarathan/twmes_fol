﻿using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Repositories.Abstract;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class AdminPrinterService : IAdminPrinterService
    {
        private readonly IAdminPrinterRepository _PrinterRepository;
        //IConfiguration _configuration;

        public AdminPrinterService(IAdminPrinterRepository PrinterRepository)
        {
            _PrinterRepository = PrinterRepository;
        }

        public async Task<Printer> GetPrintersAsync(int? areaSeq)
        {
            var result = await _PrinterRepository.GetPrintersAsync(areaSeq);
            return result;
        }

        public async Task<int> UpdatePrintersAsync(Printer Info)
        {
            var result = await _PrinterRepository.UpdatePrintersAsync(Info);
            return result;
        }

        public async Task<int> InsertPrintersAsync(Printer Info)
        {
            var result = await _PrinterRepository.InsertPrintersAsync(Info);
            return result;
        }

        public async Task<int> DeletePrintersAsync(string api_Printer_Name)
        {
            var result = await _PrinterRepository.DeletePrintersAsync(api_Printer_Name);
            return result;
        }
    }
}
