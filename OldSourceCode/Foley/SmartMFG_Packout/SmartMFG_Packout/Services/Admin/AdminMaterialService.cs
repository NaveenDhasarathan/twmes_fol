﻿using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Admin;
using SmartMFG_Packout.Repositories.Abstract;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class AdminMaterialService : IAdminMaterialService
    {
        private readonly IAdminMaterialRepository _MaterialRepository;

        public AdminMaterialService(IAdminMaterialRepository UserRepository)
        {
            _MaterialRepository = UserRepository;
        }

        public async Task<IEnumerable<Material>> GetMaterialsAsync()
        {
            var result = await _MaterialRepository.GetMaterialsAsync();
            return result;
        }

        public async Task<Material> GetMaterialAsync(int materialId)
        {
            var result = await _MaterialRepository.GetMaterialAsync(materialId);
            return result;
        }

        public async Task<Material> InsertMaterialAsync(MaterialDto materialDto)
        {
            Material newMaterial = new Material();
            try
            {
                newMaterial = await _MaterialRepository.UpsertMaterialAsync(materialDto.MATERIAL, "INSERT");
            }
            catch(Exception e) {
                throw new Exception("Error. Unable to add Material.");
            }

            try
            {
                if (materialDto.MATERIAL_LOCATIONS.Count > 0) {
                    await _MaterialRepository.InsertMaterialLocationsAsync(materialDto.MATERIAL_LOCATIONS);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error. Unable to add Material Locations.");
            }

            return newMaterial;
        }

        public async Task<Material> UpdateMaterialAsync(Material material)
        {
            var result = await _MaterialRepository.UpsertMaterialAsync(material,"UPDATE");
            return result;
        }

        public async Task<IEnumerable<MaterialLocation>> GetMaterialLocationsAsync(int materialId)
        {
            var result = await _MaterialRepository.GetMaterialLocationsAsync(materialId);
            return result;
        }

        public async Task<int> InsertMaterialLocationsAsync(List<MaterialLocation> materialLocations)
        {
            var result = await _MaterialRepository.InsertMaterialLocationsAsync(materialLocations);
            return result;
        }

        public async Task<int> DeleteMaterialLocationsAsync(List<MaterialLocation> materialLocations)
        {
            var result = await _MaterialRepository.DeleteMaterialLocationsAsync(materialLocations);
            return result;
        }

        public async Task<IEnumerable<MaterialLabelTemplate>> GetMaterialLabelTemplatesAsync()
        {
            var result = await _MaterialRepository.GetMaterialLabelTemplatesAsync();
            return result;
        }

        public async Task<IEnumerable<MaterialProductionUnit>> GetMaterialProductionUnitsAsync()
        {
            var result = await _MaterialRepository.GetMaterialProductionUnitsAsync();
            return result;
        }
        

        public async Task<IEnumerable<ContainerType>> GetContainerTypesAsync()
        {
            var result = await _MaterialRepository.GetContainerTypesAsync();
            return result;
        }
        

    }
}
