﻿using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IAdminMaterialService
    {
        Task<IEnumerable<Material>> GetMaterialsAsync();
        Task<Material> GetMaterialAsync(int materialId);
        Task<Material> InsertMaterialAsync(MaterialDto materialDto);
        Task<Material> UpdateMaterialAsync(Material material);
        Task<IEnumerable<MaterialLocation>> GetMaterialLocationsAsync(int materialId);
        Task<int> InsertMaterialLocationsAsync(List<MaterialLocation> materialLocations);
        Task<int> DeleteMaterialLocationsAsync(List<MaterialLocation> materialLocations);
        Task<IEnumerable<MaterialLabelTemplate>> GetMaterialLabelTemplatesAsync();
        Task<IEnumerable<MaterialProductionUnit>> GetMaterialProductionUnitsAsync();
        Task<IEnumerable<ContainerType>> GetContainerTypesAsync();
    }
}
