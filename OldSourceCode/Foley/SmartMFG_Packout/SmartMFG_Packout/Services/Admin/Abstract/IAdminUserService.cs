﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IAdminUserService
    {
        Task<IEnumerable<UserProfile>> GetUsersAsync();
        Task<UserProfile> GetUserAsync(string userId);
        Task<bool> UpdateUserAsync(UserProfile userProfile);
        Task<bool> InsertUsersAsync(UserProfile userProfile);
        Task<int> DeleteUsersAsync(string username);
        Task<IEnumerable<Claim>> GetClaimsAsync(string plantId);
        Task<IEnumerable<Role>> GetUserRolesAsync(string plantId);
        Task<IEnumerable<RoleClaim>> GetUserRoleClaimsAsync();
    }
}
