﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IAdminPrinterService
    {
        Task<Printer> GetPrintersAsync(int? areaSeq);
        Task<int> UpdatePrintersAsync(Printer Info);
        Task<int> InsertPrintersAsync(Printer Info);
        Task<int> DeletePrintersAsync(string api_Printer_Name);
    }
}
