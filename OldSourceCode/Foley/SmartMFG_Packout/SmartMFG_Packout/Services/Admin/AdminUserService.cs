﻿using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Repositories.Abstract;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class AdminUserService : IAdminUserService
    {
        private readonly IAdminUserRepository _UserRepository;

        public AdminUserService(IAdminUserRepository UserRepository)
        {
            _UserRepository = UserRepository;
        }

        public async Task<IEnumerable<UserProfile>> GetUsersAsync()
        {
            var result = await _UserRepository.GetUsersAsync();
            return result;
        }

        public async Task<UserProfile> GetUserAsync(string userId)
        {
            var profile = await _UserRepository.GetUserAsync(userId);
            profile.CLAIMS = await _UserRepository.GetUserClaimsAsync(userId);

            return profile;
        }

        public async Task<bool> UpdateUserAsync(UserProfile userProfile)
        {

            try
            {
                await _UserRepository.UpdateUsersAsync(userProfile);
            }
            catch
            {
                throw new Exception("Error. Unable to update user profile");
            }

            try
            {
                var result = await UpdateUserClaimsAsync(userProfile);

                if (!result) {
                    throw new Exception("Error. Unable to update user claims for profile.");
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error. Unable to update user claims for profile.");
            }

            return true;
        }

        public async Task<bool> UpdateUserClaimsAsync(UserProfile updatedUserProfile) 
        {
            IEnumerable<UserClaim> currentClaims = await _UserRepository.GetUserClaimsAsync(updatedUserProfile.USER_ID);

            List<dynamic> claimsToRemove = new List<dynamic>();
            List<dynamic> claimsToAdd = new List<dynamic>();

            try {
                foreach (UserClaim uc in updatedUserProfile.CLAIMS)
                {
                    if (!currentClaims.Any(c => c.CLAIM_ID == uc.CLAIM_ID))
                    {
                        dynamic userclaim = new ExpandoObject();
                        userclaim.USER_ID = uc.USER_ID.ToUpper();
                        userclaim.CLAIM_ID = uc.CLAIM_ID;
                        userclaim.CLAIM_VALUE = uc.CLAIM_VALUE;

                        //if it does no exist add
                        claimsToAdd.Add(userclaim);
                    }
                }

                await _UserRepository.InsertUserClaimsAsync(claimsToAdd);
            }
            catch {
                return false;
            }
            

            try {

                foreach (UserClaim uc in currentClaims)
                {

                    if (!updatedUserProfile.CLAIMS.Any(c => c.CLAIM_ID == uc.CLAIM_ID))
                    {
                        //if missing remove
                        dynamic userclaim = new ExpandoObject();
                        userclaim.USER_ID = uc.USER_ID.ToUpper();
                        userclaim.CLAIM_ID = uc.CLAIM_ID;

                        claimsToRemove.Add(userclaim);
                    }
                }

                await _UserRepository.RemoveUserClaimsAsync(claimsToRemove);
            }
            catch {
                return false;
            }


            return true;

        }

        public async Task<bool> InsertUsersAsync(UserProfile userProfile)
        {
            //normalize username 
            //add claims
            try {
                userProfile.USER_ID = userProfile.USER_ID.ToUpper();
                await _UserRepository.InsertUsersAsync(userProfile);
            }
            catch {
                throw new Exception("Error. Unable to create user profile");
            }

            try {

                List<dynamic> userClaims = new List<dynamic>();

                foreach (UserClaim uc in userProfile.CLAIMS) {
                    
                    dynamic userclaim = new ExpandoObject();
                    userclaim.USER_ID = uc.USER_ID.ToUpper();
                    userclaim.CLAIM_ID = uc.CLAIM_ID;
                    userclaim.CLAIM_VALUE = uc.CLAIM_VALUE;
                    
                    userClaims.Add(userclaim);
                }

                await _UserRepository.InsertUserClaimsAsync(userClaims);
            }
            catch(Exception e) {
                throw new Exception("Error. Unable to create user claims for profile.");
            }

            return true;
        }

        public async Task<int> DeleteUsersAsync(string username)
        {
            var result = await _UserRepository.DeleteUsersAsync(username);
            return result;
        }

        public async Task<IEnumerable<Claim>> GetClaimsAsync(string plantId)
        {
            var result = await _UserRepository.GetClaimsAsync(plantId);
            return result;
        }

        public async Task<IEnumerable<Role>> GetUserRolesAsync(string plantId)
        {
            var result = await _UserRepository.GetUserRolesAsync(plantId);
            return result;
        }

        public async Task<IEnumerable<RoleClaim>> GetUserRoleClaimsAsync()
        {
            var result = await _UserRepository.GetUserRoleClaimsAsync();
            return result;
        }
    }
}
