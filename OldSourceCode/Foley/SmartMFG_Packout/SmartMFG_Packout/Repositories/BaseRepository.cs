﻿
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SmartMFG_Packout.Repositories
{
    public class BaseRepository
    {
        IConfiguration _configuration;
        public BaseRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected IDbConnection CreateConnection(string section)
        {
            var connectionType = _configuration.GetSection("ConnectionStrings").GetSection(section).GetSection("Type").Value;
            var connectionString = _configuration.GetSection("ConnectionStrings").GetSection(section).GetSection("ConnectionString").Value;

            if (connectionType == "Sql")
            {
                var connection = new SqlConnection(connectionString);
                return connection;
            }
            else if (connectionType == "Oracle")
            {
                var connection = new OracleConnection(connectionString);
                return connection;
            }
            else
            {
                throw new ArgumentException("Incorrect connection type must be Sql or Oracle", "connectionType");
            }
        }
    }
}
