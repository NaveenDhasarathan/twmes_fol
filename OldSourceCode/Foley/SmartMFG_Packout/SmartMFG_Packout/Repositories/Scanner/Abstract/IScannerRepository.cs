﻿using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Scanner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IScannerRepository
    {
        Task<IEnumerable<Shuttle>> GetActiveShuttlesAsync(string? location, string? area);
        Task<Shuttle> CreateActiveShuttleAsync(ShuttleCreateDto shuttleDto);
        Task<IEnumerable<ShuttleContent>> GetShuttleContentAsync(int shuttleId);
        Task<ShuttleContent> AddContentToShuttle(int shuttleId, string serialId, string loader, string user, string blockedReasonText, string status);
        Task<int> RemoveContentFromShuttle(int shuttleId, string serialId, string remover);
        Task<IEnumerable<SeaContainerType>> GetSeaContainerTypesAsync();
        Task<Shuttle> FinalizeShuttleAsync(int shuttleId);
        Task<IEnumerable<ExternalWhs>> GetExternalWhsAsync(string localPlant);
        Task<IEnumerable<ShuttleReportContents>> GetShuttleReportData(int shuttleId);
        Task<ShuttleReportLocation> GetShuttleReportLocationData(int shuttleId);
        Task<IEnumerable<ShuttleSapPrintInfo>> GetShuttleReportSapData(int shuttleId);
        Task<IEnumerable<ShuttleArea>> GetShuttleAreasAsync(string? plant);
        Task<IEnumerable<ShuttleContainer>> GetShuttleContainersAsync();
    }
}
