﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Scanner;
using SmartMFG_Packout.Repositories;
using SmartMFG_Packout.Services.Abstract;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class ScannerRepository: BaseRepository, IScannerRepository
    {
        private readonly IConfiguration _Configuration;

        private readonly string DataBase = "SmartMFG_Packout_DB";

        public ScannerRepository(IConfiguration Configuration) : base(Configuration)
        {
            _Configuration = Configuration;
        }

        public async Task<IEnumerable<Shuttle>> GetActiveShuttlesAsync(string? location, string? area)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Location", location);
                parameters.Add("@Area", area);

                string sql = @"SELECT *
                          FROM [SHUTTLE_LOADS]
                          WHERE (@Location IS NULL OR [LOCATION] = @Location)
                          AND (@Area IS NULL OR [AREA] = @Area)
                          AND FINALIZED_DATE IS NULL
                          AND SHUTTLE_ID > 0";

                return await con.QueryAsync<Shuttle>(sql, parameters);
            }
        }

        public async Task<Shuttle> CreateActiveShuttleAsync(ShuttleCreateDto shuttleDto)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_LOCATION", shuttleDto.LOCATION);
                parameters.Add("@P_AREA", shuttleDto.AREA);
                parameters.Add("@P_DESTINATION", shuttleDto.DEST_PLANT);
                parameters.Add("@P_CONTAINER_ID", shuttleDto.CONTAINER);
                parameters.Add("@P_AUTHORIZER", shuttleDto.AUTHORIZER);
                parameters.Add("@P_STORAGE_TYPE", shuttleDto.STORAGE_TYPE);
                parameters.Add("@P_SEAL", shuttleDto.SEAL);
                parameters.Add("@P_CHECK_DIGIT", shuttleDto.CHK_DIGIT);
                parameters.Add("@P_DELIVERY_NO", shuttleDto.DELIVERY_NO);
                parameters.Add("@P_SHIPMENT_NO", shuttleDto.SHIPMENT_NO);

                string sql = @"[Start_Shuttle_Run]";

                return await con.QueryFirstOrDefaultAsync<Shuttle>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async  Task<IEnumerable<ShuttleContent>> GetShuttleContentAsync(int shuttleId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@ShuttleId", shuttleId);

                string sql = @"SELECT *
                          FROM [VW_SHUTTLE_CONTENT]
                          WHERE  [SHUTTLE_ID] = @ShuttleId";

                return await con.QueryAsync<ShuttleContent>(sql, parameters);
            }
        }

        public async Task<ShuttleContent> AddContentToShuttle(int shuttleId, string serialId, string loader, string user, string blockedReasonText, string status)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_SHUTTLE_ID", shuttleId);
                parameters.Add("@P_SERIAL_ID", serialId);
                parameters.Add("@P_LOADER", loader);
                parameters.Add("@P_USERID", user);
                parameters.Add("@P_BlockedReasonText", blockedReasonText);
                parameters.Add("@P_Status", status);

                string sql = @"[ADD_SERIAL_TO_SHUTTLE_RUN]";

                return await con.QueryFirstOrDefaultAsync<ShuttleContent>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<int> RemoveContentFromShuttle(int shuttleId, string serialId, string remover)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_SHUTTLE_ID", shuttleId);
                parameters.Add("@P_SERIAL_ID", serialId);
                parameters.Add("@P_REMOVER", remover);

                string sql = @"[REMOVE_SERIAL_FROM_SHUTTLE_RUN]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SeaContainerType>> GetSeaContainerTypesAsync()
        {
            using (var con = CreateConnection(DataBase))
            {
                string sql = @"SELECT DISTINCT(STORAGE_TYPE_ID), STORAGE_DESCRIPTION FROM PLANT_PROD_PARMS WHERE STORAGE_TYPE_ID <> 'DEF' ORDER BY 2 DESC";

                return await con.QueryAsync<SeaContainerType>(sql);
            }
        }

        public async Task<Shuttle> FinalizeShuttleAsync(int shuttleId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_SHUTTLE_ID", shuttleId);

                string sql = @"[Finalize_Shuttle_Run]";

                return await con.QueryFirstOrDefaultAsync<Shuttle>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ExternalWhs>> GetExternalWhsAsync(string localPlant)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LOCAL_PLANT", localPlant);

                string sql = @"SELECT PLANT, NAME FROM WAREHOUSES WHERE PLANT != @LOCAL_PLANT ORDER BY NAME";

                return await con.QueryAsync<ExternalWhs>(sql, parameters);
            }
        }

        public async Task<IEnumerable<ShuttleReportContents>> GetShuttleReportData(int shuttleId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_SHUTTLE_ID", shuttleId);

                string sql = @"[SHUTTLE_REPORT_DATA]";

                return await con.QueryAsync<ShuttleReportContents>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<ShuttleReportLocation> GetShuttleReportLocationData(int shuttleId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_SHUTTLE_ID", shuttleId);

                string sql = @"[SHUTTLE_REPORT_LOCATION_DATA]";

                return await con.QueryFirstOrDefaultAsync<ShuttleReportLocation>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ShuttleSapPrintInfo>> GetShuttleReportSapData(int shuttleId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_SHUTTLE_ID", shuttleId);

                string sql = @"[GET_SAP_PRINT_INFO]";

                return await con.QueryAsync<ShuttleSapPrintInfo>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ShuttleArea>> GetShuttleAreasAsync(string ?plant)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Plant", plant);

                string sql = @"SELECT *
                          FROM [SHUTTLE_AREAS]
                          WHERE (@Plant IS NULL OR [PLANT] = @Plant)";

                return await con.QueryAsync<ShuttleArea>(sql, parameters);
            }
        }

        public async Task<IEnumerable<ShuttleContainer>> GetShuttleContainersAsync()
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"SELECT * FROM [SHUTTLE_CONTAINERS]";

                return await con.QueryAsync<ShuttleContainer>(sql);
            }
        }
    }
}
