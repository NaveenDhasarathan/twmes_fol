﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Repositories;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class BatchLabelRepository : BaseRepository, IBatchLabelRepository
    {
        private readonly IConfiguration _Configuration;

        private readonly string DataBase = "SmartMFG_Packout_DB";

        public BatchLabelRepository(IConfiguration Configuration) : base(Configuration)
        {
            _Configuration = Configuration;
        }

        public async Task<BatchLabel> GetBatchLabelAsync(string serialId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@SERIAL_ID", serialId);

                string sql = @"SELECT * FROM [VW_BATCH_LABELS] WHERE [SERIAL_ID] = @SERIAL_ID";

                return await con.QueryFirstOrDefaultAsync<BatchLabel>(sql, parameters);
            }
        }
        public async Task<IEnumerable<BatchLabel>> GetBatchLabelsAsync(string batchId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@BATCH_ID", batchId);

                string sql = @"SELECT * FROM [VW_BATCH_LABELS] WHERE [BATCH_ID] = @BATCH_ID";

                return await con.QueryAsync<BatchLabel>(sql, parameters);
            }
        }

        public async Task<BatchLabel> CreateBatchLabelAsync(string batchId, string user)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_BATCH_ID", batchId);
                parameters.Add("@P_USER", user);

                string sql = @"[ISSUE_NEW_SERIAL_ADD_LABELS]";

                return await con.QueryFirstOrDefaultAsync<BatchLabel>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<int> UpdateBatchLabelCountAsync(string batchId, int labelCount, string user)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_BATCH_ID", batchId);
                parameters.Add("@P_LABEL_COUNT", labelCount);
                parameters.Add("@P_USER", user);

                string sql = @"[BATCH_UPDATE_LABELS]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<int> UpdateBatchLabelAsync(BatchLabel batchLabel)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_SERIAL", batchLabel.SERIAL_ID);
                parameters.Add("@P_PALLET_LOAD", batchLabel.NET_LBS);
                parameters.Add("@P_USER", batchLabel.MODIFIED_USER);
                string sql = @"[UPDATE_PALLET_WEIGHT_LBS]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<int> PrintBatchLabelAudit(string serialId, string user)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_SERIAL", serialId);
                parameters.Add("@P_USER", user);
                string sql = @"[PRINT_LABEL_AUDIT]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

    }
}
