﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Repositories;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class ProcessOrderRepository: BaseRepository, IProcessOrderRepository
    {
        private readonly IConfiguration _Configuration;

        private readonly string DataBase = "SmartMFG_Packout_DB";

        public ProcessOrderRepository(IConfiguration Configuration) : base(Configuration)
        {
            _Configuration = Configuration;
        }
        public async Task<IEnumerable<ProcessOrder>> GetProcessOrderListAsync(ProcessorOrderFilter filter)
        {
            using (var con = CreateConnection(DataBase))
            {

                var sql = new StringBuilder(@"SELECT TOP (case ISNULL(@LIMIT, 0) when 0 then 2147483647 else @LIMIT end) * 
                              FROM [VW_PROCESS_ORDER]
                              WHERE (@PROCESS_ORDER IS NULL OR CAST(PROCESS_ORDER_ID as NVARCHAR(10)) LIKE @PROCESS_ORDER + '%')
                              AND ((@START_DATE IS NULL OR [PO_START_DATE] >= @START_DATE) AND (@END_DATE IS NULL OR [PO_START_DATE] <= @END_DATE))
                              AND (@STATION_IDS IS NULL OR [STATION_SEQ] IN @STATION_IDS)
                              AND (@AREA_IDS IS NULL OR [AREA_SEQ] IN @AREA_IDS)
                              AND (@GRADE_IND IS NULL OR [GRADE_IND] = @GRADE_IND)
                              AND (@PROD_SPEC_CODE IS NULL OR [PROD_SPEC_CODE] = @PROD_SPEC_CODE) ");

                //work around for nullable IN statement in dapper
                if (filter.STATUS_IDS != null && filter.STATUS_IDS.Any())
                {
                    sql.Append("AND [PO_STATUS_ID] IN @STATUS_IDS ");
                }

                sql.Append("ORDER BY [PO_START_DATE] DESC ");

                return await con.QueryAsync<ProcessOrder>(sql.ToString(), filter);
            }
        }

        public async Task<ProcessOrder> GetProcessOrderAsync(int po)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PO", po);

                string sql = @"SELECT *
                          FROM [VW_PROCESS_ORDER]
                          WHERE [PROCESS_ORDER_ID] = @PO";

                return await con.QueryFirstOrDefaultAsync<ProcessOrder>(sql, parameters);
            }
        }

        public async Task<int> UpdateProcessOrderStatusAsync(int processOrderId, string username, int newStatus)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_PROCESS_ORDER_ID", processOrderId);
                parameters.Add("@P_USER", username);
                parameters.Add("@P_STATUS", newStatus);

                string sql = @"[CHANGE_PROCESS_ORDER_STATUS]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

    }
}
