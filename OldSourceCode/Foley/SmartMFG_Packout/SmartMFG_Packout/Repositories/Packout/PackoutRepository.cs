﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Repositories;
using SmartMFG_Packout.Services.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class PackoutRepository: BaseRepository, IPackoutRepository
    {
        private readonly IConfiguration _Configuration;

        private readonly string DataBase = "SmartMFG_Packout_DB";

        public PackoutRepository(IConfiguration Configuration) : base(Configuration)
        {
            _Configuration = Configuration;
        }

        //BATCH MODES
        public async Task<IEnumerable<BatchMode>> GetPackoutBatchModesAsync()
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();

                string sql = @"SELECT *
                          FROM [BATCH_MODES]";

                return await con.QueryAsync<BatchMode>(sql, parameters);
            }
        }

        //AREAS
        public async Task<IEnumerable<PackoutArea>> GetPackoutAreasAsync(int plant)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PLANT", plant);

                string sql = @"SELECT *
                          FROM [PACKOUT_AREAS]
                          WHERE [PLANT] = @PLANT";

                return await con.QueryAsync<PackoutArea>(sql, parameters);
            }
        }

        //STATIONS
        public async Task<IEnumerable<PackoutStation>> GetPackoutStationsAsync(int areaSeq)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@AREA_SEQ", areaSeq);

                string sql = @"SELECT *
                          FROM [PACKOUT_AREA_STATIONS]
                          WHERE [AREA_SEQ] = @AREA_SEQ";

                return await con.QueryAsync<PackoutStation>(sql, parameters);
            }
        }

        //SOURCES
        public async Task<IEnumerable<PackoutSource>> GetPackoutSourcesAsync(int areaSeq, int stationSeq)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@AREA_SEQ", areaSeq);
                parameters.Add("@STATION_SEQ", stationSeq);

                string sql = @"SELECT *
                          FROM [VW_PACKOUT_SOURCE]
                          WHERE [AREA_SEQ] = @AREA_SEQ
                          AND [STATION_SEQ] = @STATION_SEQ";

                return await con.QueryAsync<PackoutSource>(sql, parameters);
            }
        }

        //LOCATIONS
        public async Task<IEnumerable<Location>> GetLocationsAsync()
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"SELECT *
                          FROM [LOCATIONS]
                          WHERE [ACTIVE] = 1";

                return await con.QueryAsync<Location>(sql);
            }
        }

        //PWASTE AREAS
        public async Task<IEnumerable<PwasteArea>> GetPackoutWasteAreasAsync(int plant)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PLANT", plant);

                string sql = @"SELECT *
                          FROM [PWASTE_AREAS]
                          WHERE [PLANT] = @PLANT";

                return await con.QueryAsync<PwasteArea>(sql, parameters);
            }
        }

        //PWASTE SOURCES
        public async Task<IEnumerable<PwasteSource>> GetPackoutWasteSourcesAsync(int areaSeq)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@AREA_SEQ", areaSeq);

                string sql = @"SELECT *
                          FROM [PWASTE_SOURCES]
                          WHERE [AREA_SEQ] = @AREA_SEQ";

                return await con.QueryAsync<PwasteSource>(sql, parameters);
            }
        }

        //PWASTE CAUSE CODES
        public async Task<IEnumerable<PwasteCauseCode>> GetPackoutWasteCauseCodesAsync(int plant)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PLANT", plant);

                string sql = @"SELECT * FROM VW_PWASTE_AREA_CAUSE_CODES
                                WHERE ACTIVE_FLAG = 'Y'
                                AND PLANT = @PLANT";

                return await con.QueryAsync<PwasteCauseCode>(sql, parameters);
            }
        }

        //PWASTE TYPE
        public async Task<IEnumerable<PwasteType>> GetPackoutWasteTypesAsync(int areaSeq)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@AREA_SEQ", areaSeq);

                string sql = @"SELECT *
                          FROM [PWASTE_TYPE]
                          WHERE [AREA_SEQ] = @AREA_SEQ";

                return await con.QueryAsync<PwasteType>(sql, parameters);
            }
        }

        
        //PWASTE Materials
        public async Task<IEnumerable<Material>> GetPackoutWasteMaterialsAsync()
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"SELECT * FROM VW_MATERIALS WHERE [PROD_SPEC_CODE] = 'PWASTE' AND OBSOLETE_IND = 'N'";

                return await con.QueryAsync<Material>(sql);
            }
        }

        //F_GRADE CAUSE CODES
        public async Task<IEnumerable<FgradeCauseCode>> GetPackoutFgradeCauseCodesAsync()
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"SELECT * FROM FGRADE_CAUSE_CODES
                                WHERE ACTIVE_FLAG = 'Y'";

                return await con.QueryAsync<FgradeCauseCode>(sql);
            }
        }

        //LOCATION MATERIALS
        public async Task<IEnumerable<Material>> GetPackoutLocationMaterialsAsync(string locationCode)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LocationCode", locationCode);

                string sql = @"SELECT m.* FROM [dbo].[MATERIAL_LOCATIONS] ml LEFT JOIN VW_MATERIALS m ON  ml.[MATERIAL_ID] = m.[MATERIAL_ID] WHERE ml.[LOCATION_CODE] = @LocationCode AND m.[OBSOLETE_IND] = 'N'";

                return await con.QueryAsync<Material>(sql,parameters);
            }
        }

        //Printers
        public async Task<IEnumerable<Printer>> GetPackoutPrintersAsync(int ?areaSeq, int ?stationSeq, string ?shuttleArea, char ?printerType)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@AREA_SEQ", areaSeq);
                parameters.Add("@STATION_SEQ", stationSeq);
                parameters.Add("@SHUTTLE_AREA", shuttleArea);
                parameters.Add("@PRINTER_TYPE", printerType);

                string sql = @"SELECT *
                          FROM [VW_PRINTERS]
                          WHERE (@STATION_SEQ IS NULL OR [STATION_SEQ] = @STATION_SEQ)
                          AND (@AREA_SEQ IS NULL OR [AREA_SEQ] = @AREA_SEQ)
                          AND (@SHUTTLE_AREA IS NULL OR [SHUTTLE_AREA] = @SHUTTLE_AREA)
                          AND (@PRINTER_TYPE IS NULL OR [PRINTER_TYPE] = @PRINTER_TYPE)";

                return await con.QueryAsync<Printer>(sql, parameters);
            }
        }

        //ATLINE LINES
        public async Task<IEnumerable<PackagingLine>> GetNppPackagingLinesAsync()
        {
            using (var con = CreateConnection(DataBase))
            {
                string sql = @"SELECT *
                          FROM [NPP_PACKAGING_LINES]";

                return await con.QueryAsync<PackagingLine>(sql);
            }
        }
    }
}
