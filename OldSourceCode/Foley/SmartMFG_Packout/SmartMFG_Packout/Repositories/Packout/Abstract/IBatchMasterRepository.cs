﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IBatchMasterRepository
    {
        Task<IEnumerable<BatchMaster>> GetBatchMasterListAsync(BatchMasterFilter filter);
        Task<BatchMaster> GetBatchMasterAsync(int batchId);
        Task<BatchMaster> GetBatchMasterAsync(string batchId);
        Task<int> UpdateBatchMasterAsync(BatchMaster batchMaster);
        Task<int> GetDefaultBatchMasterPalletCountAsync(int areaSeq, string cpLine, int materialId);
        Task<BatchMaster> CreateBatchMasterAsync(BatchMasterCreateDto BatchMasterDto);
        Task<BatchMaster> CreatePwasteBatchMasterAsync(BatchMasterPwasteCreateDto BatchMasterDto);
        Task<BatchMaster> CreateTollerBatchMasterAsync(int materialId, string username, string locationCode, string businessCode);
        Task<BatchMaster> CreateExistingBatchMasterAsync(int materialId, string batchId, string username, string locationCode);
        Task<BatchMaster> SuspectBatchMasterAsync(int batchMasterKey, string batchComment, string user);
        Task<BatchMaster> UnSuspectBatchMasterAsync(int batchMasterKey, string batchComment, string user);
        Task<int> AssignBatchToAtlineAsync(int processOrderId, int batchMasterKey, string station, string user);
        Task<int> ChangeBatchModeAtLine(int processOrderId, int batchMasterKey, string station, bool assign, string user);
        Task<int> ChangeBatchModePrePrint(int batchMasterKey, string station, string user);
        Task<BatchMaster> CloseBatchAsync(int batchMasterKey, string user);
        Task<IEnumerable<BatchComment>> GetBatchCommentsAsync(int batchMasterKey);
        Task<BatchComment> CreateBatchCommentAsync(BatchCommentDto batchCommentDto);
        Task<IEnumerable<CommentCode>> GetBatchCommentCodesAsync();
    }
}
