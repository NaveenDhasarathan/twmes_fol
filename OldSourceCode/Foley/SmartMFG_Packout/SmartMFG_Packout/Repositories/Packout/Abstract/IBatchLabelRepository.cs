﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IBatchLabelRepository
    {
        Task<BatchLabel> GetBatchLabelAsync(string serialId);
        Task<IEnumerable<BatchLabel>> GetBatchLabelsAsync(string batchId);
        Task<BatchLabel> CreateBatchLabelAsync(string batchId, string user);
        Task<int> UpdateBatchLabelCountAsync(string batchId, int labelCount, string user);
        Task<int> UpdateBatchLabelAsync(BatchLabel batchLabel);
        Task<int> PrintBatchLabelAudit(string serialId, string user);
    }
}
