﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Repositories;
using SmartMFG_Packout.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class BatchMasterRepository : BaseRepository, IBatchMasterRepository
    {
        private readonly IConfiguration _Configuration;

        private readonly string DataBase = "SmartMFG_Packout_DB";

        public BatchMasterRepository(IConfiguration Configuration) : base(Configuration)
        {
            _Configuration = Configuration;
        }
        public async Task<IEnumerable<BatchMaster>> GetBatchMasterListAsync(BatchMasterFilter filter)
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"SELECT TOP (case ISNULL(@LIMIT, 0) when 0 then 2147483647 else @LIMIT end) * 
                              FROM [VW_BATCH_MASTER]
                              WHERE (@BATCH_ID IS NULL OR CAST([BATCH_ID] as NVARCHAR(10)) LIKE @BATCH_ID + '%')
                              AND (@PO IS NULL OR [PROCESS_ORDER_ID] = @PO)
                              ORDER BY [CREATED_DATE] DESC";

                return await con.QueryAsync<BatchMaster>(sql, filter);
            }
        }

        public async Task<BatchMaster> GetBatchMasterAsync(int batchId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@BATCH_ID", batchId);

                string sql = @"SELECT *
                          FROM [VW_BATCH_MASTER]
                          WHERE [BATCH_MASTER_KEY] = @BATCH_ID";

                return await con.QueryFirstOrDefaultAsync<BatchMaster>(sql, parameters);
            }
        }

        public async Task<BatchMaster> GetBatchMasterAsync(string batchId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@BATCH_ID", batchId);

                string sql = @"SELECT *
                          FROM [VW_BATCH_MASTER]
                          WHERE [BATCH_ID] = @BATCH_ID";

                return await con.QueryFirstOrDefaultAsync<BatchMaster>(sql, parameters);
            }
        }

        public async Task<int> UpdateBatchMasterAsync(BatchMaster batchMaster)
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"SELECT up.*, pa.[AREA_NAME]
                          FROM [USER_PROFILE] up
                          LEFT JOIN [PACKOUT_AREAS] pa on up.[AREA_SEQ] = pa.[AREA_SEQ]
                          WHERE UPPER([USER_ID]) = UPPER(@UserId)";

                return await con.ExecuteAsync(sql, batchMaster);
            }
        }

        public async Task<int> GetDefaultBatchMasterPalletCountAsync(int areaSeq, string cpLine, int materialId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@AreaSeq", areaSeq);
                parameters.Add("@CPline", cpLine);
                parameters.Add("@Material", materialId);

                string sql = @"GET_BATCH_PALLETS";

                return await con.QueryFirstOrDefaultAsync<int>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<BatchMaster> CreateBatchMasterAsync(BatchMasterCreateDto BatchMasterDto)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Process_Order", BatchMasterDto.Process_Order);
                parameters.Add("@Username", BatchMasterDto.Username);
                parameters.Add("@BATCH_MODE", BatchMasterDto.BATCH_MODE);
                parameters.Add("@Blender", BatchMasterDto.Blender);
                parameters.Add("@CPline", BatchMasterDto.CPline);
                parameters.Add("@PO_Station", BatchMasterDto.PO_Station);
                parameters.Add("@NO_OF_CONTAINERS", BatchMasterDto.NO_OF_CONTAINERS);
                parameters.Add("@F_GRADE_CAUSE_CODE", BatchMasterDto.F_GRADE_CAUSE_CODE);

                string sql = @"[CREATE_NEW_BATCH]";

                return await con.QueryFirstOrDefaultAsync<BatchMaster>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<BatchMaster> CreatePwasteBatchMasterAsync(BatchMasterPwasteCreateDto BatchMasterDto)
        {
            using (var con = CreateConnection(DataBase))
            {
                string sql = @"[CREATE_NEW_PWASTE_BATCH]";

                return await con.QueryFirstOrDefaultAsync<BatchMaster>(sql, BatchMasterDto, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<BatchMaster> CreateTollerBatchMasterAsync(int materialId, string username, string locationCode, string businessCode)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@p_Material_ID", materialId);
                parameters.Add("@p_Username", username);
                parameters.Add("@p_Loc_Code", locationCode);
                parameters.Add("@p_Business_Code", businessCode);
                string sql = @"[CREATE_NEW_TOLLER_BATCH]";

                return await con.QueryFirstOrDefaultAsync<BatchMaster>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<BatchMaster> CreateExistingBatchMasterAsync(int materialId, string batchId, string username, string locationCode)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_MATERIAL", materialId);
                parameters.Add("@P_USER", username);
                parameters.Add("@P_PLANT", locationCode);
                parameters.Add("@P_BATCH_ID", batchId);
                string sql = @"[CREATE_NEW_EXISTING_BATCH]";

                return await con.QueryFirstOrDefaultAsync<BatchMaster>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<BatchMaster> SuspectBatchMasterAsync(int batchMasterKey, string batchComment, string user)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Batch_Master_Key", batchMasterKey);
                parameters.Add("@Comment_Text", batchComment);
                parameters.Add("@P_User", user);

                string sql = @"[MAKE_SUSPECT]";

                return await con.QueryFirstOrDefaultAsync<BatchMaster>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<BatchMaster> UnSuspectBatchMasterAsync(int batchMasterKey, string batchComment, string user)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Batch_Master_Key", batchMasterKey);
                parameters.Add("@Comment_Text", batchComment);
                parameters.Add("@P_User", user);

                string sql = @"[MAKE_UNSUSPECT]";

                return await con.QueryFirstOrDefaultAsync<BatchMaster>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<int> AssignBatchToAtlineAsync(int processOrderId, int batchMasterKey, string station, string user)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_PROCESS_ORDER_ID", processOrderId);
                parameters.Add("@P_BATCH_MASTER_KEY", batchMasterKey);
                parameters.Add("@P_STATION", station);
                parameters.Add("@P_User", user);

                string sql = @"[UPDATE_PACKAGING_LINES]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<int> ChangeBatchModeAtLine(int processOrderId, int batchMasterKey, string station, bool assign, string user)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_PROCESS_ORDER_ID", processOrderId);
                parameters.Add("@P_BATCH_MASTER_KEY", batchMasterKey);
                parameters.Add("@P_STATION", station);
                parameters.Add("@P_ASSIGN", assign);
                parameters.Add("@P_User", user);

                string sql = @"[CHANGE_BATCH_MODE_ATLINE]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<int> ChangeBatchModePrePrint(int batchMasterKey, string station, string user)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_BATCH_MASTER_KEY", batchMasterKey);
                parameters.Add("@P_STATION", station);
                parameters.Add("@P_User", user);

                string sql = @"[CHANGE_BATCH_MODE_PREPRINT]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<BatchMaster> CloseBatchAsync(int batchMasterKey, string user)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@P_BMK", batchMasterKey);
                parameters.Add("@P_User", user);

                string sql = @"[CLOSE_BATCH]";

                return await con.QueryFirstOrDefaultAsync<BatchMaster>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<BatchComment>> GetBatchCommentsAsync(int batchMasterKey)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@BATCH_MASTER_KEY", batchMasterKey);

                string sql = @"SELECT * FROM [BATCH_COMMENTS] WHERE [BATCH_MASTER_KEY] = @BATCH_MASTER_KEY";

                return await con.QueryAsync<BatchComment>(sql, parameters);
            }
        }

        public async Task<BatchComment> CreateBatchCommentAsync(BatchCommentDto batchCommentDto)
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"[BATCH_COMMENT]";

                return await con.QueryFirstOrDefaultAsync<BatchComment>(sql, batchCommentDto, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<CommentCode>> GetBatchCommentCodesAsync()
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"SELECT * FROM [VW_COMMENT_CODES]";

                return await con.QueryAsync<CommentCode>(sql);
            }
        }
    }
}
