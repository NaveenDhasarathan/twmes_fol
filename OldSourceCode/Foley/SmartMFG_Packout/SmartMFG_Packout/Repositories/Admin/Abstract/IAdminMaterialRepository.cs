﻿using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Repositories.Abstract
{
    public interface IAdminMaterialRepository
    {
        Task<IEnumerable<Material>> GetMaterialsAsync();
        Task<Material> GetMaterialAsync(int materialId);
        Task<Material> UpsertMaterialAsync(Material material, string type);
        Task<IEnumerable<MaterialLocation>> GetMaterialLocationsAsync(int materialId);
        Task<IEnumerable<MaterialLabelTemplate>> GetMaterialLabelTemplatesAsync();
        Task<IEnumerable<MaterialProductionUnit>> GetMaterialProductionUnitsAsync();
        Task<int> InsertMaterialLocationsAsync(List<MaterialLocation> materialLocations);
        Task<int> DeleteMaterialLocationsAsync(List<MaterialLocation> materialLocations);
        Task<IEnumerable<ContainerType>> GetContainerTypesAsync();
    }
}
