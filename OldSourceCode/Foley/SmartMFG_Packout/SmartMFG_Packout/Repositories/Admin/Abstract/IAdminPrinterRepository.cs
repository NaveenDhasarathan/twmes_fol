﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Repositories.Abstract
{
    public interface IAdminPrinterRepository
    {
        Task<Printer> GetPrintersAsync(int? areaSeq);
        Task<int> UpdatePrintersAsync(Printer info);
        Task<int> InsertPrintersAsync(Printer info);
        Task<int> DeletePrintersAsync(string api_Printer_Name);
    }
}
