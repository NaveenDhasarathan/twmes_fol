﻿using SmartMFG_Packout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Repositories.Abstract
{
    public interface IAdminUserRepository
    {
        Task<IEnumerable<UserProfile>> GetUsersAsync();
        Task<UserProfile> GetUserAsync(string userId);
        Task<int> UpdateUsersAsync(UserProfile userProfile);
        Task<int> InsertUsersAsync(UserProfile userProfile);
        Task<int> InsertUserClaimsAsync(List<dynamic> userClaims);
        Task<int> RemoveUserClaimsAsync(List<dynamic> userClaims);
        Task<int> DeleteUsersAsync(string username);
        Task<IEnumerable<Claim>> GetClaimsAsync(string plantId);
        Task<IEnumerable<UserClaim>> GetUserClaimsAsync(string userId);
        Task<IEnumerable<Role>> GetUserRolesAsync(string plantId);
        Task<IEnumerable<RoleClaim>> GetUserRoleClaimsAsync();
    }
}
