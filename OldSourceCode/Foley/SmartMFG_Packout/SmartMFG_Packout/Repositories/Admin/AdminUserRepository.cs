﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Repositories
{
    public class AdminUserRepository : BaseRepository, IAdminUserRepository
    {
        private readonly IConfiguration _Configuration;

        private readonly string DataBase = "SmartMFG_Packout_DB";

        public AdminUserRepository(IConfiguration Configuration) : base(Configuration)
        {
            _Configuration = Configuration;
        }

        public async Task<IEnumerable<UserProfile>> GetUsersAsync()
        {
            using (var con = CreateConnection(DataBase))
            {
                string sql = @"SELECT up.*, pa.[AREA_NAME]
                          FROM [USER_PROFILE] up
                          LEFT JOIN [PACKOUT_AREAS] pa on up.[AREA_SEQ] = pa.[AREA_SEQ]";

                return await con.QueryAsync<UserProfile>(sql);
            }
        }

        public async Task<UserProfile> GetUserAsync(string userId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@USER_ID", userId);

                string sql = @"SELECT up.*, pa.[AREA_NAME]
                          FROM [USER_PROFILE] up
                          LEFT JOIN [PACKOUT_AREAS] pa on up.[AREA_SEQ] = pa.[AREA_SEQ]
                          WHERE [USER_ID] = @USER_ID";

                return await con.QueryFirstOrDefaultAsync<UserProfile>(sql, parameters);
            }
        }

        public async Task<int> UpdateUsersAsync(UserProfile userProfile)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@USER_ID", userProfile.USER_ID);
                parameters.Add("@EMAIL", userProfile.EMAIL);
                parameters.Add("@AREA_SEQ", userProfile.AREA_SEQ);
                parameters.Add("@DEFAULT_PRINTER", userProfile.DEFAULT_PRINTER);
                parameters.Add("@ACTIVE", userProfile.ACTIVE);

                string sql = @"UPDATE [dbo].[USER_PROFILE]
                               SET [EMAIL] = @EMAIL
                                  ,[AREA_SEQ] = @AREA_SEQ
                                  ,[DEFAULT_PRINTER] = @DEFAULT_PRINTER
                                  ,[ACTIVE] = @ACTIVE
                             WHERE [USER_ID] = @USER_ID";

                return await con.ExecuteAsync(sql, parameters);
            }
        }

        public async Task<int> InsertUsersAsync(UserProfile userProfile)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId", userProfile.USER_ID);
                parameters.Add("@Email", userProfile.EMAIL);
                parameters.Add("@PlantId", userProfile.PLANT);
                parameters.Add("@AreaSeq", userProfile.AREA_SEQ);
                parameters.Add("@DefaultPrinter", userProfile.DEFAULT_PRINTER);

                string sql = @"INSERT INTO [dbo].[USER_PROFILE]
                               ([USER_ID]
                               ,[EMAIL]
                               ,[PLANT]
                               ,[AREA_SEQ]
                               ,[DEFAULT_PRINTER])
                         VALUES
                               (@UserId
                               ,@Email
                               ,@PlantId
                               ,@AreaSeq
                               ,@DefaultPrinter)";

                return await con.ExecuteAsync(sql, parameters);
            }
        }

        public async Task<int> InsertUserClaimsAsync(List<dynamic> userClaims)
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"INSERT INTO [dbo].[USER_CLAIM]
                               ([USER_ID]
                               ,[CLAIM_ID]
                               ,[CLAIM_VALUE])
                         VALUES
                               (@USER_ID
                               ,@CLAIM_ID
                               ,@CLAIM_VALUE)";

                return await con.ExecuteAsync(sql, userClaims);
            }
        }

        public async Task<int> RemoveUserClaimsAsync(List<dynamic> userClaims)
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"DELETE FROM [dbo].[USER_CLAIM]
                               WHERE USER_ID = @USER_ID
                               AND CLAIM_ID = @CLAIM_ID";

                return await con.ExecuteAsync(sql, userClaims);
            }
        }

        public async Task<int> DeleteUsersAsync(string api_User_Name)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@API_User_NAME", api_User_Name);
                parameters.Add("@FLAG", "DELETE");

                string sql = @"[UserS_CRUD_OPERATIONS]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Claim>> GetClaimsAsync(string plantId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PlantId", plantId);

                string sql = @"SELECT *
                            FROM [VW_CLAIMS]
                            WHERE [PLANT_ID] = @PlantId";

                return await con.QueryAsync<Claim>(sql, parameters);
            }
        }

        public async Task<IEnumerable<UserClaim>> GetUserClaimsAsync(string userId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId", userId);

                string sql = @"SELECT *
                            FROM [VW_USER_CLAIMS]
                            WHERE [USER_ID] = @UserId";

                return await con.QueryAsync<UserClaim>(sql, parameters);
            }
        }

        public async Task<IEnumerable<Role>> GetUserRolesAsync(string plantId)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@PlantId", plantId);

            using (var con = CreateConnection(DataBase))
            {
                string sql = @"SELECT *
                            FROM [ROLE]
                            WHERE ACTIVE = 1
                            AND [PLANT_ID] = @PlantId";

                return await con.QueryAsync<Role>(sql, parameters);
            }
        }

        public async Task<IEnumerable<RoleClaim>> GetUserRoleClaimsAsync()
        {
            using (var con = CreateConnection(DataBase))
            {
                string sql = @"SELECT *
                            FROM [ROLE_CLAIM]";

                return await con.QueryAsync<RoleClaim>(sql);
            }
        }
    }
}
