﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Admin;
using SmartMFG_Packout.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Repositories
{
    public class AdminMaterialRepository : BaseRepository, IAdminMaterialRepository
    {
        private readonly IConfiguration _Configuration;

        private readonly string DataBase = "SmartMFG_Packout_DB";

        public AdminMaterialRepository(IConfiguration Configuration) : base(Configuration)
        {
            _Configuration = Configuration;
        }

        public async Task<IEnumerable<Material>> GetMaterialsAsync()
        {
            using (var con = CreateConnection(DataBase))
            {
                string sql = @"SELECT * FROM VW_MATERIALS";

                return await con.QueryAsync<Material>(sql);
            }
        }

        public async Task<Material> GetMaterialAsync(int materialId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MATERIAL_ID", materialId);

                string sql = @"SELECT * FROM VW_MATERIALS
                          WHERE [MATERIAL_ID] = @MATERIAL_ID";

                return await con.QueryFirstOrDefaultAsync<Material>(sql, parameters);
            }
        }

        public async Task<Material> UpsertMaterialAsync(Material material, string type)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MATERIAL_ID", material.MATERIAL_ID);
                parameters.Add("@BUSINESS_CODE", material.BUSINESS_CODE);
                parameters.Add("@LABEL_TEXT", material.LABEL_TEXT);
                parameters.Add("@SAP_DESC", material.SAP_DESC);
                parameters.Add("@OBSOLETE_IND", material.OBSOLETE_IND);
                parameters.Add("@PROD_SPEC_CODE", material.PROD_SPEC_CODE);
                parameters.Add("@GRADE_IND", material.GRADE_IND);
                parameters.Add("@TRADE_NAME", material.TRADE_NAME);
                parameters.Add("@A_GRADE_MATERIAL_ID", material.A_GRADE_MATERIAL_ID);
                parameters.Add("@F_GRADE_MATERIAL_ID", material.F_GRADE_MATERIAL_ID);

                parameters.Add("@PRINT_LOGO_FLAG", material.PRINT_LOGO_FLAG);
                parameters.Add("@@PRINT_PA66_LOGO", material.PRINT_PA66_LOGO);
                parameters.Add("@LABEL_TEMPLATE", material.LABEL_TEMPLATE);
                parameters.Add("@COLOR_CODE", material.COLOR_CODE);
                parameters.Add("@IP_MATERIALS", material.IP_MATERIALS);
                parameters.Add("@CONTAINER_CODE", material.CONTAINER_CODE);
                parameters.Add("@NET_LBS_PER_CONTAINER", material.NET_LBS_PER_CONTAINER);
                parameters.Add("@NET_KGS_PER_CONTAINER", material.NET_KGS_PER_CONTAINER);
                parameters.Add("@TARE_LBS_PER_CONTAINER", material.TARE_LBS_PER_CONTAINER);
                parameters.Add("@TARE_KGS_PER_CONTAINER", material.TARE_KGS_PER_CONTAINER);
                parameters.Add("@NO_OF_LABELS", material.NO_OF_LABELS);
                parameters.Add("@REUSABLE", material.REUSABLE);
                parameters.Add("@RC_MATERIAL", material.RC_MATERIAL);
                parameters.Add("@CREATED_USER", material.CREATED_USER);
                parameters.Add("@MODIFIED_USER", material.MODIFIED_USER);

                parameters.Add("@FLAG", type);

                string sql = @"[MATERIALS_CRUD_OPERATIONS]";

                return await con.QueryFirstOrDefaultAsync<Material>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<MaterialLocation>> GetMaterialLocationsAsync(int materialId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MATERIAL_ID", materialId);

                string sql = @"SELECT * FROM MATERIAL_LOCATIONS 
                                WHERE MATERIAL_ID = @MATERIAL_ID";

                return await con.QueryAsync<MaterialLocation>(sql, parameters);
            }
        }

        public async Task<IEnumerable<MaterialLabelTemplate>> GetMaterialLabelTemplatesAsync()
        {
            using (var con = CreateConnection(DataBase))
            {
                string sql = @"SELECT * FROM MATERIAL_LABEL_TEMPLATE";

                return await con.QueryAsync<MaterialLabelTemplate>(sql);
            }
        }

        public async Task<IEnumerable<MaterialProductionUnit>> GetMaterialProductionUnitsAsync()
        {
            using (var con = CreateConnection(DataBase))
            {
                string sql = @"SELECT * FROM MATERIAL_PRODUCTION_UNIT";

                return await con.QueryAsync<MaterialProductionUnit>(sql);
            }
        }

        public async Task<int> InsertMaterialLocationsAsync(List<MaterialLocation> materialLocations)
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"INSERT INTO [dbo].[MATERIAL_LOCATIONS]
                               ([MATERIAL_ID]
                               ,[LOCATION_CODE]
                               ,[PRODUCTION_UNIT_CODE])
                         VALUES
                               (@MATERIAL_ID
                               ,@LOCATION_CODE
                               ,@PRODUCTION_UNIT_CODE)";

                return await con.ExecuteAsync(sql, materialLocations);
            }
        }

        public async Task<int> DeleteMaterialLocationsAsync(List<MaterialLocation> materialLocations)
        {
            using (var con = CreateConnection(DataBase))
            {

                string sql = @"DELETE FROM [dbo].[MATERIAL_LOCATIONS]
                               WHERE MATERIAL_ID = @MATERIAL_ID
                               AND LOCATION_CODE = @LOCATION_CODE
                               AND PRODUCTION_UNIT_CODE = @PRODUCTION_UNIT_CODE";

                return await con.ExecuteAsync(sql, materialLocations);
            }
        }

        public async Task<IEnumerable<ContainerType>> GetContainerTypesAsync()
        {
            using (var con = CreateConnection(DataBase))
            {
                string sql = @"SELECT * FROM VW_CONTAINER_TYPES";

                return await con.QueryAsync<ContainerType>(sql);
            }
        }

    }
}
