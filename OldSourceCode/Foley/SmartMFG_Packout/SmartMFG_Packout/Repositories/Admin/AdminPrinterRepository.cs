﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Repositories
{
    public class AdminPrinterRepository : BaseRepository, IAdminPrinterRepository
    {
        private readonly IConfiguration _Configuration;

        private readonly string DataBase = "SmartMFG_Packout_DB";

        public AdminPrinterRepository(IConfiguration Configuration) : base(Configuration)
        {
            _Configuration = Configuration;
        }
        public async Task<Printer> GetPrintersAsync(int? areaSeq)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@AreaSeq", areaSeq);

                string sql = @"SELECT * FROM [PRINTERS] 
                WHERE (@AREA_SEQ IS NULL OR [AREA_SEQ] = @AREA_SEQ)";

                return await con.QueryFirstOrDefaultAsync<Printer>(sql, parameters);
            }
        }
        public async Task<int> UpdatePrintersAsync(Printer printer)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PRINTER_NAME", printer.PRINTER_NAME);
                parameters.Add("@IP_ADDRESS", printer.IP_ADDRESS);
                parameters.Add("@LOCATION_CODE", printer.LOCATION_CODE);
                parameters.Add("@AREA_SEQ", printer.AREA_SEQ);
                parameters.Add("@STATION_SEQ", printer.STATION_SEQ);
                parameters.Add("@SHUTTLE_AREA", printer.SHUTTLE_AREA);
                parameters.Add("@API_PRINTER_NAME", printer.API_PRINTER_NAME);
                parameters.Add("@FLAG", "UPDATE");

                string sql = @"[PRINTERS_CRUD_OPERATIONS]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<int> InsertPrintersAsync(Printer printer)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PRINTER_NAME", printer.PRINTER_NAME);
                parameters.Add("@IP_ADDRESS", printer.IP_ADDRESS);
                parameters.Add("@LOCATION_CODE", printer.LOCATION_CODE);
                parameters.Add("@AREA_SEQ", printer.AREA_SEQ);
                parameters.Add("@STATION_SEQ", printer.STATION_SEQ);
                parameters.Add("@SHUTTLE_AREA", printer.SHUTTLE_AREA);
                parameters.Add("@API_PRINTER_NAME", printer.API_PRINTER_NAME);
                parameters.Add("@FLAG", "INSERT");

                string sql = @"[PRINTERS_CRUD_OPERATIONS]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<int> DeletePrintersAsync(string api_Printer_Name)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@API_PRINTER_NAME", api_Printer_Name);
                parameters.Add("@FLAG", "DELETE");

                string sql = @"[PRINTERS_CRUD_OPERATIONS]";

                return await con.ExecuteAsync(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
