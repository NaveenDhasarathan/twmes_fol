﻿using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services.Abstract
{
    public interface IAuthRepository
    {
        Task<int> UpdateRefreshToken(string userId, RefreshToken refreshToken);
        Task<RefreshToken> GetRefreshToken(string userId);
        Task<UserProfile> GetUserProfileAsync(string userId);
        Task<IEnumerable<UserClaim>> GetUserClaimsAsync(string userId);
    }
}
