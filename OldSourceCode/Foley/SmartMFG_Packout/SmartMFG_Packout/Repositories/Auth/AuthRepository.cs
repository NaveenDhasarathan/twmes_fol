﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SmartMFG_Packout.Models;
using SmartMFG_Packout.Models.Auth;
using SmartMFG_Packout.Repositories;
using SmartMFG_Packout.Services.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Services
{
    public class AuthRepository: BaseRepository, IAuthRepository
    {
        private readonly IConfiguration _Configuration;

        private readonly string DataBase = "SmartMFG_Packout_DB";

        public AuthRepository(IConfiguration Configuration) : base(Configuration)
        {
            _Configuration = Configuration;
        }

        public async Task<int> UpdateRefreshToken(string userId, RefreshToken refreshToken)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId", userId);
                parameters.Add("@RefreshToken", refreshToken.REFRESH_TOKEN);
                parameters.Add("@RefreshTokenExpiry", refreshToken.REFRESH_TOKEN_EXPIRY);

                string sql = @"UPDATE [USER_PROFILE]
                          SET [REFRESH_TOKEN] = @RefreshToken,
                          [REFRESH_TOKEN_EXPIRY] = @RefreshTokenExpiry
                          WHERE UPPER([USER_ID]) = UPPER(@UserId)";

                return await con.ExecuteAsync(sql, parameters);
            }
        }

        public async Task<RefreshToken> GetRefreshToken(string userId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId", userId);

                string sql = @"SELECT up.REFRESH_TOKEN, up.REFRESH_TOKEN_EXPIRY
                          FROM [USER_PROFILE] up
                          WHERE UPPER([USER_ID]) = UPPER(@UserId)";

                return await con.QueryFirstOrDefaultAsync<RefreshToken>(sql, parameters);
            }
        }

        public async Task<UserProfile> GetUserProfileAsync(string userId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId", userId);

                string sql = @"SELECT up.*, pa.[AREA_NAME]
                          FROM [USER_PROFILE] up
                          LEFT JOIN [PACKOUT_AREAS] pa on up.[AREA_SEQ] = pa.[AREA_SEQ]
                          WHERE UPPER([USER_ID]) = UPPER(@UserId)";

                return await con.QueryFirstOrDefaultAsync<UserProfile>(sql, parameters);
            }
        }

        public async Task<IEnumerable<UserClaim>> GetUserClaimsAsync(string userId)
        {
            using (var con = CreateConnection(DataBase))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserId", userId);

                string sql = @"SELECT * 
                          FROM VW_USER_CLAIMS
                          WHERE UPPER([USER_ID]) = UPPER(@UserId)";

                return await con.QueryAsync<UserClaim>(sql, parameters);
            }
        }
    }
}
