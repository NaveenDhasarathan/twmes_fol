﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Admin
{
    public class MaterialDto
    {
        [JsonProperty("material")]
        public Material MATERIAL { get; set; }

        [JsonProperty("materialLocations")]
        public List<MaterialLocation> MATERIAL_LOCATIONS { get; set; }
    }
}
