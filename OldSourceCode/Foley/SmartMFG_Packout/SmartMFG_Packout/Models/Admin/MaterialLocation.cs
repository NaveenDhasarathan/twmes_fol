﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Admin
{
    public class MaterialLocation
    {
        [JsonProperty("materialId")]
        public int MATERIAL_ID { get; set; }

        [JsonProperty("locationCode")]
        public string LOCATION_CODE { get; set; }

        [JsonProperty("productionUnitCode")]
        public string PRODUCTION_UNIT_CODE { get; set; }
    }
}
