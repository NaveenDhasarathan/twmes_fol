﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Admin
{
    public class MaterialLabelTemplate
    {
		[JsonProperty("id")]
		public int ID { get; set; }

		[JsonProperty("name")]
		public string NAME { get; set; }

		[JsonProperty("templateName")]
		public string TEMPLATE_NAME { get; set; }
	}
}
