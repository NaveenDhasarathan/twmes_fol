﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Admin
{
	public class MaterialProductionUnit
	{
		[JsonProperty("productionUnitCode")]
		public string PRODUCTION_UNIT_CODE { get; set; }

		[JsonProperty("locationCode")]
		public string LOCATION_CODE { get; set; }

		[JsonProperty("desc")]
		public string DESC { get; set; }

	}
}
