﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Admin
{
    public class ContainerType
    {
		[JsonProperty("containerCode")]
		public string CONTAINER_CODE { get; set; }

		[JsonProperty("containerType")]
		public string CONTAINER_TYPE { get; set; }

		[JsonProperty("containerDesc")]
		public string CONTAINER_DESC { get; set; }
	}
}
