﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Auth
{
    public class RefreshTokenDto
    {
        public string  REFRESH_TOKEN{ get; set; }
        public string ACCESS_TOKEN { get; set; }

    }
}
