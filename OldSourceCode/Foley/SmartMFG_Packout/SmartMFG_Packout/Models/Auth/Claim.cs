﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class Claim
	{
		[JsonProperty("claimId")]
		public int CLAIM_ID { get; set; }

		[JsonProperty("claimType")]
		public string CLAIM_TYPE { get; set; }

		[JsonProperty("plantId")]
		public string PLANT_ID { get; set; }

		[JsonProperty("moduleId")]
		public int? MODULE_ID { get; set; }

		[JsonProperty("moduleName")]
		public string MODULE_NAME { get; set; }

		[JsonProperty("moduleDesc")]
		public string MODULE_DESC { get; set; }

		[JsonProperty("moduleActive")]
		public bool? MODULE_ACTIVE { get; set; }

		[JsonProperty("pageId")]
		public int? PAGE_ID { get; set; }

		[JsonProperty("pageName")]
		public string PAGE_NAME { get; set; }

		[JsonProperty("pageDesc")]
		public string PAGE_DESC { get; set; }

		[JsonProperty("pageActive")]
		public bool? PAGE_ACTIVE { get; set; }

	}
}
