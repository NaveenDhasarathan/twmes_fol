﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class UserProfile
    {
        [JsonProperty("userId")]
        public string USER_ID { get; set; }

        [JsonProperty("email")]
        public string EMAIL { get; set; }

        [JsonProperty("plant")]
        public int PLANT { get; set; }

        [JsonProperty("areaSeq")]
        public int AREA_SEQ { get; set; }

        [JsonProperty("areaName")]
        public string AREA_NAME { get; set; }

        [JsonProperty("defaultPrinter")]
        public string DEFAULT_PRINTER { get; set; }

        [JsonProperty("claims")]
        public IEnumerable<UserClaim> CLAIMS { get; set; }

        [JsonProperty("active")]
        public bool ACTIVE { get; set; }

    }
}
