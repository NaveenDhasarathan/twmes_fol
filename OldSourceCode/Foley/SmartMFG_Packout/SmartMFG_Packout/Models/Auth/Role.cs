﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class Role
	{
		[JsonProperty("roleId")]
		public int ROLE_ID { get; set; }

		[JsonProperty("plantId")]
		public int PLANT_ID { get; set; }

		[JsonProperty("moduleId")]
		public int MODULE_ID { get; set; }

		[JsonProperty("roleName")]
		public string ROLE_NAME { get; set; }

		[JsonProperty("roleDesc")]
		public string ROLE_DESC { get; set; }

		[JsonProperty("active")]
		public bool ACTIVE { get; set; }

	}
}
