﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class UserClaim: Claim
	{
		[JsonProperty("userId")]
		public string USER_ID { get; set; }

		[JsonProperty("claimValue")]
		public string CLAIM_VALUE { get; set; }

	}
}
