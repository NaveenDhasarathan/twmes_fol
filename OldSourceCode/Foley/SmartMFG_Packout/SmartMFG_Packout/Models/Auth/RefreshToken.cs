﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Auth
{
    public class RefreshToken
    {
        public string  REFRESH_TOKEN{ get; set; }
        public DateTime? REFRESH_TOKEN_EXPIRY { get; set; }

    }
}
