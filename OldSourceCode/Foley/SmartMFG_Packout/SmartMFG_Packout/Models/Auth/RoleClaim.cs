﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class RoleClaim
	{
		[JsonProperty("roleId")]
		public int ROLE_ID { get; set; }

		[JsonProperty("claimId")]
		public int CLAIM_ID { get; set; }

	}
}
