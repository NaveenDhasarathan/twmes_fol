﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class CommentCode
    {
        [JsonProperty("commentCode")]
        public string COMMENT_CODE { get; set; }

        [JsonProperty("commentDesc")]
        public string COMMENT_DESC { get; set; }
    }
}
