﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class BatchComment
	{
		[JsonProperty("batchMasterKey")]
		public int BATCH_MASTER_KEY { get; set; }

		[JsonProperty("commentDate")]
		public DateTime COMMENT_DATE { get; set; }

		[JsonProperty("materialId")]
		public long? MATERIAL_ID { get; set; }

		[JsonProperty("regradeCode")]
		public string REGRADE_CODE { get; set; }

		[JsonProperty("commentCode")]
		public string COMMENT_CODE { get; set; }

		[JsonProperty("commentText")]
		public string COMMENT_TEXT { get; set; }

		[JsonProperty("authorizedById")]
		public string AUTHORIZED_BY_ID { get; set; }

		[JsonProperty("sysMessageText")]
		public string SYS_MESSAGE_TEXT { get; set; }

		[JsonProperty("createdDate")]
		public DateTime CREATED_DATE { get; set; }

		[JsonProperty("createdUser")]
		public string CREATED_USER { get; set; }

		[JsonProperty("modifiedDate")]
		public DateTime? MODIFIED_DATE { get; set; }

		[JsonProperty("modifiedUser")]
		public string MODIFIED_USER { get; set; }

	}
}
