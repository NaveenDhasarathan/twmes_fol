﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class PackoutSource
	{
		[JsonProperty("plant")]
		public string PLANT { get; set; }

		[JsonProperty("packoutSourceSeq")]
		public int? PACKOUT_SOURCE_SEQ { get; set; }

		[JsonProperty("mfgSource")]
		public string MFG_SOURCE { get; set; }

		[JsonProperty("sourceActive")]
		public string SOURCE_ACTIVE { get; set; }

		[JsonProperty("packoutSource")]
		public string PACKOUT_SOURCE { get; set; }

		[JsonProperty("packoutSourceDescription")]
		public string PACKOUT_SOURCE_DESCRIPTION { get; set; }

		[JsonProperty("areaSeq")]
		public int? AREA_SEQ { get; set; }

		[JsonProperty("areaName")]
		public string AREA_NAME { get; set; }

		[JsonProperty("areaActive")]
		public string AREA_ACTIVE { get; set; }

		[JsonProperty("stationSeq")]
		public int STATION_SEQ { get; set; }

		[JsonProperty("packoutStation")]
		public string PACKOUT_STATION { get; set; }

		[JsonProperty("stationActive")]
		public string STATION_ACTIVE { get; set; }
	}
}
