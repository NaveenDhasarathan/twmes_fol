﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class BatchMasterPwasteCreateDto
    {
        [JsonProperty("processOrder")]
        public int Process_Order { get; set; }

        [JsonProperty("materialId")]
        public int MaterialId { get; set; }

        [JsonProperty("userName")]
        public string Username { get; set; }

        [JsonProperty("batchMode")]
        public int BATCH_MODE { get; set; }

        [JsonProperty("pwAreaSeq")]
        public int PwAreaSeq { get; set; }

        [JsonProperty("pwStationSeq")]
        public int PwStationSeq { get; set; }

        [JsonProperty("containerNo")]
        public decimal @NO_OF_CONTAINERS { get; set; }

        [JsonProperty("pwSourceSeq")]
        public int PwSourceSeq { get; set; }

        [JsonProperty("pwCauseCodeSeq")]
        public int PwCauseCodeSeq { get; set; }
    }
}
