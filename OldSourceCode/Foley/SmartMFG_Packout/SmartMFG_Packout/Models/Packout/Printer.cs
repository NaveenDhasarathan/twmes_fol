﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class Printer
	{
		[JsonProperty("printerName")]
		public string PRINTER_NAME { get; set; }

		[JsonProperty("printerType")]
		public char PRINTER_TYPE { get; set; }

		[JsonProperty("ipAddress")]
		public string IP_ADDRESS { get; set; }

		[JsonProperty("locationCode")]
		public string LOCATION_CODE { get; set; }

		[JsonProperty("areaSeq")]
		public int? AREA_SEQ { get; set; }

		[JsonProperty("areaName")]
		public string? AREA_NAME { get; set; }

		[JsonProperty("stationSeq")]
		public int? STATION_SEQ { get; set; }

		[JsonProperty("stationName")]
		public string? STATION_NAME { get; set; }

		[JsonProperty("shuttleArea")]
		public string? SHUTTLE_AREA { get; set; }

		[JsonProperty("apiPrinterName")]
		public string API_PRINTER_NAME { get; set; }

	}
}
