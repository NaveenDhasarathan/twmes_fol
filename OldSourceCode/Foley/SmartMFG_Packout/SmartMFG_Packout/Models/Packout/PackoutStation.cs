﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class PackoutStation
    {
		[JsonProperty("stationSeq")]
		public int STATION_SEQ { get; set; }

		[JsonProperty("description")]
		public string DESCRIPTION { get; set; }

		[JsonProperty("areaSeq")]
		public int AREA_SEQ { get; set; }

		[JsonProperty("displaySeq")]
		public int DISPLAY_SEQ { get; set; }

		[JsonProperty("activeFlag")]
		public string ACTIVE_FLAG { get; set; }

		[JsonProperty("ipId")]
		public string IP_ID { get; set; }
	}
}
