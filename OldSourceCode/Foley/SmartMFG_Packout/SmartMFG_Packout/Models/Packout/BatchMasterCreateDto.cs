﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class BatchMasterCreateDto
    {
        [JsonProperty("processOrder")]
        public int Process_Order { get; set; }

        [JsonProperty("userName")]
        public string Username { get; set; }

        [JsonProperty("batchMode")]
        public int BATCH_MODE { get; set; }

        [JsonProperty("blender")]
        public string Blender { get; set; }

        [JsonProperty("cpLine")]
        public string CPline { get; set; }

        [JsonProperty("station")]
        public string PO_Station { get; set; }

        [JsonProperty("containerNo")]
        public decimal NO_OF_CONTAINERS { get; set; }

        [JsonProperty("suspect")]
        public bool SUSPECT { get; set; }

        [JsonProperty("fGradeCauseCode")]
        public int F_GRADE_CAUSE_CODE { get; set; }

    }
}
