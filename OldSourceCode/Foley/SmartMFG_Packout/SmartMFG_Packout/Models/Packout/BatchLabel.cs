﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class BatchLabel
	{
		[JsonProperty("batchId")]
		public string BATCH_ID { get; set; }

		[JsonProperty("serialId")]
		public string SERIAL_ID { get; set; }

		[JsonProperty("materialId")]
		public long? MATERIAL_ID { get; set; }

		[JsonProperty("netLbs")]
		public decimal? NET_LBS { get; set; }

		[JsonProperty("netKgs")]
		public decimal? NET_KGS { get; set; }

		[JsonProperty("grossLbs")]
		public decimal? GROSS_LBS { get; set; }

		[JsonProperty("grossKgs")]
		public decimal GROSS_KGS { get; set; }

		[JsonProperty("noOfLabels")]
		public int NO_OF_LABELS { get; set; }

		[JsonProperty("printFlag")]
		public string PRINT_FLAG { get; set; }

		[JsonProperty("productCode")]
		public string PRODUCT_CODE { get; set; }

		[JsonProperty("msdsText")]
		public string MSDS_TEXT { get; set; }

		[JsonProperty("oldBatchId")]
		public int? OLD_BATCH_ID { get; set; }

		[JsonProperty("locationCode")]
		public string LOCATION_CODE { get; set; }

		[JsonProperty("locAreaCode")]
		public string LOC_AREA_CODE { get; set; }

		[JsonProperty("createdDate")]
		public DateTime CREATED_DATE { get; set; }

		[JsonProperty("createdUser")]
		public string CREATED_USER { get; set; }

		[JsonProperty("modifiedDate")]
		public DateTime? MODIFIED_DATE { get; set; }

		[JsonProperty("modifiedUser")]
		public string MODIFIED_USER { get; set; }

		[JsonProperty("batchMasterKey")]
		public int BATCH_MASTER_KEY { get; set; }

		[JsonProperty("batchContainerNum")]
		public int BATCH_CONTAINER_NUM { get; set; }

		[JsonProperty("processOrderId")]
		public int? PROCESS_ORDER_ID { get; set; }

		[JsonProperty("quality")]
		public string QUALITY { get; set; }

		[JsonProperty("status")]
		public string STATUS { get; set; }

		[JsonProperty("comments")]
		public string COMMENTS { get; set; }

		[JsonProperty("trackingSeq")]
		public string TRACKING_SEQ { get; set; }

		[JsonProperty("scanDate")]
		public DateTime? SCAN_DATE { get; set; }

		[JsonProperty("scanUser")]
		public string SCAN_USER { get; set; }

		[JsonProperty("labelTemplate")]
		public string LABEL_TEMPLATE { get; set; }

		[JsonProperty("labelText")]
		public string LABEL_TEXT { get; set; }

		[JsonProperty("colorCode")]
		public string COLOR_CODE { get; set; }
	}
}
