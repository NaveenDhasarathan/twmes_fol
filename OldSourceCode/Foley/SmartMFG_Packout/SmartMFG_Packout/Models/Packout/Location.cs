﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class Location
    {
		[JsonProperty("locationCode")]
		public string LOCATION_CODE { get; set; }

		[JsonProperty("description")]
		public string DESCRIPTION { get; set; }

		[JsonProperty("city")]
		public string CITY { get; set; }

		[JsonProperty("state")]
		public string STATE { get; set; }

		[JsonProperty("toller")]
		public bool TOLLER { get; set; }

		[JsonProperty("active")]
		public bool ACTIVE { get; set; }
	}
}
