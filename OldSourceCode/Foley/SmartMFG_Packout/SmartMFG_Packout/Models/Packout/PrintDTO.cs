﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class PrintDTO
    {
        public string printer { get; set; }
        public string? labelTemplate { get; set; }
        public IEnumerable<ProductionLabel> labels { get; set; }
    }
}
