﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class BatchMasterFilter
    {
        [JsonProperty("po")]
        public string? PO { get; set; }

        [JsonProperty("batchId")]
        public string? BATCH_ID { get; set; }

        [JsonProperty("limit")]
        public int? LIMIT { get; set; }

    }
}
