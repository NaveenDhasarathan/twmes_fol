﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class ProcessorOrderFilter
    {
        [JsonProperty("po")]
        public string? PROCESS_ORDER { get; set; }

        [JsonProperty("startDate")]
        public DateTime? START_DATE { get; set; }

        [JsonProperty("endDate")]
        public DateTime? END_DATE { get; set; }

        [JsonProperty("areaIds")]
        public int[] AREA_IDS { get; set; }

        [JsonProperty("statusIds")]
        public int[] STATUS_IDS { get; set; }

        [JsonProperty("stationIds")]
        public int[] STATION_IDS { get; set; }

        [JsonProperty("limit")]
        public int? LIMIT { get; set; }

        [JsonProperty("gradeInd")]
        public char? GRADE_IND { get; set; }

        [JsonProperty("prodSpecCode")]
        public string? PROD_SPEC_CODE { get; set; }

    }
}
