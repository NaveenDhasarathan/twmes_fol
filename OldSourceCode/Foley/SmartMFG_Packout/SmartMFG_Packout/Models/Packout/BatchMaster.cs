﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class BatchMaster
	{
		[JsonProperty("batchMasterKey")]
		public long BATCH_MASTER_KEY { get; set; }

		[JsonProperty("batchId")]
		public string BATCH_ID { get; set; }

		[JsonProperty("processOrderId")]
		public int? PROCESS_ORDER_ID { get; set; }

		[JsonProperty("completeDate")]
		public DateTime? COMPLETE_DATE { get; set; }

		[JsonProperty("completeUser")]
		public string COMPLETE_USER { get; set; }

		[JsonProperty("testingCompleteInd")]
		public string TESTING_COMPLETE_IND { get; set; }

		[JsonProperty("blockedInd")]
		public string BLOCKED_IND { get; set; }

		[JsonProperty("interpolationStatusCode")]
		public string INTERPOLATION_STATUS_CODE { get; set; }

		[JsonProperty("dispositioningStatusCode")]
		public string DISPOSITIONING_STATUS_CODE { get; set; }

		[JsonProperty("businessCode")]
		public string BUSINESS_CODE { get; set; }

		[JsonProperty("locationCode")]
		public string LOCATION_CODE { get; set; }

		[JsonProperty("locAreaCode")]
		public string LOC_AREA_CODE { get; set; }

		[JsonProperty("prodSpecCode")]
		public string PROD_SPEC_CODE { get; set; }

		[JsonProperty("createdDate")]
		public DateTime CREATED_DATE { get; set; }

		[JsonProperty("createdUser")]
		public string CREATED_USER { get; set; }

		[JsonProperty("modifiedDate")]
		public DateTime? MODIFIED_DATE { get; set; }

		[JsonProperty("modifiedUser")]
		public string MODIFIED_USER { get; set; }

		[JsonProperty("materialId")]
		public long? MATERIAL_ID { get; set; }

		[JsonProperty("materialDesc")]
		public string MATERIAL_DESC { get; set; }

		[JsonProperty("netLbsPerContainer")]
		public float NET_LBS_PER_CONTAINER { get; set; }

		[JsonProperty("noOfLabels")]
		public int NO_OF_LABELS { get; set; }

		[JsonProperty("containerType")]
		public string CONTAINER_TYPE { get; set; }

		[JsonProperty("containerDesc")]
		public string CONTAINER_DESC { get; set; }

		[JsonProperty("areaSeq")]
		public int? AREA_SEQ { get; set; }

		[JsonProperty("areaName")]
		public string AREA_NAME { get; set; }

		[JsonProperty("packoutStationSeq")]
		public int? PACKOUT_STATION_SEQ { get; set; }

		[JsonProperty("packoutStation")]
		public string PACKOUT_STATION { get; set; }

		[JsonProperty("packoutSourceSeq")]
		public int? PACKOUT_SOURCE_SEQ { get; set; }

		[JsonProperty("sourceDescription")]
		public string SOURCE_DESCRIPTION { get; set; }

		[JsonProperty("mfgSource")]
		public string MFG_SOURCE { get; set; }

		[JsonProperty("addEwma")]
		public string ADD_EWMA { get; set; }

		[JsonProperty("pwTypeSeq")]
		public int? PW_TYPE_SEQ { get; set; }

		[JsonProperty("pwCauseCodeSeq")]
		public int? PW_CAUSE_CODE_SEQ { get; set; }

		[JsonProperty("pwCauseCode")]
		public string PW_CAUSE_CODE { get; set; }

		[JsonProperty("fgradeCauseCodeSeq")]
		public int? FGRADE_CAUSE_CODE_SEQ { get; set; }

		[JsonProperty("createLabSamples")]
		public string CREATE_LAB_SAMPLES { get; set; }

		[JsonProperty("batchModeId")]
		public string BATCH_MODE_ID { get; set; }

		[JsonProperty("batchMode")]
		public string BATCH_MODE { get; set; }

		[JsonProperty("batchCapacity")]
		public decimal? BATCH_CAPACITY { get; set; }

		[JsonProperty("status")]
		public string STATUS { get; set; }

		[JsonProperty("totalProduced")]
		public decimal TOTAL_PRODUCED { get; set; }

	}
}
