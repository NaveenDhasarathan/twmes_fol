﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class ProductionLabel
    {
        public string batchId { get; set; }
        public string containerNo { get; set; }
        public string labelText { get; set; }
        public string netKgs { get; set; }
        public string netLbs { get; set; }
        public string grossKgs { get; set; }
        public string grossLbs { get; set; }
        public string materialId { get; set; }
        public string serialId { get; set; }
        public string colorCode { get; set; }
        public string labelTemplate { get; set; }
    }
}
