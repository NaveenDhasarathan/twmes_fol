﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class FgradeCauseCode
	{
		[JsonProperty("fgradeCcSeq")]
		public long FGRADE_CC_SEQ { get; set; }

		[JsonProperty("areaSeq")]
		public int? AREA_SEQ { get; set; }

		[JsonProperty("causeCode")]
		public string CAUSE_CODE { get; set; }

		[JsonProperty("displaySeq")]
		public int? DISPLAY_SEQ { get; set; }

		[JsonProperty("activeFlag")]
		public string ACTIVE_FLAG { get; set; }

	}
}
