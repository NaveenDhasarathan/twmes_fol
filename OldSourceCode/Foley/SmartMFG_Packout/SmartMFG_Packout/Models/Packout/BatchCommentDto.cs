﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class BatchCommentDto
    {
        [JsonProperty("batchMasterkey")]
        public int Batch_Master_Key { get; set; }

        [JsonProperty("materialId")]
        public int Material_ID { get; set; }

        [JsonProperty("regradeCode")]
        public string Regrade_Code { get; set; }

        [JsonProperty("commentCode")]
        public string @Comment_Code { get; set; }

        [JsonProperty("commentText")]
        public string @Comment_Text { get; set; }

        [JsonProperty("authorizedById")]
        public string @Authorized_By_ID { get; set; }

        [JsonProperty("sysmessageText")]
        public string @Sys_Message_Text { get; set; }

        [JsonProperty("pUser")]
        public string @P_User { get; set; }

    }
}
