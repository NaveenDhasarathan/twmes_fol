﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class PackoutArea
	{
		[JsonProperty("areaSeq")]
		public int AREA_SEQ { get; set; }

		[JsonProperty("areaName")]
		public string AREA_NAME { get; set; }

		[JsonProperty("plant")]
		public string PLANT { get; set; }

		[JsonProperty("activeFlag")]
		public string ACTIVE_FLAG { get; set; }

	}
}
