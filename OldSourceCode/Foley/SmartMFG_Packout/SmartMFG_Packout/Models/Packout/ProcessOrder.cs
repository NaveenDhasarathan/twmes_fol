﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class ProcessOrder
    {
		[JsonProperty("processOrderId")]
		public int PROCESS_ORDER_ID { get; set; }

		[JsonProperty("materialId")]
		public int? MATERIAL_ID { get; set; }

		[JsonProperty("sapStatus")]
		public string SAP_STATUS { get; set; }

		[JsonProperty("closedDateTime")]
		public DateTime? CLOSED_DATE_TIME { get; set; }

		[JsonProperty("createdDate")]
		public DateTime? CREATED_DATE { get; set; }

		[JsonProperty("createdUser")]
		public string CREATED_USER { get; set; }

		[JsonProperty("modifiedDate")]
		public DateTime? MODIFIED_DATE { get; set; }

		[JsonProperty("modifiedUser")]
		public string MODIFIED_USER { get; set; }

		[JsonProperty("locationCode")]
		public string LOCATION_CODE { get; set; }

		[JsonProperty("locAreaCode")]
		public string LOC_AREA_CODE { get; set; }

		[JsonProperty("completeUser")]
		public string COMPLETE_USER { get; set; }

		[JsonProperty("asset")]
		public string ASSET { get; set; }

		[JsonProperty("trialMaterial")]
		public string TRIAL_MATERIAL { get; set; }

		[JsonProperty("poStatusId")]
		public int? PO_STATUS_ID { get; set; }

		[JsonProperty("fStartDate")]
		public DateTime? F_START_DATE { get; set; }

		[JsonProperty("fEndDate")]
		public DateTime? F_END_DATE { get; set; }

		[JsonProperty("sapConfirmedQty")]
		public decimal? SAP_CONFIRMED_QTY { get; set; }

		[JsonProperty("active")]
		public string ACTIVE { get; set; }

		[JsonProperty("phase")]
		public string PHASE { get; set; }

		[JsonProperty("uom")]
		public string UOM { get; set; }

		[JsonProperty("dateTimeAdded")]
		public DateTime? DATE_TIME_ADDED { get; set; }

		[JsonProperty("prodSpecCode")]
		public string PROD_SPEC_CODE { get; set; }

		[JsonProperty("labelText")]
		public string LABEL_TEXT { get; set; }

		[JsonProperty("gradeInd")]
		public string GRADE_IND { get; set; }

		[JsonProperty("sapDesc")]
		public string SAP_DESC { get; set; }

		[JsonProperty("dateTimeUpdate")]
		public DateTime? DATE_TIME_UPDATE { get; set; }

		[JsonProperty("poStartDate")]
		public DateTime? PO_START_DATE { get; set; }

		[JsonProperty("poEndDate")]
		public DateTime? PO_END_DATE { get; set; }

		[JsonProperty("poAmtSched")]
		public decimal? PO_AMT_SCHED { get; set; }

		[JsonProperty("poResourceId")]
		public int? PO_RESOURCE_ID { get; set; }

		[JsonProperty("stationSeq")]
		public string STATION_SEQ { get; set; }

		[JsonProperty("stationDesc")]
		public string STATION_DESC { get; set; }

		[JsonProperty("areaSeq")]
		public int? AREA_SEQ { get; set; }

		[JsonProperty("areaDesc")]
		public string AREA_DESC { get; set; }

		[JsonProperty("netKgsPerContainer")]
		public decimal? NET_KGS_PER_CONTAINER { get; set; }

		[JsonProperty("netLbsPerContainer")]
		public decimal? NET_LBS_PER_CONTAINER { get; set; }

		[JsonProperty("noOfLabels")]
		public int? NO_OF_LABELS { get; set; }

		[JsonProperty("containerType")]
		public string CONTAINER_TYPE { get; set; }

		[JsonProperty("containerDesc")]
		public string CONTAINER_DESC { get; set; }

		[JsonProperty("amountProduced")]
		public decimal AMOUNT_PRODUCED { get; set; }
	}
}
