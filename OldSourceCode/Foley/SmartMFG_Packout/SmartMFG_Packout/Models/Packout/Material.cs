﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
	public class Material
	{
		[JsonProperty("materialId")]
		public int MATERIAL_ID { get; set; }

		[JsonProperty("businessCode")]
		public string BUSINESS_CODE { get; set; }

		[JsonProperty("labelText")]
		public string LABEL_TEXT { get; set; }

		[JsonProperty("sapDesc")]
		public string SAP_DESC { get; set; }

		[JsonProperty("obsoleteInd")]
		public string OBSOLETE_IND { get; set; }

		[JsonProperty("prodSpecCode")]
		public string PROD_SPEC_CODE { get; set; }

		[JsonProperty("gradeInd")]
		public string GRADE_IND { get; set; }

		[JsonProperty("tradeName")]
		public string TRADE_NAME { get; set; }

		[JsonProperty("aGradeMaterialId")]
		public int? A_GRADE_MATERIAL_ID { get; set; }

		[JsonProperty("fGradeMaterialId")]
		public int? F_GRADE_MATERIAL_ID { get; set; }

		[JsonProperty("createdDate")]
		public DateTime CREATED_DATE { get; set; }

		[JsonProperty("createdUser")]
		public string CREATED_USER { get; set; }

		[JsonProperty("modifiedDate")]
		public DateTime? MODIFIED_DATE { get; set; }

		[JsonProperty("modifiedUser")]
		public string MODIFIED_USER { get; set; }

		[JsonProperty("printLogoFlag")]
		public string PRINT_LOGO_FLAG { get; set; }

		[JsonProperty("printPa66Logo")]
		public string PRINT_PA66_LOGO { get; set; }

		[JsonProperty("labelTemplate")]
		public string LABEL_TEMPLATE { get; set; }

		[JsonProperty("colorCode")]
		public string COLOR_CODE { get; set; }

		[JsonProperty("ipMaterials")]
		public int? IP_MATERIALS { get; set; }

		[JsonProperty("containerCode")]
		public string CONTAINER_CODE { get; set; }

		[JsonProperty("netLbsPerContainer")]
		public decimal? NET_LBS_PER_CONTAINER { get; set; }

		[JsonProperty("netKgsPerContainer")]
		public decimal? NET_KGS_PER_CONTAINER { get; set; }

		[JsonProperty("tareLbsPerContainer")]
		public decimal? TARE_LBS_PER_CONTAINER { get; set; }

		[JsonProperty("tareKgsPerContainer")]
		public decimal? TARE_KGS_PER_CONTAINER { get; set; }

		[JsonProperty("noOfLabels")]
		public int? NO_OF_LABELS { get; set; }

		[JsonProperty("rcMaterial")]
		public int? RC_MATERIAL { get; set; }

		[JsonProperty("reusable")]
		public string REUSABLE { get; set; }

		[JsonProperty("containerType")]
		public string CONTAINER_TYPE { get; set; }

		[JsonProperty("containerDesc")]
		public string CONTAINER_DESC { get; set; }
	}
}
