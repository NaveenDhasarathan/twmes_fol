﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class PackagingLine
    {
		[JsonProperty("lineName")]
		public string LINE_NAME { get; set; }

		[JsonProperty("processOrderId")]
		public long? PROCESS_ORDER_ID { get; set; }

		[JsonProperty("batchMasterKey")]
		public long? BATCH_MASTER_KEY { get; set; }

		[JsonProperty("active")]
		public string ACTIVE { get; set; }

		[JsonProperty("servicePollingTimestamp")]
		public DateTime? SERVICE_POLLING_TIMESTAMP { get; set; }

		[JsonProperty("deltavInterlockPath")]
		public string DELTAV_INTERLOCK_PATH { get; set; }

		[JsonProperty("lineType")]
		public string LINE_TYPE { get; set; }

		[JsonProperty("serviceExceptionText")]
		public string SERVICE_EXCEPTION_TEXT { get; set; }

		[JsonProperty("scannedAgcSun")]
		public string SCANNED_AGC_SUN { get; set; }

		[JsonProperty("scannedAgcTimestamp")]
		public DateTime? SCANNED_AGC_TIMESTAMP { get; set; }

		[JsonProperty("loaderId")]
		public string LOADER_ID { get; set; }

		[JsonProperty("shuttleId")]
		public int? SHUTTLE_ID { get; set; }

		[JsonProperty("agcUnloadedZone")]
		public int? AGC_UNLOADED_ZONE { get; set; }

		[JsonProperty("agcLoadedZone")]
		public int? AGC_LOADED_ZONE { get; set; }

		[JsonProperty("agcDestZone")]
		public int? AGC_DEST_ZONE { get; set; }

		[JsonProperty("agcRejectZone")]
		public int? AGC_REJECT_ZONE { get; set; }

		[JsonProperty("nominalPounds")]
		public decimal? NOMINAL_POUNDS { get; set; }

		[JsonProperty("cpLine")]
		public string CP_LINE { get; set; }

		[JsonProperty("area")]
		public string AREA { get; set; }
	}
}
