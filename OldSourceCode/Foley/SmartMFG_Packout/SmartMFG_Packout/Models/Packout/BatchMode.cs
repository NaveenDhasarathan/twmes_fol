﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models
{
    public class BatchMode
    {
        [JsonProperty("batchModeId")]
        public int BATCH_MODE_ID { get; set; }

        [JsonProperty("batchModeDescr")]
        public string BATCH_MODE_DESCR { get; set; }

    }
}
