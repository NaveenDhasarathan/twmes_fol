﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
	public class ShuttleArea
	{
		[JsonProperty("name")]
		public string NAME { get; set; }

		[JsonProperty("area")]
		public string AREA { get; set; }

		[JsonProperty("plant")]
		public string PLANT { get; set; }

		[JsonProperty("printEnabled")]
		public bool PRINT_ENABLED { get; set; }

		[JsonProperty("sapContainerDefault")]
		public string SAP_CONTAINER_DEFAULT { get; set; }

	}
}
