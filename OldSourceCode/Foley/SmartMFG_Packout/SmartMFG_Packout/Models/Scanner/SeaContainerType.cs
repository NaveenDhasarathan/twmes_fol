﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
    public class SeaContainerType
    {
		[JsonProperty("storageTypeId")]
		public string STORAGE_TYPE_ID { get; set; }

		[JsonProperty("storageDescription")]
		public string STORAGE_DESCRIPTION { get; set; }
	}
}
