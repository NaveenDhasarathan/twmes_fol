﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
    public class ShuttleContent
    {
		[JsonProperty("shuttleId")]
		public long SHUTTLE_ID { get; set; }

		[JsonProperty("serialId")]
		public string SERIAL_ID { get; set; }

		[JsonProperty("createdDate")]
		public DateTime CREATED_DATE { get; set; }

		[JsonProperty("loader")]
		public string LOADER { get; set; }

		[JsonProperty("netLbs")]
		public decimal? NET_LBS { get; set; }

		[JsonProperty("grossLbs")]
		public decimal? GROSS_LBS { get; set; }

		public ShuttleContent() { }
		public ShuttleContent(JObject apiResult) {
			SHUTTLE_ID = Convert.ToInt32(apiResult["SHUTTLE_ID"]);
			SERIAL_ID = Convert.ToString(apiResult["SERIAL_ID"]);
			CREATED_DATE = Convert.ToDateTime(apiResult["CREATED_DATE"]);
			LOADER = Convert.ToString(apiResult["LOADER"]);
			NET_LBS = Convert.ToDecimal(apiResult["NET_LBS"]);
			GROSS_LBS = Convert.ToDecimal(apiResult["GROSS_LBS"]);
		}
	}
}
