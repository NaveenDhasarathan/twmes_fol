﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
    public class ShuttleReportContents
	{
		[JsonProperty("shuttleId")]
		public int SHUTTLE_ID { get; set; }

		[JsonProperty("coontainerId")]
		public string CONTAINER_ID { get; set; }

		[JsonProperty("sapContainer")]
		public string SAP_CONTAINER { get; set; }

		[JsonProperty("status")]
		public string STATUS { get; set; }

		[JsonProperty("execSapContainer")]
		public string EXC_SAP_CONTAINER { get; set; }

		[JsonProperty("serialId")]
		public int SERIAL_ID { get; set; }

		[JsonProperty("initialShuttleId")]
		public int INITIAL_SHUTTLE_ID { get; set; }

		[JsonProperty("intialLoadTime")]
		public DateTime INITIAL_LOAD_TIME { get; set; }

		[JsonProperty("currentShuttleId")]
		public int CURRENT_SHUTTLE_ID { get; set; }

		[JsonProperty("currentLoadTime")]
		public DateTime CURRENT_LOAD_TIME { get; set; }

		[JsonProperty("materialId")]
		public int MATERIAL_ID { get; set; }

		[JsonProperty("batchId")]
		public string BATCH_ID { get; set; }

		[JsonProperty("batchContainerNum")]
		public int BATCH_CONTAINER_NUM { get; set; }

		[JsonProperty("netLbs")]
		public decimal NET_LBS { get; set; }

		[JsonProperty("grossLbs")]
		public decimal GROSS_LBS { get; set; }
	}
}
