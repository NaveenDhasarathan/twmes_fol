﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
	public class ShuttleContainer
	{
		[JsonProperty("containerId")]
		public string CONTAINER_ID { get; set; }

		[JsonProperty("maxLbs")]
		public decimal MAX_LBS { get; set; }

	}
}
