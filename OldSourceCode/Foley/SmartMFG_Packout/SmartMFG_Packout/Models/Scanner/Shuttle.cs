﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
    public class Shuttle
    {
		[JsonProperty("shuttleId")]
		public long SHUTTLE_ID { get; set; }

		[JsonProperty("containerId")]
		public string CONTAINER_ID { get; set; }

		[JsonProperty("startDate")]
		public DateTime START_DATE { get; set; }

		[JsonProperty("finalizedDate")]
		public DateTime? FINALIZED_DATE { get; set; }

		[JsonProperty("sapContainer")]
		public string SAP_CONTAINER { get; set; }

		[JsonProperty("starter")]
		public string STARTER { get; set; }

		[JsonProperty("location")]
		public string LOCATION { get; set; }

		[JsonProperty("area")]
		public string AREA { get; set; }

		[JsonProperty("destination")]
		public string DESTINATION { get; set; }

		[JsonProperty("storageType")]
		public string STORAGE_TYPE { get; set; }

		[JsonProperty("seal")]
		public string SEAL { get; set; }

		[JsonProperty("checkDigit")]
		public string CHECK_DIGIT { get; set; }

		[JsonProperty("deliveryNo")]
		public string DELIVERY_NO { get; set; }

		[JsonProperty("shipmentNo")]
		public string SHIPMENT_NO { get; set; }
	}
}
