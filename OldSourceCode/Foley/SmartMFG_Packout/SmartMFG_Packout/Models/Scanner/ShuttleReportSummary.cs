﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
    public class ShuttleReportSummary
	{
		public decimal netLbs { get; set; }

		public decimal grossLbs { get; set; }

		public int packageCount { get; set; }

		public string sapContainer { get; set; }

	}
}
