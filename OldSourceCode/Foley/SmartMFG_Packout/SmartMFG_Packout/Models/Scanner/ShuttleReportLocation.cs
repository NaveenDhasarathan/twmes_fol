﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
    public class ShuttleReportLocation
	{
		[JsonProperty("sapContainer")]
		public string SAP_CONTAINER { get; set; }

		[JsonProperty("startDate")]
		public DateTime START_DATE { get; set; }

		[JsonProperty("location")]
		public string LOCATION { get; set; }

		[JsonProperty("destination")]
		public string DESTINATION { get; set; }

		[JsonProperty("fromName")]
		public string FROM_NAME { get; set; }

		[JsonProperty("fromStreet")]
		public string FROM_STREET { get; set; }

		[JsonProperty("fromLocation")]
		public string FROM_LOCATION { get; set; }

		[JsonProperty("toName")]
		public string TO_NAME { get; set; }

		[JsonProperty("toStreet")]
		public string TO_STREET { get; set; }

		[JsonProperty("toLocation")]
		public string TO_LOCATION { get; set; }
	}
}
