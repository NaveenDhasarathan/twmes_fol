﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
    public class ShuttleSapPrintInfo
	{
		public int shuttleId { get; set; }

		public string location { get; set; }

		public string destination { get; set; }

		public string warehouse { get; set; }

		public string storage_type { get; set; }

		public string sap_container { get; set; }

		public DateTime start_date { get; set; }

		public DateTime end_date { get; set; }

	}
}
