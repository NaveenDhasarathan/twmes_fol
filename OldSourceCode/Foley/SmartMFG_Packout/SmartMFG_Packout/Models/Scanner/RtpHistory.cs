﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
	public class RtpHistory
	{
		//[JsonProperty("serialId")]
		public int serial_id { get; set; }

		//[JsonProperty("batchId")]
		public string batch_id { get; set; }

		//[JsonProperty("batchContainerNum")]
		public string batch_container_num { get; set; }

		//[JsonProperty("rtpAction")]
		public string rtp_action { get; set; }

		//[JsonProperty("createdDate")]
		public DateTime created_date { get; set; }

		//[JsonProperty("rtpDate")]
		public DateTime rtp_date { get; set; }
	}
}
