﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
	public class RepackageHistory
	{
		public int SERIAL_ID { get; set; }

		public float NET_LBS { get; set; }

		public string BATCH_ID { get; set; }

		public string PROD_SPEC { get; set; }

	}
}
