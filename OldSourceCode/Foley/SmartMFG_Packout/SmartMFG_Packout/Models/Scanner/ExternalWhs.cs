﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
    public class ExternalWhs
    {
		[JsonProperty("plant")]
		public string PLANT { get; set; }

		[JsonProperty("name")]
		public string NAME { get; set; }
	}
}
