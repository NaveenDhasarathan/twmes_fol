﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartMFG_Packout.Models.Scanner
{
    public class ShuttleCreateDto
	{
		[JsonProperty("location")]
		public string LOCATION { get; set; }

		[JsonProperty("area")]
		public string AREA { get; set; }

		[JsonProperty("destPlant")]
		public string DEST_PLANT { get; set; }

		[JsonProperty("container")]
		public string CONTAINER { get; set; }

		[JsonProperty("authorizer")]
		public string AUTHORIZER { get; set; }

		[JsonProperty("storageType")]
		public string STORAGE_TYPE { get; set; }

		[JsonProperty("seal")]
		public string SEAL { get; set; }

		[JsonProperty("chkDigit")]
		public string CHK_DIGIT { get; set; }

		[JsonProperty("deliveryNo")]
		public string DELIVERY_NO { get; set; }

		[JsonProperty("shipmentNo")]
		public string SHIPMENT_NO { get; set; }
	}
}
