﻿$(function () {

    $(document).ready(function () {
        $('#Amt_Received').on("blur", function () {
            $("#Amt_Received").val(parseFloat($("#Amt_Received").val()).toFixed(3));
        });
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear() + "-" + (month) + "-" + (day);
        $('#Product_Received_Date').val(today);
        $('#label-btn-icon').click(function () {
            $(this).toggleClass('fa fa-plus fa fa-minus', 1000);
        });
        $('#scan-btn-icon').click(function () {
            $(this).toggleClass('fa fa-plus fa fa-minus', 1000);
        });
        $("#POLabelform").validate({
            messages: {
                Lot_Nbr: "* Required",
                Product_Received_Date: "* A date is required.",
                Tot_Bag_No: "* A number is required.",
                palletNo: "* A number is required.",
                CountryOrigin: "* Required",
                Amt_Received: "* Requried",
                Batch_No: "* Required"
            }
        });

        $("#labelModalForm").validate({
            messages: {
                lotNo: "* Required",
                bagNo: "* A number is required.",
                origin: "* Required",
                amt: "* Requried",
                batchNo: "* Required"
            }
        });
        //packagingMaterialListing
        $('#packagingMaterialListing').dataTable({
            dom: 'Bfrtip',
            searchHighlight: true,
            buttons: [
                {
                    extend: 'excel',
                    title: $('#DocNum').val() + '_' + $('#Delivery_No').val() + '_' + $('#POItemNbr').val(),
                    exportOptions: {
                        columns: 'th:not(.noprint),tr:not(.noprint)',
                        headers: 'th:not(.noprint),tr:not(.noprint)'
                    }
                    , header: false
                }
                //'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "language": {
                "search": "Scan/Search"
            },
            "lengthMenu": [[50, 100, -1], [50, 100, "All"]],
            "columnDefs": [{
                "orderable": false,
                "className": 'select-checkbox',
                "targets": 0
            }, {
                "orderable": false,
                "targets": 9
            }],
            "paging": true,
            "ordering": true,
            "aaSorting": [1, 'desc'],
            "info": true,
            "scrollX": true,
            "dom": '<"top">t<"bottom"lp><"clear">',
            "initComplete": function () {
                this.api().columns('.searchable').every(function () {
                    var column = this;
                    var select = $('<select class="input-group input-group-sm"><option value="">Show All</option></select>')
                        //.appendTo($(column.footer()).empty())
                        .appendTo($(column.header()))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        if (d.trim() != "")
                            if (!InArray(d))
                                select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            }
        });
        $('#labelListing').dataTable({
            dom: 'Bfrtip',
            searchHighlight: true,
            buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: 'th:not(.noprint),tr:not(.noprint)',
                        headers: 'th:not(.noprint),tr:not(.noprint)'
                    }
                    , header: false
                }
                //'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "language": {
                "search": "Scan/Search"
            },
            "lengthMenu": [[50, 100, -1], [50, 100, "All"]],
            /*"searching": true,*/
            "columnDefs": [{
                "orderable": false,
                "className": 'select-checkbox',
                "targets": 0
            }, {
                "orderable": false,
                "targets": 9
            }],
            "select": {
                //style: 'os',
                style: 'multi',
                selector: 'td:first-child'
            },
            "paging": true,
            "ordering": true,
            "aaSorting": [1, 'desc'],
            "info": true,
            "scrollX": true,
            "dom": '<"top">t<"bottom"lp><"clear">',
            "initComplete": function () {
                this.api().columns('.searchable').every(function () {
                    var column = this;
                    var select = $('<select class="input-group input-group-sm"><option value="">Show All</option></select>')
                        //.appendTo($(column.footer()).empty())
                        .appendTo($(column.header()))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        if (d.trim() != "")
                            if (!InArray(d))
                                select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            }
        });

        $('#ExportExcelPOPM').click(() => {
            //tableSTO.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
            $('#packagingMaterialListing').DataTable().buttons(0, 0).trigger()
            //tableSTO.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
        });
        $('#ExportExcelPO').click(() => {
            //tableSTO.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
            $('#labelListing').DataTable().buttons(0, 0).trigger()
            //tableSTO.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
        });
        //$('#labelListing').dataTable.on('select', function (e, dt, type, indexes) {

        //});
        var table = $('#labelListing').DataTable();

        $('#labelListing tbody').on('click', 'td.details-control', function () {
            //alert('hey');
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                //row.child(row.data()).show();
                tr.addClass('shown');
                //tr.css('background-color', '#f5cccc');
            }
        });







        function format(d) {
            // `d` is the original data object for the row
            console.log(d);
            var str = '<table class="noprint table table-bordered table-hover table-sm blue2-header">'
                + '<thead><tr><th>Label ID</th><th>Qty Received</th><th>Supplier Batch/Lot No.</th><th>Product Received Date</th>'
                + '<th>Label Print Date</th><th>Label Scan Date</th>'
                + '</tr></thead><tbody>';
            //+ '<th></th></tr></thead><tbody>';
            if (d[10] != "") {
                var arr = d[10].split("~");
                // var actionName = '';
                var counterArr = 0;

                for (i = 0; i < arr.length - 1; i++) {
                    counterArr += 1;

                    var arr2 = arr[i].split("|");
                    //if (d[1].indexOf("STO") !== -1) {
                    //    actionName = '/InboundRawMaterial/LineItemLabel/';
                    //} else {
                    //    actionName = '/PurchaseOrderLabels/LineItemExternalLabel/';
                    //}
                    //str = str + "<tr onclick=location.href='" + actionName + arr2[2] + "'>"
                    str += '<tr>'

                    for (j = 0; j < arr2.length; j++) {
                        if (j == 0) {
                            str = str + '<td>' + arr2[j] + '-' + counterArr.toString() + '&nbsp;<i class="far fa-copy" data-toggle="tooltip" title="Scan Label"></i></td>';
                        }
                        else if (j == 1) {

                            // str = str + '<td>' + (parseFloat(parseFloat(arr2[j].split(" ")[0]) / (parseFloat(arr.length) - 1)) + ' ' + arr2[j].split(" ")[1]).toString() + '</td>';
                            str = str + '<td>' + (parseFloat(parseFloat(arr2[j].split(" ")[0])) + ' ' + arr2[j].split(" ")[1]).toString() + '</td>';
                        }
                        else if (j == 2) {
                            str = str + '<td style="display:none;">' + arr2[j] + '</td>';
                            //str = str + '<td>' + arr2[j] + '</td>';
                        }
                        else {
                            str = str + '<td>' + arr2[j] + '</td>';
                        }
                    }
                    //str = str + '<td><button type = "button" class="btnUpdate btn btn-link pt-1 px-0" title = "Update" data - id="@item.Pol_Seq" > <i class="fas fa-edit"></i></button>'
                    //    + '<button type="button" class="btnDelete btn btn-link pt-1 px-0" title="Delete" data-id="@item.Pol_Seq"><i class="far fa-trash-alt"></i></button>'
                    //    + '<button type="button" class="btnReprint btn btn-link pt-1 px-0" title="Reprint"' + +'><i class="fas fa-tags"></i></button></td>';

                    str = str + "</tr>";
                }
            }
            str = str + "</tbody></table>";
            return str;
        }
    });
    var HeaderArray = ["PO Number", "Delivery Number", "Request Delivery Date", "Document Type", "Material Type", "SAP Sent", "Supplier Name", "Supplier Number",
        "Item Number", "Material Number", "Storage Unit Number", "Batch Number", "Batch ID", "Quantity Received", "Date Received", "Label Scan Date", "Delivery Scan Date", "Date Sent to SAP", "Print Date"
        , "Order No.", "Delivery No. :", "Item No", "Label ID", "Container Weight", "Material No.", "Total Bags", "Vendor Batch", "Ascend Batch", "Material Description", "Received Date", "Quantity", "Scan Date", "Storage Location"
        , "Storage Unit No", "Pallet Weight"];
    function InArray(needle) {
        var result = false;
        result = HeaderArray.includes(needle)
        if (!result) {
            for (var i = 0; i <= HeaderArray.length; i++) {
                result = needle.includes(HeaderArray[i]);
                if (result)
                    return result;
            }
        }

        //return HeaderArray.Any(s => s.includes(needle));
        return result;
    }

    $('#Amt_Received').on('blur', function () {
        //var palletNo = $('#palletNo').val();
        //var val = $('#Amt_Received').val();
        //if (palletNo != "" && val != "") {
        //    var imrId = $('#TotQty').val();
        //    var current = $('#CurrentQty').val();
        //    var amtExpected = parseInt(imrId) - parseInt(current);
        //    var totReceived = parseInt(val) * parseInt(palletNo);
        //    if (!ValidateAmountReceived(totReceived, imrId, current)) {
        //        $("#Amt_Received").removeClass("is-valid").addClass("is-invalid");
        //        $("#Amt_Received")[0].setCustomValidity("Please enter correct amount! The amount entered may exceed more than expected.");
        //        $("#amtMsg").html("Please enter correct amount! The amount entered should be no more than " + amtExpected);
        //        $("#amtMsg").show();
        //    }
        //    else {
        //        $("#Amt_Received").removeClass("is-invalid").addClass("is-valid");
        //        $("#amtMsg").hide();
        //    }
        //}
    });

    function ValidateAmountReceived(val, imrId, current) {
        if (parseInt(val) + parseInt(current) > parseInt(imrId) || parseInt(val) == 0) {
            return false;
        }
        else
            return true;
    }

    //Reprint button on the top
    $('#selectReprint').on('click', function (e) {
        var printType = [];
        $.each($("input[name='type']:checked"), function () {
            printType.push($(this).val());
        });
        //alert(printType);

        var table = $('#labelListing').dataTable();
        var tblData = table.DataTable().rows('.selected').data();
        var arrLabel = [];
        $.each(tblData, function (i, val) {
            $.each(val, function (j, value) {
                if (j == 1) {
                    arrLabel.push(value);
                }
            });
        });
        if (arrLabel.length < 1 /*|| printType.length < 1*/) {
            $("#sMsg").html("Please select labels to print.");
            $('#alertMsg').show();
            setTimeout(function () {
                $('#alertMsg').hide();
            }, 2000);
        }
        else {
            $("#modalTitle").html("Sending reprint request...");
            $("#labelModal").modal('show');
            $('#cancelModal').hide();
            $('#labelModalSpinner').show();
            $(".modal-backdrop").hide();
            $.ajax({
                type: "POST",
                url: "/RawMaterial/ConfirmReprintLabels",
                traditional: true,
                data: {
                    arrLabel: arrLabel,
                    arrLabelType: printType
                },
                //contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: function () {
                    showErrorMessage("Error occurred, label printing request was not sent successfully.");
                    $('#labelModalSpinner').hide();
                },
                success: function (data) {

                    if (data.success) {
                        var msg = "Label printing request was sent.";

                        if (!data.updatePrintDate)
                            msg += " But, the last print date was not updated successfully.";
                        showSuccessMessage(msg);
                        setTimeout(function () {
                            $("#labelModal").modal('hide');
                            location.reload();
                        }, 3000);
                    }
                    else {
                        showErrorMessage("Error occurred, label printing request was not sent successfully.");
                        $('#cancelModal').show();
                    }
                    $('#labelModalSpinner').hide();
                }
            });
        }
    });

    //Delete
    $('.btnDelete').click(function () {
        $('#updateLabelHtml').hide();
        $('#success-alert').hide();
        $("#delRec1").hide();
        $("#btnReprintConfirm").hide();
        $("#btnUpdateConfirm").hide();
        $("#modalTitle").html("Are you sure to delete all the labels?");
        $("#labelModal").modal('show');
        $('#labelModalSpinner').show();
        $(".modal-backdrop").hide();

        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: "/RawMaterial/GetLabelForDel",
            data: { pol_seq: id },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                alert("Error occurred, no label info found.");
                $("#labelModal").modal('hide');
                $('#labelModalSpinner').hide();
            },
            success: function (data) {
                $('#labelModalSpinner').hide();
                var str = "";
                /* $.each(data, function (ind, value) {*/
                $.each(data, function (key, val) {
                    if (val == null)
                        val = "";
                    if (key == 'pol_seq')
                        $('#seq').val(val);
                    if (key == 'label_Id')
                        str += "<tr><td class='font-weight-bold'>Label Id:</td><td>" + val + "<input type='hidden' name='labelId' id='labelId' value='" + val + "'></td></tr>";
                    if (key == 'p')
                        str += "<tr><td class='font-weight-bold'>Product Received Date:</td><td>" + val + "</td></tr>";
                    if (key == 'v')
                        str += "<tr><td class='font-weight-bold'>Label Print Date:</td><td>" + val + "</td></tr>";
                    if (key == 'amt_Received')
                        str += "<tr><td class='font-weight-bold'>Amount Received:</td><td>" + val + " KG </td></tr>";
                    if (key == 'batch_No')
                        str += "<tr><td class='font-weight-bold'>Vendor Batch #:</td><td>" + val + "</td></tr>";
                    if (key == 'lot_Nbr')
                        str += "<tr><td class='font-weight-bold'>Pallet #:</td><td>" + val + "</td></tr>";
                    if (key == 'z')
                        str += "<tr><td class='font-weight-bold'>Label Scan Date:</td><td>" + val + "</td></tr>";
                    if (key == 'tot_Bag_No')
                        str += "<tr><td class='font-weight-bold'>Total No. of Bag:</td><td>" + val + "</td></tr>";

                });
                /*  });*/
                $("#delRec").html("<table class='table'>" + str + "</table>");
                $("#delConfirm").show();
            }
        });
    });

    $('#delConfirm').click(function () {
        var id = $('#labelId').val();
        $.ajax({
            type: "POST",
            url: "/RawMaterial/ConfirmLabelDel",
            data: { labelId: id },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                showErrorMessage("Error occurred, no label is deleted.");
            },
            success: function (data) {
                if (data.affected > 0) {
                    showSuccessMessage("Success! Label has been deleted.");
                    setTimeout(function () {
                        $("#labelModal").modal('hide');
                        location.reload();
                    }, 2000);
                }
                else
                    showErrorMessage("Error occurred, no label is deleted.");
            }
        });
    });

    //Update
    $('.btnUpdate').click(function () {
        $('#success-alert').hide();
        $('#error-alert').hide();
        $("#delRec").html("");
        $("#btnReprintConfirm").hide();
        $("#delConfirm").hide();
        $("#modalTitle").html("Edit Labels");
        $("#labelModal").modal('show');
        $('#labelModalSpinner').show();
        $(".modal-backdrop").hide();

        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: "/RawMaterial/GetLabelForDel",
            data: { pol_seq: id },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                $("#labelModal").modal('hide');
                alert("Error occurred, no label info found.");
                $('#labelModalSpinner').hide();
            },
            success: function (data) {
                $('#labelModalSpinner').hide();
                var str = "";
                /* $.each(data, function (ind, value) {*/
                $.each(data, function (key, val) {
                    if (val == null)
                        val = "";
                    if (key == 'pol_seq')
                        $('#labelRecId').val(val);
                    if (key == 'label_Id')
                        $('#labelIdUpd').val(val);

                    if (key == 'p')
                        $('#labelReceiveDate').val(val);

                    //if (key == 'v')
                    //    str += "<tr><td class='font-weight-bold'>Label Print Date:</td><td><input type='date' id='pDate' value='" + val +"' required></td></tr>";
                    if (key == 'amt_Received')
                        $("#amtUpd").val(val);

                    if (key == 'lot_Nbr')
                        $('#lotNoUpd').val(val);

                    if (key == 'tot_Bag_No') {
                        $('#bagNoUpd').val(val);
                        $('#labelTotalBags').val(val);
                    }

                    if (key == 'vendor_Batch') {
                        $('#batchNoUpd').val(val);
                        $('#labelVendorBatch').val(val);
                    }

                    if (key == 'countryOrigin') {
                        $("#originUpd option").filter(function () {
                            //may want to use $.trim in here
                            return $.trim($(this).text()) == $.trim(val);
                        }).prop('selected', true);
                    }

                });

                $('#updateLabelHtml').show();
                $("#btnUpdateConfirm").show();
            }
        });
    });

    //Print
    $('.btnReprint').click(function () {
        $('#success-alert').hide();
        $("#delRec").html("");
        $('#updateLabelHtml').hide();
        $("#delConfirm").hide();
        $("#btnUpdateConfirm").hide();
        $('#labelModalSpinner').show();
        $("#modalTitle").html("Reprint Labels");
        $("#labelModal").modal('show');
        $(".modal-backdrop").hide();

        var id = $(this).data('id');
        var matId = $("#DocNum").val();
        $.ajax({
            type: "POST",
            url: "/RawMaterial/GetLabelForDel",
            data: { pol_seq: id },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                alert("Error occurred, no label info found.");
                $("#labelModal").modal('hide');
                $('#labelModalSpinner').hide();
            },
            success: function (data) {
                $('#labelModalSpinner').hide();
                var str = "";
                var totBag = 0;
                $.each(data, function (key, val) {
                    if (val == null)
                        val = "";
                    if (key == 'pol_seq')
                        $('#seq').val(val);
                    if (key == 'label_Id') {
                        $('#lblidtoprint').val(val);
                        str += "<tr><td class='font-weight-bold'>Pallet Label Id:</td><td>" + val + "</td></tr>";
                    }
                    if (key == 'p')
                        str += "<tr><td class='font-weight-bold'>Product Received Date:</td><td>" + val + "</td></tr>";
                    if (key == 'v')
                        str += "<tr><td class='font-weight-bold'>Label Print Date:</td><td>" + val + "</td></tr>";
                    if (key == 'amt_Received')
                        str += "<tr><td class='font-weight-bold'>Amount Received:</td><td>" + val + " KG</td></tr>";
                    if (key == 'vendor_Batch')
                        str += "<tr><td class='font-weight-bold'>Vendor Batch #:</td><td>" + val + "</td></tr>";
                    if (key == 'lot_Nbr')
                        str += "<tr><td class='font-weight-bold'>Pallet #:</td><td>" + val + "</td></tr>";

                    if (key == 'tot_Bag_No') {
                        totBag = val;
                    }
                });
                console.log('Bags : ' + totBag);
                if (totBag > 0) {
                    var drp = "";
                    for (var i = 1; i < totBag + 1; i++) {
                        drp += "<option value='" + i + "'>" + i + "</option>";
                    }
                    str += "<tr><td colspan='2' class='font-weight-bold'>Please select bag # to print</td></tr><tr><td class='font-weight-bold'>From <select id='from' class='form-control wid-100'>" + drp + "</select> </td><td class='font-weight-bold'>To <select id='to' class='form-control wid-100'>" + drp + "</select></td></tr>";
                }

                $("#delRec1").html("<input type='hidden' id='matId' value='" + matId + "'><table class='table-sm'>" + str + "</table>");
                //$('#updateLabelHtml').show();
                $("#btnReprintConfirm").show();
            }
        });
    });

    //Reprint with begin no and end no for single bag only
    $('#btnReprintConfirm').click(function () {
        var arrLabel = [];
        var id = $('#lblidtoprint').val();
        arrLabel.push(id);
        var fm = $('#from').val();
        var to = $('#to').val();
        //var matId = $("#matId").val();

        $.ajax({
            type: "POST",
            url: "/RawMaterial/ConfirmReprintLabelsSingle",
            data: { arrLabel: arrLabel, beginBag: fm, endBag: to/*, matId: matId */ },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                showErrorMessage("Error occurred, label printing request was not sent successfully.");
            },
            success: function (data) {

                if (data.success) {
                    var msg = "Label printing request was sent.";

                    if (!data.updatePrintDate)
                        msg += " But, the last print date was not updated successfully.";
                    showSuccessMessage(msg);
                    //$("#labelModal").modal('hide');
                    location.reload();
                }
                else {
                    showErrorMessage("Error occurred, label printing request was not sent successfully.");
                }
            }
        });
    });

    //Update label - only for lot no, batch no, country origin   
    $('#labelModalForm').submit(function (e) {
        e.preventDefault();
        $('#success-alert').hide();
        $('#error-alert').hide();
        var arr = [];
        arr.push($("#labelIdUpd").val());
        var amt = ($("#amtUpd").val() != "") ? arr.push($("#amtUpd").val()) : "";
        arr.push($("#lotNoUpd").val());
        arr.push($("#bagNoUpd").val());
        arr.push($("#batchNoUpd").val());
        var countryOrigin = ($("#originUpd option:selected").text() != "") ? arr.push($("#originUpd option:selected").text()) : "";
        arr.push($('#labelRecId').val());
        arr.push($('#labelReceiveDate').val());
        arr.push($('#labelVendorBatch').val());
        arr.push($('#labelTotalBags').val());

        if (arr.length != 9) {
            //if (arr.length != 8) {
            showErrorMessage("Please enter all the required fields!");
        }
        else {
            $.ajax({
                type: "POST",
                url: "/RawMaterial/ConfirmLabelUpdate",
                traditional: true,
                data: {
                    arr: arr
                },
                //contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: function () {
                    showErrorMessage("Error occurred, no label is updated.");
                },
                success: function (data) {

                    if (data.affected != 0) {
                        showSuccessMessage("Success! Label has been updated.");
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                    else
                        showErrorMessage("Error occurred, no label is updated.");
                }
            });
        }
    }
    );

    //Save Label
    $("#POLabelform").submit(function (e) {
        e.preventDefault();

        var arr = [];
        arr.push($("#DocNum").val());//0       
        arr.push($("#Product_Received_Date").val());//1           

        var amt = $("#Amt_Received").val(); //2  
        if (amt != "") {
            var totReceived = amt * parseInt($("#palletNo").val());
            var imrId = $('#TotQty').val();
            var current = $('#CurrentQty').val();
            var amtExpected = parseInt(imrId) - parseInt(current);
            if (!ValidateAmountReceived(totReceived, imrId, current)) {
                $("#Amt_Received").removeClass("is-valid").addClass("is-invalid");
                /* $("#Amt_Received")[0].setCustomValidity("Please enter correct amount! The amount entered may exceed more than expected.");*/
                $("#amtMsg").html("Please enter correct amount! The amount entered should be no more than " + amtExpected);
                //$("#amtMsg").show();
                arr.push(amt);

            }
            else {
                $("#Amt_Received").removeClass("is-invalid").addClass("is-valid");
                $("#amtMsg").hide();
                arr.push(amt);
            }
        }
        arr.push($("#Lot_Nbr").val());  //3  
        arr.push($("#Tot_Bag_No").val());//4         
        arr.push($("#Batch_No").val());//5
        arr.push($("#CountryOrigin option:selected").text());//6
        arr.push($("#palletNo").val());//7
        arr.push($("#PONum").val());//8        
        arr.push($("#MatNo").val());//9
        arr.push($("#Bill_Of_Lading").val());//10
        arr.push($("#Note").val());//11
        arr.push($("#PackagingMaterial").val());//12
        //arr.push($("#RMuom").val());//13
        arr.push($("#UnitPick").val());//13
        arr.push($("#POSupplierNo").val());//14
        //POSupplierNo
        if ($("#Product_Received_Date").val() != '' && amt != '') {
            $("#modalTitle").html("Create and save Labels");
            $("#labelModal").modal('show');
            $("#labelModalSpinner").show();
            $('#cancelModal').hide();
            $(".modal-backdrop").hide();
            $.ajax({
                type: "POST",
                url: "/RawMaterial/CreateLabel",
                traditional: true,
                data: {
                    arr: arr
                },
                dataType: "json",
                error: function () {
                    $("#labelModalSpinner").hide();
                    showErrorMessage("Error! Failed to create label(s).");
                },
                success: function (data) {
                    $("#labelModalSpinner").hide();

                    var msg = "";
                    if (data.newId > 0) {
                        msg = "Success! Label(s) created. ";
                        showSuccessMessage(msg);
                        setTimeout(function () { $("#labelModal").modal('hide'); location.reload(); }, 3000);
                    }
                    else {
                        msg = "Error! Failed to create label(s). ";
                        showErrorMessage(msg);
                        $('#cancelModal').show();
                    }
                }
            });
        }
    });

    //Report to SAP
    $('#ReportToSAP').click(function () {
        //console.log("yes");
        $('#ReportToSAP').off("click");
        //return;
        $('#success-alert3').hide();
        $('#error-alert3').hide();
        $("#labelModalSpinner3").show();
        //$("#labelModal").modal('show');
        //$("#modalTitle").html("Sending Raw Material Receiving to SAP...");
        var itemId = $("#DocNum").val();
        $.ajax({
            type: "POST",
            url: "/RawMaterial/SendPOToSAP",
            traditional: true,
            data: {
                itemId
            },
            dataType: "json",
            error: function () {
                showErrorMessage3("Error occurred, no label is updated.");
                $("#labelModalSpinner3").hide();
            },
            success: function (data) {

                if (data.success) {
                    showSuccessMessage3("Receiving information was sent successfully!");
                    setTimeout(function () {
                        $("#labelModal").modal('hide');
                        location.reload();
                    }, 3000);
                }
                else
                    showErrorMessage3("Error occurred, no receiving information was sent.");
                $("#labelModalSpinner3").hide();
            }
        });

    }
    );

});


$(document).on("change", "#from", function () {

    var tot = $('#from option').length;
    var valFm = parseInt($("#from").val());
    var valTo = parseInt($("#to").val());

    if (valFm > valTo) {
        if (valFm == tot)
            $("#to").val(valFm);
        else
            $("#to").val(parseInt(valFm) + 1);
    }
});

$(document).on("change", "#to", function () {
    var tot = $('#to option').length;
    var valFm = parseInt($('#from').val());
    var valTo = parseInt($('#to').val());

    if (valTo < valFm) {
        alert("The ending bag number should be equal or greater than the beginning bag number.")
        if (valFm == tot)
            $('#to').val(valFm);
        else
            $('#to').val(parseInt(valFm) + 1);
    }

});



