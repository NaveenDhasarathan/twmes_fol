﻿$(document).ready(function () {

    $('#prodSched').dataTable({
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        //"searching": true,
        "columnDefs": [{
            "targets": [11], 
            "visible": false
        },
        {   "targets": [12],
            "visible": false
        }
        ],
        "paging": true,
        "ordering": true,
        "aaSorting": [0, 'desc'],
        "info": true,
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select class="input-group input-group-sm"><option value="">Show All</option></select>')                   
                    .appendTo($(column.header()))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
    $('#prodSched').show();
    $(document).on("click", ".process-order", function () {
        var myBookId = $(this).data('id');
        $('#processOrderNo').val(myBookId);
    });
});

function AddLink(id, action, controller) {
    window.location.href = "/" + controller +"/" + action + "/?po=" + id;
}