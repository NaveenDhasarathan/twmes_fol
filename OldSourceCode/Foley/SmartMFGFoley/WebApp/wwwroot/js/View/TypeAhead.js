﻿   



//$(function () {
//    $(document).ready(function () {
       
//    });

$(document).ready(function () {

    $("#matDesc").typeahead({
        hint: true,
        highlight: true,
        minLength: 2,
        source: function (request, response) {
            $.ajax({
                url: '/ProcessOrder/GetSAPList/',
                data: { matNum: request },
                dataType: "json",
                type: "POST",
                /*contentType: "application/json; charset=utf-8",*/
                success: function (data) {
                    items = [];
                    map = {};
                    $.each(data.sapList, function (i, item) {
                  
                        var id = item.sap_Matl_Nbr;
                        var name = item.sap_Matl_Descr;
                        map[name] = { id: id, name: name };
                        items.push(name);
                    });
                    response(items);
                    $(".dropdown-menu").css("height", "auto");
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        updater: function (item) {
            $('#updateTypeAhead').val(map[item].id);
            return item;
        }
    });
    //    });     
});