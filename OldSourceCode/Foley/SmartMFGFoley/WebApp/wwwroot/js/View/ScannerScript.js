﻿$(function () {
    $('#scanSTO').click(function () {
        $('#updateLabelHtml').hide();
        //$("#delRec").html("<div class='text-center' style='margin:10px;'><textarea id='scan_label' onkeyup='labelscanchange();' placeholder='Please put the mouse cursor here!' class='form-control' style='max-width:445px !important;height: 132px;margin-bottom: -24px;' ></textarea><br></div>");
        $("#delRec1").html("");
        $("#delRec1").html("<div class='text-center' style='margin:10px;'><textarea id='LabelScanSTO' onkeyup='labelscanchange(); ' placeholder='Please put the mouse cursor here!' class='form-control col-12' ></textarea><br></div>");
        $("#ScanButtonContainer").html('');
        $('#ScanButtonContainer').append("<button id='btnscan' class='btn apm-bg-blue-gradient' style='float:right;' type='button' value='Scan' onclick='ScanningSTONew();' >Scan</button>");
        $("#modalTitle1").html("Scan Labels");
        $("#btnReprintConfirm").hide();
        $("#delConfirm").hide();
        $("#btnUpdateConfirm").hide();
        $("#labelModal").modal('show');
        $("#scan_label").focus();
        $('#labelModal').appendTo("body");
        //$('#ExtraButtons').html("<button class='btn apm-bg-blue-gradient'  type='button' value='Scan' onclick='ScanMultiple();' >Scan</button>");
        //$('#updateLabelHtml').html("<button class='btn apm-bg-blue-gradient'  type='button' value='Scan' onclick='ScanningSTOhNew();' >Scan</button>");
    });

    $('#scanPO').click(function () {
        $('#updateLabelHtml').hide();
        //$("#delRec").html("<div class='text-center' style='margin:10px;'><textarea id='scan_label' onkeyup='labelscanchange();' placeholder='Please put the mouse cursor here!' class='form-control' style='max-width:445px !important;height: 132px;margin-bottom: -24px;' ></textarea><br></div>");
        $("#delRec1").html("");
        $("#scanbox").html('');
        $("#scanbox").append("<div class='text-center' style='margin:10px;'><textarea id='LabelScanPO' onkeyup='labelscanchange(); ' placeholder='Please put the mouse cursor here!' class='form-control col-12' ></textarea><br></div>");
        $("#ScanButtonContainer").html('');
        $('#ScanButtonContainer').append("<button id='btnscan' class='btn apm-bg-blue-gradient' style='float:right;' type='button' value='Scan' onclick='ScanLabelPONew();' >Scan</button>");
        $("#modalTitle").html("Scan Labels");
        $("#btnReprintConfirm").hide();
        $("#delConfirm").hide();
        $("#btnUpdateConfirm").hide();
        $("#labelModal").modal('show');
        $("#scan_label").focus();
        $('#labelModal').appendTo("body");
        $(".modal-backdrop").hide();
        //$('#ExtraButtons').html("<button class='btn apm-bg-blue-gradient'  type='button' value='Scan' onclick='ScanMultiple();' >Scan</button>");
        //$('#updateLabelHtml').html("<button class='btn apm-bg-blue-gradient'  type='button' value='Scan' onclick='ScanningSTONew();' >Scan</button>");
    });

    //$('#scanPO').click(function () {
    //    $("#delRec").html("<div class='form-group'><input id='LabelScanPO' placeholder='Please put the mouse cursor here!' class='form-control col-10' /></div>");
    //    $("#modalTitle").html("Scan Labels");
    //    $("#updateLabelHtml").hide();
    //    $("#btnReprintConfirm").hide();
    //    $("#delConfirm").hide();
    //    $("#btnUpdateConfirm").hide();
    //    $("#labelModal").modal('show');
    //    $("#LabelScanPO").focus();
    //}); 

    //$("#labelModal").on("change", "#LabelScanPO", function () { 
    //$("#LabelScanPO").change(function () { 
    //    var scanText = $('#LabelScanPO').val();
    //    scanText.replace(/[\n\r]+/g, '');
    //    scanText.replace(/\s{2,10}/g, ' ');
    //    var docNumText = $('#DocNum').val();
    //    $("#labelModalSpinner").show();
    //    $.ajax({
    //        type: "POST",
    //        //contentType: 'application/json; charset=utf-8',
    //        dataType: "json",
    //        url: "/RawMaterial/Scanned",
    //        data: {
    //            labelId: scanText,
    //            rawMtlsId: docNumText
    //        },            
    //        error: function () {

    //            $("#labelModalSpinner").hide();
    //            $('#success-alert').hide();
    //            $('#error-alert').html("Scan did not find a record!");
    //            $('#error-alert').show();
    //            //document.getElementById("LabelScanPO").value = "";
    //        },
    //        success: function (data) {
    //            //document.getElementById("LabelScanPO").value = "";
    //            $("#labelModalSpinner").hide();

    //            $('#success-alert').html("Scanned! Label scan date has been updated.");
    //            $('#success-alert').show();
    //            if (!data.batchSentSuccess) {
    //                $('#error-alert').html("Batch information was not successfully sent.")
    //                $('#error-alert').show();
    //            }
    //            setTimeout(function () { $("#labelModal").modal('hide'); location.reload(); }, 6000);
    //        }
    //    });
    //});

    $('#LabelScanGlobal').change(function () {
        var scanText = $('#LabelScanGlobal').val();
        if (scanText != "") {
            $.ajax({
                type: "POST",
                url: "/RawMaterial/Scanned",
                data: { labelId: scanText },
                //contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: function () {
                    $("#alertMsgError").show();
                    $("#alertMsgError").fadeTo(5000, 3000).slideUp(3000, function () {
                        $("#alertMsgError").slideUp(1000);
                    });
                    document.getElementById("LabelScanGlobal").value = "";
                },
                success: function (data) {

                    if (data.success) {
                        $("#alertMsg").show();
                        $("#alertMsg").fadeTo(5000, 3000).slideUp(2000, function () {
                            $("#alertMsg").slideUp(1000);
                        });
                    }
                    else {
                        $("#alertMsgError").show();
                        $("#alertMsgError").fadeTo(5000, 3000).slideUp(3000, function () {
                            $("#alertMsgError").slideUp(1000);
                        });
                    }

                    document.getElementById("LabelScanGlobal").value = "";
                }
            });
        }
        else {
            $("#alertMsgError").html("Please scan your label.");
            $("#alertMsgError").show();
            $("#alertMsgError").fadeTo(3000, 2000).slideUp(1000, function () {
                $("#alertMsgError").slideUp(1000);
            });

        }
    });


    //$('#scanSTO').change(function () {
    //    var mtldetail = ($("#scanSTO").val() + ",").split(",");
    //    var serialNumber = [];
    //    for (var i = 0; i < mtldetail.length; i++) {
    //        if (mtldetail[i] != null && mtldetail[i] != "")
    //            serialNumber.push(mtldetail[i]);
    //    }

    //    $("#labelModal").modal('show');
    //    $('#modalTitle1').html("Scan Label ...");

    //    //var serialNumber = $('#scanSTO').val();
    //    //serialNumber.replace(/[\n\r]+/g, '');
    //    //serialNumber.replace(/\s{2,10}/g, ' ');
    //    if ($('#scanSTO').val() != "") {
    //        $("#labelModalSpinner").show();
    //        $.ajax({
    //            type: "POST",
    //            url: "/RawMaterial/STOScan",
    //            data: { serialNumber },
    //            dataType: "json",
    //            error: function () {
    //                showErrorMessage("The serial number is not found.");
    //                setTimeout(function () { $("#labelModal").modal('hide'); location.reload(); }, 3000);
    //                document.getElementById("scanSTO").value = "";
    //            },
    //            success: function (data) {
    //                $("#labelModalSpinner").hide();
    //                if (data.success) {
    //                    showSuccessMessage("Received!");
    //                    setTimeout(function () { $("#labelModal").modal('hide'); location.reload(); }, 3000);
    //                }
    //                else {
    //                    showErrorMessage("The Label ID :" + $('#scanSTO').val() + " is not found.");
    //                    ScanExternalSTO();
    //                    //setTimeout(function () { $("#labelModal").modal('hide'); location.reload(); }, 3000);
    //                }

    //                document.getElementById("scanSTO").value = "";
    //            }
    //        });
    //    }
    //    else {
    //        $("#alertMsgError").html("Please scan your label.");
    //        $("#alertMsgError").show();
    //        $("#alertMsgError").fadeTo(3000, 2000).slideUp(1000, function () {
    //            $("#alertMsgError").slideUp(1000);
    //        });

    //    }
    //});
    $('#scanSerialNo').change(function () {
        $("#stoModal").modal('show');
        var StorageUnitNumberMultiple = $('#scanSerialNo').val().split("/");
        var ItemNo = $('#Item_Nbr').val();
        var DeliveryNo = $('#DeliveryNo').val();
        var DocNum = $('#DocNum').val();
        var mtldetail = $("#Item_Nbr option:selected").text().split("/");
        var ItemID = mtldetail[0];
        StorageUnitNumber.replace(/[\n\r]+/g, '');
        StorageUnitNumber.replace(/\s{2,10}/g, ' ');
        var load = $("#labelModalSpinner2");

        if (StorageUnitNumber != "") {
            load.show();
            $.ajax({
                type: "POST",
                url: "/RawMaterial/SearchForStorageUnitMultiple",
                data: { StorageUnitNumberMultiple, DeliveryNo, ItemNo, DocNum, ItemID },
                dataType: "json",
                error: function () {
                    showErrorMessage("The serial number is not found.");
                    setTimeout(function () { $("#stoModal").modal('hide'); location.reload(); }, 3000);
                    document.getElementById("scanSTO").value = "";
                },
                success: function (data) {
                    if (data.success) {
                        showSuccessMessage("Received!");
                        setTimeout(function () { $("#stoModal").modal('hide'); location.reload(); }, 3000);
                        //data.UnitNumber
                        //data.MaterialId
                        //data.BatchId
                        //data.NetQuantity
                        //data.UnitOfMeasure
                    }
                    else {
                        showErrorMessage("The delivery number is not found.");
                        setTimeout(function () { $("#stoModal").modal('hide'); location.reload(); }, 3000);
                    }

                    document.getElementById("scanSTO").value = "";
                }
            });
        }
        else {
            $("#alertMsgError").html("Please scan your label.");
            $("#alertMsgError").show();
            $("#alertMsgError").fadeTo(3000, 2000).slideUp(1000, function () {
                $("#alertMsgError").slideUp(1000);
            });

        }
    });


    $('#GetUnConsumeLabels').change(function () {
        var scanText = $('#GetUnConsumeLabels').val();
        $.ajax({
            type: "POST",
            url: "/Purchase_Order_Labels/UnConsumptionScanned",
            data: { scan: scanText },
            dataType: "json",
            success: function (data) {
                data = $.parseJSON(data);
                document.getElementById("materialDescription").value = data.SAPMat;
                document.getElementById("orignialPounds").value = data.AmtRec;
                document.getElementById("scannedMaterials").value = data.ScannedMaterial;
            },
            error: function () {
                alert("Scan did not find a record!");
                document.getElementById("GetUnConsumeLabels").value = "";
            }
        });
    });

    $('#UnConsumeSave').click(function () {
        var scanText = $('#GetUnConsumeLabels').val();
        var adjusedAmt = $('#adjustedPounds').val();
        $.ajax({
            type: "POST",
            url: "/RawMaterial/UnConsumptionSaved",
            data: '{ scan: "' + scanText + '", adjusedAmt: "' + adjusedAmt + '" }',
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                alert("We didn't save that, please scan again.");
                document.getElementById("GetUnConsumeLabels").value = "";
            },
            success: function () {
                location.reload();
            }
        });
    });

})