﻿
$(document).ready(function () { 

    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    $("#matModal").on("change", "#GetLabelsMat", function () {
   
   // $('#GetLabelsMat').change(function () {
        var scanText = $('#GetLabelsMat').val();
    $.ajax({
        type: "POST",
        url: "/ProcessOrder/ScannedInRawMaterial",
        data: { scan: scanText, qtyScanned: $('#matQtyScanned').val(), poPicked: $('#poPicked').val() },
        dataType: "json",
        success: function (data) {
            if (data.rtn > 0) {
                $('#' + data.label_Id + "row").addClass("match");
            }
            alert(data.totScanned)
            $('#matQtyScanned').val(data.totScanned);
            //alert("Scan successful!");
            //$.each(data, function (ind, value) {
            //    if (ind == "matNum") document.getElementById("materialNumber").value = value;
            //    if (ind == "sapMat") document.getElementById("materialDescription").value = value;
            //    if (ind == "amtRec") document.getElementById("scannedPounds").value = value;
            //    if (ind == "scannedMaterial")document.getElementById("scannedMaterials").value = value;
            //});
            //$('#MatScanned').show();
           
        },
        error: function () {
            alert("Scan did not find a record!");
            document.getElementById("GetLabelsMat").value = "";
        }
    });
});

//$('#RawMatSave').click(function () {
//    var scanText = $('#GetLabelsMat').val();
//    var poNum = $('#processOrderNumber').val();
//    var feeder = $('#FeederRaw').val();
//    var manAmt = $('#enteredPounds').val();
//    $.ajax({
//        type: "POST",
//        url: "/ProcessOrder/ScanSaved",
//        data: { scan: scanText, PONum:poNum, feeder:feeder, manAmt:manAmt },
//       //contentType: 'application/json; charset=utf-8',
//        dataType: "json",
//        error: function () {
//            alert("We didn't save that, please scan again.");
//            document.getElementById("GetLabelsMat").value = "";
//        },
//        success: function () {
//            //alert("Scan successful!");
//            location.reload();
//        }
//    });
//});

$('#AltBOMSave').click(function () {
    var poNum = $('#processOrderNumber').val();
    var matNum = $('#updateTypeAhead').val();
    var matAmt = $('#materialAmt').val();
    $.ajax({
        type: "POST",
        url: "/ProcessOrder/AltBOMSave",
        data: { PONum: poNum, matNum:matNum, matAmt:matAmt},       
        dataType: "json",
        error: function () {
            showErrorMessage("Error occurred, no material was added.");  
            document.getElementById("GetLabels").value = "";
        },
        success: function () {
            showSuccessMessage("Success! Alt Material has been added.");
            setTimeout(function () {
                location.reload();
            }, 2000);            
        }
    });
});

    $('.btnDelete').click(function () {
        var id = $(this).data('id');
        $('#altModalDelete').modal("show");       
       
        $.ajax({
            type: "POST",
            url: "/ProcessOrder/ConfirmAltDel",
            data: { pol_seq: id },          
            dataType: "json",
            error: function () {
                showErrorMessage("Error occurred, the record is NOT deleted.");
            },
            success: function (data) {
                if (data.rtn > 0) {
                    $('#success-alertDelete').html("Success! The record has been deleted.");
                    $('#success-alertDelete').show();
                    $('#error-alertDelete').hide();
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
                else {
                    $('#success-alert').hide();
                    $('#error-alert').html("Error occurred, the record is NOT deleted.");
                    $('#error-alert').show();
                }                  
            }
        });
    });

    $('.btnBlend').click(function () {       
        var tr = "";
        var cnt = 0;    
        var tot = 0;
        $('#BOMList input:checked').each(function () {
            var val = $(this).attr('value');
            tot += parseFloat($('#bom_qty_' + val).html());        
        });

        $('#BOMList input:checked').each(function () {
            var val = $(this).attr('value');            
            tr += "<tr><td class='col-2'>" + $('#bom_matdesc_' + val).html() + "</td><td class='col-1'><input value='" + $('#bom_percent_' + val).html() + "' class='form-control'></td><td class='col-1'><input class='form-control' value='" + (parseFloat($('#bom_qty_' + val).html())/tot).toFixed(4)*100 + "'></td><td class='col-1'><input value='" + $('#bom_qty_' + val).html() + "' class='form-control'></td></tr>";
            cnt++;           
        });

        if (cnt > 0) {
            $('#BlendList').show();
            $('#btnAddDrums').show();
            $('#TotItems').val(cnt);
            $('.BlendListBody').append(tr);
        }
     
    });

    $('#btnAddMix').click(function () {
        var tr = "";
        var cnt = 0;
        $('#BOMList input:checked').each(function () {
            var val = $(this).attr('value');
            var current = "";
            current = "<div class='row'><div class='col-6'><div class='input-group input-group-sm mb-3'><div class='input-group-prepend'>\
                        <span class='input-group-text' id='inputGroup-sizing-sm'>"+ $('#bom_matdesc_' + val).html().slice(0, $('#bom_matdesc_' + val).html().indexOf(" "))+ "</span></div>\
                        <input type='text' class='form-control' aria-label='Small' aria-describedby='inputGroup-sizing-sm' id='m_"+ cnt + "' value='"+ $('#bom_matno_' + val).html() + "'></div></div>\
                        <div class='col-3'><div class='input-group input-group-sm mb-3'><div class='input-group-prepend'>\
                        <span class='input-group-text' id='inputGroup-sizing-sm'>Lot No.</span></div>\
                        <input type='text' class='form-control' aria-label='Small' aria-describedby='inputGroup-sizing-sm' id='mLot_"+ cnt +"' ></div></div>\
                        <div class='col-3'><div class='input-group input-group-sm mb-3'><div class='input-group-prepend'>\
                        <span class='input-group-text' id='inputGroup-sizing-sm'>Weight</span></div>\
                        <input type='text' class='form-control' aria-label='Small' aria-describedby='inputGroup-sizing-sm' id='mWgt_"+ cnt +"' ></div></div></div>";
            tr += current;
            cnt++;
        });

        if (cnt > 0) {
            $('#BlendList').show();
            $('#btnAddDrums').show();
            $('#TotItems').val(cnt);       
            $('#mixDetail').html("<div class='col-12'>" +tr+"</div>");
        }

    });

    $('#SaveMixture').click(function () {
        var tot = $('#TotItems').val(); 
        var arrLot = [];
        var arrWgt = [];
        if (parseInt(tot) > 0) {
            for (var i = 0; i < tot; i++){
                arrLot.push($("#mLot_" + i).val());
                arrWgt.push($("#mWgt_" + i).val());
    }
        }
        var arr = [];
        $.ajax({
            type: "POST",
            url: "/RunSheet/SaveMixture",
            data: {arrLot,arrWgt },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                alert("Error occurred, no label info found.");
            },
            success: function (data) {
                if (data.rtn > 0) {
                    showSuccessMessage("Success! Alt Material has been updated.");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
                else
                    showErrorMessage("Error occurred, alt Material has NOT been updated.");

            }
        });

    });

$('.btnUpdate').click(function () {
    $('#success-alert').hide();
   
    var id = $(this).data('id');
    $('#matDesc').val($(this).data('desc'));
    $('#matDesc').attr('readonly', true);
    $('#altModal').modal("show");
    $('#AltBOMUpdate').show();
    $('#AltBOMSave').hide();
    $('#modalTitle').html('Edit Alternate BOM');

    $.ajax({
        type: "POST",
        url: "/ProcessOrder/GetAltMaterialForUpdate",
        data: { pol_seq: id },
        //contentType: 'application/json; charset=utf-8',
        dataType: "json",
        error: function () {
            alert("Error occurred, no label info found.");
        },
        success: function (data) {           
            /* $.each(data, function (ind, value) {*/
            $.each(data.altMat, function (key, val) {    
                
                if (key == 'po_Alt_Bom_Seq')
                    $('#Po_Alt_Bom_Seq').val(val);

                if (key == 'po_Nbr')
                    $('#processOrderNumber').val(val);           
                                              
                if (key == 'subst_Matl_Nbr') {
                    $('#updateTypeAhead').val(val);
                    $('#updateTypeAhead').attr('readonly', true);
                }
              
                if (key == 'subst_Qty')
                    $('#materialAmt').val(val);
            });           
        
        }
    });
});

   

    $('#AltBOMUpdate').click(function () {

        $.ajax({
            type: "POST",
            url: "/ProcessOrder/SaveAltBomUpdate",
            data: { pol_seq: $('#Po_Alt_Bom_Seq').val(), subQty: $('#materialAmt').val() },
            //contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function () {
                alert("Error occurred, no label info found.");
            },
            success: function (data) {
                if (data.rtn > 0) {
                    showSuccessMessage("Success! Alt Material has been updated.");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
                else
                    showErrorMessage("Error occurred, alt Material has NOT been updated."); 
             
            }
        });

    });

$('.btnPickMaterial').click(function () {
  
    $('#success-alert').hide();
    $('#matModal').modal('show');  
    $("#matSelected").html("Material Number: " + $(this).data('id'));
    $("#matDescSelected").html($(this).data('desc'));
    $("#matQty").val($(this).data('qty'));
    $('#poPicked').val($(this).data('po'));//hidden

    $.ajax({
        type: "POST",
        url: "/ProcessOrder/GetInventory",
        data: { matNbr: $(this).data('id') },
        //contentType: 'application/json; charset=utf-8',
        dataType: "json",
        error: function () {
            alert("Error occurred, no label info found.");
        },
        success: function (data) {
            var strRow = "";
            var matNum = "";
            $.each(data.mat, function (ind, value) {
                var str = "";
               
                $.each(value, function (key, val) {
                   
                    if (key == 'label_Id')
                        str = "<td id='" + val+"row'>" + val + "</td>";
                    if (key == 'amt_Received')
                        str +="<td>"+val+"</td>";
                    if (key == 'location')
                        str += "<td>" + val + "</td>";
                    if (key == "product_Received_Date")
                        str += "<td>" + val + "</td>";
                   
                });
                strRow += "<tr>" + str + "</tr>";
            });           
     
            $('#content').html("<thead><th>Label ID</th><th>Qty</th><th>Warehouse Location</th><th>Material Received Date</th></thead><tbody>" + strRow+"</tbody>");         
        }
    });
});
});

function showErrorMessage(msg) {
    $('#success-alert').hide();
    $('#error-alert').html(msg);
    $('#error-alert').show();
}

function showSuccessMessage1(msg) {
    $('#success-alert').html(msg);
    $('#success-alert').show();
    $('#error-alert').hide();
}

