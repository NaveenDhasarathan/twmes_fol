﻿$("#NewSTOForm").validate({
    messages: {
        serialNo: "* Required",
        Product_Received_Date: "* A date is required.",
        Amt_Kg: "* Required.",
        CountryOrigin: "* Required",
        Batch_No: "* Required",
        Bill_Of_Lading: "* Required"
    }
});   