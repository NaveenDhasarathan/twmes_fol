﻿function showErrorMessage(msg) {
    $('#success-alert').hide();
    $('#error-alert').html(msg);
    $('#error-alert').show();
}

function showSuccessMessage(msg) {
    $('#success-alert').html(msg);
    $('#success-alert').show();
    $('#error-alert').hide();
}
function showSuccessMessage3(msg) {
    $('#success-alert3').html(msg);
    $('#success-alert3').show();
    $('#error-alert3').hide();
}
function showErrorMessage3(msg) {
    $('#success-alert3').hide();
    $('#error-alert3').html(msg);
    $('#error-alert3').show();
}

function checkValidation(val, id) {
    var inpObj = document.getElementById(id);

    if (val == null || val == "") {
        $("#" + id).removeClass("is-valid").addClass("is-invalid");
    } else if (!inpObj.checkValidity()) // invalid
    {
        $("#" + id).removeClass("is-valid").addClass("is-invalid");
    } else if (inpObj.checkValidity()) //valid
    {
        $("#" + id).removeClass("is-invalid").addClass("is-valid");
    }
}

function checkValidationAllowNull(val, id) {
    var inpObj = document.getElementById(id);

    if (!inpObj.checkValidity()) // invalid
    {
        $("#" + id).removeClass("is-valid").addClass("is-invalid");
    } else if (inpObj.checkValidity()) //valid
    {
        $("#" + id).removeClass("is-invalid").addClass("is-valid");
    }
}

function checkValidationNotZero(val, id) {
    var inpObj = document.getElementById(id);
    if (val == null || val == "" || val == "0" || val == "0.000") {
        $("#" + id).removeClass("is-valid").addClass("is-invalid");
    } else if (!isNumeric(val)) // invalid
    {
        $("#" + id).removeClass("is-valid").addClass("is-invalid");
    }
    else if (inpObj.checkValidity()) //valid
    {
        $("#" + id).removeClass("is-invalid").addClass("is-valid");
    }
}
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

$('.selectAll').click(function () {
    var example = $('.selectAllRow').dataTable().DataTable();
    if ($("th.select-checkbox").hasClass("selected")) {
        example.rows().deselect();
        $("th.select-checkbox").removeClass("selected");
    } else {
        example.rows().select();
        $("th.select-checkbox").addClass("selected");
    }
}).on("select deselect", function () {
    var example = $('.selectAllRow').dataTable().DataTable();
    if (example.rows({
        selected: true
    }).count() !== example.rows().count()) {
        $("th.select-checkbox").removeClass("selected");
    } else {
        $("th.select-checkbox").addClass("selected");
    }
});