﻿/*$(function () {*/
$(document).ready(function () {

    //STO table     
    $('#RawMatsSTO').show();
    $('#RawMatsSTO').dataTable({
        dom: 'Bfrtip',
        searchHighlight: true,
        buttons: [
            {
                extend: 'excel',
                exportOptions: {
                    columns: 'th:not(.noprint),tr:not(.noprint)'
                },
                header: false
            }
            //'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "language": {
            "search": "Scan/Search"
        },
        "lengthMenu": [[15, 25, 50, 100, -1], [15, 25, 50, 100, "All"]],
        "columnDefs": [{
            "targets": [9], //Don't forget to change format(d)
            "visible": false
        }],
        "paging": true,
        "ordering": true,
        "aaSorting": [0, 'desc'],
        "info": true,
        "scrollX": true,
        "bDestroy": true,
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select class="input-group input-group-sm noprint"><option value="">Show All</option></select>')
                    //.appendTo($(column.footer()).empty())
                    .appendTo($(column.header()))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    if (!InArray(d))
                        select.append('<option class="noprint" value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
    var tableSTO = $('#RawMatsSTO').DataTable();
    $('#ExportExcelSTO').click(() => {
        //tableSTO.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
        $('#RawMatsSTO').DataTable().buttons(0, 0).trigger()
        //tableSTO.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
    });
    $('#searchInput').on('keyup', function () {
        tableSTO.search(this.value).draw();
    });

    $('#RawMatsSTO tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tableSTO.row(tr);

        //console.log(row.child);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            console.log(row.data());
            row.child(formatSTO(row.data())).show();
            tr.addClass('shown');
        }
    });

    //PO Table
    $('#RawMats').show();
    $('#RawMats').dataTable({
        dom: 'Bfrtip',
        searchHighlight: true,
        buttons: [
            {
                extend: 'excel',
                exportOptions: {
                    columns: 'th:not(.noprint),tr:not(.noprint)'
                },
                header: false
            }
            //'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "language": {
            "search": "Scan/Search"
        },
        "lengthMenu": [[15, 25, 50, 100, -1], [15, 25, 50, 100, "All"]],
        //"searching": true,
        "columnDefs": [{
            "targets": [5], //Don't forget to change format(d)
            "visible": false
        }],
        "paging": true,
        "ordering": true,
        "aaSorting": [0, 'desc'],
        "info": true,
        "scrollX": true,
        "bDestroy": true,
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select class="input-group input-group-sm"><option value="">Show All</option></select>')
                    //.appendTo($(column.footer()).empty())
                    .appendTo($(column.header()))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    if (!InArray(d))
                        select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
    $('#ExportExcelPO').click(() => {
        $('#RawMats').DataTable().buttons(0, 0).trigger()
    })
    var table = $('#RawMats').DataTable();
    $('#searchInput').on('keyup', function () {
        table.search(this.value).draw();
    });

    $('#RawMats tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        console.log(row.child);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
    $('.buttons-excel').hide();
    filterDates();
});

var HeaderArray = ["PO Number", "Delivery Number", "Request Delivery Date", "Document Type", "Material Type", "SAP Sent", "Supplier Name", "Supplier Number"];
function InArray(needle) {
    return (HeaderArray.indexOf(needle) > -1);
}

function format(d) {
    // `d` is the original data object for the row

    //alert(d[5]);

    //Data is from the nth column which is invisible
    if (d[5] != "") {
        console.log(d[5]);
        var arr = d[5].split("~");
        var str = '';
        var uom = "";
        for (i = 0; i < arr.length - 1; i++) {
            var arr2 = arr[i].split("|");
            if (arr2[7] == "PO")
                str = str + "<tr onclick=location.href='../RawMaterial/PurchaseOrderLabel/" + arr2[2] + "'>"
            else
                str = str + "<tr onclick=location.href='../RawMaterial/InternalSTO/" + arr2[2] + "'>";
            for (j = 0; j < arr2.length; j++) {

                if (j != 2 && j != 7 && j != 8)
                    str = str + '<td>' + arr2[j] + '</td>';
            }
            str = str + "</tr>";
            uom = arr2[8];
        }
        str = '<table class="table table-bordered table-hover table-sm blue2-header"><thead><tr><th>Item Number</th><th>Material Number</th><th>Material Description</th><th>Qty (' + uom + ')</th><th>Qty Received (' + uom + ')</th><th>Request Delivery Date</th></tr></thead><tbody>' + str + '</tbody></table>';
    }

    return str;
}

function formatSTO(d) {
    //alert(d[7]);
    //Data is from the nth column which is invisible
    console.log(d[10]);
    if (d[10] != "") {
        var arr = d[10].split("~");
        var str = '';
        var uom = "";
        for (i = 0; i < arr.length - 1; i++) {
            var arr2 = arr[i].split("|");
            if (arr2[7] == "PO")
                str = str + "<tr onclick=location.href='./RawMaterial/PurchaseOrderLabel/" + arr2[2] + "'>";
            else
                str = str + "<tr style='cursor:pointer' onclick=location.href='./RawMaterial/InternalSTO/" + arr2[2] + "'>";
            for (j = 0; j < arr2.length - 1; j++) {
                console.log(arr2[j]);
                if (j != 2 && j != 6 && j != 7 && j != 8) {
                    //if (j == 9)
                    //    str = str + '<td style="display:none;">' + arr2[j] + '</td>';
                    //else
                    str = str + '<td>' + arr2[j] + '</td>';
                }
            }
            str = str + "</tr>";
            uom = arr2[8];
        }
        str = '<table class="table table-bordered table-hover table-sm blue2-header"><thead><tr><th>Item Number</th><th>Material Number</th><th>Material Description</th><th>Qty (' + uom + ')</th><th>Qty Received (' + uom + ')</th><th style="display:none;">Request Delivery Date</th></tr></thead><tbody>' + str + '</tbody></table>';
    }

    return str;
}
