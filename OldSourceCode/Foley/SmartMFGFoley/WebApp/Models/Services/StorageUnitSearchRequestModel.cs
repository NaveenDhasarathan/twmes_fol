﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Services
{
    public class StorageUnitSearchRequestModel
    {
        [Required(ErrorMessage = "Storage Unit Number is required.")]
        [StringLength(9, MinimumLength = 9, ErrorMessage = "Storage Unit Number must be 9 characters long.")]
        public string StorageUnitNumber { get; set; }
        public string[] StorageUnitNumberMultiple { get; set; }
    }
}
