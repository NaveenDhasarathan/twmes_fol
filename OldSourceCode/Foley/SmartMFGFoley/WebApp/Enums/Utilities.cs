﻿namespace WebApp.Enums
{
    public class Utilities
    {
        public enum UnitOfMeasure
        {
            Unknown = 0,
            Kilogram = 1,
            Pound = 2,
        }
    }
}
