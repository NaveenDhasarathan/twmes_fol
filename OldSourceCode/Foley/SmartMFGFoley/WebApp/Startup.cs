using DataLibrary.DataAccess;
using DataLibrary.DataAccess.Abstract;
using DataLibrary.Processors;
using DataLibrary.Processors.Abstract;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Common.Services;
using WebApp.Common.Services.Abstract;

namespace SmartMFG_SUZ
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            
            services.AddScoped<IDataAccessBase, DataAccessBase>();
            services.AddScoped<IReadDataAccess, ReadDataAccess>();
            services.AddScoped<IReadWriteDataAccess, ReadWriteDataAccess>();
            services.AddSingleton<INppqsDataAccess, NppqsDataAccess>();
            services.AddSingleton<IShopFloorDataAccess, ShopFloorDataAccess>();

            services.AddScoped<IInboundRawMaterialsProcessor, InboundRawMaterialsProcessor>();
            services.AddScoped<ISapMatlMasterProcessor, SapMatlMasterProcessor>();
            services.AddScoped<IRawMaterialProcessor, RawMaterialProcessor>();
            services.AddScoped<IProcessOrder, ProductionScheduleProcessor>();
            services.AddScoped<IProcessOrderMaterialsProcessor, ProcessOrderMaterialsProcessor>();
            services.AddScoped<IRunSheet, RunSheetProcessor>();

            services.AddScoped<ILimsService, LimsService>();
            services.AddScoped<IRunSheet, RunSheetProcessor>();
            //services.AddControllersWithViews();
            services.AddControllersWithViews(x => x.SuppressAsyncSuffixInActionNames = false).AddRazorRuntimeCompilation();
            services.AddScoped<IRawMaterialsService, RawMaterialsService>();
            
            services.AddControllersWithViews();           

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
