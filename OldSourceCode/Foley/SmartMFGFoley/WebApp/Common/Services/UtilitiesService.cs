﻿namespace WebApp.Common.Services
{
    public static class UtilitiesService
    {
        public static decimal PoundToKilogramRatio = 2.2046226218488M;

        public static decimal ConvertWeight(decimal weight, Enums.Utilities.UnitOfMeasure fromUnit, Enums.Utilities.UnitOfMeasure toUnit)
        {
            if (fromUnit == Enums.Utilities.UnitOfMeasure.Pound && 
                toUnit == Enums.Utilities.UnitOfMeasure.Kilogram)
            {
                return weight / PoundToKilogramRatio;
            }
            else if (fromUnit == Enums.Utilities.UnitOfMeasure.Kilogram && 
                     toUnit == Enums.Utilities.UnitOfMeasure.Pound)
            {
                return weight * PoundToKilogramRatio;
            }
            else
            {
                return weight;
            }
        }

        public static string ConvertWeightAsString(decimal weight, Enums.Utilities.UnitOfMeasure fromUnit, Enums.Utilities.UnitOfMeasure toUnit) => 
            ConvertWeight(weight, fromUnit, toUnit).ToString("N3");

        public static Enums.Utilities.UnitOfMeasure ParseUnitOfMeasure(string unitOfMeasure)
        {
            return unitOfMeasure.ToUpper() switch
            {
                "KG" => Enums.Utilities.UnitOfMeasure.Kilogram,
                "KGS" => Enums.Utilities.UnitOfMeasure.Kilogram,
                "KILOGRAM" => Enums.Utilities.UnitOfMeasure.Kilogram,
                "KILOGRAMS" => Enums.Utilities.UnitOfMeasure.Kilogram,
                "LB" => Enums.Utilities.UnitOfMeasure.Pound,
                "LBS" => Enums.Utilities.UnitOfMeasure.Pound,
                "POUND" => Enums.Utilities.UnitOfMeasure.Pound,
                "POUNDS" => Enums.Utilities.UnitOfMeasure.Pound,
                "#" => Enums.Utilities.UnitOfMeasure.Pound,
                _ => Enums.Utilities.UnitOfMeasure.Unknown,
            };
        }
    }
}
