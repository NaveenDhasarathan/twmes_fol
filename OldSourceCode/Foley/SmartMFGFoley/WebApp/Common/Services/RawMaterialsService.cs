﻿using DataLibrary.DataAccess.Abstract;
using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using WebApp.Common.Services.Abstract;
using WebApp.Models.Warehouse;
using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using WebApp.Models.Services;

namespace WebApp.Common.Services
{
    public class RawMaterialsService : IRawMaterialsService
    {
        private readonly IRawMaterialProcessor _rawMaterialProcessor;
        private readonly INppqsDataAccess _nppqsDataAccess;
        private readonly IShopFloorDataAccess _shopFloorDataAccess;
        private readonly ILogger<RawMaterialsService> _logger;

        public RawMaterialsService(
            IRawMaterialProcessor rawMaterialProcessor,
            INppqsDataAccess nppqsDataAccess,
            IShopFloorDataAccess shopFloorDataAccess,
            ILogger<RawMaterialsService> logger
        )
        {
            _rawMaterialProcessor = rawMaterialProcessor;
            _nppqsDataAccess = nppqsDataAccess;
            _shopFloorDataAccess = shopFloorDataAccess;
            _logger = logger;
        }

        public StorageUnitSearchResultModel SearchForStorageUnit(StorageUnitSearchRequestModel request)
        {
            StorageUnitModel result = SearchSmartMfgForStorageUnit(request.StorageUnitNumber);

            if (result == null)
            {
                result = SearchExternalForStorageUnit(request.StorageUnitNumber);

                if (result == null)
                {
                    return new StorageUnitSearchResultModel
                    {
                        StorageUnit = null,
                        InSmartMfg = false,
                        Success = false,
                        ErrorMessage = "Unable to find data for storage unit.",
                    };
                }
                else
                {
                    return new StorageUnitSearchResultModel
                    {
                        StorageUnit = result,
                        InSmartMfg = false,
                        Success = true,
                        ErrorMessage = "",
                    };
                }
            }
            else
            {
                return new StorageUnitSearchResultModel
                {
                    StorageUnit = result,
                    InSmartMfg = true,
                    Success = true,
                    ErrorMessage = "",
                };
            }
        }

        public StorageUnitModel SearchSmartMfgForStorageUnit(string storageUnitNumber)
        {
            PurchaseOrderLabelsModel result = _rawMaterialProcessor.GetLabelByLabelId(storageUnitNumber);

            if (result == null)
            {
                return null;
            }

            return new StorageUnitModel
            {
                UnitNumber = result.Label_Id,
                BatchId = result.Batch_Id,
                MaterialId = null,
                NetQuantity = result.Amt_Received,
                UnitOfMeasure = null,
            };
        }

        public StorageUnitModel SearchExternalForStorageUnit(string storageUnitNumber)
        {
            try
            {
                StorageUnitModel result = storageUnitNumber[0] switch
                {
                    '8' => SearchShopFloorForStorageUnit(storageUnitNumber),
                    _ => SearchNppqsForStorageUnit(storageUnitNumber),
                };

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "Failed to get storage unit data for {0}", storageUnitNumber);
                return null;
            }
        }
        public bool SearchExternalForBatchID(string BatchID)
        {
            bool result = false;
            try
            {
                StorageUnitModel nppqs = SearchNppqsForBatchID(BatchID);
                StorageUnitModel shopfloor = SearchShopFloorForBatchID(BatchID);
                if (nppqs != null || shopfloor != null)
                    result = true;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "Failed to get Batch ID data for {0}", BatchID);
                return result;
            }
        }
        public bool SearchForBatchID(string BatchID)
        {
            bool result = false;
            try
            {
                StorageUnitModel nppqs = SearchNppqsForBatchID(BatchID);
                StorageUnitModel shopfloor = SearchShopFloorForBatchID(BatchID);
                if (nppqs != null || shopfloor != null)
                    result = true;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "Failed to get Batch ID data for {0}", BatchID);
                return result;
            }
        }

        public StorageUnitModel SearchNppqsForStorageUnit(string storageUnitNumber)
        {
            DynamicParameters queryParams = new DynamicParameters();
            queryParams.Add(":StorageUnitNumber", storageUnitNumber);

            return _nppqsDataAccess.LoadData<StorageUnitModel>(
                @"SELECT serial_id AS UnitNumber,
                             material_id AS MaterialId,
                             batch_id AS BatchId,
                             net_kgs AS NetQuantity,
                             'KG' AS UnitOfMeasure
                        FROM nppqaa.batch_labels
                       WHERE serial_id = :StorageUnitNumber",
                queryParams
            ).FirstOrDefault();
        }

        public StorageUnitModel SearchNppqsForBatchID(string BatchID)
        {
            DynamicParameters queryParams = new DynamicParameters();
            queryParams.Add(":BatchID", BatchID);

            return _nppqsDataAccess.LoadData<StorageUnitModel>(
                @"SELECT 
                            serial_id AS UnitNumber,
                             material_id AS MaterialId,
                             batch_id AS BatchId,
                             net_kgs AS NetQuantity,
                             'KG' AS UnitOfMeasure
                        FROM nppqaa.batch_labels
                       WHERE batch_id = :BatchID",
                queryParams
            ).FirstOrDefault();
        }

        public StorageUnitModel SearchShopFloorForStorageUnit(string storageUnitNumber)
        {
            DynamicParameters queryParams = new DynamicParameters();
            queryParams.Add(":StorageUnitNumber", storageUnitNumber);

            return _shopFloorDataAccess.LoadData<StorageUnitModel>(
                @"SELECT storage_unit_no AS UnitNumber,
                             material_no AS MaterialId,
                             sap_batch AS BatchId,
                             net_qty AS NetQuantity,
                             uom AS UnitOfMeasure
                        FROM shpflr.sf_production
                       WHERE storage_unit_no = :StorageUnitNumber",
                queryParams
            ).FirstOrDefault();
        }
        public StorageUnitModel SearchShopFloorForBatchID(string BatchID)
        {
            DynamicParameters queryParams = new DynamicParameters();
            queryParams.Add(":BatchID", BatchID);

            return _shopFloorDataAccess.LoadData<StorageUnitModel>(
                @"SELECT  
                            storage_unit_no AS UnitNumber,
                             material_no AS MaterialId,
                             sap_batch AS BatchId,
                             net_qty AS NetQuantity,
                             uom AS UnitOfMeasure
                        FROM shpflr.sf_production
                       WHERE sap_batch = :BatchID",
                queryParams
            ).FirstOrDefault();
        }
    }
}
