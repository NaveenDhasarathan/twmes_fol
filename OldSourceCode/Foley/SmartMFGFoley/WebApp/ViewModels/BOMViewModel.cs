﻿using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels
{
    public class BOMListViewModel
    {             
        public List<BOMViewModel> BOMList { get; set; }
        public decimal Total { get; set; }
    }

    public class BOMViewModel
    {
        public int Po_Nbr { get; set; }
        public string Sap_Matl_Nbr { get; set; }
        public int Quantity_Required { get; set; }
        public string Material_Description { get; set; }
        public string UOM { get; set; }
        public string Matl_Type { get; set; }
        public string RowStyle { get; set; }
        public decimal Percent { get; set; }
    }
}
