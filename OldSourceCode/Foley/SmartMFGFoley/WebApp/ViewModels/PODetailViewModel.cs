﻿using DataLibrary.Models;
using System.Collections.Generic;

namespace WebApp.ViewModels
{
    public class PODetailViewModel
    {       
        public long Document_Nbr { get; set; }     
        public string Supplier_Name { get; set; }      
        public string Supplier_Nbr { get; set; }

        public string MaterialDesc { get; set; }

        public List<InboundRawMaterialsModel> ItemList { get; set; }

    }
}
