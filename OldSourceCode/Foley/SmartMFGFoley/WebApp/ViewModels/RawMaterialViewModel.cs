﻿using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace WebApp.ViewModels
{
    public class RawMaterialViewModel
    {
        public RawMaterialDetailModel Ibr { get; set; }
        public IEnumerable<PurchaseOrderLabelsModel> Polabels { get; set; }
        public SAPMatlMasterModel Sapm { get; set; }
        public bool PackingMaterial { get; set; }    

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n0}")]
        public decimal PoLablesAmount { get; set; }
        public decimal ScannedAmount { get; set; }
        public bool Completed { get; set; }
        public bool disabled { get; set; }
        public DateTime? SapSentDate { get; set; }
        public bool AllScanned { get; set; }
    }

    public class InternalSTOViewModel
    {
        public RawMaterialDetailModel Ibr { get; set; }
        public List <PurchaseOrderLabelsModel> sto { get; set; }
        public List <STODeliveryDetailsModel> stoOld { get; set; }
        public SAPMatlMasterModel Sapm { get; set; }
        public bool SentAll { get; set; }
        public DateTime? sentSAPDate { get; set; }
        public decimal TotalScanned { get; set; }
        public decimal TotalReceived { get; set; }
        public string Uom { get; set; }
        public bool NeedScan { get; set; }
        public bool PackingMaterial { get; set; }
        public decimal ExpectedQty { get; set; }
        public bool ReceivedAll { get; set; }
        public bool notScanned { get; set; }

    }
}
