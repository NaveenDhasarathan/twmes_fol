﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using DataLibrary.Models;

namespace WebApp.Components
{
    public class UserInfo : ViewComponent
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        protected string _userID;
        protected string _firstName;
        protected bool _isUserAuthenticated;

        public UserInfo(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _userID = "";
        }

        public IViewComponentResult Invoke()
        {
            if (!string.IsNullOrWhiteSpace(_httpContextAccessor.HttpContext.User.Identity.Name))
            _userID = _httpContextAccessor.HttpContext.User.Identity.Name.Replace("APM\\", "").ToLower();
            return View(new UserModel {UserId= _userID });
        }
    }
}
