﻿using DataLibrary.DataAccess.Abstract;
using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using DataLibrary.Processors.Sql;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Transactions;
using WebApp.Common;
using WebApp.Common.Services.Abstract;
using WebApp.ViewModels;
using WebApp.Models.Services;

namespace WebApp.Controllers
{
    public class RawMaterialController : Controller
    {
        private readonly IRawMaterialProcessor _RMProcessor;
        private readonly IReadWriteDataAccess _readWriteAccess;
        private readonly IInboundRawMaterialsProcessor _rawMaterialProcessor;
        private readonly ISapMatlMasterProcessor _sapMaterialProcessor;
        private readonly IConfiguration _config;
        private readonly ILimsService _lims;
        private readonly ILogger<RawMaterialController> _logger;
        private readonly IRawMaterialsService _rawMaterialsService;

        public RawMaterialController(IRawMaterialProcessor labelProcessor,
            IReadWriteDataAccess readWriteAccess,
            IInboundRawMaterialsProcessor rawMaterialProcessor,
            ISapMatlMasterProcessor sapMaterialProcessor,
            IConfiguration config,
            ILimsService lims,
            ILogger<RawMaterialController> logger,
            IRawMaterialsService rawMaterialsService)
        {
            _RMProcessor = labelProcessor;
            _readWriteAccess = readWriteAccess;
            _rawMaterialProcessor = rawMaterialProcessor;
            _sapMaterialProcessor = sapMaterialProcessor;
            _config = config;
            _lims = lims;
            _logger = logger;
            _rawMaterialsService = rawMaterialsService;
        }
        // GET: RawMaterialController
        public ActionResult Index()
        {
            return View();
        }

        public IActionResult PurchaseOrderLabel(long id)
        {
            RawMaterialViewModel lblVm = new RawMaterialViewModel();
            try
            {
                //bool POAutoLabelCreation = false;
                RawMaterialDetailModel irm = _rawMaterialProcessor.GetRawMaterialByRawMatlId(id);
                IEnumerable<PurchaseOrderLabelsModel> labelList = _RMProcessor.GetLabelsByRawMtlsIdAll(irm?.Ib_Raw_Mtls_Id).OrderByDescending(v => v.Label_Id); ;
                IEnumerable<PurchaseOrderLabelsModel> labelListChilds = labelList.Where(x => x.Bag_No != 0);
                labelList = labelList.Where(x => x.Bag_No == 0);
                var sapMaterial = _sapMaterialProcessor.GetSapMaterialByMATLNBR(irm?.Matl_Nbr);
                lblVm.Ibr = irm;
                lblVm.Sapm = sapMaterial;
                bool IsPackagingMaterial = (sapMaterial?.Sap_Matl_Type?.ToUpper() == AppSettings.GetPackagingMaterialValue()) ? true : false;

                ViewData["Labels_CSV_ALL"] = "";
                ViewData["Labels_CSV_ALL"] = String.Join(",", labelList.Where(x => x.Label_Scan_Date == null || x.Delivery_Scan_Date == null).ToList().Select(x => x.Label_Id.ToString()).ToArray());


                //if(irm.FRM.ToLower() == "im" && irm.Too.ToLower() == "im" && !string.IsNullOrEmpty(sapMaterial.BATCH_MANAGED) && sapMaterial.BATCH_MANAGED =="0")

                bool disabled = false;
                bool AllScanned = true;

                //if (irm.FRM.ToLower() == "im" && irm.Too.ToLower() == "im")
                //{
                //    if (labelList.Count() < 1)
                //    {
                //        List<string> arr = new List<string>();
                //        arr.Add(irm.Ib_Raw_Mtls_Id.ToString()); //0 Material Id
                //        arr.Add(DateTime.Now.ToString()); //1 Product Received Date
                //        arr.Add(irm.Quantity.ToString()); //2 Amount Received
                //        arr.Add(irm.Lot_Nbr?.ToString()); //3 Lot Number
                //        arr.Add("1"); //4 total Bag Number
                //        arr.Add(null); //5 Vendor Batch
                //        arr.Add(null); //6 Country of origin
                //        arr.Add("1"); //7 Pallent Number
                //        arr.Add(irm.Ib_Raw_Mtls_Id.ToString()); //8 PO Number
                //        arr.Add(irm.Matl_Nbr.ToString()); //9 Material Number
                //        arr.Add(null); //10 Bill of Lading
                //        arr.Add(null); //11 Note
                //        arr.Add(IsPackagingMaterial.ToString()); //12 IsPackaging Material
                //        arr.Add(irm.Uom); //13 UOM
                //        arr.Add(irm.Supplier_Nbr); //14
                //        CreateLabel(arr.ToArray());


                //        labelList = _RMProcessor.GetLabelsByRawMtlsIdAll(irm?.Ib_Raw_Mtls_Id).OrderByDescending(v => v.Label_Id); ;
                //        labelListChilds = labelList.Where(x => x.Bag_No != 0);
                //        labelList = labelList.Where(x => x.Bag_No == 0);
                //        POAutoLabelCreation = true;
                //    }
                //    else
                //    {
                //        POAutoLabelCreation = true;
                //    }
                //}





                decimal sumAmount = 0;
                decimal scannedAmountTotal = 0;
                if (labelList.Count() > 0)
                {
                    var sapDate = labelList.OrderByDescending(l => l.Sent_Sap_Date).FirstOrDefault();
                    var sapNotSent = labelList.Where(l => l.Sent_Sap_Date == null);
                    lblVm.SapSentDate = sapDate?.Sent_Sap_Date;
                    var labelScanned = labelList.Where(l => l.Label_Scan_Date == null);
                    if (IsPackagingMaterial)
                    {
                        disabled = (sapNotSent.Count() > 0) ? false : true;
                        AllScanned = true;
                        scannedAmountTotal = labelList.Sum(n => (n.Uom == irm.Uom ? n.Amt_Received : (n.Uom == "KG" && irm.Uom == "LB" ? KG_to_LB(n.Amt_Received) : (n.Uom == "LB" && irm.Uom == "KG" ? LB_to_KG(n.Amt_Received) : n.Amt_Received))));
                    }
                    else
                    {
                        if (labelScanned.Count() > 0)
                            disabled = true;
                        else
                            disabled = (sapNotSent.Count() > 0) ? false : true;
                        AllScanned = (labelList.Where(x => x.Label_Scan_Date == null).Count() > 0 ? false : true);
                        scannedAmountTotal = labelList.Where(l => l.Label_Scan_Date != null).Sum(n => (n.Uom == irm.Uom ? n.Amt_Received : (n.Uom == "KG" && irm.Uom == "LB" ? KG_to_LB(n.Amt_Received) : (n.Uom == "LB" && irm.Uom == "KG" ? LB_to_KG(n.Amt_Received) : n.Amt_Received))));
                    }

                    foreach (var rec in labelList)
                    {
                        sumAmount += (rec.Uom == irm.Uom ? rec.Amt_Received : (rec.Uom == "KG" && irm.Uom == "LB" ? KG_to_LB(rec.Amt_Received) : (rec.Uom == "LB" && irm.Uom == "KG" ? LB_to_KG(rec.Amt_Received) : rec.Amt_Received)));
                        string lineItem = "";
                        foreach (var item in labelListChilds.Where(x => x.Label_Id == rec.Label_Id))
                        {
                            if (item != null)
                            {
                                //item.Amt_Received = (item.TotalNoofPallets.ToString() != null && item.TotalNoofPallets.ToString() != "" && item.TotalNoofPallets > 0) ? (item.Uom == "KG" && item.Uom == "LB" ? KG_to_LB((item.Amt_Received / item.TotalNoofPallets)) : LB_to_KG((item.Amt_Received / item.TotalNoofPallets))) : (item.Uom == "KG" && item.Uom == "LB" ? KG_to_LB((item.Amt_Received Pallets)) : LB_to_KG((item.Amt_Received)));
                                // item.Amt_Received = (item.TotalNoofPallets.ToString() != null && item.TotalNoofPallets.ToString() != "" && item.TotalNoofPallets > 0) ? (item.Amt_Received / item.TotalNoofItemsPerPallets) : item.Amt_Received;
                                lineItem += item.Label_Id + "|" + (item.Uom == irm.Uom ? item.Amt_Received : (item.Uom == "KG" && irm.Uom == "LB" ? KG_to_LB(item.Amt_Received) : (item.Uom == "LB" && irm.Uom == "KG" ? LB_to_KG(item.Amt_Received) : item.Amt_Received))) + " " + item.Uom + "|"
                                    + item.Bag_No + "|" + item.Batch_Id + "|"
                                    + item.Product_Received_Date + "| " + item.Label_Print_Date + "|"
                                    + item.Label_Scan_Date + "~";
                            }
                        }
                        rec.LineItems = lineItem;
                    }
                }
                else
                {
                    disabled = true;
                }


                //if (POAutoLabelCreation)
                //{
                //    AllScanned = true;
                //}
                lblVm.Polabels = labelList;
                lblVm.PoLablesAmount = sumAmount;
                lblVm.ScannedAmount = scannedAmountTotal;
                lblVm.AllScanned = true;

                lblVm.Completed = scannedAmountTotal < irm?.Quantity ? false : true;
                lblVm.disabled = disabled;
                lblVm.PackingMaterial = IsPackagingMaterial;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error for PurchaseOrderLabel of RawMaterialController: " + ex.Message);
            }

            return View(lblVm);
        }

        public JsonResult SendPOToSAP(string itemId)
        {
            bool blnSuccess = false;
            if (!string.IsNullOrEmpty(itemId))
            {
                try
                {
                    //JObject jLabels = PreparePurchaseOrderForSAP(long.Parse(itemId));
                    List<PurchaseOrderLabelsModel> labelInfo = _RMProcessor.GetLabelsByRawMtlsId(long.Parse(itemId));

                    if (labelInfo != null)
                    {
                        POMaterialModel poMaterialSAP = _rawMaterialProcessor.GetIRMInfoForSAP(long.Parse(itemId));
                        List<RawMaterialSAPModel> rmList = new List<RawMaterialSAPModel>();
                        //foreach (var item in labelInfo)
                        foreach (var item in labelInfo.Where(l => l.Sent_Sap_Date == null))
                        {
                            rmList.Add(new RawMaterialSAPModel
                            {
                                Amt_Received = item.Amt_Received,
                                Storage_Unit_Nbr = item.Label_Id,
                                Batch_Nbr = item.Batch_Id,
                                Uom = item.Uom,
                                Bill_Of_Lading = item.Bill_Of_Lading,
                                Item_Nbr = poMaterialSAP.Item_Nbr,
                                Matl_Nbr = poMaterialSAP.Matl_Nbr
                            });
                        }

                        JObject jLabelObject = CreateJObjectForSAP(poMaterialSAP, rmList, false, "No");
                        string result = SapAPI.SendRawMaterialsToSAP(jLabelObject, false).Result;
                        if (result.ToUpper().Contains("SUCCESS"))
                        {
                            blnSuccess = true;
                            string userId = User.Identity.Name.Replace("APM\\", "").ToLower();
                            int affected = _RMProcessor.UpdateSentSapDate(long.Parse(itemId), DateTime.Now, userId);
                        }
                    }

                }
                catch (Exception ex)
                {
                    _logger.LogError("Error for SendPOToSAP of RawMaterialController: " + ex.Message);
                    blnSuccess = false;
                }
            }
            return Json(new { success = blnSuccess });
        }
        public bool ValidBatchID(string batchid)
        {
            if (batchid == null || batchid == "")
                return false;
            else
            {
                Regex rgx = new Regex(@"^[A-Z]{1}[A-L]{1}[0-9]{2}[A-Z]{2}[0-9]{2}");
                //      I      F    24      VY       04
                return rgx.IsMatch(batchid);
            }
        }

        public void CreateBatchID(long id)
        {
            RawMaterialDetailModel irm = _rawMaterialProcessor.GetRawMaterialByRawMatlId(id);
            List<STODeliveryDetailsModel> stoModelOld = (!string.IsNullOrEmpty(irm.Delivery_No)) ? _RMProcessor.GetSTOByDeliveryNoteSTODetail(long.Parse(irm.Delivery_No), irm.Item_Nbr) : null;
            stoModelOld = stoModelOld.FindAll(x => x.Storage_Unit_Nbr != null && x.Storage_Unit_Nbr != "0" && x.Storage_Unit_Nbr.ToUpper() != "NEEDLABEL" && x.Storage_Unit_Nbr.ToUpper() != "INV MGT" && (x.Batch_Id == null || x.Batch_Id == ""));
            if (stoModelOld != null)
            {
                string userId = User.Identity.Name.Replace("APM\\", "").ToLower();
                string itmnbr = "";
                string batch_Id = "";
                foreach (var item in stoModelOld)
                {
                    //if (itmnbr != item.Item_Nbr)
                    //{
                    if (ValidBatchID(item.Batch_Nbr))
                        batch_Id = item.Batch_Nbr;
                    else
                        batch_Id = _RMProcessor.GetBatchIdSTO(irm.Document_Nbr, Int32.Parse(item.Matl_Nbr), "R", userId, item.Batch_Nbr, Int32.Parse(item.IB_Raw_Mtls_Id));
                    //    itmnbr = item.Item_Nbr;
                    //}
                    _RMProcessor.UpdateSTOBatchID(new STODeliveryDetailsModel
                    {
                        Sto_DD_Seq = item.Sto_DD_Seq,
                        Batch_Id = batch_Id
                    });
                }
            }
        }

        public IActionResult InternalSTO(long id)
        {
            CreateBatchID(id);
            InternalSTOViewModel lblVm = new InternalSTOViewModel();
            try
            {
                bool STOAutoLabelCreation = false;
                int STOLabelCount = 0;
                RawMaterialDetailModel irm = _rawMaterialProcessor.GetRawMaterialByRawMatlId(id);
                var qtyExpected = _rawMaterialProcessor.GetRawMaterialByDeliveryNo(irm?.Delivery_No);
                var sapMaterial = _sapMaterialProcessor.GetSapMaterialByMATLNBR(irm?.Matl_Nbr);
                bool IsPackagingMaterial = (sapMaterial?.Sap_Matl_Type?.ToUpper() == AppSettings.GetPackagingMaterialValue()) ? true : false;
                lblVm.Ibr = irm;
                lblVm.Sapm = sapMaterial;
                List<STODeliveryDetailsModel> stoModelDropDown = (!string.IsNullOrEmpty(irm.Delivery_No)) ? _RMProcessor.GetSTOByDeliveryNoteSTO(long.Parse(irm.Delivery_No), irm.Item_Nbr) : null;
                List<STODeliveryDetailsModel> stoModelLabel = (!string.IsNullOrEmpty(irm.Delivery_No)) ? _RMProcessor.GetSTOByDeliveryNoteSTODetail(long.Parse(irm.Delivery_No), irm.Item_Nbr) : null;
                STOLabelCount = stoModelLabel.Count();
                stoModelLabel = stoModelLabel.FindAll(x => x.Storage_Unit_Nbr != null && x.Storage_Unit_Nbr != "0");
                //stoModelDropDown = stoModelDropDown.FindAll(x => x.Storage_Unit_Nbr != null && x.Storage_Unit_Nbr != "0");
                foreach (STODeliveryDetailsModel item in stoModelDropDown)
                {
                    item.ValidBatchID = SearchForBatchID(item.Batch_Nbr);
                }
                ViewData["Item_Nbr_Container"] = stoModelDropDown;
                ViewData["Item_Nbr"] = stoModelDropDown.Select(m => new SelectListItem { Value = m.IB_Raw_Mtls_Id.ToString(), Text = m.Item_Nbr.ToString() + "/" + m.Matl_Nbr.ToString() + "/" + m.SAP_MATL_DESCR.Replace("/", " ") + "/" + m.STO_DOC_ITEM_NBR + "/" + m.Batch_Nbr });
                List<PurchaseOrderLabelsModel> stoModel = (!string.IsNullOrEmpty(irm.Delivery_No)) ? _RMProcessor.GetSTOByDeliveryNote(long.Parse(irm.Delivery_No), irm.Item_Nbr) : null;
                ViewData["Labels_CSV_ALL"] = "";
                ViewData["Labels_CSV"] = "";
                ViewData["Labels_CSV_ALL"] = String.Join(",", stoModelLabel.Where(x => x.Storage_Unit_Nbr != null && x.Storage_Unit_Nbr != "0" && x.Scan_Date == null).ToList().Select(x => x.Storage_Unit_Nbr.ToString()).ToArray());
                ViewData["Labels_CSV"] = String.Join(",", stoModel.Where(x => x.Delivery_Scan_Date == null || x.Label_Scan_Date == null).ToList().Select(x => x.Label_Id.ToString()).ToArray());
                bool oldSapNoteSent = false;

                //if (irm.FRM.ToLower() == "im" && irm.Too.ToLower() == "im")
                //{
                //    if(stoModelLabel.Count() < 1)
                //    {

                //    }
                //}


                if (irm.FRM?.ToLower() == "im" && irm.Too?.ToLower() == "im")
                {
                    if (stoModel.Count() < 1)
                    {
                        List<string> arr = new List<string>();

                        arr.Add(irm.Ib_Raw_Mtls_Id.ToString());//0
                        arr.Add(DateTime.Now.ToString());//1
                        arr.Add(irm.Quantity.ToString());//2
                        arr.Add("1");  //container
                        arr.Add(irm.STO_Batch);//4
                        arr.Add(null);//5
                        arr.Add(irm.Ib_Raw_Mtls_Id.ToString());//6       
                        arr.Add(irm.Matl_Nbr.ToString());//7
                        arr.Add(null);//8 Bill of lading
                        arr.Add(null);//9
                        arr.Add(IsPackagingMaterial ? "Yes" : "No");//10
                        arr.Add(irm.Uom);//11
                        arr.Add(null);//12 store number 
                        arr.Add(irm.Storage_Location);//13 source plant
                        arr.Add(null);//14 seriel number
                        arr.Add(irm.Item_Nbr.ToString());//15 itmdetil_15 itmdetil_16
                        arr.Add(irm.Item_Nbr.ToString());//16
                        arr.Add("1");//17
                        arr.Add(irm.Supplier_Nbr);//18
                        SaveSTO(arr.ToArray());

                        stoModelLabel = (!string.IsNullOrEmpty(irm.Delivery_No)) ? _RMProcessor.GetSTOByDeliveryNoteSTODetail(long.Parse(irm.Delivery_No), irm.Item_Nbr) : null;
                        stoModel = (!string.IsNullOrEmpty(irm.Delivery_No)) ? _RMProcessor.GetSTOByDeliveryNote(long.Parse(irm.Delivery_No), irm.Item_Nbr) : null;

                        stoModelLabel = stoModelLabel.FindAll(x => x.Storage_Unit_Nbr != null && x.Storage_Unit_Nbr != "0");

                        //labelList = _RMProcessor.GetLabelsByRawMtlsIdAll(irm?.Ib_Raw_Mtls_Id).OrderByDescending(v => v.Label_Id); ;
                        //labelListChilds = labelList.Where(x => x.Bag_No != 0);
                        //labelList = labelList.Where(x => x.Bag_No == 0);
                        STOAutoLabelCreation = true;
                    }
                    else
                    {
                        STOAutoLabelCreation = true;
                    }
                }


                lblVm.ExpectedQty = qtyExpected.Where(x => x.Ib_Raw_Mtls_Id == id).Sum(a => a.Quantity);
                bool disabled = true;
                PurchaseOrderLabelsModel dateSap = null;
                STODeliveryDetailsModel dateSapOld = null;
                string uom = "";
                decimal totScanned = 0;
                decimal totReceived = 0;
                bool needScan = false;
                bool notScanned = false;
                int totnotscannedcountold = 0;
                bool oldReceivedAll = false;
                if (stoModelLabel.Count > 0)
                {
                    if (IsPackagingMaterial)
                    {
                        oldSapNoteSent = (stoModelLabel.FindAll(x => x.Date_Sent_Sap == null).Count() > 0 ? true : false);
                        lblVm.ReceivedAll = (stoModelLabel.FindAll(x => x.Date_Received == null && x.Scan_Date == null).Count() > 0 ? false : true);
                        totnotscannedcountold = stoModelLabel.FindAll(x => x.Scan_Date == null || x.Date_Received == null).Count();
                        //totScanned += stoModelOld.FindAll(x => x.Date_Received != null).Sum(n => n.Amt_Received);
                        //totScanned += (item.UOM == irm.Uom ? item.AmountReceived : (item.UOM == "KG" && irm.Uom == "LB" ? KG_to_LB(item.AmountReceived) : LB_to_KG(item.AmountReceived)));
                        totScanned += stoModelLabel.FindAll(x => x.Date_Received != null).Sum(n => (n.Uom == irm.Uom ? n.Amt_Received : (n.Uom == "KG" && irm.Uom == "LB" ? KG_to_LB(n.Amt_Received) : (n.Uom == "LB" && irm.Uom == "KG" ? LB_to_KG(n.Amt_Received) : n.Amt_Received))));
                    }
                    else
                    {
                        stoModelLabel = stoModelLabel.FindAll(x => x.Storage_Unit_Nbr != null && x.Storage_Unit_Nbr != "0" && x.Storage_Unit_Nbr.ToUpper() != "NEEDLABEL" && x.Storage_Unit_Nbr.ToUpper() != "INV MGT");
                        //totScanned += stoModelOld.FindAll(x => x.Scan_Date != null).Sum(n => n.Amt_Received);
                        totScanned += stoModelLabel.FindAll(x => x.Scan_Date != null).Sum(n => (n.Uom == irm.Uom ? n.Amt_Received : (n.Uom == "KG" && irm.Uom == "LB" ? KG_to_LB(n.Amt_Received) : (n.Uom == "LB" && irm.Uom == "KG" ? LB_to_KG(n.Amt_Received) : n.Amt_Received))));
                    }
                    totReceived += stoModelLabel.Sum(n => (n.Uom == irm.Uom ? n.Amt_Received : (n.Uom == "KG" && irm.Uom == "LB" ? KG_to_LB(n.Amt_Received) : (n.Uom == "LB" && irm.Uom == "KG" ? LB_to_KG(n.Amt_Received) : n.Amt_Received)))); ;
                    dateSapOld = stoModelLabel.Where(n => n.Date_Sent_Sap != null).FirstOrDefault();
                }
                if (stoModel == null)
                {
                    needScan = true;
                }
                else
                {

                    if (stoModel.Count != 0)
                    {
                        //var scanIn = stoModel.Where(n => n.Scanned_In == "Yes");
                        uom = stoModel.Where(n => n.Uom != null).FirstOrDefault().Uom;
                        var receivedAll = stoModel.Where(i => i.Product_Received_Date == null);
                        dateSap = stoModel.Where(n => n.Sent_Sap_Date != null).FirstOrDefault();
                        totScanned += stoModel.Where(n => n.Delivery_Scan_Date != null && n.Label_Scan_Date != null).Sum(n => (n.Uom == irm.Uom ? n.Amt_Received : (n.Uom == "KG" && irm.Uom == "LB" ? KG_to_LB(n.Amt_Received) : (n.Uom == "LB" && irm.Uom == "KG" ? LB_to_KG(n.Amt_Received) : n.Amt_Received)))); ;
                        var sapNotSent = stoModel.Where(l => l.Sent_Sap_Date == null);

                        var notScannedCount = stoModel.Where(l => l.Delivery_Scan_Date == null || l.Label_Scan_Date == null);
                        notScanned = (stoModel.Where(l => l.Delivery_Scan_Date == null || l.Label_Scan_Date == null).Count() > 0) || (totnotscannedcountold > 0) ? true : false;
                        totReceived += stoModel.Sum(n => (n.Uom == irm.Uom ? n.Amt_Received : (n.Uom == "KG" && irm.Uom == "LB" ? KG_to_LB(n.Amt_Received) : (n.Uom == "LB" && irm.Uom == "KG" ? LB_to_KG(n.Amt_Received) : n.Amt_Received))));

                        if (IsPackagingMaterial)
                        {
                            disabled = (oldSapNoteSent ? true : false);
                            lblVm.ReceivedAll = (stoModelDropDown.FindAll(x => x.Date_Received == null).Count() > 0 ? false : true);
                        }
                        else
                        {
                            if (receivedAll.Count() > 0)
                            {
                                disabled = true;
                                lblVm.ReceivedAll = false;
                            }
                            else
                            {
                                lblVm.ReceivedAll = true;
                                disabled = (sapNotSent.Count() > 0) ? false : true;
                            }
                        }
                        if (notScanned)
                        {
                            disabled = true;
                        }


                    }
                    else
                        needScan = true;

                }
                if (totScanned >= lblVm.ExpectedQty && !notScanned)
                {
                    disabled = false;
                }
                else
                {
                    disabled = true;
                }
                if (IsPackagingMaterial && lblVm.ReceivedAll)
                {
                    disabled = false;
                }

                if (STOAutoLabelCreation)
                {
                    needScan = false;
                }

                if (STOAutoLabelCreation)
                {
                    totScanned = totReceived;
                    notScanned = false;
                }

                lblVm.sto = stoModel;
                lblVm.notScanned = notScanned;
                lblVm.stoOld = stoModelLabel;
                lblVm.SentAll = disabled;
                lblVm.NeedScan = needScan;
                lblVm.sentSAPDate = (dateSap != null ? dateSap?.Sent_Sap_Date : dateSapOld?.Date_Sent_Sap);
                lblVm.TotalScanned = totScanned;
                lblVm.TotalReceived = totReceived;
                lblVm.Uom = uom;
                lblVm.PackingMaterial = IsPackagingMaterial;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error for InternalSTO of RawMaterialController: " + ex.Message);
            }


            return View(lblVm);
        }
        public JsonResult STOToSap(string deliveryNbr, string matl, string packaging, string stoItemNbr, string frm_value, string too_value)
        {
            bool blnSuccess = false;
            string errMsg = "";
            if (!string.IsNullOrEmpty(deliveryNbr))
            {
                try
                {
                    //var stoList = _RMProcessor.GetSTOByDeliveryNote(long.Parse(deliveryNbr),Int32.Parse(stoItemNbr));
                    var stoList = _RMProcessor.GetSTOByDeliveryNote_SAPSend(long.Parse(deliveryNbr), Int32.Parse(stoItemNbr));
                    if (frm_value.ToLower() == "im" && too_value.ToLower() == "im")
                    {
                        stoList = stoList.Where(x => x.Label_Id != null)?.ToList();
                    }
                    if (packaging != "Yes")
                        stoList = stoList.FindAll(x => x.Label_Id != "INV MGT" && x.Label_Id != "NEEDLABEL");
                    if (stoList != null)
                    {
                        decimal totReceived = stoList.Where(n => n.Product_Received_Date != null).Sum(n => n.Amt_Received);
                        string uomReceived = stoList.Where(n => n.Product_Received_Date != null).FirstOrDefault().Uom;
                        POMaterialModel poMaterialSAP = _rawMaterialProcessor.GetIRMInfoForSAPPOMaterials(long.Parse(deliveryNbr));
                        decimal totItem = poMaterialSAP.Quantity;
                        string uomItem = poMaterialSAP.Uom;


                        List<RawMaterialSAPModel> rmList = new List<RawMaterialSAPModel>();
                        foreach (var item in stoList)
                        {
                            rmList.Add(new RawMaterialSAPModel
                            {
                                Amt_Received = item.Amt_Received,
                                Storage_Unit_Nbr = item.Label_Id,
                                Batch_Nbr = item.Batch_Id,
                                Vendor_Batch = item.Vendor_Batch,
                                Uom = item.Uom,
                                Bill_Of_Lading = item.Bill_Of_Lading,
                                Item_Nbr = int.Parse(item.ITEM_NBR),
                                Matl_Nbr = item.MATL_NBR,
                                ItemNo = int.Parse(item.ItemNo),
                                MatlItemNbr = item.MatlItemNbr
                            });
                        }

                        JObject jLabelObject = CreateJObjectForSAP(poMaterialSAP, rmList, true, packaging);

                        string result = SapAPI.SendRawMaterialsToSAP(jLabelObject, true).Result;
                        if (result.ToUpper().Contains("SUCCESS"))
                        {
                            blnSuccess = true;
                            string userId = User.Identity.Name.Replace("APM\\", "").ToLower();
                            int affected = _RMProcessor.UpdateSTOSapDate(long.Parse(deliveryNbr), DateTime.Now, userId);
                        }
                        //}

                    }
                }
                catch (Exception ex)
                {
                    blnSuccess = false;
                    errMsg = ex.Message;
                    _logger.LogError("Error for InternalSTO of RawMaterialController: " + errMsg);
                }
            }

            return Json(new { success = blnSuccess, errMsg });
        }

        public JObject CreateJObjectForSAP(POMaterialModel materialSAP, List<RawMaterialSAPModel> stoList, bool STO, string packaging)
        {
            if (packaging != "Yes")
                stoList = stoList.FindAll(x => x.Batch_Nbr != null && x.Batch_Nbr != "");
            var batchList = stoList.Select(n => n.Batch_Nbr).Distinct().ToList();
            int cnt = 0;
            JArray JArrayBatch = new JArray();
            int batch_Seq = 0;
            foreach (string batchId in batchList)
            {
                batch_Seq += 1;
                string qualityCheck = (materialSAP.Sap_Matl_Type == "ROH") ? "0" : "";
                JArray JArrayPallet = new JArray();
                decimal tot = 0;

                if (materialSAP.Storage_Location.ToUpper() == AppSettings.GetWM_VALUE_BL())
                {
                    string STG_Type = (materialSAP.Storage_Location.ToUpper() == AppSettings.GetWM_VALUE() || materialSAP.Storage_Location.ToUpper() == AppSettings.GetWM_VALUE_BL()) ? AppSettings.GetWM_STG_TYPE() : AppSettings.GetWM_STG_TYPE_FG();
                    string DestSTG_Type = (materialSAP.Storage_Location.ToUpper() == AppSettings.GetWM_VALUE() || materialSAP.Storage_Location.ToUpper() == AppSettings.GetWM_VALUE_BL()) ? AppSettings.GetDEST_WM_STG_TYPE() : AppSettings.GetWM_STG_TYPE_FG();
                    var stoBatchList = stoList.Where(s => s.Batch_Nbr == batchId);
                    int pallet_Seq = 1;
                    bool addDEST = true;
                    addDEST = (stoBatchList.First().Vendor_Batch == batchId ? false : true);
                    string PO_STG_Type = (materialSAP.Storage_Location.ToUpper() == AppSettings.GetWM_VALUE_BL() ? STG_Type : AppSettings.GetIM_STG_TYPE());
                    foreach (var sto in stoBatchList)
                    {
                        //pallet_Seq += 1;
                        if (STO)
                        {
                            var jLabelObj = new JObject(
                                               new JProperty("WM_LINE_ID", (/*sto.Item_Nbr + "" +*/ batch_Seq.ToString("00") + "" + pallet_Seq.ToString("00")).ToString()),
                                               new JProperty("WM_QUANTITY", (materialSAP.Uom == "KG" && sto.Uom == "LB" ? LB_to_KG(sto.Amt_Received).ToString() : (materialSAP.Uom == "LB" && sto.Uom == "KG" ? KG_to_LB(sto.Amt_Received).ToString("0.000") : sto.Amt_Received.ToString("0.000")))),
                                               new JProperty("WM_UOM", materialSAP.Uom),
                                               new JProperty("WM_BATCH", sto.Batch_Nbr),
                                               //new JProperty("VALIDBATCHID", ValidBatchID((sto.Batch_Nbr == sto.Vendor_Batch ? "" : sto.Batch_Nbr)) ? (sto.Batch_Nbr == sto.Vendor_Batch ? "" : sto.Batch_Nbr) : ""),

                                               new JProperty("WM_STG_TYPE", (STG_Type)),
                                               new JProperty("WM_STG_SECTION", (AppSettings.GetWM_STG_SECTION())),
                                               new JProperty("WM_STG_BIN", (AppSettings.GetWM_STG_BIN())),
                                               new JProperty("WM_STG_UNIT_NUMBER", sto.Storage_Unit_Nbr),
                                               new JProperty("WM_STG_UNIT_TYPE", (AppSettings.GetWM_STG_UNIT_TYPE())),

                                               new JProperty("DEST_WM_STG_TYPE", (addDEST ? DestSTG_Type : "")),
                                               new JProperty("DEST_WM_STG_SECTION", (addDEST ? AppSettings.GetWM_STG_SECTION() : "")),
                                               new JProperty("DEST_WM_STG_BIN", (addDEST ? AppSettings.GetWM_STG_BIN() : "")),
                                               new JProperty("DEST_WM_STG_UNIT_NUMBER", (addDEST ? sto.Storage_Unit_Nbr : "")),
                                               new JProperty("DEST_WM_STG_UNIT_TYPE", (addDEST ? AppSettings.GetWM_STG_UNIT_TYPE() : ""))
                                              );

                            JArrayPallet.Add(jLabelObj);
                        }
                        else
                        {
                            var jLabelObj = new JObject(
                                               new JProperty("WM_LINE_ID", (/*sto.Item_Nbr + "" +*/ batch_Seq.ToString("00") + "" + pallet_Seq.ToString("00")).ToString()),
                                               new JProperty("WM_QUANTITY", (materialSAP.Uom == "KG" && sto.Uom == "LB" ? LB_to_KG(sto.Amt_Received).ToString() : (materialSAP.Uom == "LB" && sto.Uom == "KG" ? KG_to_LB(sto.Amt_Received).ToString("0.000") : sto.Amt_Received.ToString("0.000")))),
                                               new JProperty("WM_UOM", materialSAP.Uom),
                                               new JProperty("WM_BATCH", sto.Batch_Nbr),
                                               //new JProperty("VALIDBATCHID", ValidBatchID((sto.Batch_Nbr == sto.Vendor_Batch ? "" : sto.Batch_Nbr)) ? (sto.Batch_Nbr == sto.Vendor_Batch ? "" : sto.Batch_Nbr) : ""),

                                               new JProperty("WM_STG_TYPE", PO_STG_Type),
                                               new JProperty("WM_STG_SECTION", AppSettings.GetWM_STG_SECTION()),
                                               new JProperty("WM_STG_BIN", AppSettings.GetWM_STG_BIN()),
                                               new JProperty("WM_STG_UNIT_NUMBER", sto.Storage_Unit_Nbr),
                                               new JProperty("WM_STG_UNIT_TYPE", AppSettings.GetWM_STG_UNIT_TYPE())
                                              );

                            JArrayPallet.Add(jLabelObj);
                        }

                        tot += (materialSAP.Uom == "KG" && sto.Uom == "LB" ? LB_to_KG(sto.Amt_Received) : (materialSAP.Uom == "LB" && sto.Uom == "KG" ? KG_to_LB(sto.Amt_Received) : sto.Amt_Received));
                    }

                    if (STO)
                    {
                        JArrayBatch.Add(new JObject(
                       new JProperty("DELIVERY_NO", materialSAP.Delivery_No),
                       new JProperty("DELIVERY_ITEM", stoBatchList.FirstOrDefault().MatlItemNbr.ToString()),
                       new JProperty("MATERIAL", stoBatchList.FirstOrDefault().Matl_Nbr.ToString()),
                       new JProperty("PLANT", AppSettings.GetPlant()),
                       new JProperty("SLOC", (stoBatchList.First().Vendor_Batch == null || !ValidBatchID(stoBatchList.First().Vendor_Batch) ? AppSettings.GetWM_VALUE_BL() : materialSAP.Storage_Location)),
                       new JProperty("DEST_SLOC", (addDEST ? materialSAP.LGFSB?.ToString() : "")),
                       new JProperty("DEST_BATCH", (addDEST ? batchId : "")),
                       new JProperty("BATCH", stoBatchList.First().Vendor_Batch),
                       new JProperty("MOVE_TYPE", (STG_Type == "BALE" ? AppSettings.GetWM_STG_TYPE_FG() : AppSettings.GetSTO_MOVE_TYPE())),
                       new JProperty("STOCK_TYPE", qualityCheck),
                       new JProperty("IM_QUANTITY", tot.ToString("0.000")),
                       new JProperty("IM_UOM", materialSAP.Uom),
                       new JProperty("IM_ITEM_TEXT", ""),
                       new JProperty("IM_GOODS_RECIPIENT", ""),
                       new JProperty("IM_UNLOAD_PT", ""),
                       new JProperty("IM_LINE_ID", (/*stoBatchList.FirstOrDefault().Item_Nbr.ToString() + "" + */batch_Seq.ToString("00") + "01").ToString()),
                       new JProperty("Pallets",
                       new JArray(JArrayPallet))));
                    }
                    else
                    {
                        JArrayBatch.Add(new JObject(
                            new JProperty("PO_NUMBER", materialSAP.Document_Nbr.ToString()),
                            new JProperty("PO_ITEM", materialSAP.Item_Nbr.ToString()),
                            new JProperty("MATERIAL", materialSAP.Matl_Nbr.ToString()),
                            new JProperty("PLANT", AppSettings.GetPlant()),
                            //new JProperty("SLOC", (stoBatchList.First().Vendor_Batch == "TBD" ? "SZIM" : materialSAP.Storage_Location)),
                            new JProperty("SLOC", materialSAP.Storage_Location),
                            //new JProperty("DEST_SLOC", (addDEST ? materialSAP.LGFSB?.ToString() : "")),
                            new JProperty("BATCH", batchId),
                            new JProperty("MOVE_TYPE", AppSettings.GetMOVE_TYPE()),
                            new JProperty("STOCK_TYPE", qualityCheck),
                            new JProperty("IM_QUANTITY", tot.ToString("0.000")),
                            new JProperty("IM_UOM", materialSAP.Uom),
                            new JProperty("IM_ITEM_TEXT", ""),
                            new JProperty("IM_GOODS_RECIPIENT", ""),
                            new JProperty("IM_UNLOAD_PT", ""),
                            new JProperty("IM_LINE_ID", (/*materialSAP.Item_Nbr.ToString() + "" +*/ batch_Seq.ToString("00") + "01").ToString()),
                            new JProperty("Pallets",
                            new JArray(JArrayPallet))));
                    }
                }
                else
                {
                    var stoBatchList = stoList.Where(s => s.Batch_Nbr == batchId);
                    int pallet_seq = 1;
                    string STG_Type = (materialSAP.Storage_Location.ToUpper() != AppSettings.GetWM_VALUE_BL()) ? AppSettings.IM_PO_STG_TYPE() : AppSettings.GetWM_STG_TYPE_FG();
                    string DestSTG_Type = (materialSAP.Storage_Location.ToUpper() == AppSettings.GetWM_VALUE()) ? AppSettings.GetDEST_WM_STG_TYPE() : AppSettings.GetWM_STG_TYPE_FG();
                    //var sto = stoBatchList.FirstOrDefault();
                    tot = stoList.Where(s => s.Batch_Nbr == batchId).Sum(sto => (materialSAP.Uom == "KG" && sto.Uom == "LB" ? LB_to_KG(sto.Amt_Received) : (materialSAP.Uom == "LB" && sto.Uom == "KG" ? KG_to_LB(sto.Amt_Received) : sto.Amt_Received)));

                    RawMaterialSAPModel sto1 = null;
                    bool addDEST = true;
                    addDEST = (stoBatchList.First().Vendor_Batch == batchId && materialSAP.Storage_Location != AppSettings.GetWM_VALUE_BL() ? false : true);
                    foreach (var sto in stoBatchList)
                    {
                        sto1 = sto;
                        //    pallet_seq += 1;
                        //pallet_seq += 1;

                        if (STO && materialSAP.Storage_Location == AppSettings.GetWM_VALUE_BL())
                        {
                            var jLabelObj = new JObject(
                                     new JProperty("WM_LINE_ID", (/*sto.Item_Nbr.ToString() + "" +*/ batch_Seq.ToString("00") + pallet_seq.ToString("00")).ToString()),
                                     new JProperty("WM_QUANTITY", (materialSAP.Uom == "KG" && sto.Uom == "LB" ? LB_to_KG(sto.Amt_Received).ToString() : (materialSAP.Uom == "LB" && sto.Uom == "KG" ? KG_to_LB(sto.Amt_Received).ToString("0.000") : sto.Amt_Received.ToString("0.000")))),
                                     new JProperty("WM_UOM", materialSAP.Uom),
                                     new JProperty("WM_BATCH", sto.Batch_Nbr?.ToString()),
                                     //new JProperty("VALIDBATCHID", ValidBatchID((sto.Batch_Nbr == sto.Vendor_Batch ? "" : sto.Batch_Nbr)) ? (sto.Batch_Nbr == sto.Vendor_Batch ? "" : sto.Batch_Nbr) : ""),
                                     new JProperty("WM_STG_TYPE", (packaging == "Yes" ? (STG_Type) : (addDEST ? STG_Type : ""))),
                                     new JProperty("WM_STG_SECTION", ""),
                                     new JProperty("WM_STG_BIN", (packaging == "Yes" ? (sto.Batch_Nbr?.ToString() != sto.Vendor_Batch?.ToString() ? AppSettings.GetWM_STG_BIN() : "") : (addDEST ? AppSettings.GetWM_STG_BIN() : ""))),
                                     new JProperty("WM_STG_UNIT_NUMBER", sto.Storage_Unit_Nbr),
                                     new JProperty("WM_STG_UNIT_TYPE", ""),
                                     new JProperty("DEST_WM_STG_TYPE", (packaging == "Yes" ? (DestSTG_Type) : (addDEST ? DestSTG_Type : ""))),
                                     new JProperty("DEST_WM_STG_SECTION", (packaging == "Yes" ? (sto.Batch_Nbr?.ToString() != sto.Vendor_Batch?.ToString() ? AppSettings.GetWM_STG_SECTION() : "") : (addDEST ? AppSettings.GetWM_STG_SECTION() : ""))),
                                     new JProperty("DEST_WM_STG_BIN", (packaging == "Yes" ? (sto.Batch_Nbr?.ToString() != sto.Vendor_Batch?.ToString() ? AppSettings.GetWM_STG_BIN() : "") : (addDEST ? AppSettings.GetWM_STG_BIN() : ""))),
                                     new JProperty("DEST_WM_STG_UNIT_NUMBER", (packaging == "Yes" ? (sto.Storage_Unit_Nbr) : (addDEST ? sto.Storage_Unit_Nbr : ""))),
                                     new JProperty("DEST_WM_STG_UNIT_TYPE", (packaging == "Yes" ? (sto.Batch_Nbr?.ToString() != sto.Vendor_Batch?.ToString() ? AppSettings.GetWM_STG_UNIT_TYPE() : "") : (addDEST ? AppSettings.GetWM_STG_UNIT_TYPE() : "")))
                                    );
                            JArrayPallet.Add(jLabelObj);
                        }
                        else
                        {
                            var jLabelObj = new JObject(
                                     new JProperty("WM_LINE_ID", (/*sto.Item_Nbr.ToString() + "" +*/ batch_Seq.ToString("00") + pallet_seq.ToString("00")).ToString()),
                                     new JProperty("WM_QUANTITY", (materialSAP.Uom == "KG" && sto.Uom == "LB" ? LB_to_KG(sto.Amt_Received).ToString() : (materialSAP.Uom == "LB" && sto.Uom == "KG" ? KG_to_LB(sto.Amt_Received).ToString("0.000") : sto.Amt_Received.ToString("0.000")))),
                                     new JProperty("WM_UOM", materialSAP.Uom),
                                     new JProperty("WM_BATCH", sto.Batch_Nbr?.ToString()),
                                      //new JProperty("WM_BATCH", sto.Vendor_Batch?.ToString()),
                                      //new JProperty("VALIDBATCHID", ValidBatchID((sto.Batch_Nbr == sto.Vendor_Batch ? "" : sto.Batch_Nbr)) ? (sto.Batch_Nbr == sto.Vendor_Batch ? "" : sto.Batch_Nbr) : ""),
                                      new JProperty("WM_STG_TYPE", STG_Type),
                                      new JProperty("WM_STG_SECTION", AppSettings.GetWM_STG_SECTION()),
                                      new JProperty("WM_STG_BIN", AppSettings.GetWM_STG_BIN()),
                                      new JProperty("WM_STG_UNIT_NUMBER", sto.Storage_Unit_Nbr),
                                      new JProperty("WM_STG_UNIT_TYPE", AppSettings.GetWM_STG_UNIT_TYPE())
                                    );
                            JArrayPallet.Add(jLabelObj);
                        }

                    }

                    if (STO)
                    {
                        JArrayBatch.Add(new JObject(
                         new JProperty("DELIVERY_NO", materialSAP.Delivery_No),
                         new JProperty("DELIVERY_ITEM", sto1.MatlItemNbr.ToString()),
                         new JProperty("MATERIAL", sto1.Matl_Nbr.ToString()),
                         new JProperty("PLANT", AppSettings.GetPlant()),
                         //new JProperty("SLOC", (stoBatchList.First().Vendor_Batch == "TBD" ? "SZIM" : materialSAP.Storage_Location)),
                         new JProperty("SLOC", materialSAP.Storage_Location),
                         new JProperty("DEST_SLOC", (addDEST ? materialSAP.LGFSB?.ToString() : "")),
                         new JProperty("BATCH", sto1.Vendor_Batch),
                         new JProperty("DEST_BATCH", (addDEST ? batchId : "")),
                         new JProperty("MOVE_TYPE", AppSettings.GetSTO_MOVE_TYPE()),
                         new JProperty("STOCK_TYPE", qualityCheck),
                         new JProperty("IM_QUANTITY", tot.ToString()),
                         new JProperty("IM_UOM", materialSAP.Uom),
                         new JProperty("IM_ITEM_TEXT", ""),
                         new JProperty("IM_GOODS_RECIPIENT", ""),
                         new JProperty("IM_UNLOAD_PT", ""),
                         new JProperty("IM_LINE_ID", (/*sto1.Item_Nbr + "" +*/ batch_Seq.ToString("00") + "01").ToString()),
                         new JProperty("Pallets",
                         new JArray(JArrayPallet))));
                    }
                    else
                    {
                        JArrayBatch.Add(new JObject(
                            new JProperty("PO_NUMBER", materialSAP.Document_Nbr.ToString()),
                            new JProperty("PO_ITEM", sto1.Item_Nbr.ToString()),
                            new JProperty("MATERIAL", sto1.Matl_Nbr.ToString()),
                            new JProperty("PLANT", AppSettings.GetPlant()),
                            //new JProperty("SLOC", (stoBatchList.First().Vendor_Batch == "TBD" ? "SZIM" : materialSAP.Storage_Location)),
                            new JProperty("SLOC", materialSAP.Storage_Location),
                            new JProperty("DEST_SLOC", (addDEST ? materialSAP.LGFSB?.ToString() : "")),
                            new JProperty("BATCH", sto1.Batch_Nbr),
                            //new JProperty("BATCH", sto1.Vendor_Batch),
                            new JProperty("MOVE_TYPE", AppSettings.GetMOVE_TYPE()),
                            new JProperty("STOCK_TYPE", qualityCheck),
                            new JProperty("IM_QUANTITY", tot.ToString()),
                            new JProperty("IM_UOM", materialSAP.Uom),
                            new JProperty("IM_ITEM_TEXT", ""),
                            new JProperty("IM_GOODS_RECIPIENT", ""),
                            new JProperty("IM_UNLOAD_PT", ""),
                            new JProperty("IM_LINE_ID", (/*sto1.Item_Nbr + "" +*/ batch_Seq.ToString("00") + "01").ToString()),
                            new JProperty("Pallets",
                            new JArray(JArrayPallet))));
                    }

                    cnt++;
                }
            }

            var jLabelObject = new JObject(
                 new JProperty("POST_DATE", DateTime.Now.ToString("yyyyMMdd")),
                 new JProperty("DOC_DATE", materialSAP.Document_Date.ToString("yyyyMMdd")),
                 new JProperty("MES_TXN_ID", materialSAP.Ib_Raw_Mtls_Id.ToString()),
                 new JProperty("DOC_HDR_TEXT", STO ? "STO Raw Material Receiving" : "PO"),
                 new JProperty("REF_DOC_NO", materialSAP.Document_Nbr.ToString()),
                 new JProperty("BILL_OF_LADING_LONG", stoList[0].Bill_Of_Lading),
                 new JProperty("Materials",
                 new JArray(JArrayBatch)));

            return jLabelObject;
        }
        public JsonResult CreateLabel(string[] arr)
        {
            int cnt = 0;
            try
            {
                string userId = User.Identity.Name.Replace("APM\\", "").ToLower();
                string batch_Id = _RMProcessor.GetBatchId(Int64.Parse(arr[8]), Int32.Parse(arr[9]), "R", userId, arr[5], Int64.Parse(arr[0]), arr[14]);
                int totBag = (!String.IsNullOrEmpty(arr[4])) ? Int32.Parse(arr[4]) : 0;

                List<long> IB_Raw_Mtls_Id = new List<long>();//0
                List<string> Label_Id = new List<string>();
                List<DateTime> Product_Received_Date = new List<DateTime>();//1
                List<DateTime> Label_Print_Date = new List<DateTime>();
                List<decimal> Amt_Received = new List<decimal>();//2
                List<string> UOM = new List<string>();
                List<string> Lot_Nbr = new List<string>();//3 
                List<int> Tot_Bag_No = new List<int>();//4
                List<string> Vendor_Batch = new List<string>(); //5
                List<string> CountryOrigin = new List<string>(); //6
                List<int> Bag_No = new List<int>();
                List<DateTime> Last_Modified_Time = new List<DateTime>();
                List<string> Last_Modified_By = new List<string>();
                List<string> Batch_Id = new List<string>();
                List<string> Batch_Sent_Time = new List<string>();
                List<string> Bill_Of_Lading = new List<string>();//10
                List<string> Note = new List<string>();//11
                List<PurchaseOrderLabelsModel> Lst = new List<PurchaseOrderLabelsModel>();

                using (var transScope = new TransactionScope())
                {
                    string seedLabel = "";
                    bool packaging = false;
                    if (arr[12] == "Yes")
                    {
                        //seedLabel = _readWriteAccess.LoadData<string>(DataLibrary.Processors.Sql.PurchaseOrderLabelsSql.GetNewPackagingMaterialId).FirstOrDefault();
                        //if (string.IsNullOrEmpty(seedLabel)) seedLabel = AppSettings.GetPackagingMaterialIdSeed();
                        packaging = true;
                        seedLabel = "000000000";
                    }
                    else
                    {
                        seedLabel = _readWriteAccess.LoadData<string>(DataLibrary.Processors.Sql.PurchaseOrderLabelsSql.GetNewLabelId).FirstOrDefault();
                        if (string.IsNullOrEmpty(seedLabel)) seedLabel = AppSettings.GetLabelIdSeed();
                        packaging = false;
                    }


                    long lbl = long.Parse(seedLabel);
                    //Loop through each pallet
                    for (int j = 1; j < Int32.Parse(arr[7]) + 1; j++)
                    {
                        Lst.Add(new PurchaseOrderLabelsModel
                        {
                            Ib_Raw_Mtls_Id = Int64.Parse(arr[0]),
                            Label_Id = lbl.ToString("D9"),
                            Product_Received_Date = DateTime.Parse(arr[1]),
                            Amt_Received = decimal.Parse(arr[2]) / Int32.Parse(arr[7]),
                            Uom = arr[13],
                            Lot_Nbr = arr[3],
                            Tot_Bag_No = totBag,
                            Vendor_Batch = arr[5],
                            CountryOrigin = arr[6],
                            Bag_No = 0,
                            Last_Modified_Time = DateTime.Now,
                            Last_Modified_By = userId,
                            Batch_Id = batch_Id,
                            Bill_Of_Lading = arr[10],
                            Note = arr[11],
                            Packaging_Material = packaging
                        });

                        if (arr[12] != "Yes")
                        {
                            for (int i = 1; i < totBag + 1; i++)
                            {
                                decimal amt = (totBag == 0 || totBag == 0) ? decimal.Parse(arr[2]) : (decimal.Parse(arr[2]) / Int32.Parse(arr[7])) / totBag;
                                int bagNo = (totBag == 0 || i == 0) ? 0 : i;

                                Lst.Add(new PurchaseOrderLabelsModel
                                {
                                    Ib_Raw_Mtls_Id = long.Parse(arr[0]),
                                    Label_Id = lbl.ToString("D9"),
                                    Product_Received_Date = DateTime.Parse(arr[1]),
                                    Amt_Received = amt,
                                    Uom = arr[13],
                                    Lot_Nbr = arr[3],
                                    Tot_Bag_No = totBag,
                                    Vendor_Batch = arr[5],
                                    CountryOrigin = arr[6],
                                    Bag_No = bagNo,
                                    Last_Modified_Time = DateTime.Now,
                                    Last_Modified_By = userId,
                                    Batch_Id = batch_Id,
                                    Bill_Of_Lading = arr[10],
                                    Note = arr[11],
                                    Packaging_Material = packaging
                                });
                            }
                        }
                        lbl = lbl + 1;
                    }

                    cnt = _RMProcessor.AddLabel(Lst.ToArray(), packaging);

                    transScope.Complete();
                }
                //cnt = labelListNew.Count;

                // _lims.CreateSUZRawMaterialSample();

                ////Print new labels(both pallet and bags) - commented out to separate the printing functionality from creating the label record
                //PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = PrinterSettingsModel.Processes.RawMatRec.ToString() }).FirstOrDefault();
                ////Prepare label array object
                //JArray jLabels = GetLabelInfoForNewLabels(_RMProcessor.GetLabelInfoBySeqId(labelListNew.ToArray()));
                //JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                //result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                //result = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                cnt = -1;
                _logger.LogError("Error for CreateLabel of RawMaterialController: " + msg);
                //result = HttpStatusCode.BadRequest;               
            }

            return Json(new { newId = cnt });

        }

        public ActionResult UpdateSTO(string labelIdUpd, string batchNoUpd, string amtUpd, string originUpd)
        {
            int rtn = 0;
            try
            {
                string userId = User.Identity.Name.Replace("APM\\", "").ToLower();
                rtn = _RMProcessor.UpdateSTODetail(new STODeliveryDetailsUpdateModel
                {
                    Storage_Unit_Nbr = labelIdUpd,
                    Batch_Nbr = batchNoUpd,
                    Matl_Nbr = originUpd,
                    Amt_Received = decimal.Parse(amtUpd.ToString())
                });
            }
            catch (Exception ex)
            {
                rtn = -1;
                _logger.LogError("Error for SaveSTO of RawMaterialController: " + ex.Message);
            }

            //return Json(new { newId = rtn });
            return RedirectToAction("InternalSTO", new { @id = rtn });

        }

        public JsonResult SaveSTO(string[] arr)
        {
            int rtn = 0;
            try
            {
                string userId = User.Identity.Name.Replace("APM\\", "").ToLower();
                for (int RowsToCreate = 0; RowsToCreate < int.Parse(arr[17]); RowsToCreate++)
                {
                    int nop = 1;
                    nop = int.Parse(arr[3].Split(",")[RowsToCreate]);
                    if (nop != null && nop > 0)
                    {
                        string batch_Id = arr[4].Split(",")[RowsToCreate];
                        if (!ValidBatchID(arr[4].Split(",")[RowsToCreate]))
                            batch_Id = _RMProcessor.GetBatchId(Int64.Parse(arr[6]), Int32.Parse(arr[7]), "R", userId, arr[4].Split(",")[RowsToCreate], Int32.Parse(arr[0].Split(",")[RowsToCreate]), arr[18]);

                        for (int i = 0; i < nop; i++)
                        {
                            string seedLabel = _readWriteAccess.LoadData<string>(DataLibrary.Processors.Sql.PurchaseOrderLabelsSql.GetNewLabelIdSTO).FirstOrDefault();
                            if (string.IsNullOrEmpty(seedLabel)) seedLabel = _readWriteAccess.LoadData<string>(DataLibrary.Processors.Sql.PurchaseOrderLabelsSql.GetNewLabelIdSEED).FirstOrDefault();

                            rtn += _RMProcessor.SaveSTODetail(new STODeliveryDetailsModel
                            {
                                IB_Raw_Mtls_Id = arr[0].Split(",")[RowsToCreate],
                                Delivery_Nbr = Int64.Parse(arr[6]),
                                Storage_Unit_Nbr = seedLabel,
                                Source_Plant = arr[14],
                                Batch_Nbr = arr[4].Split(",")[RowsToCreate],
                                Matl_Nbr = arr[5],
                                Delivery_Date = null,
                                Date_Added = null,
                                Date_Updated = DateTime.Now,
                                Date_Received = DateTime.Parse(arr[1]),
                                Date_Sent_Sap = null,
                                Amt_Received = decimal.Parse(arr[2].Split(",")[RowsToCreate]) / nop,
                                Uom = arr[11].Split(",")[RowsToCreate],
                                Received_By = userId,
                                Scanned_In = (arr[10] != "Yes") ? "Yes" : "",
                                Bill_Of_Lading = arr[8],
                                Note = arr[9],
                                Sap_Sent_By = userId,
                                Item_Nbr = arr[15].Split(",")[RowsToCreate],
                                Batch_Id = batch_Id,
                                ItemNo = arr[16].Split(",")[RowsToCreate]
                            });
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                rtn = -1;
                _logger.LogError("Error for SaveSTO of RawMaterialController: " + ex.Message);
            }

            return Json(new { newId = rtn });
        }

        public int SaveSTO_FROM_NPPQS(Models.Warehouse.StorageUnitModel sum, string DeliveryNo, string ItemNo, string DocNum, string ItemID)
        {
            int rtn = 0;
            try
            {
                if (sum != null && sum.MaterialId != null && DeliveryNo != null)
                {
                    List<RawMaterialDetailModel> listirm = _rawMaterialProcessor.GetRawMaterialByDeliveryNo(DeliveryNo.ToString());
                    RawMaterialDetailModel irm = listirm.Find(x => x.Ib_Raw_Mtls_Id.ToString() == ItemNo);
                    string userId = User.Identity.Name.Replace("APM\\", "").ToLower();
                    int nop = 1;
                    string itmno = "";
                    string mtlno = "";
                    string sourceplant = "";
                    if (irm != null)
                    {
                        rtn = _RMProcessor.SaveSTODetail(new STODeliveryDetailsModel
                        {
                            IB_Raw_Mtls_Id = irm.Ib_Raw_Mtls_Id.ToString(),
                            Delivery_Nbr = Int64.Parse(irm.Delivery_No),
                            Storage_Unit_Nbr = sum.UnitNumber,
                            Source_Plant = sourceplant,
                            Batch_Nbr = sum.BatchId,
                            Matl_Nbr = mtlno,
                            Delivery_Date = null,
                            Date_Added = null,
                            Date_Updated = DateTime.Now,
                            Date_Received = DateTime.Now,
                            Date_Sent_Sap = null,
                            Amt_Received = decimal.Parse(sum.NetQuantity.ToString()),
                            Uom = sum.UnitOfMeasure,
                            Received_By = userId,
                            Scanned_In = "Yes",
                            Bill_Of_Lading = "",
                            Note = "nppqs",
                            Sap_Sent_By = userId,
                            Item_Nbr = irm.Item_Nbr.ToString(),
                            Batch_Id = sum.BatchId,
                            ItemNo = ItemID
                        });
                    }
                    else
                    {
                        rtn = -1;
                    }

                }
            }
            catch (Exception ex)
            {
                rtn = -1;
                _logger.LogError("Error for SaveSTO of RawMaterialController: " + ex.Message);
            }

            return rtn;

        }

        public JsonResult UpdateSTOPackaging(string deliveryNo)
        {
            try
            {
                string userId = User.Identity.Name.Replace("APM\\", "").ToLower();
                string[] deliveryarr = new string[1];
                deliveryarr[0] = deliveryNo;
                int rslt = _RMProcessor.UpdateSTODeliveryDetail(deliveryarr, DateTime.Now, DateTime.Now, userId);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error for UpdateSTOPackaging of UpdateSTOPackaging: " + ex.Message);
                return Json(new { success = false });
            }
        }


        public JsonResult ConfirmLabelUpdate(string[] arr)
        {
            //arr[7] received date
            try
            {
                //long recId = long.Parse(arr[6]);
                long recId = long.Parse(arr[5]);
                int oldTotBag = (!string.IsNullOrEmpty(arr[8])) ? Int32.Parse(arr[8]) : 0;
                int newTotBag = (!string.IsNullOrEmpty(arr[3])) ? Int32.Parse(arr[3]) : 0;
                bool blnTotChange = (oldTotBag != newTotBag) ? true : false;
                int rtn = _RMProcessor.UpdateLabelRecords(new PurchaseOrderLabelsModel
                {
                    Pol_Seq = recId,
                    Label_Id = arr[0],
                    Amt_Received = Int32.Parse(arr[1]),
                    Lot_Nbr = arr[2],
                    Tot_Bag_No = newTotBag,
                    CountryOrigin = arr[5],
                    Last_Modified_By = User.Identity.Name.Replace("APM\\", "").ToLower()
                }, blnTotChange);

                return Json(new { affected = rtn });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error for ConfirmLabelUpdate of RawMaterialController: " + ex.Message);
                return Json(new { affected = 0 });
            }

        }

        //Printing Labels
        //For Pallets' bag label
        public JsonResult ConfirmReprintPO(long pol_seq, int beginBag, int endBag)
        {
            HttpStatusCode result;
            int affectedRow = 0;
            try
            {
                JArray jLabels = GetLabelInfoForBags(pol_seq, beginBag, endBag);
                PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = PrinterSettingsModel.Processes.PalletRawMatRec.ToString() }).FirstOrDefault();
                JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                //The following code need to be uncommented when printer is ready to go
                result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                result = HttpStatusCode.OK;
                long[] polArr = new long[1];
                polArr[0] = pol_seq;
                if (result == HttpStatusCode.OK)
                    affectedRow = _RMProcessor.UpdateLabelPrintDatePO(polArr, DateTime.Now);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error for ConfirmReprint of RawMaterialController: " + ex.Message);
                result = HttpStatusCode.BadRequest;
            }

            if (result == HttpStatusCode.OK)
                return Json(new { success = true, updatePrintDate = (affectedRow > 0) ? true : false });
            else
                return Json(new { success = false });
        }

        public JsonResult ConfirmReprintSTO(string[] pol_seq)
        {
            HttpStatusCode result;
            int affectedRow = 0;
            try
            {
                JArray jLabels = GetLabelInfoForBagsByLabelID(pol_seq);
                PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = PrinterSettingsModel.Processes.PalletRawMatRec.ToString() }).FirstOrDefault();
                JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                //The following code need to be uncommented when printer is ready to go
                //result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                result = HttpStatusCode.OK;
                string[] polArr = pol_seq;
                if (result == HttpStatusCode.OK)
                    affectedRow = _RMProcessor.UpdateLabelPrintDateForPO(polArr, DateTime.Now);

            }
            catch (Exception ex)
            {
                _logger.LogError("Error for ConfirmReprint of RawMaterialController: " + ex.Message);
                result = HttpStatusCode.BadRequest;
            }

            if (result == HttpStatusCode.OK)
                return Json(new { success = true, updatePrintDate = (affectedRow > 0) ? true : false });
            else
                return Json(new { success = false });
        }
        public JsonResult ConfirmReprintSTO_WithAmount(string[] pol_seq, string amtUpd)
        {
            HttpStatusCode result;
            int affectedRow = 0;
            try
            {
                JArray jLabels = GetLabelInfoForBagsByLabelID_WithAmount(pol_seq, amtUpd);
                PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = PrinterSettingsModel.Processes.PalletRawMatRec.ToString() }).FirstOrDefault();
                JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                //The following code need to be uncommented when printer is ready to go
                //result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                result = HttpStatusCode.OK;
                string[] polArr = pol_seq;
                if (result == HttpStatusCode.OK)
                    affectedRow = _RMProcessor.UpdateLabelPrintDateForPO(polArr, DateTime.Now);

            }
            catch (Exception ex)
            {
                _logger.LogError("Error for ConfirmReprint of RawMaterialController: " + ex.Message);
                result = HttpStatusCode.BadRequest;
            }

            if (result == HttpStatusCode.OK)
                return Json(new { success = true, updatePrintDate = (affectedRow > 0) ? true : false });
            else
                return Json(new { success = false });
        }

        public JsonResult ConfirmReprint(string[] pol_seq)
        {
            HttpStatusCode result;
            int affectedRow = 0;
            try
            {
                JArray jLabels = GetLabelInfoForBagsByLabelID(pol_seq);
                PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = PrinterSettingsModel.Processes.PalletRawMatRec.ToString() }).FirstOrDefault();
                JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                //The following code need to be uncommented when printer is ready to go
                //result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                result = HttpStatusCode.OK;
                string[] polArr = pol_seq;
                if (result == HttpStatusCode.OK)
                    affectedRow = _RMProcessor.UpdateLabelPrintDateForPO(polArr, DateTime.Now);

            }
            catch (Exception ex)
            {
                _logger.LogError("Error for ConfirmReprint of RawMaterialController: " + ex.Message);
                result = HttpStatusCode.BadRequest;
            }

            if (result == HttpStatusCode.OK)
                return Json(new { success = true, updatePrintDate = (affectedRow > 0) ? true : false });
            else
                return Json(new { success = false });
        }

        private JArray GetLabelInfoForBags(long pol_Seq, int beginBag, int endBag)
        {
            JArray JArrayLabels = new JArray();
            PurchaseOrderLabelsModel labelInfo = _RMProcessor.GetLabel(pol_Seq);

            if (labelInfo != null)
            {
                RawMaterialDetailModel rawMat = _rawMaterialProcessor.GetRawMaterialByRawMatlId(labelInfo.Ib_Raw_Mtls_Id);
                SAPMatlMasterModel sapMat = _sapMaterialProcessor.GetSapMaterialByMATLNBR(rawMat.Matl_Nbr);

                for (int i = beginBag; i < endBag + 1; i++)
                {
                    string labelId = (labelInfo.Tot_Bag_No == 0) ? labelInfo.Label_Id : labelInfo.Label_Id + "-" + i.ToString();
                    //var jLabelObject = new JObject(
                    //                   new JProperty("LabelID", labelId),
                    //                   new JProperty("MatDesc", sapMat.Sap_Matl_Descr),
                    //                   new JProperty("MatNo", rawMat.Matl_Nbr),
                    //                   new JProperty("PONum", rawMat.Document_Nbr.ToString()),
                    //                   new JProperty("DateRec", labelInfo.Product_Received_Date.ToShortDateString()),
                    //                   new JProperty("BagNo", i.ToString()),
                    //                   new JProperty("BatchNo", labelInfo.Vendor_Batch),
                    //                   new JProperty("Origin", labelInfo.CountryOrigin),
                    //                   new JProperty("WGT", labelInfo.Amt_Received.ToString()),
                    //                   new JProperty("QRCode", labelId));
                    //JArrayLabels.Add(jLabelObject);
                    var jLabelObject = new JObject(
                                      new JProperty("ascendBatch", labelInfo.Vendor_Batch),
                                      new JProperty("materialName", sapMat.Sap_Matl_Descr),
                                      new JProperty("materialNumber", rawMat.Matl_Nbr),
                                      new JProperty("serialNumber", labelId.ToString()),
                                      new JProperty("serialExt1", "-1"),
                                      new JProperty("serialExt2", "-1"),
                                      new JProperty("serialExt3", "-1"));
                    JArrayLabels.Add(jLabelObject);
                }

                return JArrayLabels;
            }

            return null;
        }
        private JArray GetLabelInfoForBagsByLabelID(string[] pol_Seq)
        {
            JArray JArrayLabels = new JArray();
            for (int i = 0; i < pol_Seq.Length; i++)
            {
                PurchaseOrderLabelsModel labelInfo = _RMProcessor.GetStoLabelDetails(pol_Seq[i]);

                if (labelInfo != null)
                {
                    RawMaterialDetailModel rawMat = _rawMaterialProcessor.GetRawMaterialByRawMatlId(labelInfo.Ib_Raw_Mtls_Id);
                    SAPMatlMasterModel sapMat = _sapMaterialProcessor.GetSapMaterialByMATLNBR(rawMat.Matl_Nbr);

                    string labelId = labelInfo.Label_Id;
                    //var jLabelObject = new JObject(
                    //                   new JProperty("LabelID", labelId),
                    //                   new JProperty("MatDesc", sapMat.Sap_Matl_Descr),
                    //                   new JProperty("MatNo", rawMat.Matl_Nbr),
                    //                   new JProperty("PONum", rawMat.Document_Nbr.ToString()),
                    //                   new JProperty("DateRec", labelInfo.Product_Received_Date.ToShortDateString()),
                    //                   new JProperty("BatchNo", labelInfo.Vendor_Batch),
                    //                   new JProperty("Origin", labelInfo.CountryOrigin),
                    //                   new JProperty("WGT", labelInfo.Amt_Received.ToString()),
                    //                   new JProperty("QRCode", labelId));
                    //JArrayLabels.Add(jLabelObject);
                    var jLabelObject = new JObject(
                                            new JProperty("vendorBatch", labelInfo.Vendor_Batch),
                                            new JProperty("ascendBatch", labelInfo.Batch_Id),
                                            new JProperty("materialName", sapMat.Sap_Matl_Descr.Split(" ")[0].Split(",")[0]),
                                            new JProperty("materialNumber", rawMat.Matl_Nbr),
                                            new JProperty("weightKg", (labelInfo.Uom == "KG" ? labelInfo.Amt_Received : LB_to_KG(labelInfo.Amt_Received)).ToString("00.000")),
                                            new JProperty("weightLb", (labelInfo.Uom == "LB" ? labelInfo.Amt_Received : KG_to_LB(labelInfo.Amt_Received)).ToString("00.000")),
                                            new JProperty("serialNumber", labelInfo.Label_Id.ToString()));
                    JArrayLabels.Add(jLabelObject);
                }
            }
            return JArrayLabels;
        }

        private JArray GetLabelInfoForBagsByLabelID_WithAmount(string[] pol_Seq, string amtUpd)
        {
            JArray JArrayLabels = new JArray();
            for (int i = 0; i < pol_Seq.Length; i++)
            {
                PurchaseOrderLabelsModel labelInfo = _RMProcessor.GetStoLabelDetails(pol_Seq[i]);

                if (labelInfo != null)
                {
                    RawMaterialDetailModel rawMat = _rawMaterialProcessor.GetRawMaterialByRawMatlId(labelInfo.Ib_Raw_Mtls_Id);
                    SAPMatlMasterModel sapMat = _sapMaterialProcessor.GetSapMaterialByMATLNBR(rawMat.Matl_Nbr);

                    string labelId = labelInfo.Label_Id;
                    //var jLabelObject = new JObject(
                    //                   new JProperty("LabelID", labelId),
                    //                   new JProperty("MatDesc", sapMat.Sap_Matl_Descr),
                    //                   new JProperty("MatNo", rawMat.Matl_Nbr),
                    //                   new JProperty("PONum", rawMat.Document_Nbr.ToString()),
                    //                   new JProperty("DateRec", labelInfo.Product_Received_Date.ToShortDateString()),
                    //                   new JProperty("BatchNo", labelInfo.Vendor_Batch),
                    //                   new JProperty("Origin", labelInfo.CountryOrigin),
                    //                   new JProperty("WGT", labelInfo.Amt_Received.ToString()),
                    //                   new JProperty("QRCode", labelId));
                    //JArrayLabels.Add(jLabelObject);
                    var jLabelObject = new JObject(
                                            new JProperty("vendorBatch", labelInfo.Vendor_Batch),
                                            new JProperty("ascendBatch", labelInfo.Batch_Id),
                                            new JProperty("materialName", sapMat.Sap_Matl_Descr.Split(" ")[0].Split(",")[0]),
                                            new JProperty("materialNumber", rawMat.Matl_Nbr),
                                            new JProperty("weightKg", (labelInfo.Uom == "KG" ? decimal.Parse(amtUpd) : LB_to_KG(decimal.Parse(amtUpd))).ToString("00.000")),
                                            new JProperty("weightLb", (labelInfo.Uom == "LB" ? labelInfo.Amt_Received : KG_to_LB(labelInfo.Amt_Received)).ToString("00.000")),
                                            new JProperty("serialNumber", labelInfo.Label_Id.ToString()));
                    JArrayLabels.Add(jLabelObject);
                }
            }
            return JArrayLabels;
        }

        public int PrintBagAndPalletLabels(IEnumerable<LabelInfoModel> labels, string arrLabelType)
        {
            HttpStatusCode result;
            int affectedRow = 0;
            try
            {
                JArray jLabels = GetLabelInfo(labels, arrLabelType);
                PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = (arrLabelType == "Pallet" ? PrinterSettingsModel.Processes.PalletRawMatRec.ToString() : PrinterSettingsModel.Processes.ItemRawMatRec.ToString()) }).FirstOrDefault();
                JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                result = HttpStatusCode.OK;//For testing purpose
                return affectedRow + 1;
            }
            catch (Exception ex)
            {
                return affectedRow;
            }
        }
        public int PrintBagAndPalletLabelsSingle(IEnumerable<LabelInfoModel> labels, string arrLabelType, int beginBag, int endBag)
        {
            HttpStatusCode result;
            int affectedRow = 0;
            try
            {
                JArray jLabels = GetLabelInfoSingle(labels, arrLabelType, beginBag, endBag);
                PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = (arrLabelType == "Pallet" ? PrinterSettingsModel.Processes.PalletRawMatRec.ToString() : PrinterSettingsModel.Processes.ItemRawMatRec.ToString()) }).FirstOrDefault();
                JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                result = HttpStatusCode.OK;//For testing purpose
                return affectedRow + 1;
            }
            catch (Exception ex)
            {
                return affectedRow;
            }
        }

        //For checkbox selection print function
        public JsonResult ConfirmReprintLabels(string[] arrLabel, string[] arrLabelType)
        {
            HttpStatusCode result;
            int affectedRow = 0;
            try
            {
                long[] LongNum = arrLabel.Select(long.Parse).ToArray();
                IEnumerable<LabelInfoModel> labelList = _RMProcessor.GetLabelInformation(arrLabel);
                long[] seqList = labelList.Select(l => l.Pol_Seq).ToArray();
                foreach (var item in arrLabelType)
                {
                    affectedRow += PrintBagAndPalletLabels(labelList, item);
                }
                ////arrLabel: array of pallet label
                //JArray jLabels = GetLabelInfo(labelList, arrLabelType);
                //PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = PrinterSettingsModel.Processes.RawMatRec.ToString() }).FirstOrDefault();
                //JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                //result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                //result = HttpStatusCode.OK;//For testing purpose
                //if (result == HttpStatusCode.OK)
                affectedRow = _RMProcessor.UpdateLabelPrintDate(LongNum, DateTime.Now);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error for ConfirmReprintLabels of RawMaterialController: " + ex.Message);
                result = HttpStatusCode.BadRequest;
            }

            if (affectedRow > 0)
                return Json(new { success = true, updatePrintDate = (affectedRow > 0) ? true : false });
            else
                return Json(new { success = false });
        }

        public JsonResult ConfirmReprintLabelsSingle(string[] arrLabel, int beginBag, int endBag)
        {
            HttpStatusCode result;
            int affectedRow = 0;
            try
            {
                long[] LongNum = arrLabel.Select(long.Parse).ToArray();
                IEnumerable<LabelInfoModel> labelList = _RMProcessor.GetLabelInformation(arrLabel);
                long[] seqList = labelList.Select(l => l.Pol_Seq).ToArray();
                string[] arrLabelType = new string[] { "Pallet", "Bag" };
                foreach (var item in arrLabelType)
                {
                    affectedRow += PrintBagAndPalletLabelsSingle(labelList, item, beginBag, endBag);
                }
                ////arrLabel: array of pallet label
                //JArray jLabels = GetLabelInfo(labelList, arrLabelType);
                //PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = PrinterSettingsModel.Processes.RawMatRec.ToString() }).FirstOrDefault();
                //JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                //result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                //result = HttpStatusCode.OK;//For testing purpose
                //if (result == HttpStatusCode.OK)
                affectedRow = _RMProcessor.UpdateLabelPrintDate(LongNum, DateTime.Now);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error for ConfirmReprintLabels of RawMaterialController: " + ex.Message);
                result = HttpStatusCode.BadRequest;
            }

            if (affectedRow > 0)
                return Json(new { success = true, updatePrintDate = (affectedRow > 0) ? true : false });
            else
                return Json(new { success = false });
        }

        //For checkbox selection print function
        public JsonResult ConfirmReprintLabelsPO(string[] arrLabel, string[] arrLabelType)
        {
            HttpStatusCode result;
            int affectedRow = 0;
            try
            {
                long[] LongNum = arrLabel.Select(long.Parse).ToArray();
                IEnumerable<LabelInfoModel> labelList = _RMProcessor.GetLabelInformation(arrLabel);
                long[] seqList = labelList.Select(l => l.Pol_Seq).ToArray();
                foreach (var item in arrLabelType)
                {
                    PrintBagAndPalletLabels(labelList, item);
                }
                ////arrLabel: array of pallet label
                //JArray jLabels = GetLabelInfo(labelList, arrLabelType);
                //PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = PrinterSettingsModel.Processes.RawMatRec.ToString() }).FirstOrDefault();
                //JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                //result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                //result = HttpStatusCode.OK;//For testing purpose
                //if (result == HttpStatusCode.OK)
                affectedRow = _RMProcessor.UpdateLabelPrintDate(LongNum, DateTime.Now);

                //IEnumerable<LabelInfoModel> labelList = _RMProcessor.GetLabelInformationForSTO(arrLabel);
                //long[] seqList = labelList.Select(l => l.Pol_Seq).ToArray();
                ////arrLabel: array of pallet label
                //JArray jLabels = GetLabelInfo(labelList, arrLabelType);
                //PrinterSettingsModel pSetting = _readWriteAccess.LoadData<PrinterSettingsModel>(PurchaseOrderLabelsSql.GetPrinterSettings, new { Process = PrinterSettingsModel.Processes.RawMatRec.ToString() }).FirstOrDefault();
                //JObject jPrintInput = new JObject(new JProperty("printer", pSetting.PrinterName), new JProperty("labelTemplate", pSetting.LabelName), new JProperty("labels", jLabels));
                //result = ZebraAPI.SendLabelPrintingRequest(jPrintInput).Result;
                //result = HttpStatusCode.OK;//For testing purpose
                //if (result == HttpStatusCode.OK)
                //    affectedRow = _RMProcessor.UpdateLabelPrintDate(seqList, DateTime.Now);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error for ConfirmReprintLabels of RawMaterialController: " + ex.Message);
                result = HttpStatusCode.BadRequest;
            }

            if (affectedRow > 0)
                return Json(new { success = true, updatePrintDate = (affectedRow > 0) ? true : false });
            else
                return Json(new { success = false });
        }

        //select record to print from label listing grid
        private JArray GetLabelInfo(IEnumerable<LabelInfoModel> labels, string arrLabelType)
        {
            List<LabelInfoModel> filteredList = null;
            string labelType = "";
            if (arrLabelType == "Pallet")
            {
                filteredList = labels.Where(l => l.Bag_No == 0).ToList();
                labelType = "Pallet";
            }
            else
            {
                labelType = "Bag";
                filteredList = labels.Where(l => l.Bag_No != 0).ToList();
            }

            JArray JArrayLabels = new JArray();
            if (filteredList.Count > 0)
            {
                string labelIdForPrint = "";
                var jLabelObject = new JObject();
                string[] arrlbl = labels.Select(x => x.Label_Id).Distinct().ToArray();
                foreach (string lbl in arrlbl)
                {
                    if (labelType == "Bag")
                    {
                        int lblcnt = 1;
                        LabelInfoModel info = filteredList.FindAll(x => x.Label_Id == lbl).FirstOrDefault();

                        int flcount = filteredList.FindAll(x => x.Label_Id == lbl).Count();
                        while (lblcnt <= flcount)
                        {
                            var jLabelObjectBase = new JObject(
                                new JProperty("ascendBatch", info.Vendor_Batch),
                                new JProperty("materialName", info.Sap_Matl_Descr.Split(" ")[0]),
                                new JProperty("materialNumber", info.Sap_Matl_Nbr),
                                new JProperty("serialNumber", info.Label_Id.ToString()));
                            for (int i = 1; i <= 3; i++)
                            {
                                if (i == 1)
                                {
                                    jLabelObject = jLabelObjectBase;
                                }
                                if (lblcnt <= flcount)
                                    jLabelObject.Add(new JProperty("serialExt" + i.ToString(), "-" + lblcnt));
                                else
                                    jLabelObject.Add(new JProperty("serialExt" + i.ToString(), "-" + 0));
                                lblcnt += 1;
                            }
                            JArrayLabels.Add(jLabelObject);
                        }
                    }
                    if (labelType == "Pallet")
                    {
                        LabelInfoModel info = filteredList.FindAll(x => x.Label_Id == lbl).FirstOrDefault();

                        labelIdForPrint = info.Label_Id;
                        jLabelObject = new JObject(
                                           new JProperty("vendorBatch", info.Vendor_Batch),
                                           new JProperty("ascendBatch", info.batch_id),
                                           new JProperty("materialName", info.Sap_Matl_Descr.Split(" ")[0]),
                                           new JProperty("materialNumber", info.Sap_Matl_Nbr),
                                           new JProperty("weightKg", (info.UOM == "KG" ? info.Amt_Received : LB_to_KG(info.Amt_Received)).ToString("00.000")),
                                           new JProperty("weightLb", (info.UOM == "LB" ? info.Amt_Received : KG_to_LB(info.Amt_Received)).ToString("00.000")),
                                           new JProperty("serialNumber", info.Label_Id.ToString()));
                        JArrayLabels.Add(jLabelObject);
                    }
                }
                return JArrayLabels;
            }
            return null;
        }

        private JArray GetLabelInfoSingle(IEnumerable<LabelInfoModel> labels, string arrLabelType, int beginBag, int endBag)
        {
            List<LabelInfoModel> filteredList = null;
            string labelType = "";
            if (arrLabelType == "Pallet")
            {
                filteredList = labels.Where(l => l.Bag_No == 0).ToList();
                labelType = "Pallet";
            }
            else
            {
                labelType = "Bag";
                filteredList = labels.Where(l => l.Bag_No != 0).ToList();
            }

            JArray JArrayLabels = new JArray();
            if (filteredList.Count > 0)
            {
                string labelIdForPrint = "";
                var jLabelObject = new JObject();
                string[] arrlbl = labels.Select(x => x.Label_Id).Distinct().ToArray();
                foreach (string lbl in arrlbl)
                {
                    if (labelType == "Bag")
                    {
                        int lblcnt = beginBag;
                        LabelInfoModel info = filteredList.FindAll(x => x.Label_Id == lbl).FirstOrDefault();

                        int flcount = endBag;
                        while (lblcnt <= flcount)
                        {
                            var jLabelObjectBase = new JObject(
                                new JProperty("ascendBatch", info.Vendor_Batch),
                                new JProperty("materialName", info.Sap_Matl_Descr.Split(" ")[0]),
                                new JProperty("materialNumber", info.Sap_Matl_Nbr),
                                new JProperty("serialNumber", info.Label_Id.ToString()));
                            for (int i = 1; i <= 3; i++)
                            {
                                if (i == 1)
                                {
                                    jLabelObject = jLabelObjectBase;
                                }
                                if (lblcnt <= flcount)
                                    jLabelObject.Add(new JProperty("serialExt" + i.ToString(), "-" + lblcnt));
                                else
                                    jLabelObject.Add(new JProperty("serialExt" + i.ToString(), "-" + 0));
                                lblcnt += 1;
                            }
                            JArrayLabels.Add(jLabelObject);
                        }
                    }
                    if (labelType == "Pallet")
                    {
                        LabelInfoModel info = filteredList.FindAll(x => x.Label_Id == lbl).FirstOrDefault();

                        labelIdForPrint = info.Label_Id;
                        jLabelObject = new JObject(
                                           new JProperty("vendorBatch", info.Vendor_Batch),
                                           new JProperty("ascendBatch", info.batch_id),
                                           new JProperty("materialName", info.Sap_Matl_Descr.Split(" ")[0]),
                                           new JProperty("materialNumber", info.Sap_Matl_Nbr),
                                           new JProperty("weightKg", (info.UOM == "KG" ? info.Amt_Received : LB_to_KG(info.Amt_Received)).ToString("00.000")),
                                           new JProperty("weightLb", (info.UOM == "LB" ? info.Amt_Received : KG_to_LB(info.Amt_Received)).ToString("00.000")),
                                           new JProperty("serialNumber", info.Label_Id.ToString()));
                        JArrayLabels.Add(jLabelObject);
                    }
                }
                return JArrayLabels;
            }
            return null;
        }


        public ActionResult Scan()
        {
            return View("ScannerView");
        }

        [HttpPost]
        public IActionResult Scanned(string[] labelId) //LineItem Scan Label Id
        {
            string _labelId = "";
            int rtn = 0;
            for (int i = 0; i < labelId.Length; i++)
            {
                if (labelId[i] != "")
                {
                    var arr = labelId[i].Split("-"); //bag label:688000001-1
                    labelId[i] = arr[0].Trim();
                }
            }

            try
            {
                //PurchaseOrderLabelsModel scanSearch = _RMProcessor.GetLabelByLabelId(_labelId);
                List<PurchaseOrderLabelsModel> scanSearch = _RMProcessor.GetLabelByLabelIdPO(labelId);
                if (scanSearch != null)
                {
                    for (int i = 0; i < labelId.Length; i++)
                    {
                        int rslt = _RMProcessor.UpdateLabelScanDateForPO(labelId, DateTime.Now);
                    }
                    foreach (PurchaseOrderLabelsModel item in scanSearch)
                    {
                        //if (item.Label_Scan_Date == null)
                        //{
                        //    //Update scan date to the label record                  
                        //    int rslt = _RMProcessor.UpdateLabelScanDate(new PurchaseOrderLabelsModel { Label_Id = item.Label_Id, Label_Scan_Date = DateTime.Now });

                        //}

                        //Send batch info to LIMS
                        if (item.Batch_Sent_Time == null)
                        {
                            var batchModel = _RMProcessor.GetLabelMaterialInfoBySeqId(item.Pol_Seq);
                            //if (!_lims.CreateSUZRawMaterialSample(batchModel))
                            //{
                            rtn += _RMProcessor.UpdateBatchSentDate(batchModel.Batch_Id, DateTime.Now);
                            //}

                        }
                    }

                    return Json(new { success = true, batchSentSuccess = ((rtn > 0) || (rtn == 0)) ? true : false });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error for Scanned of RawMaterialController: " + ex.Message);
            }

            return null;
        }

        //Scan in Delivery Number from Pack List
        public IActionResult STOScan(string[] serialNumber) //LineItem Scan Label Id
        {
            try
            {
                var scanSearch = _RMProcessor.GetSTOByDeliveryNoteforScanForPO(serialNumber);
                if (scanSearch != null && scanSearch.Count > 0)
                {
                    string userId = User.Identity.Name.Replace("APM\\", "").ToLower();
                    //Update scan date to the label record                  
                    int rslt = _RMProcessor.UpdateSTODeliveryDetail(serialNumber, DateTime.Now, DateTime.Now, userId);
                    return Json(new { success = true });
                }
                else
                    return Json(new { success = false });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error for STOScan of RawMaterialController: " + ex.Message);
                return Json(new { success = false });
            }
        }


        public JsonResult GetLabelForDel(string pol_seq)
        {
            PurchaseOrderLabelsModel labelModel = _RMProcessor.GetLabel(long.Parse(pol_seq));
            if (labelModel != null)
            {

                return Json(new
                {
                    pol_seq,
                    labelModel.Label_Id,
                    p = labelModel.Product_Received_Date.ToShortDateString(),
                    v = labelModel.Label_Print_Date?.ToShortDateString(),
                    labelModel.Amt_Received,
                    labelModel.Lot_Nbr,
                    z = labelModel.Label_Scan_Date?.ToShortDateString(),
                    labelModel.Tot_Bag_No,
                    labelModel.Vendor_Batch,
                    labelModel.CountryOrigin
                });
            }
            return null;
        }
        public JsonResult GetLabelForDel_STO(string pol_seq)
        {
            PurchaseOrderLabelsModel labelModel = _RMProcessor.GetLabelSTO(long.Parse(pol_seq));
            if (labelModel != null)
            {
                return Json(new
                {
                    pol_seq,
                    labelModel.Label_Id,
                    p = labelModel.Product_Received_Date.ToShortDateString(),
                    v = labelModel.Label_Print_Date?.ToShortDateString(),
                    labelModel.Amt_Received,
                    labelModel.Lot_Nbr,
                    z = labelModel.Label_Scan_Date?.ToShortDateString(),
                    labelModel.Tot_Bag_No,
                    labelModel.Vendor_Batch,
                    labelModel.CountryOrigin
                });
            }
            return null;
        }

        public JsonResult GetLabelForDel_Label(string pol_seq)
        {
            try
            {
                PurchaseOrderLabelsModel labelModel = _RMProcessor.GetStoLabelDetails(pol_seq);
                if (labelModel != null)
                {
                    return Json(new
                    {
                        pol_seq,
                        labelModel.Label_Id,
                        p = labelModel.Product_Received_Date.ToShortDateString(),
                        v = labelModel.Label_Print_Date?.ToShortDateString(),
                        labelModel.Amt_Received,
                        labelModel.Lot_Nbr,
                        z = labelModel.Label_Scan_Date?.ToShortDateString(),
                        labelModel.Tot_Bag_No,
                        labelModel.Vendor_Batch,
                        labelModel.CountryOrigin
                    });
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return null;
        }

        public JsonResult ConfirmLabelDel(string labelId)
        {
            int rtn = 0;
            try
            {
                rtn = _RMProcessor.DeleteLabel(labelId, false);
            }
            catch
            {
                rtn = -1;
            }

            return Json(new { affected = rtn });
        }
        public JsonResult ConfirmLabelDelForSTODetail(string labelId)
        {
            int rtn = 0;
            try
            {
                rtn = _RMProcessor.DeleteLabelForSTO(labelId);
            }
            catch
            {
                rtn = -1;
            }

            return Json(new { affected = rtn });
        }


        //public JObject PrepareIRMInfoForSAP(long Pol_Seq)
        //{
        //    PurchaseOrderLabelsModel labelInfo = _RMProcessor.GetLabel(Pol_Seq);

        //    if (labelInfo != null)
        //    {                
        //        POMaterialModel poMaterialSAP = _rawMaterialProcessor.GetIRMInfoForSAP(labelInfo.Ib_Raw_Mtls_Id);
        //        string qualityCheck = "";
        //        if (poMaterialSAP.Sap_Matl_Type == "ROH")
        //        {
        //            qualityCheck = "0";
        //        }

        //        var jLabelObject = new JObject(
        //          new JProperty("POST_DATE", DateTime.Now.ToString("yyyyMMdd")),
        //          new JProperty("DOC_DATE", poMaterialSAP.Document_Date.ToString("yyyyMMdd")),
        //          new JProperty("MES_TXN_ID", labelInfo.Pol_Seq),
        //          new JProperty("DOC_HDR_TEXT", "Info Text"),
        //          new JProperty("REF_DOC_NO", poMaterialSAP.Document_Nbr),
        //          new JProperty("Materials",
        //          new JArray(new JObject(
        //                      new JProperty("PO_NUMBER", poMaterialSAP.Document_Nbr.ToString()),
        //                      new JProperty("PO_ITEM", poMaterialSAP.Item_Nbr.ToString()),
        //                      new JProperty("MATERIAL", poMaterialSAP.Matl_Nbr.ToString()),
        //                      new JProperty("PLANT", AppSettings.GetPlant()),
        //                      new JProperty("SLOC", "50V"),
        //                      new JProperty("BATCH", ""),
        //                      new JProperty("MOVE_TYPE", AppSettings.GetMOVE_TYPE()),
        //                      new JProperty("STOCK_TYPE", qualityCheck),
        //                      new JProperty("IM_QUANTITY", labelInfo.Amt_Received.ToString()),
        //                      new JProperty("IM_UOM", poMaterialSAP.Uom),
        //                      new JProperty("IM_ITEM_TEXT", ""),
        //                      new JProperty("IM_GOODS_RECIPIENT", ""),
        //                      new JProperty("IM_UNLOAD_PT", ""),
        //                      new JProperty("IM_LINE_ID", "1"),
        //                      new JProperty("Pallets",
        //                      new JArray(new JObject(
        //                              new JProperty("WM_LINE_ID", "1"),
        //                              new JProperty("WM_QUANTITY", ""),
        //                              new JProperty("WM_UOM", ""),
        //                              new JProperty("WM_BATCH", ""),
        //                              new JProperty("WM_STG_TYPE", ""),
        //                              new JProperty("WM_STG_SECTION", ""),
        //                              new JProperty("WM_STG_BIN", ""),
        //                              new JProperty("WM_STG_UNIT_NUMBER", ""),
        //                              new JProperty("WM_STG_UNIT_TYPE", "")
        //      )))))));

        //        return jLabelObject;
        //    }
        //    return null;
        //}

        [HttpPost]
        /// Searches SmartMFG and external systems for the storage unit specified.
        /// If found in an external system, the quantity is converted to match the shipment's unit of measure.
        public IActionResult SearchForStorageUnit(Models.Services.StorageUnitSearchRequestModel searchRequest, string DeliveryNo, string ItemNo, string DocNum, string ItemID)
        {
            List<StorageUnitSearchResultModel> listresult = new List<StorageUnitSearchResultModel>();
            int cnt = 0;
            if (!string.IsNullOrEmpty(searchRequest.StorageUnitNumber))
            {
                string[] arr = searchRequest.StorageUnitNumber.Split(",");
                foreach (var item in arr)
                {
                    Models.Services.StorageUnitSearchRequestModel sto = new Models.Services.StorageUnitSearchRequestModel();
                    sto.StorageUnitNumber = item;
                    var result = _rawMaterialsService.SearchForStorageUnit(sto);

                    if (result.Success)
                    {
                        if (SaveSTO_FROM_NPPQS(result.StorageUnit, DeliveryNo, ItemNo, DocNum, ItemID) > 0)
                            return Ok(result);
                        else
                            return NotFound(result);
                    }

                    return NotFound(result);
                }
                return Ok(cnt);
            }

            return HandleBadRequest(ModelState);
        }

        public bool SearchForBatchID(string BatchID)
        {
            return _rawMaterialsService.SearchForBatchID(BatchID);
        }

        [HttpPost]
        /// Searches SmartMFG and external systems for the storage unit specified is CSV multiple values.
        /// If found in an external system, the quantity is converted to match the shipment's unit of measure.
        public IActionResult SearchForStorageUnitMultiple(Models.Services.StorageUnitSearchRequestModel searchRequest, string DeliveryNo, string ItemNo, string DocNum, string ItemID)
        {
            List<StorageUnitSearchResultModel> listresult = new List<StorageUnitSearchResultModel>();
            int cnt = 0;
            if (searchRequest.StorageUnitNumberMultiple.Length > 0)
            {
                foreach (var item in searchRequest.StorageUnitNumberMultiple)
                {
                    Models.Services.StorageUnitSearchRequestModel sto = new Models.Services.StorageUnitSearchRequestModel();
                    sto.StorageUnitNumber = item;
                    var result = _rawMaterialsService.SearchForStorageUnit(sto);

                    if (result.Success)
                    {
                        cnt += SaveSTO_FROM_NPPQS(result.StorageUnit, DeliveryNo, ItemNo, DocNum, ItemID);
                    }
                }
                return Ok(cnt);
            }

            return HandleBadRequest(ModelState);
        }

        public decimal KG_to_LB(decimal vl)
        {
            //return vl * decimal.Parse("2.204");
            return vl / decimal.Parse("0.4536");
        }
        public decimal LB_to_KG(decimal vl)
        {
            // return vl * decimal.Parse("0.45");
            return vl * decimal.Parse("0.4536");

        }

        private ActionResult HandleBadRequest(
            Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary modelState
        )
        {
            string errorMessages = modelState.Values
                .SelectMany(entry => entry.Errors)
                .Select(error => error.ErrorMessage)
                .Aggregate((allMessages, errorMessage) => $"{allMessages}\n{errorMessage}");

            return BadRequest(new { success = false, errorMessage = errorMessages });
        }
    }
}
