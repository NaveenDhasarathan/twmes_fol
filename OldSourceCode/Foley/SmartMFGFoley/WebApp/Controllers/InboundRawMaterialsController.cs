﻿using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApp.ViewModels;


namespace WebApp.Controllers
{
    public class InboundRawMaterialsController : Controller
    {
        private readonly IInboundRawMaterialsProcessor _rawMaterialProcessor;
        private readonly ISapMatlMasterProcessor _sapMaterialProcessor;
        private readonly IRawMaterialProcessor _poLabelProcessor;


        public InboundRawMaterialsController(IInboundRawMaterialsProcessor rawMaterialProcessor, ISapMatlMasterProcessor sapMaterialProcessor, IRawMaterialProcessor poLabelProcessor)
        {
            _rawMaterialProcessor = rawMaterialProcessor;
            _sapMaterialProcessor = sapMaterialProcessor;
            _poLabelProcessor = poLabelProcessor;
        }

        [Route("RawMaterialReceiving/{rmType?}")]
        public IActionResult Index(string rmType)
        {

            ViewBag.TitlePart = (!String.IsNullOrEmpty(rmType)) ? /*" - " +*/ rmType.Replace("_", " ") : "";
            List<POMaterialModel> POList = (!String.IsNullOrEmpty(rmType)) ? _rawMaterialProcessor.GetRawMaterialByDocType(rmType.Substring(0, 1)) : _rawMaterialProcessor.GetAllSTO();
            POList.Add(new POMaterialModel { Document_Nbr = 0 });   //for not missing last record      

            List<RawMaterialChildViewModel> modelList = new List<RawMaterialChildViewModel>();
            long testNbr = POList[0].Document_Nbr;
            decimal qty = 0;
            string itemStr = "";
            long docNbr = 0;
            int senttosap = 0;
            string supName = "";
            string supNo = "";
            string maDesc = "";
            string maType = "";
            string docType = "";
            string matTypeDesc = "";
            string matAdditionalInfo = "";
            decimal amtReceived = 0;
            decimal TotalamtReceived = 0;

            DateTime? deliveryDate = null;
            foreach (var rec in POList)
            {
                if (testNbr != rec.Document_Nbr)
                {
                    RawMaterialChildViewModel childVM = new RawMaterialChildViewModel();
                    POMaterialModel po = new POMaterialModel();
                    po.Document_Nbr = docNbr;
                    po.Supplier_Name = supName;
                    po.Supplier_Nbr = supNo;
                    po.Quantity = qty;
                    po.Amt_Received = TotalamtReceived;
                    po.Sap_Matl_Descr = maDesc;
                    po.Sap_Matl_Type = maType;
                    po.Matl_type_descr = matTypeDesc;
                    po.Additional_info = matAdditionalInfo;
                    po.Document_Type = docType;
                    po.Requested_Delivery_Date = deliveryDate;
                    po.SENT_TO_SAP = senttosap;
                    childVM.POMaterial = po;
                    if (docType.ToLower() == "po")
                        childVM.Style = " class=internal ";
                    else
                        childVM.Style = "";

                    childVM.LineItems = itemStr;
                    modelList.Add(childVM);
                    qty = 0;
                    TotalamtReceived = 0;
                    itemStr = "";
                }

                docNbr = rec.Document_Nbr;
                supName = rec.Supplier_Name;
                supNo = rec.Supplier_Nbr;
                maDesc = rec.Sap_Matl_Descr;
                maType = rec.Sap_Matl_Type;
                docType = rec.Document_Type;
                matTypeDesc = rec.Matl_type_descr;
                matAdditionalInfo = rec.Additional_info;
                deliveryDate = rec.Requested_Delivery_Date;
                amtReceived = rec.Amt_Received;

                if (rec.Frm?.ToLower() == "im" && rec.Too?.ToLower() == "im")
                {
                    amtReceived = rec.Amt_Received;
                }
                else if (rec.Frm?.ToLower() != "wm")
                {
                    amtReceived = rec.Quantity_Received;
                }

                qty += rec.Quantity;
                senttosap = rec.SENT_TO_SAP;
                TotalamtReceived += rec.Amt_Received;
                //itemStr += rec.Item_Nbr + "|" + rec.Matl_Nbr + "|" + rec.Ib_Raw_Mtls_Id + "|" + rec.Sap_Matl_Descr + "|" + rec.Quantity.ToString("#,##0.000") + "|" + rec.Amt_Received.ToString("#,##0.000") + "|" + rec.Requested_Delivery_Date?.ToShortDateString() + "|" + docType + "|" + rec.Uom + "~";
                itemStr += rec.Item_Nbr + "|" + rec.Matl_Nbr + "|" + rec.Ib_Raw_Mtls_Id + "|" + rec.Sap_Matl_Descr + "|" + rec.Quantity.ToString("#,##0.000") + "|" + (rec.Uom == "LB" ? KG_to_LB(amtReceived) : amtReceived).ToString("#,##0.000") + "|" + rec.Requested_Delivery_Date?.ToShortDateString() + "|" + docType + "|" + rec.Uom + "~";

                testNbr = rec.Document_Nbr;
            }

            var model = new RawMaterialsGroupedViewModel
            {
                PurchaseOrderList = modelList,
                TotalQTY = POList.Sum(p => p.Quantity)
            };

            return View(model);
        }

        [Route("RawMaterialReceivingSTO")]
        public IActionResult STO(string rmType)
        {
            ViewBag.TitlePart = "Stock Transfer Order";
            List<POMaterialModel> POList = _rawMaterialProcessor.GetInternalSTO();
            //POList = POList.FindAll(x => x.Delivery_No == "860136717");
            POList.Add(new POMaterialModel { Delivery_No = "" });   //for not missing last record   

            List<RawMaterialChildViewModel> modelList = new List<RawMaterialChildViewModel>();
            string testNbr = POList[0].Delivery_No;
            decimal qty = 0;
            string itemStr = "";
            long docNbr = 0;
            string supName = "";
            string supNo = "";
            string maDesc = "";
            string maType = "";
            string docType = "";
            string matTypeDesc = "";
            string matAdditionalInfo = "";
            decimal amtReceived = 0;
            decimal TotalamtReceived = 0;
            string deliveryNo = "";
            long ib_Raw_Mtls_Id = 0;
            int SENT_TO_SAP = 0;
            DateTime? deliveryDate = null;
            string ittmNbr = "";
            string po_closed = "";
            foreach (var rec in POList)
            {
                if (testNbr != rec.Delivery_No)
                {
                    RawMaterialChildViewModel childVM = new RawMaterialChildViewModel();
                    POMaterialModel po = new POMaterialModel();
                    po.Document_Nbr = docNbr;
                    po.Supplier_Name = supName;
                    po.Supplier_Nbr = supNo;
                    po.Quantity = qty;
                    po.Amt_Received = TotalamtReceived;
                    po.Sap_Matl_Descr = maDesc;
                    po.Sap_Matl_Type = maType;
                    po.Matl_type_descr = matTypeDesc;
                    po.Additional_info = matAdditionalInfo;
                    po.Document_Type = docType;
                    po.Requested_Delivery_Date = deliveryDate;
                    po.Delivery_No = deliveryNo;
                    po.Ib_Raw_Mtls_Id = ib_Raw_Mtls_Id;
                    po.Matl_Nbr = rec.Matl_Nbr;
                    po.Item_Nbr = rec.Item_Nbr;
                    po.SENT_TO_SAP = SENT_TO_SAP;
                    po.po_closed = po_closed;
                    po.Frm = rec.Frm;
                    po.Too = rec.Too;
                    po.Quantity_Received = rec.Quantity_Received;
                    //to-do:
                    //var stoChildren = _rawMaterialProcessor.GetSTOByMtrl_Nbr(rec.Matl_Nbr);

                    //itemStr += rec.Item_Nbr+".."+rec.Document_Nbr + "|" + rec.Matl_Nbr + "|" + rec.Ib_Raw_Mtls_Id + "|" + rec.Sap_Matl_Descr + "|" + rec.Quantity.ToString("#,##0") + "|" + rec.Amt_Received.ToString("#,##0") + "|" + rec.Requested_Delivery_Date?.ToShortDateString() + "|" + docType + "|" + rec.Uom + "|" + rec.storage_unit_nbr + "~";

                    childVM.POMaterial = po;
                    childVM.LineItems = itemStr;
                    modelList.Add(childVM);
                    qty = 0;
                    TotalamtReceived = 0;
                    itemStr = "";
                }

                docNbr = rec.Document_Nbr;
                supName = rec.Supplier_Name;
                supNo = rec.Supplier_Nbr;
                maDesc = rec.Sap_Matl_Descr;
                maType = rec.Sap_Matl_Type;
                docType = rec.Document_Type;
                matTypeDesc = rec.Matl_type_descr;
                matAdditionalInfo = rec.Additional_info;
                deliveryDate = rec.Requested_Delivery_Date;
                amtReceived = rec.Amt_Received;


                if (rec.Frm?.ToLower() == "im" && rec.Too?.ToLower() == "im")
                {
                    amtReceived = rec.Quantity_Received;
                }
                else if (rec.Frm?.ToLower() != "wm")
                {
                    amtReceived = rec.Amt_Received;
                }




                deliveryNo = rec.Delivery_No;
                ib_Raw_Mtls_Id = rec.Ib_Raw_Mtls_Id;
                SENT_TO_SAP = rec.SENT_TO_SAP;
                qty += rec.Quantity;
                TotalamtReceived += rec.Amt_Received;
                po_closed = rec.po_closed;
                //if (ittmNbr != rec.Item_Nbr.ToString())
                itemStr += rec.Item_Nbr + "|" + rec.Matl_Nbr + "|" + rec.Ib_Raw_Mtls_Id + "|" + rec.Sap_Matl_Descr + "|" + rec.Quantity.ToString("#,##0.000") + "|" + (rec.Uom == "LB" ? KG_to_LB(amtReceived) : amtReceived).ToString("#,##0.000") + "|" + rec.Requested_Delivery_Date?.ToShortDateString() + "|" + docType + "|" + rec.Uom + "|" + rec.storage_unit_nbr + "~";

                testNbr = rec.Delivery_No;
                ittmNbr = rec.Item_Nbr.ToString();
            }

            var model = new RawMaterialsGroupedViewModel
            {
                PurchaseOrderList = modelList,
                TotalQTY = POList.Sum(p => p.Quantity)
            };

            return View("StoListing", model);
        }
        public IActionResult PODetails(long id)
        {
            PODetailViewModel vm = new PODetailViewModel();
            var rawMat = _rawMaterialProcessor.GetExternalPOByNBR(id);
            if (rawMat != null)
            {
                vm.Document_Nbr = rawMat[0].Document_Nbr;
                vm.Supplier_Name = rawMat[0].Supplier_Name;
                vm.Supplier_Nbr = rawMat[0].Supplier_Nbr;
                var sapMat = _sapMaterialProcessor.GetSapMaterialByMATLNBR(rawMat[0].Matl_Nbr);
                if (sapMat != null)
                    vm.MaterialDesc = sapMat.Sap_Matl_Descr;
                vm.ItemList = _rawMaterialProcessor.GetExternalPOByNBR(id);
            }
            return View("PODetails", vm);
        }

        public decimal KG_to_LB(decimal vl)
        {
            //return vl * decimal.Parse("2.20462262185");
            return vl / decimal.Parse("0.4536");
        }
        public decimal LB_to_KG(decimal vl)
        {
            //return vl * decimal.Parse("0.45359237");
            return vl * decimal.Parse("0.4536");
        }
        //[HttpPost]
        //public IActionResult Scanned(string labelId, string rawMtlsId) //LineItem Scan Label Id
        //{
        //    string _labelId="";
        //    if (labelId !="")
        //    {
        //        var arr = labelId.Split("-");
        //        _labelId = arr[0];
        //    }

        //    PurchaseOrderLabelsModel scanSearch = _poLabelProcessor.GetLabelByLabelId_MtlId(_labelId, long.Parse(rawMtlsId));

        //    if (scanSearch != null)
        //    {
        //        if (scanSearch.Label_Scan_Date == null)
        //        {
        //            //Update scan date to the label record                  
        //            int rslt = _poLabelProcessor.UpdateLabelScanDate(new PurchaseOrderLabelsModel { Label_Id = scanSearch.Label_Id, Label_Scan_Date = DateTime.Now });
        //            if (rslt > 0)
        //            {
        //                JObject jLabels = ReportToSAP(scanSearch.Pol_Seq);
        //                //prepSAPReport(scanSearch.Pol_Seq);
        //                string result = SapAPI.SengdingRawMaterialsToSAP(jLabels).Result;
        //            }
        //        }
        //        return Json(new { success = true });
        //    }

        //    return null;
        //}

        //public void prepSAPReport(long Pol_Seq)
        //{
        //    JObject jLabels = ReportToSAP(Pol_Seq);
        //    try
        //    {
        //        string result = SapAPI.SengdingRawMaterialsToSAP(jLabels).Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //        //Need to implement error page?
        //    }
        //}

        //public JObject ReportToSAP(long Pol_Seq)
        //{
        //    PurchaseOrderLabelsModel labelInfo = _poLabelProcessor.GetLabel(Pol_Seq);

        //    if (labelInfo != null)
        //    {
        //        InboundRawMaterialsModel rawMat = _rawMaterialProcessor.GetRawMaterialByRawMatlId(labelInfo.Ib_Raw_Mtls_Id);
        //        SAPMatlMasterModel sapMat = _sapMaterialProcessor.GetSapMaterialByMATLNBR(rawMat.Matl_Nbr);
        //        string qualityCheck = string.Empty;
        //        if (labelInfo.Quality == false)
        //        {
        //            qualityCheck = "X";
        //        }
        //        else
        //        {
        //            qualityCheck = " ";
        //        }
        //        var jLabelObject = new JObject(
        //          new JProperty("POST_DATE", DateTime.Now.ToString("yyyyMMdd")),
        //          new JProperty("DOC_DATE", rawMat.Document_Date.ToString("yyyyMMdd")),
        //          new JProperty("MES_TXN_ID", labelInfo.Pol_Seq),
        //          new JProperty("DOC_HDR_TEXT", "Info Text"),
        //          new JProperty("REF_DOC_NO", rawMat.Document_Nbr),
        //          new JProperty("Materials",
        //          new JArray(new JObject(
        //                      new JProperty("PO_NUMBER", rawMat.Document_Nbr.ToString()),
        //                      new JProperty("PO_ITEM", rawMat.Item_Nbr.ToString()),
        //                      new JProperty("MATERIAL", rawMat.Matl_Nbr.ToString()),
        //                      new JProperty("PLANT", "6802"),
        //                      new JProperty("SLOC", "50V"),
        //                      new JProperty("BATCH", ""),
        //                      new JProperty("MOVE_TYPE", "101"),
        //                      new JProperty("STOCK_TYPE", qualityCheck),
        //                      new JProperty("IM_QUANTITY", labelInfo.Amt_Received.ToString()),
        //                      new JProperty("IM_UOM", "KG"),
        //                      new JProperty("IM_ITEM_TEXT", ""),
        //                      new JProperty("IM_GOODS_RECIPIENT", ""),
        //                      new JProperty("IM_UNLOAD_PT", ""),
        //                      new JProperty("IM_LINE_ID", "1"),
        //                      new JProperty("Pallets",
        //                      new JArray(new JObject(
        //                              new JProperty("WM_LINE_ID", "1"),
        //                              new JProperty("WM_QUANTITY", ""),
        //                              new JProperty("WM_UOM", ""),
        //                              new JProperty("WM_BATCH", ""),
        //                              new JProperty("WM_STG_TYPE", ""),
        //                              new JProperty("WM_STG_SECTION", ""),
        //                              new JProperty("WM_STG_BIN", ""),
        //                              new JProperty("WM_STG_UNIT_NUMBER", ""),
        //                              new JProperty("WM_STG_UNIT_TYPE", "")
        //      )))))));

        //        return jLabelObject;
        //    }
        //    return null;           
        //}     



    }
}
