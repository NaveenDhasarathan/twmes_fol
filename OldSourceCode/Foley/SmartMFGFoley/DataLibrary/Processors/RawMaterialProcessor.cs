﻿using DataLibrary.Common;
using DataLibrary.DataAccess.Abstract;
using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace DataLibrary.Processors
{
    public class RawMaterialProcessor : IRawMaterialProcessor
    {
        private readonly IReadWriteDataAccess _smartMfgDb;

        public RawMaterialProcessor(IReadWriteDataAccess smartMfgDb)
        {
            _smartMfgDb = smartMfgDb;
        }
        public List<PurchaseOrderLabelsModel> GetLabelsByRawMtlsId(long? rawMtlsId)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetLabelsByRawMtlsId, new { @IB_RAW_MTLS_ID = rawMtlsId });
        }
        public List<PurchaseOrderLabelsModel> GetLabelsByRawMtlsIdAll(long? rawMtlsId)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetLabelsByRawMtlsIdAll, new { @IB_RAW_MTLS_ID = rawMtlsId });
        }

      
        public int UpdateLabelRecords(PurchaseOrderLabelsModel labelModel, bool blnTotalBagChange)
        {
            int rtn = 0;
            using (var transScope = new TransactionScope())
            {
                rtn = UpdatePalletLabel(labelModel);
                PurchaseOrderLabelsModel updatedLabel = GetLabel(labelModel.Pol_Seq);
                if (blnTotalBagChange)
                {
                    rtn = DeleteLabel(labelModel.Label_Id, true);
                    if (labelModel.Tot_Bag_No != 0)
                    {
                        for (int i = 0; i < labelModel.Tot_Bag_No; i++)
                        {
                            rtn = _smartMfgDb.SaveData<PurchaseOrderLabelsModel>(
                                    Sql.PurchaseOrderLabelsSql.AddLabel,
                                    new PurchaseOrderLabelsModel
                                    {
                                        Ib_Raw_Mtls_Id = updatedLabel.Ib_Raw_Mtls_Id,
                                        Label_Id = updatedLabel.Label_Id,
                                        Lot_Nbr = updatedLabel.Lot_Nbr,
                                        Product_Received_Date = updatedLabel.Product_Received_Date,
                                        Tot_Bag_No = updatedLabel.Tot_Bag_No,
                                        CountryOrigin = updatedLabel.CountryOrigin,
                                        Amt_Received = updatedLabel.Amt_Received / updatedLabel.Tot_Bag_No,
                                        Vendor_Batch = updatedLabel.Vendor_Batch,
                                        Label_Print_Date = updatedLabel.Label_Print_Date,
                                        Uom = "KG",
                                        Bag_No = i + 1,
                                        Last_Modified_By = labelModel.Last_Modified_By,
                                        Last_Modified_Time = DateTime.Now,
                                        Batch_Id = updatedLabel.Batch_Id
                                    }, true
                                );
                        }
                    }
                }
                else
                {
                    if (labelModel.Tot_Bag_No != 0)
                    {
                        rtn = UpdateBagLabels(new PurchaseOrderLabelsModel
                        {
                            Label_Id = labelModel.Label_Id,
                            Lot_Nbr = labelModel.Lot_Nbr,
                            Tot_Bag_No = labelModel.Tot_Bag_No,
                            CountryOrigin = labelModel.CountryOrigin,
                            Amt_Received = labelModel.Amt_Received / labelModel.Tot_Bag_No,
                            Last_Modified_By = labelModel.Last_Modified_By,
                            Last_Modified_Time = DateTime.Now,
                            Batch_Id = updatedLabel.Batch_Id
                        });
                    }
                }

                transScope.Complete();
            }

            return rtn;
        }

        public PurchaseOrderLabelsModel GetLabel(long pol_seq)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetLabel, new { @Pol_Seq = pol_seq }).FirstOrDefault();
        }
        public PurchaseOrderLabelsModel GetLabelSTO(long pol_seq)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetLabelSTO, new { @Pol_Seq = pol_seq }).FirstOrDefault();
        }
        //public PurchaseOrderLabelsModel GetLabelsFromDeliveryAndReceiveRawMtls_ByLabelID(long pol_seq)
        //{
        //    return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetLabelsFromDeliveryAndReceiveRawMtls, new { @Pol_Seq = pol_seq }).FirstOrDefault();
        //}
        public PurchaseOrderLabelsModel GetStoLabelDetails(string pol_seq)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetLabelsFromDeliveryAndReceiveRawMtlsSingle, new { @Pol_Seq = pol_seq }).FirstOrDefault();
        }
        public List<PurchaseOrderLabelsModel> GetLabelForSTO(string[] pol_seq)
        {
            return _smartMfgDb.LoadData<List<PurchaseOrderLabelsModel>>(Sql.PurchaseOrderLabelsSql.GetLabelForSTO, new { @Pol_Seq = pol_seq }).FirstOrDefault();
        }

        public List<PurchaseOrderLabelsModel> GetAllStoLabelDetails(string[] pol_seq)
        {
            return _smartMfgDb.LoadData<List<PurchaseOrderLabelsModel>>(Sql.PurchaseOrderLabelsSql.GetAllStoLabelDetails, new { @Pol_Seq = pol_seq }).FirstOrDefault();
        }


        public PurchaseOrderLabelsModel GetLabelByLabelId_MtlId(string labelId, long rawMtlsId)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetLabelByLabelId_MtlId, new { @label_Id = labelId, @Ib_RAW_MTLS_ID = rawMtlsId }).FirstOrDefault();
        }

        

        public int UpdateBagLabels(PurchaseOrderLabelsModel labelModel)
        {
            return _smartMfgDb.SaveData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.UpdateBagLabels, labelModel);
        }

        public int UpdateLabelScanDate(PurchaseOrderLabelsModel labelModel)
        {
            return _smartMfgDb.SaveData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.UpdateLabelScanDate, labelModel);
        }

        public int UpdateLabelScanDateForPO(string[] labelModel, DateTime labelScanDate)
        {
            return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.UpdateLabelScanDateForPO, new { @Label_Id = labelModel, @Label_Scan_Date = labelScanDate });
        }

        public int UpdateLabelPrintDate(long[] pol_seq, DateTime print_time)
        {
            return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.UpdateLabelPrintDate, new { @Pol_Seq = pol_seq, @Label_Print_Date = print_time });
        }
        public int UpdateLabelPrintDate_ByPOS(long[] pol_seq, DateTime print_time)
        {
            return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.UpdateLabelPrintDateByPOS, new { @Pol_Seq = pol_seq, @Label_Print_Date = print_time });
        }
        public int UpdateLabelPrintDatePO(long[] pol_seq, DateTime print_time)
        {
            return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.UpdateLabelPrintDatePO, new { @Pol_Seq = pol_seq, @Label_Print_Date = print_time });
        }
        public int UpdateLabelPrintDateForPO(string[] pol_seq, DateTime print_time)
        {
            return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.UpdateLabelPrintDate, new { @Pol_Seq = pol_seq, @Label_Print_Date = print_time });
        }
        public int DeleteLabel(string Label_Id, bool blnDeletBagLabel)
        {
            if (!blnDeletBagLabel)
                return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.DeleteLabel, new { Label_Id = Label_Id });
            else
                return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.DeleteBagLabel, new { Label_Id = Label_Id });

        }
        public int DeleteLabelForSTO(string Label_Id)
        {
            return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.DeleteLabelForSTO, new { Label_Id = Label_Id });
        }

        public PurchaseOrderLabelsModel GetLabelByLabelId(string labelId)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetLabelByLabelId, new { @label_Id = labelId }).FirstOrDefault();
        }
        //public List<PurchaseOrderLabelsModel> GetLabelByLabelIdNew(string[] labelId)
        //{
        //    return _smartMfgDb.LoadData<List<PurchaseOrderLabelsModel>>(Sql.PurchaseOrderLabelsSql.GetLabelByLabelId, new { @label_Id = labelId }).FirstOrDefault();
        //}
        public List<PurchaseOrderLabelsModel> GetLabelByLabelIdPO(string[] labelId)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetLabelByLabelIdPO, new { @label_Id = labelId });
        }

        public IEnumerable<LabelInfoModel> GetLabelInformation(string[] labelIds)
        {
            return _smartMfgDb.LoadData<LabelInfoModel>(Sql.PurchaseOrderLabelsSql.GetLabelInfo, new { @labelIds = labelIds });
        }
        public IEnumerable<LabelInfoModel> GetLabelInformationForSTO(string[] labelIds)
        {
            return _smartMfgDb.LoadData<LabelInfoModel>(Sql.PurchaseOrderLabelsSql.GetLabelInfoForSTO, new { @labelIds = labelIds });
        }

        public List<LabelInfoModel> GetLabelInfoBySeqId(long[] Ids)
        {
            return _smartMfgDb.LoadData<LabelInfoModel>(Sql.PurchaseOrderLabelsSql.GetLabelInfoBySeqId, new { @Ids = Ids });
        }

        public BatchLabelModel GetLabelMaterialInfoBySeqId(long Pol_Seq)
        {
            return _smartMfgDb.LoadData<BatchLabelModel>(Sql.PurchaseOrderLabelsSql.GetLabelMaterialInfoBySeqId, new { @Pol_Seq = Pol_Seq }).FirstOrDefault();
        }

        public string GetBatchId(long PO_NBR, int MATL_NBR, string BATCH_TYPE, string USER, string VendorBatch, long Ib_Raw_Mtls_Id,string POSupplierNo)
        {

            var labelBatch = _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetCurrentBatchId, new { @CurrentDate = DateTime.Now.Day, @CurrentMonth = DateTime.Now.Month, @CurrentYear = DateTime.Now.Year, @VendorBatch = VendorBatch, @Ib_Raw_Mtls_Id = MATL_NBR, @POSupplierNo= POSupplierNo }).FirstOrDefault();
            return (labelBatch?.Batch_Id == null) ? _smartMfgDb.LoadData<string>(Sql.PurchaseOrderLabelsSql.GetBatchId, new { PONBR = PO_NBR, MATNBR = MATL_NBR, BATCH_TYPE = "R", USER = USER }).FirstOrDefault() : labelBatch.Batch_Id;
        }
        
        public string GetBatchIdSTO(long PO_NBR, int MATL_NBR, string BATCH_TYPE, string USER, string VendorBatch, long Ib_Raw_Mtls_Id)
        {

            var labelBatch = _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.GetCurrentBatchIdSTODelivery, new { @VendorBatch = VendorBatch, @Ib_Raw_Mtls_Id = Ib_Raw_Mtls_Id }).FirstOrDefault();
            return (labelBatch?.Batch_Id == null) ? _smartMfgDb.LoadData<string>(Sql.PurchaseOrderLabelsSql.GetBatchId, new { PONBR = PO_NBR, MATNBR = MATL_NBR, BATCH_TYPE = "R", USER = USER }).FirstOrDefault() : labelBatch.Batch_Id;
        }

        public LabelInfoModel GetSingleLabelInfo(string labelId, int bagNo)
        {
            return _smartMfgDb.LoadData<LabelInfoModel>(Sql.PurchaseOrderLabelsSql.GetSingleLabelInfo, new { labelId = labelId, bagNo = bagNo }).FirstOrDefault();
        }

        public int UpdatePalletLabel(PurchaseOrderLabelsModel labelModel)
        {
            return _smartMfgDb.SaveData<PurchaseOrderLabelsModel>(Sql.PurchaseOrderLabelsSql.UpdatePalletLabel, labelModel);
        }

        public int UpdateBatchSentDate(string Batch_Id, DateTime sent_time)
        {
            return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.UpdateBatchSentDate, new { @Batch_Id = Batch_Id, @Batch_Sent_Time = sent_time });
        }

        public List<STODeliveryDetailsModel> GetSTOByDeliveryNoteSTO(long dNote, int Item_Nbr)
        {
            return _smartMfgDb.LoadData<STODeliveryDetailsModel>(Sql.InternalSTOSql.GetSTOByDeliveryNoteSTO, new { @Delivery_Nbr = dNote, @Item_Nbr = Item_Nbr });
        }
        public List<STODeliveryDetailsModel> GetSTOByDeliveryNoteSTODetail(long dNote, int Item_Nbr)
        {
            return _smartMfgDb.LoadData<STODeliveryDetailsModel>(Sql.InternalSTOSql.GetSTOByDeliveryNoteSTODetail, new { @Delivery_Nbr = dNote, @Item_Nbr = Item_Nbr });
        }
        public List<PurchaseOrderLabelsModel> GetSTOByDeliveryNote(long dNote, int Item_Nbr)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.InternalSTOSql.GetSTOByDeliveryNote, new { @Delivery_Nbr = dNote, @Item_Nbr = Item_Nbr });
        }
        public List<PurchaseOrderLabelsModel> GetSTOByDeliveryNote_SAPSend(long dNote, long Item_Nbr)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.InternalSTOSql.GetSTOByDeliveryNote_SAPSend, new { @Delivery_Nbr = dNote, @Item_Nbr = Item_Nbr });
        }

        //public List<STODeliveryDetailsModel> GetSTOByDeliveryNoteSTOData(string dNote, string DocNum)
        //{
        //    return _smartMfgDb.LoadData<STODeliveryDetailsModel>(Sql.InternalSTOSql.GetSTOByDeliveryNoteforScanOldData, new { @Delivery_Nbr = dNote, @DocNum = DocNum });
        //}

        //public List<PurchaseOrderLabelsModel> GetSTOByDeliveryNoteforScan(string dNote)
        //{
        //    return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.InternalSTOSql.GetSTOByDeliveryNoteforScan, new { @Delivery_Nbr = dNote });
        //}
        public List<PurchaseOrderLabelsModel> GetSTOByDeliveryNoteforScanForPO(string[] dNote)
        {
            return _smartMfgDb.LoadData<PurchaseOrderLabelsModel>(Sql.InternalSTOSql.GetSTOByDeliveryNoteforScanForPO, new { @Delivery_Nbr = dNote });
        }

        public int UpdateSTODeliveryDetail(string[] Delivery_Nbr, DateTime Date_Received, DateTime Date_Updated, string Received_By)
        {
            return _smartMfgDb.SaveData(Sql.InternalSTOSql.UpdateSTODeliveryDetail, new { @Delivery_Nbr = Delivery_Nbr, @Date_Received = Date_Received, @Date_Updated = Date_Updated, @Received_By = Received_By });
        }

        //public STODeliveryDetailsModel GetSTOByStorageNumber(string Storage_Unit_Nbr)
        //{
        //    return _smartMfgDb.LoadData<STODeliveryDetailsModel>(Sql.InternalSTOSql.GetSTOByStorageNumber, new { @Storage_Unit_Nbr = Storage_Unit_Nbr }).FirstOrDefault();
        //}



        //public int UpdateSapDate(string[] StorageArr, DateTime Date_Sent_Sap)
        //{
        //    return _smartMfgDb.SaveData(Sql.InternalSTOSql.UpdateSAPDate, new { @StorageArr = StorageArr, @Date_Sent_Sap = Date_Sent_Sap });
        //}
        public int UpdateSTOSapDate(long DeliveryNo, DateTime Date_Sent_Sap, string Sap_Sent_By)
        {
            return _smartMfgDb.SaveData(Sql.InternalSTOSql.UpdateSTOSAPDate, new { @Delivery_NBR = DeliveryNo, @Date_Sent_Sap = Date_Sent_Sap, @Sap_Sent_By = Sap_Sent_By });
        }
        public int UpdateSentSapDate(long Id, DateTime Sent_Sap_Date, string Sent_Sap_By)
        {
            return _smartMfgDb.SaveData(Sql.PurchaseOrderLabelsSql.UpdateSentSAPDate, new { @Ib_Raw_Mtls_Id = Id, @Sent_Sap_By = Sent_Sap_By, @Sent_Sap_Date = Sent_Sap_Date });
        }

        public int AddLabel(PurchaseOrderLabelsModel[] lst, bool Packaging)
        {
            if (Packaging)
                return _smartMfgDb.SaveDataInTransaction(Sql.PurchaseOrderLabelsSql.AddLabelForPackagingMat, lst);
            else
                return _smartMfgDb.SaveDataInTransaction(Sql.PurchaseOrderLabelsSql.AddLabel, lst);
        }

        public int UpdateSTOBatchID(STODeliveryDetailsModel sto)
        {
            return _smartMfgDb.SaveData<STODeliveryDetailsModel>(Sql.InternalSTOSql.UpdateSTOBatchID, sto, true);
        }
        //public int SaveSTO(STODeliveryDetailsModel sto)
        //{
        //    return _smartMfgDb.SaveData<STODeliveryDetailsModel>(Sql.InternalSTOSql.SaveSTO, sto, true);
        //}
        public int SaveSTODetail(STODeliveryDetailsModel sto)
        {
            return _smartMfgDb.SaveData<STODeliveryDetailsModel>(Sql.InternalSTOSql.SaveSTODetail, sto, true);
        }
        public int UpdateSTODetail(STODeliveryDetailsUpdateModel sto)
        {
            return _smartMfgDb.SaveData<STODeliveryDetailsUpdateModel>(Sql.InternalSTOSql.UpdateSTODetail, sto, true);
        }


    }
}
