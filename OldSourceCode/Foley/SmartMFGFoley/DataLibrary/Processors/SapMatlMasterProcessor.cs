﻿using DataLibrary.DataAccess.Abstract;
using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors
{
    public class SapMatlMasterProcessor : ISapMatlMasterProcessor
    {
        private readonly IReadWriteDataAccess _smartMfgDb;

        public SapMatlMasterProcessor(IReadWriteDataAccess smartMfgDb)
        {
            _smartMfgDb = smartMfgDb;
        }

        public List<SAPMatlMasterModel> GetSapMaterialByMATLDesc(string matDesc)
        {
            return _smartMfgDb.LoadData<SAPMatlMasterModel>(Sql.SapMatlMasterSql.GetSapMaterialByMATLDesc, new { @SAP_MATL_DESCR = matDesc });
        }

        public SAPMatlMasterModel GetSapMaterialByMATLNBR(string nbr)
        {
            return _smartMfgDb.LoadData<SAPMatlMasterModel>(Sql.SapMatlMasterSql.GetSapMaterialByMATLNBR, new { @SapMatl_NBR = nbr }).FirstOrDefault();
        }
    }
}
