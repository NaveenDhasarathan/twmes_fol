﻿using DataLibrary.DataAccess.Abstract;
using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors
{
    public class ProductionScheduleProcessor : IProcessOrder
    {
        private readonly IReadWriteDataAccess _smartMfgDb;

        public ProductionScheduleProcessor(IReadWriteDataAccess smartMfgDb)
        {
            _smartMfgDb = smartMfgDb;
        }
        public List<ProductionScheduleModel> LoadMaterialList()
        {
            return _smartMfgDb.LoadData<ProductionScheduleModel>(Sql.ProductionScheduleSql.LoadMaterialList);
        }

        public List<ProductionScheduleModel> LoadMaterials()
        {
            return _smartMfgDb.LoadData<ProductionScheduleModel>(Sql.ProductionScheduleSql.LoadMaterialList);
        }

        public List<ProductionScheduleModel> LoadProductionSchedule()
        {
            return _smartMfgDb.LoadData<ProductionScheduleModel>(Sql.ProductionScheduleSql.LoadProductionSchedule);
        }

        public ProductionScheduleModel LoadPSModel(long? po)
        {
            return _smartMfgDb.LoadData<ProductionScheduleModel>(Sql.ProductionScheduleSql.LoadPSModel, new { @po = po }).FirstOrDefault();
        }

        public List<ProductionScheduleModel> LoadResources()
        {
            return _smartMfgDb.LoadData<ProductionScheduleModel>(Sql.ProductionScheduleSql.LoadResources);
        }
    }
}
