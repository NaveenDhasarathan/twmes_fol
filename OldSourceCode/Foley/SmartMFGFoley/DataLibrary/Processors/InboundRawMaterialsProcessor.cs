﻿using DataLibrary.DataAccess.Abstract;
using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using System.Collections.Generic;
using System.Linq;

namespace DataLibrary.Processors
{
    public class InboundRawMaterialsProcessor : IInboundRawMaterialsProcessor
    {
        private readonly IReadWriteDataAccess _smartMfgDb;

        public InboundRawMaterialsProcessor(IReadWriteDataAccess smartMfgDb)
        {
            _smartMfgDb = smartMfgDb;
        }

        public List<POMaterialModel> GetRawMaterialByDocType(string docType)
        {
            return _smartMfgDb.LoadData<POMaterialModel>(Sql.InBoundRawMaterialsSql.GetRawMaterialByDocType, new { DocumentType = @docType });

        }

        public List<POMaterialModel> GetAllSTO()
        {
            return _smartMfgDb.LoadData<POMaterialModel>(Sql.InBoundRawMaterialsSql.GetAllSTO);
        }

        public List<POMaterialModel> GetInternalSTO()
        {
            return _smartMfgDb.LoadData<POMaterialModel>(Sql.InBoundRawMaterialsSql.GetInternalSTO);
        }
        public List<InboundRawMaterialsModel> GetExternalPOByNBR(long docNbr)
        {
            return _smartMfgDb.LoadData<InboundRawMaterialsModel>(Sql.InBoundRawMaterialsSql.GetExternalPOByNBR, new { Document_NBR = docNbr });
        }

        public RawMaterialDetailModel GetRawMaterialByRawMatlId(long rawMatlId)
        {
            return _smartMfgDb.LoadData<RawMaterialDetailModel>(Sql.InBoundRawMaterialsSql.GetRawMaterialByRawMatlId, new { @IB_RAW_MTLS_ID = rawMatlId }).FirstOrDefault();
        }
        public RawMaterialDetailModel GetRawMaterialByRawMatlNo(long rawMatlId)
        {
            return _smartMfgDb.LoadData<RawMaterialDetailModel>(Sql.InBoundRawMaterialsSql.GetRawMaterialByRawMatlId, new { @IB_RAW_MTLS_ID = rawMatlId }).FirstOrDefault();
        }

        public List<RawMaterialDetailModel> GetRawMaterialByDeliveryNo(string DeliveryNo)
        {
            return _smartMfgDb.LoadData<RawMaterialDetailModel>(Sql.InBoundRawMaterialsSql.GetRawMaterialByDeliveryNo, new { @Delivery_No = DeliveryNo });
        }

        public POMaterialModel GetIRMInfoForSAP(long rawMatlId)
        {
            return _smartMfgDb.LoadData<POMaterialModel>(Sql.InBoundRawMaterialsSql.GetIRMInfoForSAP, new { Ib_Raw_Mtls_Id = rawMatlId }).FirstOrDefault();
        }
        public POMaterialModel GetIRMInfoForSAPPOMaterials(long DeliveryNo)
        {
            return _smartMfgDb.LoadData<POMaterialModel>(Sql.InBoundRawMaterialsSql.GetIRMInfoForSAPPOMaterials, new { @Delivery_No = DeliveryNo }).FirstOrDefault();
        }

        public List<STODeliveryDetailsModel> GetSTOByMtrl_Nbr(string Mtrl_Nbr)
        {
            return _smartMfgDb.LoadData<STODeliveryDetailsModel>(Sql.InternalSTOSql.GetSTOByMtrl_Nbr, new { @Mtrl_Nbr = Mtrl_Nbr });

        }
    }
}
