﻿using DataLibrary.Models;
using System.Collections.Generic;

namespace DataLibrary.Processors.Abstract
{
    public interface IRunSheet
    {
        public RunSheetModel GetOldRunSheet(int Material_Num, string Ext_Nbr);
    
        public List<BOMModel> GetAltBomScanned(long PO);

        public List<BOMModel> GetBOMScanned(long PO);

        public IEnumerable<BOMModel> GetBOMPOScanned(int Type, long PO, bool altCheck);
        public int AddMixture(Mixture mix);
    }
}
