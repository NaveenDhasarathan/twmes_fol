﻿using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Abstract
{
    public interface IProcessOrder
    {
        public List<ProductionScheduleModel> LoadProductionSchedule();
        public List<ProductionScheduleModel> LoadResources();
        public List<ProductionScheduleModel> LoadMaterials();
        public List<ProductionScheduleModel> LoadMaterialList();
        public ProductionScheduleModel LoadPSModel(long? po);
       
    }
}
