﻿using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Abstract
{
    public interface IRawMaterialProcessor
    {
        List<PurchaseOrderLabelsModel> GetLabelsByRawMtlsId(long? rawMtlsId);
        List<PurchaseOrderLabelsModel> GetLabelsByRawMtlsIdAll(long? rawMtlsId);
        PurchaseOrderLabelsModel GetLabelByLabelId(string labelId);
        List<PurchaseOrderLabelsModel> GetLabelByLabelIdPO(string[] labelId);

        PurchaseOrderLabelsModel GetLabel(long pol_seq);
        PurchaseOrderLabelsModel GetLabelSTO(long pol_seq);
        PurchaseOrderLabelsModel GetStoLabelDetails(string pol_seq);
        List<PurchaseOrderLabelsModel> GetLabelForSTO(string[] pol_seq);
        List<PurchaseOrderLabelsModel> GetAllStoLabelDetails(string[] pol_seq);
        int UpdateBagLabels(PurchaseOrderLabelsModel labelModel);
        int UpdateLabelRecords(PurchaseOrderLabelsModel labelModel, bool blnTotalBagChange);
        int UpdatePalletLabel(PurchaseOrderLabelsModel labelModel);
        int UpdateLabelScanDate(PurchaseOrderLabelsModel labelModel);
        int UpdateLabelScanDateForPO(string[] labelModel, DateTime scanDate);
        int DeleteLabel(string Label_Id, bool blnDeleteBagLabel);
        int DeleteLabelForSTO(string Label_Id);
        IEnumerable<LabelInfoModel> GetLabelInformation(string[] labelIds);
        IEnumerable<LabelInfoModel> GetLabelInformationForSTO(string[] labelIds);
        LabelInfoModel GetSingleLabelInfo(string labelId, int bagNo);
        List<LabelInfoModel> GetLabelInfoBySeqId(long[] Ids);
        public string GetBatchId(long PO_NBR, int MATL_NBR, string BATCH_TYPE, string USER, string VendorBatch, long Ib_Raw_Mtls_Id,string POSupplierNo);
        public string GetBatchIdSTO(long PO_NBR, int MATL_NBR, string BATCH_TYPE, string USER, string VendorBatch, long Ib_Raw_Mtls_Id);
        public BatchLabelModel GetLabelMaterialInfoBySeqId(long Pol_Seq);
        public int UpdateLabelPrintDate(long[] pol_seq, DateTime print_time);
        public int UpdateLabelPrintDatePO(long[] pol_seq, DateTime print_time);
        public int UpdateLabelPrintDateForPO(string[] pol_seq, DateTime print_time);
        public int UpdateBatchSentDate(string Batch_Id, DateTime sent_time);
        public List<STODeliveryDetailsModel> GetSTOByDeliveryNoteSTO(long dNote, int Item_Nbr);
        public List<STODeliveryDetailsModel> GetSTOByDeliveryNoteSTODetail(long dNote, int Item_Nbr);
        public List<PurchaseOrderLabelsModel> GetSTOByDeliveryNote(long dNote, int Item_Nbr);
        public List<PurchaseOrderLabelsModel> GetSTOByDeliveryNote_SAPSend(long dNote, long Item_Nbr);


        public List<PurchaseOrderLabelsModel> GetSTOByDeliveryNoteforScanForPO(string[] dNote);

        public int UpdateSTODeliveryDetail(string[] Delivery_Nbr, DateTime Date_Received, DateTime Date_Updated, string Received_By);

        public int UpdateSTOSapDate(long DeliveryNo, DateTime Date_Sent_Sap, string Sent_Sap_By);
        public int AddLabel(PurchaseOrderLabelsModel[] lst, bool Packaging);
        public int UpdateSTOBatchID(STODeliveryDetailsModel sto);
        public int SaveSTODetail(STODeliveryDetailsModel sto);
        public int UpdateSTODetail(STODeliveryDetailsUpdateModel sto);
        public int UpdateSentSapDate(long Id, DateTime Sent_Sap_Date, string Sent_Sap_By);
    }
}
