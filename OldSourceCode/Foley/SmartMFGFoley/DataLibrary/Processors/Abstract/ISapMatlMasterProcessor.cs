﻿using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Abstract
{
    public interface ISapMatlMasterProcessor
    {
        SAPMatlMasterModel GetSapMaterialByMATLNBR(string nbr);
        List<SAPMatlMasterModel> GetSapMaterialByMATLDesc(string matDesc);
    }
}
