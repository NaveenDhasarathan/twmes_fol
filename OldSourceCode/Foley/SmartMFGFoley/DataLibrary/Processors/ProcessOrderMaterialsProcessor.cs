﻿using DataLibrary.DataAccess.Abstract;
using DataLibrary.Models;
using DataLibrary.Processors.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors
{
    public class ProcessOrderMaterialsProcessor : IProcessOrderMaterialsProcessor
    {
        private readonly IReadWriteDataAccess _smartMfgDb;

        public ProcessOrderMaterialsProcessor(IReadWriteDataAccess smartMfgDb)
        {
            _smartMfgDb = smartMfgDb;
        }
        public List<BOMModel> LoadALTBOMScanned()
        {
            return _smartMfgDb.LoadData<BOMModel>(Sql.ProcessOrderMaterialsSql.LoadALTBOMScanned);
        }

        public List<POAltMaterialDescModel> LoadAltMaterial(long po)
        {
            return _smartMfgDb.LoadData<POAltMaterialDescModel>(Sql.ProcessOrderMaterialsSql.GetAltMaterials, new { PO_NBR = po });
        }

        public List<BOMModel> LoadBOM(long? PO)
        {
            return _smartMfgDb.LoadData<BOMModel>(Sql.ProcessOrderMaterialsSql.LoadBOM, new { PO = PO });
        }

        public List<BOMModel> LoadBOMScanned()
        {
            throw new NotImplementedException();
        }

        public List<ProductionScheduleModel> LoadDetails()
        {
            throw new NotImplementedException();
        }

        public List<BOMModel> LoadEquipmentActual()
        {
            throw new NotImplementedException();
        }

        public List<BOMModel> LoadEquipmentSetup()
        {
            throw new NotImplementedException();
        }

        public int AddAlternateMaterials(POAlternateBOM altBOM)
        {
            return _smartMfgDb.SaveData<POAlternateBOM>(
                        Sql.ProcessOrderMaterialsSql.AddAltMaterials,
                        new POAlternateBOM
                        {
                            Po_Nbr = altBOM.Po_Nbr,
                            Substitution_Type = altBOM.Substitution_Type,
                            Org_Matl_Nbr = altBOM.Org_Matl_Nbr,
                            Org_Qty = altBOM.Org_Qty,
                            Subst_Matl_Nbr = altBOM.Subst_Matl_Nbr,
                            Subst_Qty = altBOM.Subst_Qty
                        });
        }

        public int UpdateAltMaterials(long Po_Alt_Bom_Seq, decimal SubQty)
        {
            return _smartMfgDb.SaveData(Sql.ProcessOrderMaterialsSql.UpdateAltMaterials, new { @Po_Alt_Bom_Seq= Po_Alt_Bom_Seq, @SUBST_QTY= SubQty } );
        }

        public int DeleteAltMaterials(long Po_Alt_Bom_Seq)
        {
            return _smartMfgDb.SaveData(Sql.ProcessOrderMaterialsSql.DeleteAltMaterials, new { @Po_Alt_Bom_Seq = Po_Alt_Bom_Seq });
        }

        public POAlternateBOM GetAltMaterialForUpdate(long Po_Alt_Bom_Seq)
        {
            return _smartMfgDb.LoadData<POAlternateBOM>(Sql.ProcessOrderMaterialsSql.GetAltMaterialsForUpdate, new POAlternateBOM { @Po_Alt_Bom_Seq= Po_Alt_Bom_Seq }).FirstOrDefault();
        }

        public List<SapStorageBinInventory> GetInventoryByMatNbr(string matNbr)
        {
            return _smartMfgDb.LoadData<SapStorageBinInventory>(Sql.ProcessOrderMaterialsSql.GetInventoryByMatNbr, new { @Matl_Nbr = matNbr });
        }

        public int AddRawMaterialsToStaging(RawMaterialStagingModel rmModel)
        {
            return _smartMfgDb.SaveData<RawMaterialStagingModel>(
                        Sql.ProcessOrderMaterialsSql.AddRawMaterialsToStaging,
                        new RawMaterialStagingModel
                        {
                            Po_Nbr = rmModel.Po_Nbr,
                            Sap_Matl_Nbr = rmModel.Sap_Matl_Nbr,
                            Feeder_No = rmModel.Feeder_No,
                            Scanned_Amt = rmModel.Scanned_Amt,
                            Scanned_Uom = rmModel.Scanned_Uom,
                            Scanned_Date_Time = rmModel.Scanned_Date_Time,
                            Scanned_By = rmModel.Scanned_By,                          
                            Label_Id = rmModel.Label_Id
                            //Consumed_Amt = rmModel.Consumed_Amt
                        });
        }

    }
}
