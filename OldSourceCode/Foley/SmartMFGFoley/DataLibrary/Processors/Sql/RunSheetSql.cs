﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Sql
{
    public class RunSheetSql
    {
        public static string GetOldRunSheet = @"SELECT  * FROM RUNSHEET WHERE Material_Num = @Material_Num AND Ext_Nbr=@Ext_Nbr;";

        public static string GetBOMScanned = @"SELECT  POM.PO_NBR,
		                                        POM.SAP_MATL_NBR,
		                                        SMM.SAP_DESCR_SIMPLE MATERIAL_DESCRIPTION,
		                                        POM.QUANTITY_REQUIRED,
		                                        POM.QUANTITY_COMPLETED,
												LTRIM(RTRIM(POM.UOM)) UOM,
		                                        POM.MATL_TYPE,		                            
		                                        0 AS SCANNED_AMT,
		                                        0 AS FEEDER_NO
		                                        FROM PROCESS_ORDER_MATERIALS POM		
		                                        JOIN SAP_MATL_MASTER SMM ON POM.SAP_MATL_NBR = SMM.SAP_MATL_NBR		
		                                        WHERE POM.PO_NBR = @PO
		                                        AND POM.MATL_TYPE IN ('C','F') AND POM.UOM IN ('LB', 'KG')              
												UNION			
												SELECT  RMS.PO_NBR,
		                                        RMS.SAP_MATL_NBR,
		                                        SMM.SAP_DESCR_SIMPLE MATERIAL_DESCRIPTION,
		                                        0 AS QUANTITY_REQUIRED,
		                                        0 AS QUANTITY_COMPLETED,
		                                        '' AS UOM,
		                                        '' AS MATL_TYPE,		                            
		                                        SUM(CASE WHEN RMS.ENTERED_AMT IS NULL THEN RMS.SCANNED_AMT ELSE RMS.ENTERED_AMT END) AS [SCANNED_AMT],
		                                        RMS.FEEDER_NO
		                                        FROM RAW_MATERIAL_STAGING RMS		
		                                        JOIN SAP_MATL_MASTER SMM ON RMS.SAP_MATL_NBR = SMM.SAP_MATL_NBR
		                                        WHERE RMS.PO_NBR = @PO
		                                        GROUP BY RMS.PO_NBR,
				                                        RMS.SAP_MATL_NBR,
				                                        RMS.FEEDER_NO,
				                                        SMM.SAP_DESCR_SIMPLE;";

		public static string GetAltBomScanned = @"SELECT  ALT.PO_NBR,
												ALT.SUBST_MATL_NBR SAP_MATL_NBR,
												SMM.SAP_DESCR_SIMPLE MATERIAL_DESCRIPTION,
												ALT.SUBST_QTY QUANTITY_REQUIRED,
												0 AS QUANTITY_COMPLETED,
												'' AS UOM,
												'' AS MATL_TYPE,	                            
												0 AS SCANNED_AMT,
												0 AS FEEDER_NO
												FROM PO_ALTERNATE_BOM ALT		
												JOIN SAP_MATL_MASTER SMM ON ALT.SUBST_MATL_NBR = SMM.SAP_MATL_NBR		
												WHERE ALT.PO_NBR = @PO	
										UNION	
										SELECT  RMS.PO_NBR,
												RMS.SAP_MATL_NBR,
												SMM.SAP_DESCR_SIMPLE MATERIAL_DESCRIPTION,
												0 AS QUANTITY_REQUIRED,
												0 AS QUANTITY_COMPLETED,
												'' AS UOM,
												'' AS MATL_TYPE,		                            
												SUM(CASE WHEN RMS.ENTERED_AMT IS NULL THEN RMS.SCANNED_AMT ELSE RMS.ENTERED_AMT END) AS [SCANNED_AMT],
												RMS.FEEDER_NO
												FROM RAW_MATERIAL_STAGING RMS		
												JOIN SAP_MATL_MASTER SMM ON RMS.SAP_MATL_NBR = SMM.SAP_MATL_NBR
												WHERE RMS.PO_NBR = @PO
												GROUP BY RMS.PO_NBR,
														RMS.SAP_MATL_NBR,
														RMS.FEEDER_NO,
														SMM.SAP_DESCR_SIMPLE;";
	}
}
