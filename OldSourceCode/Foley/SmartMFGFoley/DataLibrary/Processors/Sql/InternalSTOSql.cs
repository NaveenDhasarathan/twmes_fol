﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Sql
{
    public class InternalSTOSql
    {
        public static string GetSTOByDeliveryNoteSTO = @"
                SELECT distinct i.DELIVERY_NBR,i.ITEM_NBR,i.MATL_NBR,s.SAP_MATL_DESCR,R.IB_RAW_MTLS_ID,i.STO_DOC_ITEM_NBR,i.batch_nbr
                FROM [dbo].[STO_DELIVERY_DETAILS] i 
                INNER JOIN INBOUND_RAW_MATERIALS R ON R.DELIVERY_NO = i.DELIVERY_NBR AND R.item_nbr=i.STO_DOC_ITEM_NBR
                inner join SAP_MATL_MASTER s on s.SAP_MATL_NBR=i.MATL_NBR 
                where i.DELIVERY_NBR=@Delivery_Nbr and (i.Item_Nbr= @Item_Nbr or STO_DOC_ITEM_NBR=@Item_Nbr)";

        public static string GetSTOByDeliveryNoteSTODetail = @"
                SELECT i.*,s.SAP_MATL_DESCR,R.IB_RAW_MTLS_ID 
                FROM [dbo].[STO_DELIVERY_DETAILS] i 
                INNER JOIN INBOUND_RAW_MATERIALS R ON R.DELIVERY_NO = i.DELIVERY_NBR AND R.item_nbr=i.STO_DOC_ITEM_NBR
                inner join SAP_MATL_MASTER s on s.SAP_MATL_NBR=i.MATL_NBR 
                where i.DELIVERY_NBR=@Delivery_Nbr
				and (i.Item_Nbr= @Item_Nbr or STO_DOC_ITEM_NBR=@Item_Nbr)";

        public static string GetSTOByDeliveryNote = @"
                SELECT 
                    r.*,i.ITEM_NBR,i.MATL_NBR,r.ItemNo
                FROM [dbo].[RECEIVED_RAW_MATERIALS] r 
                inner join INBOUND_RAW_MATERIALS i on i.IB_RAW_MTLS_ID=r.IB_RAW_MTLS_ID where i.DELIVERY_NO=@Delivery_Nbr and i.item_nbr=@Item_Nbr  order by Label_ID";

        public static string GetSTOByDeliveryNote_SAPSend = @"
                SELECT 
                     r.Ib_Raw_Mtls_Id,POL_SEQ,LABEL_ID,Product_Received_Date,Label_Print_Date,Amt_Received,
                	 r.Lot_Nbr,Label_Scan_Date,Tot_Bag_No,Vendor_Batch,r.batch_id,CountryOrigin
                ,i.item_nbr,i.matl_nbr,r.ItemNo,r.uom,r.itemno MatlItemNbr
                FROM [dbo].[RECEIVED_RAW_MATERIALS] r 
                inner join INBOUND_RAW_MATERIALS i on i.IB_RAW_MTLS_ID=r.IB_RAW_MTLS_ID 
                where i.DELIVERY_NO=@Delivery_Nbr  and i.item_nbr=@Item_Nbr 
                UNION ALL
                SELECT 
	                i.Ib_Raw_Mtls_Id,s.STO_DD_SEQ POL_SEQ,s.STORAGE_UNIT_NBR LABEL_ID,s.DELIVERY_DATE Product_Received_Date,s.DELIVERY_DATE Label_Print_Date,s.AMT_RECEIVED Amt_Received,
	                s.BILL_OF_LADING Lot_Nbr,s.SCAN_DATE Label_Scan_Date,1 Tot_Bag_No,s.BATCH_NBR Vendor_Batch,s.BATCH_ID,s.BILL_OF_LADING CountryOrigin
                ,i.item_nbr,i.matl_nbr,s.STO_DOC_ITEM_NBR ItemNo,s.uom,s.ITEM_NBR MatlItemNbr
                FROM STO_DELIVERY_DETAILS s
				inner join INBOUND_RAW_MATERIALS i on i.delivery_no=s.delivery_nbr
                where i.DELIVERY_NO=@Delivery_Nbr and (i.Item_Nbr= @Item_Nbr or STO_DOC_ITEM_NBR=@Item_Nbr) --and STORAGE_UNIT_NBR !='INV MGT' and  STORAGE_UNIT_NBR !='NEEDLABEL'  
                ";

        public static string GetSTOByDeliveryNoteforScanOldData = @"
                SELECT * FROM [dbo].[STO_DELIVERY_DETAILS] where (DELIVERY_NBR=@Delivery_Nbr or STORAGE_UNIT_NBR=@Delivery_Nbr or STO_DOC_NBR = @DocNum) and  STORAGE_UNIT_NBR!='NEEDLABEL' order by Item_Nbr";

        public static string GetSTOByDeliveryNoteforScan = @"
                SELECT r.* FROM [dbo].[RECEIVED_RAW_MATERIALS] r inner join INBOUND_RAW_MATERIALS i on i.IB_RAW_MTLS_ID=r.IB_RAW_MTLS_ID where i.DELIVERY_NO=@Delivery_Nbr  or r.label_id=@Delivery_Nbr  order by Label_ID";

        public static string GetSTOByDeliveryNoteforScanForPO = @"
                SELECT 
                     r.Ib_Raw_Mtls_Id,POL_SEQ,LABEL_ID,Product_Received_Date,Label_Print_Date,Amt_Received,
                	 r.Lot_Nbr,Label_Scan_Date,Tot_Bag_No,Vendor_Batch,CountryOrigin,i.item_nbr,i.matl_nbr
                FROM [dbo].[RECEIVED_RAW_MATERIALS] r 
                inner join INBOUND_RAW_MATERIALS i on i.IB_RAW_MTLS_ID=r.IB_RAW_MTLS_ID 
                where i.DELIVERY_NO in @Delivery_Nbr  or r.label_id in @Delivery_Nbr  
                UNION ALL
                SELECT 
	                i.Ib_Raw_Mtls_Id,s.STO_DD_SEQ POL_SEQ,s.STORAGE_UNIT_NBR LABEL_ID,s.DELIVERY_DATE Product_Received_Date,s.DELIVERY_DATE Label_Print_Date,s.AMT_RECEIVED Amt_Received,
	                s.BILL_OF_LADING Lot_Nbr,s.SCAN_DATE Label_Scan_Date,1 Tot_Bag_No,s.BATCH_NBR Vendor_Batch,s.BILL_OF_LADING CountryOrigin
                ,i.item_nbr,i.matl_nbr
                FROM STO_DELIVERY_DETAILS s
				inner join INBOUND_RAW_MATERIALS i on i.delivery_no=s.delivery_nbr
                where i.DELIVERY_NO in @Delivery_Nbr  or STORAGE_UNIT_NBR in @Delivery_Nbr
                ";


        public static string UpdateSTODeliveryDetail = @"UPDATE [dbo].[RECEIVED_RAW_MATERIALS] 
	    SET 
		LABEL_SCAN_DATE=(case when LABEL_ID in @Delivery_Nbr then @Date_Received else LABEL_SCAN_DATE end), 
		Delivery_SCAN_DATE=(case when IB_RAW_MTLS_ID IN (SELECT IB_RAW_MTLS_ID FROM INBOUND_RAW_MATERIALS WHERE DELIVERY_NO in @Delivery_Nbr) then @Date_Received else Delivery_SCAN_DATE end), 
		last_modified_time=@Date_Updated, 
		last_modified_by=@Received_By 
		WHERE (IB_RAW_MTLS_ID IN 
			(SELECT IB_RAW_MTLS_ID FROM INBOUND_RAW_MATERIALS WHERE DELIVERY_NO in @Delivery_Nbr) 
			OR LABEL_ID in @Delivery_Nbr);
        
        UPDATE STO_DELIVERY_DETAILS SET  
			SCAN_DATE=(case when STORAGE_UNIT_NBR in @Delivery_Nbr then @Date_Received else SCAN_DATE end),
			DATE_RECEIVED=(case when DELIVERY_NBR in @Delivery_Nbr then @Date_Received else DATE_RECEIVED end),
			received_by=@Received_By
        where DELIVERY_NBR in @Delivery_Nbr  or STORAGE_UNIT_NBR in @Delivery_Nbr ;
";

        public static string GetSTOByStorageNumber = @"
                SELECT * FROM [dbo].[STO_DELIVERY_DETAILS] where Storage_Unit_Nbr=@Storage_Unit_Nbr";

		public static string GetSTOByMtrl_Nbr = @"
                SELECT * FROM [dbo].[STO_DELIVERY_DETAILS] where matl_nbr=@Mtrl_Nbr and storage_unit_nbr ='NEEDLABEL'";

		public static string UpdateSTOSAPDate = @"
                UPDATE [dbo].[STO_DELIVERY_DETAILS] SET Date_Sent_SAP=@Date_Sent_Sap, Sap_Sent_By=@Sap_Sent_By WHERE Delivery_NBR=@Delivery_NBR;

                UPDATE dbo.RECEIVED_RAW_MATERIALS SET SENT_SAP_DATE=@Date_Sent_Sap,SENT_SAP_BY=@Sap_Sent_By 
                WHERE (IB_RAW_MTLS_ID IN 
			    (SELECT IB_RAW_MTLS_ID FROM INBOUND_RAW_MATERIALS WHERE DELIVERY_NO=@Delivery_NBR))
                ";
		public static string UpdateSTOBatchID = @"UPDATE [dbo].[STO_DELIVERY_DETAILS] SET Batch_ID=@Batch_Id WHERE STO_DD_SEQ=@Sto_DD_Seq";

		public static string SaveSTO = @"INSERT INTO [dbo].[STO_DELIVERY_DETAILS](
                                        DELIVERY_NBR,
										STORAGE_UNIT_NBR,
										SOURCE_PLANT,
										BATCH_NBR,
										MATL_NBR,
										DELIVERY_DATE,
										DATE_ADDED,
										DATE_UPDATED,
										DATE_RECEIVED,
										DATE_SENT_SAP,
										AMT_RECEIVED,
										UOM,
										RECEIVED_BY, SCANNED_IN, BILL_OF_LADING,NOTE,SAP_SENT_BY,Item_Nbr,BATCH_ID) 
                                        
                                    VALUES (@Delivery_Nbr,
										@Storage_Unit_Nbr,
										@Source_Plant,
										@Batch_Nbr,
										@Matl_Nbr,
										@Delivery_Date,
										@Date_Added,
										@Date_Updated,
										@Date_Received,
										@Date_Sent_Sap,
										@Amt_Received,
										@Uom,
										@Received_By,@Scanned_In,@Bill_Of_Lading,@Note,@Sap_Sent_By,@Item_Nbr,@Batch_Id);SELECT SCOPE_IDENTITY() AS NewID";
        public static string SaveSTODetail = @"
            INSERT INTO dbo.RECEIVED_RAW_MATERIALS
                        (IB_Raw_Mtls_Id,
                        Label_Id,
                        Product_Received_Date,                     
                        Amt_Received,
                        UOM,
                        Lot_Nbr,
                        Tot_Bag_No,
                        Vendor_Batch,
                        CountryOrigin,
                        Bag_No,
                        Last_Modified_Time,
                        Last_Modified_By,
                        Batch_Id, 
                        Bill_Of_Lading,
                        Note,
                        Packaging_Material,
                        LABEL_PRINT_DATE,
                        ItemNo
                        )
                    VALUES
                        (@IB_Raw_Mtls_Id,
                        @Storage_Unit_Nbr,
                        @Date_Received,                     
                        @Amt_Received,
                        @UOM,
                        @Source_Plant,
                        @Source_Plant,
                        @Batch_Nbr,
                        @Matl_Nbr,
                        @Source_Plant,
                        @Date_Updated,
                        @Received_By,
                        @Batch_Id,
                        @Bill_Of_Lading,
                        @Note,
                        @Item_Nbr,
                        NULL,
                        @ItemNo
                        );SELECT SCOPE_IDENTITY() AS NewID";

        public static string UpdateSTODetail = @"
            UPDATE dbo.RECEIVED_RAW_MATERIALS SET Amt_Received=@Amt_Received,Vendor_Batch=@Batch_Nbr,CountryOrigin=@Matl_Nbr where Label_Id=@Storage_Unit_Nbr; select IB_RAW_MTLS_ID from dbo.RECEIVED_RAW_MATERIALS where Label_Id=@Storage_Unit_Nbr";

    }
}
