﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Processors.Sql
{
    public class SapMatlMasterSql
    {
        public static string GetSapMaterialByMATLNBR = @"
                select  SAP_MATL_NBR,
                        SAP_MATL_DESCR,
                        SAP_MATL_TYPE,
                        SAP_DESCR_SIMPLE,
                        DATE_TIME_ADDED,
                        CREATED_BY,
                        DATE_MODIFIED,
                        MODIFIED_BY,
                        DISABLED,
                        PRODUCT_CODE,
                        MATL_GRADE,
                        TRADE_NAME,
                        PRINT_LOGO_FLAG,
                        PRINT_PA66_LOGO,
                        CONTAINER_CODE,
                        NET_WT_MIN,
                        NET_WT_MAX,
                        TARE_WT,
                        LTRIM(RTRIM(UOM)) UOM,
                        NUMBER_OF_LABELS,
                        MATL_COLOR,
                        BATCH_MANAGED
                        from SAP_MATL_MASTER where SAP_MATL_NBR=@SapMatl_NBR";

        public static string GetSapMaterialByMATLDesc = @"
                select  SAP_MATL_NBR,
                        SAP_MATL_DESCR                     
                        from SAP_MATL_MASTER where SAP_MATL_DESCR like @SAP_MATL_DESCR";
     
    }
}
