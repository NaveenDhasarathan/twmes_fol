﻿using Dapper;
using DataLibrary.DataAccess.Abstract;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataLibrary.DataAccess
{
    public class ShopFloorDataAccess : DataAccessBase, IShopFloorDataAccess
    {
        private readonly IDbConnection connection;

        public ShopFloorDataAccess(IConfiguration config) : base(config)
        {
            connection = CreateConnection("ShopFloorDb");
        }

        public List<T> LoadData<T>(string sql)
        {
            return connection.Query<T>(sql).ToList();
        }

        public List<T> LoadData<T>(string sql, object data)
        {
            return connection.Query<T>(sql, data).ToList();
        }

        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
