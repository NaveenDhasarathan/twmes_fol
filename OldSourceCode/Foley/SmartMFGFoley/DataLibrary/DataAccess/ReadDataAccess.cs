﻿using DataLibrary.DataAccess.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace DataLibrary.DataAccess
{
    public class ReadDataAccess: IReadDataAccess
    {
        private readonly IDbConnection connection;

        //public ReadDataAccess(string connectionName, IDataAccessBase dataAccess)
        //{
        //    connection = dataAccess.CreateConnection(connectionName);
        //}
        public ReadDataAccess(IDataAccessBase dataAccess)   {            
            connection = dataAccess.CreateConnection();
        }
        public List<T> LoadData<T>(string sql)
        {
            return connection.Query<T>(sql).ToList();
        }

        public List<T> LoadData<T>(string sql, object data)
        {
            return connection.Query<T>(sql, data).ToList();
        }

        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
