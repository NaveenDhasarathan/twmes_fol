﻿using DataLibrary.DataAccess.Abstract;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DataLibrary.DataAccess
{
    public class DataAccessBase : IDataAccessBase
    {
        private readonly IConfiguration _config;
        public DataAccessBase(IConfiguration config)
        {
            _config = config;
        }

        public IDbConnection CreateConnection()
        {           
            return new SqlConnection(_config["ConnectionStrings:SUZContextDb"]);
        }

        public IDbConnection CreateConnection(string section)
        {
            var connectionType = _config.GetSection("ConnectionStrings").GetSection(section).GetSection("Type").Value;
            var connectionString = _config.GetSection("ConnectionStrings").GetSection(section).GetSection("ConnectionString").Value;

            if (connectionType == "Sql")
            {
                var connection = new SqlConnection(connectionString);
                return connection;
            }
            else if (connectionType == "Oracle")
            {
                var connection = new OracleConnection(connectionString);
                return connection;
            }
            else
            {
                throw new ArgumentException("Incorrect connection type must be Sql or Oracle", "connectionType");
            }
        }
    }
}
