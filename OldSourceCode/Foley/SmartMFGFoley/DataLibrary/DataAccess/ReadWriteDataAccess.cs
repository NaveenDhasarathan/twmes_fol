﻿using Dapper;
using DataLibrary.DataAccess.Abstract;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace DataLibrary.DataAccess
{
    public class ReadWriteDataAccess : IReadWriteDataAccess, System.IDisposable
    {
        private readonly IDbConnection connection;    

        //public ReadWriteDataAccess(string connectionName, IDataAccessBase dataAccess)
        //{

        //    connection = dataAccess.CreateConnection(connectionName);
        //}

        public ReadWriteDataAccess(IDataAccessBase dataAccess)  {           
            connection = dataAccess.CreateConnection();          
        }

        public List<T> LoadData<T>(string sql)
        {
            return connection.Query<T>(sql).ToList();
        }

        public List<T> LoadData<T>(string sql, object data)
        {
            return connection.Query<T>(sql, data).ToList();
        }

        public int SaveData<T>(string sql, T data, bool returnId = false)
        {
            if (returnId)
            {
                return connection.ExecuteScalar<int>(sql, data);
            }
            else
            {
                return connection.Execute(sql, data);
            }
        }

        public int SaveDataInTransaction<T>(string sql, T data, bool returnId = false)
        {
            int rtn = 0;
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    if (returnId)
                    {
                        rtn= connection.ExecuteScalar<int>(sql, data);
                    }
                    else
                    {
                        rtn= connection.Execute(sql, data);
                    }
                    transaction.Commit();
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                }
            }
            return rtn;
        }
        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
