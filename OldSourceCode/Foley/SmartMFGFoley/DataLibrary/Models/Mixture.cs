﻿using System;

namespace DataLibrary.Models
{
    public class Mixture
    {
		public long M_seq { get; set; }
		public long Po_Nbr { get; set; }
		public string Matl_Nbr { get; set; }
		public string Lot_Nbr { get; set; }
		public decimal Weight { get; set; }	
	    public int Drum_Nbr { get; set; }
		public DateTime Create_Date { get; set; }
		public string Created_By { get; set; }
		public string Instructions { get; set; }
		public DateTime Scan_Date { get; set; }
		public string Scan_By { get; set; }
		public decimal Drum_Wgt { get; set; }
		public DateTime Weight_Date { get; set; }
		public string Weighted_By { get; set; }
		public DateTime Used_Date { get; set; }
		public string Used_By { get; set; }
	}
}
