﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class RawMaterialDetailModel    
    {        
        public long Ib_Raw_Mtls_Id { get; set; }

        [DisplayName("Order No.")]
        public long Document_Nbr { get; set; }

        [DisplayName("Item No.")]
        public int Item_Nbr { get; set; }

        [DisplayName("Order Type")]
        public string Document_Type { get; set; }

        [DisplayName("Supplier Number")]
        public string Supplier_Nbr { get; set; }

        [DisplayName("Supplier Name")]
        public string Supplier_Name { get; set; }

        [DisplayName("Order Date")]
        public DateTime Document_Date { get; set; }

        [DisplayName("Material No.")]
        public string Matl_Nbr { get; set; }

        [DisplayName("Storage Location")]
        public string Storage_Location { get; set; }
        public decimal Quantity { get; set; }
        public string Uom { get; set; }
        public string Lot_Nbr { get; set; }
        public bool? Quality { get; set; }

        [DisplayName("Delivery No.")]
        public string Delivery_No { get; set; }

        [DisplayName("Material Description")]
        public string Sap_Matl_Descr { get; set; }

        [DisplayName("Batch Managed")]
        public bool Batch_Managed { get; set; }

        [DisplayName("Material Type")]
        public string Sap_Matl_Type { get; set; }
        public string STO_Batch { get; set; }
        public string FRM { get; set; }
        public string Too { get; set; }

    }
}
