﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class SapStorageBinInventory
    {
        //public long Storage_Bin_Id { get; set; }
        //public string Storage_Bin { get; set; }
        //public string Bin_Position { get; set; }
        //public string Storage_Unit_Nbr { get; set; }
        //public string Batch { get; set; }
        //public string Matl_Nbr { get; set; }
        //public float Total_Qty { get; set; }
        //public string Uom { get; set; }
        //public string Storage_Unit_Type { get; set; }
        public string Label_Id { get; set; }
        public decimal Amt_Received { get; set; }
        public string Location { get; set; }
        public DateTime Product_Received_Date { get; set; }
    }
}
