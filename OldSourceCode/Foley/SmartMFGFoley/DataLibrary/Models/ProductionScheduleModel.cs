﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class ProductionScheduleModel
    {
        public int Process_Order_Number { get; set; }
        public int Material_Number { get; set; }
        public string Material_Description { get; set; }
        public string Resource { get; set; }
        public DateTime Start_Date { get; set; }
        public DateTime End_Date { get; set; }
        public int Po_Amt_Sched { get; set; }
        public int Mes_Confirmed { get; set; }
        public int Percent_Produced { get; set; }
        public string Po_Status_Descr { get; set; }
        public string Ip21_Tag { get; set; }
        public DateTime Last_Date_Time_Produced { get; set; }
        public string Active { get; set; }
        public string Uom { get; set; }
        public DateTime Date_Time_Created { get; set; }
        public int Sap_Confirmed { get; set; }
        public string Spc { get; set; }
        public string Production_Ready { get; set; }
    }
}
