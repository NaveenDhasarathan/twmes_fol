﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataLibrary.Models
{
    public class POAltMaterialDescModel
    {
        public long Po_Alt_Bom_Seq { get; set; }
        public long Po_Nbr { get; set; }

        [Required]
        [StringLength(1)]
        public string Substitution_Type { get; set; }

        [Required]
        [StringLength(18)]
        public string Org_Matl_Nbr { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Org_Qty { get; set; }

        [Required]
        [StringLength(18)]
        public string Subst_Matl_Nbr { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Subst_Qty { get; set; }
        public string Sap_Descr_Simple { get; set; }
    }
}
