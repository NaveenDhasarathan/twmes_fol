﻿
namespace DataLibrary.Models
{
    public class BOMModel
    {
        public int PO_NBR { get; set; }
        public string SAP_MATL_NBR { get; set; }
        public int QUANTITY_REQUIRED { get; set; }
        public int QUANTITY_COMPLETED { get; set; }
        public string MATERIAL_DESCRIPTION { get; set; }
        public string UOM { get; set; }
        public string MATL_TYPE { get; set; }
        public string SETTING_NAME { get; set; }
        public int EXPECTED_VALUE { get; set; }
        public int ACTUAL_VALUE { get; set; }
        public int VALUE_RECORDED { get; set; }
        public string KEPWARE_TAG { get; set; }
        public int SCANNED_AMT { get; set; }
        public int FEEDER_NO { get; set; }
    }
}
