﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DataLibrary.Models
{
    public class InboundRawMaterialsModel
    {
        
        [Key]
        public long Ib_Raw_Mtls_Id { get; set; }

        [DisplayName("Purchase Order")]
        public long Document_Nbr { get; set; }

        [DisplayName("Item Number")]
        public int Item_Nbr { get; set; }


        [StringLength(1)]
        [DisplayName("Document Type")]
        public string Document_Type { get; set; }


        [StringLength(25)]
        [DisplayName("Supplier Number")]
        public string Supplier_Nbr { get; set; }


        [StringLength(50)]
        [DisplayName("Supplier Name")]
        public string Supplier_Name { get; set; }

        [DisplayName("Purchase Order Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Document_Date { get; set; }


        [StringLength(8)]
        [DisplayName("Material Number")]
        public string Matl_Nbr { get; set; }

        [StringLength(25)]
        [DisplayName("Storage Location")]
        public string Storage_Location { get; set; }

        [Column(TypeName = "numeric")]
        [DisplayName("Quantity")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n0}")]
        public decimal Quantity { get; set; }

        [StringLength(3)]
        public string Uom { get; set; }
   
        public string Lot_Nbr { get; set; }

        public bool? Quality { get; set; }
        public string Delivery_No { get; set; }
      
    }
}
