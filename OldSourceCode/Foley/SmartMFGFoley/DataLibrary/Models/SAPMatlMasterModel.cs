﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace DataLibrary.Models
{
    public class SAPMatlMasterModel
    {
        [Key]
        [StringLength(18)]
        public string Sap_Matl_Nbr { get; set; }

        [StringLength(40)]
        [DisplayName("SAP Material Description")]
        public string Sap_Matl_Descr { get; set; }  

        [StringLength(20)]
        public string Sap_Descr_Simple { get; set; }

        public DateTime Date_Time_Added { get; set; }

        [StringLength(20)]
        public string Created_By { get; set; }

        public DateTime? Date_Modified { get; set; }

        [StringLength(20)]
        public string Modified_By { get; set; }

        [StringLength(1)]
        public string Disabled { get; set; }

        public string Product_Code { get; set; }

        public string Matl_Grade { get; set; }

        public string Trade_Name { get; set; }

        public string Print_Logo_Flag { get; set; }

        public string Print_Pa66_Logo { get; set; }

        public string Container_Code { get; set; }

        public decimal? Net_Wt_Min { get; set; }

        public decimal? Net_Wt_Max { get; set; }

        public decimal? Tare_Wt { get; set; }

        public string Uom { get; set; }

        public int? Number_Of_Labels { get; set; }

        public string Matl_Color { get; set; }

        public string Sap_Matl_Type { get; set; }

        //public string Source_Unload_Date { get; set; }

        //public decimal? Min_Qty_Ud_Manual { get; set; }
        public string BATCH_MANAGED { get; set; }
    }
}
